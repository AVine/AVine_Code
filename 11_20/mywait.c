#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
int main()
{
    pid_t id = fork();
    if(id == -1)
    {
        perror("fork fail");
    }
    else if(id == 0)
    {
        printf("I am child process,my pid is:%d\n",getpid());
        sleep(5);       //等待5秒
        exit(10);       //结束子进程
    }
    else 
    {
        //使用非阻塞等待
        int status = 0;
        while(1)
        {
            pid_t ret = waitpid(id,&status,WNOHANG);
            if(ret == 0)
            {
                printf("The child process is not done yet\n");
                printf("I am parent process,I am going to do my own thing\n");
            }
            else if(ret > 0)
            {
                printf("The child process is ok\n");
            printf("child process pid:%d,the eixt code:%d,sig number:%d\n",ret,(status>>8)&0xFF, status & 0x7F);
            }
            else 
            {
                perror("wiatpid fail");
            }
            sleep(1);    
         }
    }
    return 0;
}

