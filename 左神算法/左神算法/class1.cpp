#define _CRT_SECURE_NO_WARNINGS 1

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

#define MAKE_RAND() srand((size_t)time(nullptr) ^ 134 ^ 32423)


/////////////////////////////////排序//////////////////////////////////////
void selectSort(vector<int>& vec)
{
	for (int i = 0; i < vec.size(); i++)
	{
		int minIndex = i;	//假设这是最小数
		for (int j = i + 1; j < vec.size(); j++)
		{
			minIndex = vec[j] < vec[minIndex] ? j : minIndex;
		}
		swap(vec[minIndex], vec[i]);
	}
}

void bubbleSort(vector<int>& vec)
{
	for (int i = 0; i < vec.size()-1; i++)
	{
		for (int j = 0; j < vec.size() - 1 - i; j++)
		{
			if (vec[j] > vec[j + 1])
			{
				swap(vec[j], vec[j + 1]);
			}
		}
	}
}

void insertSort(vector<int>& vec)
{
	for (int i = 1; i < vec.size(); i++)
	{
		for (int j = i; j > 0; j--)
		{
			if (vec[j] < vec[j - 1])
			{
				swap(vec[j], vec[j - 1]);
			}
		}
	}
}

bool isSameSort()
{
	int size = 10000;	//测试多少次
	int arrSize = 100;	//数组大小
	int arrNum = 100000;	//数组元素范围

	while (size--)
	{
		vector<int> vec1(rand() % arrSize);
		for (auto& e : vec1)
		{
			e = rand() & arrNum;
		}

		//selectSort(vec1);
		insertSort(vec1);

		vector<int> vec2 = vec1;
		sort(vec2.begin(), vec2.end());

		if (vec1 != vec2)
		{
			return false;
		}
	}
	
	return true;
}


////////////////////////////////////////////二分查找//////////////////////////////////////
bool binarySearch(vector<int>& vec, int key)
{
	int left = 0, right = vec.size();
	while (left < right)
	{
		int mid = (left + right) / 2;
		if (vec[mid] > key)
		{
			right = mid;
		}
		else if (vec[mid] < key)
		{
			left = mid - 1;
		}
		else
		{
			return true;
		}
	}
}

bool nomalSearch(vector<int>& vec, int key)
{
	for (auto& e : vec)
	{
		if (e == key)
		{
			return true;
		}
	}
	return false;
}


int leftBorderNomal(const vector<int>& vec,const int& key)
{
	for (int i = 0; i < vec.size(); i++)
	{
		if (vec[i] == key)
		{
			return i;
		}
	}
	return -1;
}

int leftBorderBinary(const vector<int>& vec, const int& key)
{
	int left = 0, right = vec.size();
	while (left < right)
	{
		int mid = (left + right) / 2;
		if (vec[mid] > key)
		{
			right = mid;
		}
		else if (vec[mid] < key)
		{
			left = mid + 1;
		}
		else
		{
			right = mid;
			if (mid == 0 || vec[mid-1] < key)
			{
				return mid;
			}
		}
	}

	return -1;
}


bool isSameSearch()
{
	int size = 100;	//测试多少次
	int arrSize = 100;	//数组大小
	int arrNum = 100000;	//数组元素范围

	while (size--)
	{
		vector<int> vec1(rand() % arrSize);
		for (auto& e : vec1)
		{
			e = rand() & arrNum;
		}
		sort(vec1.begin(), vec1.end());

		int key = vec1[rand() % vec1.size()];

		if (leftBorderBinary(vec1, key) != leftBorderNomal(vec1, key))
		{
			return false;
		}
	}

	return true;
}



int maxNum(const vector<int>& vec,int left,int right)
{
	if (left == right)
	{
		return vec[left];
	}
	
	int mid = left + ((right - left) >> 1);
	int l = maxNum(vec, left, mid);
	int r = maxNum(vec, mid + 1, right);
	return l > r ? l : r;
}

int main()
{
	MAKE_RAND();
	//cout << isSameSort() << endl;

	//cout << isSameSearch() << endl;

	vector<int> vec{ 245,67,234,46,5,23,78,135,58,127,478,35,346 };
	cout << maxNum(vec, 0, vec.size()-1) << endl;
	return 0;
}