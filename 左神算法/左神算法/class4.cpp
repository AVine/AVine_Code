#define _CRT_SECURE_NO_WARNINGS 1


// 力扣 剑指offer II 024 反转链表
class Solution {
public:
    ListNode* reverseList(ListNode* head) // 题目给了链表的头(一般是没有哨兵卫)
    {
        ListNode* prev = nullptr;   //假设这个节点是head的前一个节点
        ListNode* next = nullptr;   //假设这个节点是head的后一个节点

        ListNode* cur = head;   //为了安全起见，使用cur代替head遍历
        while (cur)
        {
            next = cur->next;//记录cur->next
            cur->next = prev;//当前cur->next指向前一个节点
            prev = cur;//cur向后遍历之前，先记录cur的当前位置
            cur = next;//cur向后遍历
        }
        //最后到这里cur已经为空了，所以最新的头应该是prev
        return prev;
    }
};


// 力扣 92.反转链表II
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int left, int right)
    {
        ListNode* cur = head;
        ListNode* prev = nullptr;//记录cur的前一个节点
        int size = 1;//初始值设为1，用来找第几个节点
        while (cur)
        {
            if (size == left)//当要找的节点是第left个
            {
                ListNode* end = cur;//先记录反转链表区间最后一个节点的下一个节点
                int cnt = right - left + 1;
                while (cnt--)
                {
                    end = end->next;
                }
                ListNode* newHead = reverseList(cur, right - left + 1);//反转链表区间
                if (prev != nullptr)//说明反转区间的第一个节点不是head
                {
                    prev->next = newHead;
                }
                else //说明反转区间的第一个节点是head
                {
                    head = newHead;
                }
                while (newHead && newHead->next != nullptr)//得到反转链表的尾节点
                {
                    newHead = newHead->next;
                }
                newHead->next = end;//链接原节点
                return head;//返回
            }
            //如果当前节点不是反转链表区间的入口，继续往后遍历寻找
            prev = cur;
            cur = cur->next;
            ++size;
        }
        return head;
    }

    ListNode* reverseList(ListNode* head, int cnt) // 题目给了链表的头(一般是没有哨兵卫)
    {
        ListNode* prev = nullptr;   //假设这个节点是head的前一个节点
        ListNode* next = nullptr;   //假设这个节点是head的后一个节点

        ListNode* cur = head;   //为了安全起见，使用cur代替head遍历
        while (cur && cnt--)
        {
            next = cur->next;//记录cur->next
            cur->next = prev;//当前cur->next指向前一个节点
            prev = cur;//cur向后遍历之前，先记录cur的当前位置
            cur = next;//cur向后遍历

        }
        //最后到这里cur已经为空了，所以最新的头应该是prev
        return prev;
    }
};


// 力扣 1669.合并两个链表
class Solution {
public:
    ListNode* mergeInBetween(ListNode* list1, int a, int b, ListNode* list2) {
        ListNode* cur1 = list1;
        ListNode* prev = nullptr;
        int size = 0;
        while (cur1)
        {
            if (size == a)//此时找到了要删除的区间的入口
            {
                int cnt = b - a + 1;
                while (cnt--)//cur1为要删除的区间的最后一个节点的下一个节点
                {
                    cur1 = cur1->next;
                }
                if (prev != nullptr)//说明删除区间的头节点不是head1
                {
                    prev->next = list2;
                }
                else //说明删除区间的头节点是head1
                {
                    list1 = list2;
                }
                ListNode* newCur = list2;
                while (newCur && newCur->next)//找list2的尾节点
                {
                    newCur = newCur->next;
                }
                newCur->next = cur1;
                return list1;
            }
            prev = cur1;
            cur1 = cur1->next;
            ++size;
        }
        return list1;
    }
};



// 力扣 剑指offer 027.回文链表
class Solution {
public:
    bool isPalindrome(ListNode* head) {
        stack<ListNode*> st;//用栈记录链表的每一个节点
        ListNode* cur = head;
        while (cur)
        {
            st.push(cur);
            cur = cur->next;
        }
        //在这个位置，栈中记录了链表的所有节点
        //如果一个一个地将栈中的节点弹出
        //那么将是原链表的逆序
        while (!st.empty())
        {
            //回文结构是正着读和倒着读是一样的
            //所以对比顺序遍历的节点和逆序遍历的节点是否一样
            if (head->val != st.top()->val)
            {
                return false;
            }
            st.pop();
            head = head->next;
        }
        return true;
    }
};


// 力扣 剑指offer 027.回文链表(空间复杂度O(1)求解)
class Solution {
public:
    bool isPalindrome(ListNode* head) {
        ListNode* fast = head;//快指针，一次走两步
        ListNode* slow = head;//慢指针，一次走一步
        ListNode* prev = nullptr;//记录slow的前一个指针(分割成两个链表的关键)
        while (fast && fast->next != nullptr)
        {
            fast = fast->next->next;
            prev = slow;
            slow = slow->next;
        }
        if (prev == nullptr)//说明slow就是head
        {
            head->next = nullptr;
        }
        else //说明slow不是head
        {
            prev->next = nullptr;
        }

        //从slow开始逆置链表,返回的就是从原链表拆下来的"新链表"
        ListNode* newHead = reverseList(slow);
        while (head && newHead)
        {
            if (head->val != newHead->val)
            {
                return false;
            }
            head = head->next;
            newHead = newHead->next;
        }
        return true;
    }

    ListNode* reverseList(ListNode* head) // 题目给了链表的头(一般是没有哨兵卫)
    {
        ListNode* prev = nullptr;   //假设这个节点是head的前一个节点
        ListNode* next = nullptr;   //假设这个节点是head的后一个节点

        ListNode* cur = head;   //为了安全起见，使用cur代替head遍历
        while (cur)
        {
            next = cur->next;//记录cur->next
            cur->next = prev;//当前cur->next指向前一个节点
            prev = cur;//cur向后遍历之前，先记录cur的当前位置
            cur = next;//cur向后遍历
        }
        //最后到这里cur已经为空了，所以最新的头应该是prev
        return prev;
    }
};


// 力扣 138.复制带随机指针的链表
class Solution {
public:
    Node* copyRandomList(Node* head) {
        map<Node*, Node*> m;//使用map记录。key为旧节点，value为旧节点的拷贝
        Node* cur = head;
        while (cur)
        {
            m[cur] = new Node(cur->val);//拷贝旧节点作为value
            cur = cur->next;
        }
        cur = head;
        while (cur)
        {
            m[cur]->next = m[cur->next];//新链表的next指针映射与旧链表一致
            m[cur]->random = m[cur->random];//新链表的random指针与旧链表一致
            cur = cur->next;
        }
        return m[head];
    }
};


// 力扣 138.复制带随机指针的链表(空间复杂度降低)
class Solution {
public:
    Node* copyRandomList(Node* head) {
        Node* copy = nullptr;
        Node* next = nullptr;
        Node* cur = head;
        while (cur)
        {
            next = cur->next;//记录当前cur的下一个节点
            copy = new Node(cur->val);
            cur->next = copy;//cur的next指向新插入的节点
            copy->next = next;//三个节点进行连接
            cur = next;//cur又遍历到保存好的next节点
        }
        cur = head;
        while (cur)
        {
            copy = cur->next;//cur->next就是新插入的节点
            next = copy->next;//记录当前cur的下一个节点
            if (cur->random != nullptr)
            {
                copy->random = cur->random->next;
            }
            else
            {
                copy->random = nullptr;
            }
            cur = next;
        }
        Node* newHead = nullptr;
        Node* newTail = nullptr;
        cur = head;
        while (cur)
        {
            copy = cur->next;
            next = copy->next;
            cur->next = copy->next;//恢复原链表
            if (newHead == nullptr)
            {
                newHead = newTail = copy;
            }
            else
            {
                newTail->next = copy;
                newTail = newTail->next;
            }
            cur = next;
        }
        return newHead;
    }
};


// 力扣 面试题 02.04.分割链表
class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
        ListNode* SHead = nullptr;//小于部分的头
        ListNode* STail = nullptr;//小于部分的尾
        ListNode* BEHead = nullptr;//大于部分的头
        ListNode* BETail = nullptr;//大于部分的尾
        ListNode* cur = head;
        while (cur)
        {
            if (cur->val < x)//如果小于，尾插在小于部分的链表中
            {
                if (SHead == nullptr)
                {
                    SHead = STail = cur;
                }
                else
                {
                    STail->next = cur;
                    STail = STail->next;
                }
            }
            else//如果大于等于，尾插在大于等于部分的链表中
            {
                if (BEHead == nullptr)
                {
                    BEHead = BETail = cur;
                }
                else
                {
                    BETail->next = cur;
                    BETail = BETail->next;
                }
            }
            cur = cur->next;
        }
        //这里需要把两个个链表合并到一起
        if (STail != nullptr)//如果小于部分的尾不为空，说明小于部分是有节点的
        {
            STail->next = BEHead;//连接两个部分的链表
            if (BETail != nullptr)//如果大于等于部分的尾不为空，说明大于等于部分是有节点的
            {
                BETail->next = nullptr;//以空作结束
            }
            //如果大于等于部分的尾为空，那么头必定也为空，直接可作链表结束标志
        }

        //如果小于部分的头不为空，直接返回。若为空，返回大于等于部分的节点
        return SHead != nullptr ? SHead : BEHead;

    }
};


