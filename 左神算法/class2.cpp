#define _CRT_SECURE_NO_WARNINGS 1

#include <vector>
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;


#define MAKE_RAND() srand((size_t)time(nullptr) ^ 12432 ^ 342)

void mergeSort(vector<int>& vec, int left, int right)
{
	if (left == right)	//只有一个数不需要排序
	{
		return;
	}

	int mid = left + ((right - left) >> 1);
	mergeSort(vec, left, mid);
	mergeSort(vec, mid + 1, right);

	int ptrLeft = left;
	int ptrRight = mid+1;
	vector<int> tmp;
	while (ptrLeft <= mid && ptrRight <= right)
	{
		tmp.push_back(vec[ptrLeft] < vec[ptrRight] ? vec[ptrLeft++] : vec[ptrRight++]);
	}

	while (ptrLeft <= mid)
	{
		tmp.push_back(vec[ptrLeft++]);
	}

	while (ptrRight <= right)
	{
		tmp.push_back(vec[ptrRight++]);
	}

	for (int i = 0; i < tmp.size(); i++)
	{
		vec[i + left] = tmp[i];
	}
}

void mergeSort(vector<int>& vec)
{
	mergeSort(vec, 0, vec.size() - 1);
}

bool isSameSort()
{
	int time = 100;	//测试多少次
	int arrSize = 1000;	// 数组大小
	int arrNum = 100000;	//数组元素数据范围

	while (time--)
	{
		vector<int> vec1(rand() % arrSize);
		for (auto& e : vec1)
		{
			e = rand() % arrNum;
		}

		vector<int> vec2(vec1);

		mergeSort(vec1);
		sort(vec2.begin(), vec2.end());

		if (vec1 != vec2)
		{
			return false;
		}
	}
	return true;
}


int nomalSmallSum(vector<int>& vec)
{
	int val = 0;
	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < i; j++)
		{
			if (vec[j] < vec[i])
			{
				val += vec[j];
			}
		}
	}
	return val;
}
void smallSum(vector<int>& vec, int left, int right,int& val)
{
	if (left == right)
	{
		return;
	}
	
	int mid = left + ((right - left) >> 1);
	smallSum(vec, left, mid,val);
	smallSum(vec, mid + 1, right,val);

	int ptrLeft = left;
	int ptrRight = mid + 1;
	vector<int> tmp;
	while (ptrLeft <= mid && ptrRight <= right)
	{
		val += vec[ptrLeft] < vec[ptrRight] ? (right - ptrRight + 1) * vec[ptrLeft] : 0;
		tmp.push_back(vec[ptrLeft] < vec[ptrRight] ? vec[ptrLeft++] : vec[ptrRight++]);
	}

	while (ptrLeft <= mid)
	{
		tmp.push_back(vec[ptrLeft++]);
	}

	while (ptrRight <= right)
	{
		tmp.push_back(vec[ptrRight++]);
	}

	for (int i = 0; i < tmp.size(); i++)
	{
		vec[i + left] = tmp[i];
	}

}

bool isSameSmallSum()
{
	int time = 100;	//测试多少次
	int arrSize = 1000;	// 数组大小
	int arrNum = 100000;	//数组元素数据范围

	while (time--)
	{
		vector<int> vec1(rand() % arrSize);
		for (auto& e : vec1)
		{
			e = rand() % arrNum;
		}

		vector<int> vec2(vec1);

		int val = 0;
		smallSum(vec2, 0, vec2.size() - 1, val);
		if (nomalSmallSum(vec1) != val)
		{
			return false;
		}
	}
	return true;
}



void quickSort1(vector<int>& vec,int left,int right)
{
	// <= >
	if (left >= right)
	{
		return;
	}

	int key = vec[right];
	int leftBorder = left-1;
	int i = left;

	while (i <= right)
	{
		if (vec[i] <= key)
		{
			swap(vec[++leftBorder], vec[i++]);
		}
		else if (vec[i] > key)
		{
			++i;
		}
	}

	quickSort1(vec, left, leftBorder-1);
	quickSort1(vec, leftBorder+1, right);
}

void quickSort1(vector<int>& vec)
{
	quickSort1(vec, 0, vec.size() - 1);
	for (auto& e : vec)
	{
		cout << e << " ";
	}
	cout << endl;
}


void quickSort2(vector<int>& vec, int left, int right)
{
	// <= >
	if (left >= right)
	{
		return;
	}

	int key = vec[right];
	int leftBorder = left - 1;
	int rightBorder = right + 1;
	int i = left;

	while (i < rightBorder)
	{
		if (vec[i] < key)
		{
			swap(vec[++leftBorder], vec[i++]);
		}
		else if (vec[i] == key)
		{
			++i;
		}
		else
		{
			swap(vec[--rightBorder], vec[i]);
		}
	}

	quickSort2(vec, left, leftBorder);
	quickSort2(vec, rightBorder, right);
}

void quickSort2(vector<int>& vec)
{
	quickSort2(vec, 0, vec.size() - 1);
//	for (auto& e : vec)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
}



void quickSort3(vector<int>& vec, int left, int right)
{
	// <= >
	if (left >= right)
	{
		return;
	}

	
	int mid = left + ((right - left) >> 1);
	int key = vec[mid];

	int leftBorder = left - 1;
	int rightBorder = right + 1;
	int i = left;

	while (i < rightBorder)
	{
		if (vec[i] < key)
		{
			swap(vec[++leftBorder], vec[i++]);
		}
		else if (vec[i] == key)
		{
			++i;
		}
		else
		{
			swap(vec[--rightBorder], vec[i]);
		}
	}

	quickSort3(vec, left, leftBorder);
	quickSort3(vec, rightBorder, right);
}

void quickSort3(vector<int>& vec)
{
	quickSort3(vec, 0, vec.size() - 1);
	//for (auto& e : vec)
	//{
	//	cout << e << " ";
	//}
	//cout << endl;
}

bool isSameQuickSort()
{
	int time = 100;	//测试多少次
	int arrSize = 1000;	// 数组大小
	int arrNum = 100000;	//数组元素数据范围

	while (time--)
	{
		vector<int> vec1(rand() % arrSize);
		for (auto& e : vec1)
		{
			e = rand() % arrNum;
		}

		vector<int> vec2(vec1);

		quickSort3(vec1);
		sort(vec2.begin(), vec2.end());

		if (vec1 != vec2)
		{
			return false;
		}
	}
	return true;
}


void minMoveSort(vector<int>& vec, int k)
{
	priority_queue<int,vector<int>,greater<int>> heap;	//小堆

	int index = 0;
	for (index = 0; index <= k; index++)
	{
		heap.push(vec[index]);	//数组的前k+1个数放入堆
	}

	int i = 0;
	for (i = 0; i < vec.size(); i++,index++)
	{
		vec[i] = heap.top();
		heap.pop();

		if (index < vec.size())
			heap.push(vec[index]);
	}

	while (!heap.empty())
	{
		vec[i++] = heap.top();
		heap.pop();
	}
	
}

void minMoveSort()
{
	vector<int> vec{ 3,4,3,6,7,9,8 };
	int k = 3;
	minMoveSort(vec, k);

	for (auto& e : vec)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	MAKE_RAND();

	//cout << isSameSort() << endl;
	//cout << isSameSmallSum() << endl;
	
	//cout << isSameQuickSort() << endl;

	//vector<int> vec{ 9,8,7,6,5,4,3,2,1 };
	//quickSort3(vec);

	minMoveSort();
	return 0;
}