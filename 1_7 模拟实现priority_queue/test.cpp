#define _CRT_SECURE_NO_WARNINGS 1


#include "priority_queue.h"
#include <iostream>

using namespace ly;
using std::cout;
using std::endl;

void test1()
{
	//priority_queue<int> q;
	//q.push(1);
	//q.push(45);
	//q.push(1231);
	//q.push(4);

	int arr[] = { 123,1234,243,568,1234,768,556,789 ,45612,567};

	priority_queue<int> q(std::begin(arr), std::end(arr));


	while (!q.empty())
	{
		cout << q.top() << " ";
		q.pop();
	}
	cout << endl;

	priority_queue<int,std::vector<int>,greater<int>> q1(std::begin(arr), std::end(arr));


	while (!q1.empty())
	{
		cout << q1.top() << " ";
		q1.pop();
	}
	cout << endl;
}
int main()
{
	test1();
	return 0;
}