#pragma once

#include <vector>
#include <algorithm>
namespace ly
{
	//设计两个仿函数
	template<class T>
	struct less
	{
		bool operator()(const T& x, const T& y)
		{
			return x < y;
		}
	};

	template<class T>
	struct greater
	{
		bool operator()(const T& x, const T& y)
		{
			return x > y;
		}
	};

	//		数据类型		默认容器							默认比较方式
	template<class T,class Container = std::vector<T>,class Compare = less<T>>
	class priority_queue
	{
	public:
		priority_queue()	//默认构造
		{}

		void AdjustDown(size_t parent)
		{
			Compare com;	

			size_t child = parent * 2 + 1;
			while (child < _con.size())
			{
				if (child + 1 < _con.size() && _con[child + 1] > _con[child])
				{
					++child;
				}
				
				if (com(_con[parent], _con[child]))	
				{
					std::swap(_con[parent], _con[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}

		void AdjustUp(size_t child)
		{
			Compare com;
			
			size_t parent = (child - 1) / 2;
			while (child > 0)
			{
				if (com(_con[parent], _con[child]))
				{
					std::swap(_con[parent], _con[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}

		template<class Iterator>
		priority_queue(Iterator first, Iterator last)
			:_con(first,last)	//初始化容器
		{
			//建堆
			for (int i = (_con.size() - 2) / 2; i >= 0; i--)
			{
				AdjustDown(i);	//向下调整建堆
			}
		}

		void push(const T& val)
		{
			_con.push_back(val);
			AdjustUp(_con.size() - 1);	//向上调整建堆
		}

		bool empty()
		{
			return _con.empty();
		}

		const T& top()
		{
			return _con[0];
		}

		void pop()	//删除堆顶元素
		{
			std::swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			AdjustDown(0);	//向下调整建堆
		}


	private:
		Container _con;
	};
}

//#include <vector>
//#include <algorithm>
//namespace ly
//{
//	template<class T>
//	struct less
//	{
//		bool operator()(T& x, T& y)
//		{
//			return x < y;
//		}
//	};
//
//
//	template<class T>
//	struct greater
//	{
//		bool operator()(T& x, T& y)
//		{
//			return x > y;
//		}
//	};
//
//	template<class T,class Container = std::vector<T>,class Compare = less<T>>
//	class priority_queue
//	{
//	public:
//		
//		priority_queue()
//		{}
//
//		template<class Iterator>
//		priority_queue(Iterator first,Iterator last)
//			:_con(first,last)
//		{
//			//建堆
//			for (int i = (_con.size() - 2) / 2; i >= 0; i--)
//			{
//				AdjustDown(i);
//			}
//		}
//		
//
//		void AdjustUp(size_t child)
//		{
//			Compare com;
//			size_t parent = (child - 1) / 2;
//
//			while (child > 0)
//			{
//				//if (_con[child] > _con[parent])
//				if(com(_con[parent] , _con[child]))
//				{
//					std::swap(_con[child], _con[parent]);
//					child = parent;
//					parent = (child - 1) / 2;
//				}
//				else
//				{
//					break;
//				}
//			}
//
//		}
//
//		void push(const T& val)
//		{
//			_con.push_back(val);
//			AdjustUp(_con.size() - 1);		//从最后一个位置开始向上调整
//		}
//
//
//		void AdjustDown(size_t parent)
//		{
//			Compare com;
//
//			size_t child = parent * 2 + 1;		//默认是左孩子
//
//			while (child < _con.size())
//			{
//				if (child + 1 < _con.size() && _con[child + 1] > _con[child])
//				{
//					++child;
//				}
//
//				//if (_con[child] > _con[parent])
//				if (com(_con[parent] , _con[child]))
//				{
//					std::swap(_con[child], _con[parent]);
//					parent = child;
//					child = parent * 2 + 1;
//				}
//				else
//				{
//					break;
//				}
//			}
//		}
//		void pop()
//		{
//			std::swap(_con[0], _con[_con.size() - 1]);
//			_con.pop_back();
//			AdjustDown(0);
//		}
//
//		const T& top()
//		{
//			return _con[0];
//		}
//
//		bool empty()
//		{
//			return _con.size() == 0;
//		}
//
//	
//	private:
//		Container _con;
//	};
//}