#pragma once


// 节点
template <class K>
struct BST_node
{
	BST_node<K>* _left;		//左子树
	BST_node<K>* _right;	//右子树
	K _key;

	BST_node(const K& key)
		:_key(key),_left(nullptr),_right(nullptr)
	{}
};

template <class K>		//节点键值的数据类型
class BST
{
	typedef BST_node<K> Node;
public:
	BST()
		:_root(nullptr)
	{}

	~BST()
	{
		Destructor(_root);
		_root = nullptr;
	}

	

	BST(const BST<K>& t)
	{
		_root = Copy(t._root);
	}

	
	BST<K>& operator==(BST<K> t)	//现代写法
	{
		swap(_root, t._root);
		return *this;
	}

	bool insert(const K& key)
	{
		if (_root == nullptr)
		{
			_root = new Node(key);
			return true;
		}
		
		Node* prev = nullptr;	// cur的父节点
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)	//如果比根节点的键值小
			{
				prev = cur;
				cur = cur->_left;
			}
			else if(key > cur->_key)	//如果比根节点的键值大
			{
				prev = cur;
				cur = cur->_right;
			}
			else
			{
				// 我们不允许插入重复的数据
				return false;
			}
		}

		// 直到遍历到空，才施行插入
		cur = new Node(key);
		if (key < prev->_key)
		{
			prev->_left = cur;
		}
		else if (key > prev->_key)
		{
			prev->_right = cur;
		}
		return true;
	}

	bool find(const K& key)
	{
		if (_root == nullptr)
		{
			return false;
		}
		
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)
			{
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				cur = cur->_right;
			}
			else
			{
				// 找到了
				return true;
			}
		}
		return false;
	}

	bool erase(const K& key)
	{
		if (_root == nullptr)
		{
			return false;
		}

		Node* prev = _root;
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)
			{
				prev = cur;
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				prev = cur;
				cur = cur->_right;
			}
			else
			{
				// 如果左子树为空
				if (cur->_left == nullptr)
				{
					// 假设右子树不为空，则将右子树托孤给父节点
					if (_root == cur)
					{
						_root = _root->_right;
					}
					else if (prev->_left == cur)
					{
						prev->_left = cur->_right;
					}
					else if (prev->_right == cur)
					{
						prev->_right = cur->_right;
					}

					delete cur;
					return true;
				}

				// 如果右子树为空
				else if (cur->_right == nullptr)
				{
					//假设左子树不为空，则将左子树托孤给父节点
					if (_root == cur)
					{
						_root = _root->_left;
					}
					else if (prev->_left == cur)
					{
						prev->_left = cur->_left;
					}
					else if (prev->_right == cur)
					{
						prev->_right = cur->_left;
					}

					delete cur;
					return true;
				}

				// 如果左右子树都不为空
				else
				{
					// 假设使用右子树的最小值替代
					Node* prev = _root;
					Node* minRight = cur->_right;

					while (minRight->_left)		//二叉树特性，越往左越小
					{
						prev = minRight;
						minRight = minRight->_left;
					}

					cur->_key = minRight->_key;

					// 替换好后，就要删除minRight
					if (prev->_left == minRight)
					{
						prev->_left = minRight->_right;
					}
					else if (prev->_right == minRight)
					{
						prev->_right = minRight->_right;
					}
					
					delete minRight;
					return true;
				}
			}
		}
		return false;
	}

	
	void MidTraval()	//此接口作公有
	{
		__MidTraval(_root);
		cout << endl;
	}


	bool findR(const K& key)
	{
		return __findR(_root, key);
	}

	bool insertR(const K& key)
	{
		return __insertR(_root, key);
	}

	bool eraseR(const K& key)
	{
		return __eraseR(_root, key);
	}

	bool __eraseR(Node*& root, const K& key)	//此接口作私有
	{
		if (root == nullptr)
		{
			return false;
		}

		if (key < root->_key)
		{
			return __eraseR(root->_left, key);
		}
		else if (key > root->_key)
		{
			return __eraseR(root->_right, key);
		}
		else
		{
			Node* del = root;
			if (root->_left == nullptr)
			{
				// 此时root就是要删除的节点，并且是root的父节点的子节点的引用(root == root->_left...)
				root = root->_right;

				delete del;
				return true;
			}
			else if (root->_right == nullptr)
			{
				root = root->_left;

				delete del;
				return true;
			}
			else
			{
				Node* prev = _root;
				Node* minRight = root->_right;

				while (minRight->_left)		//二叉树特性，越往左越小
				{
					prev = minRight;
					minRight = minRight->_left;
				}

				root->_key = minRight->_key;

				// 替换好后，就要删除minRight
				if (prev->_left == minRight)
				{
					prev->_left = minRight->_right;
				}
				else if (prev->_right == minRight)
				{
					prev->_right = minRight->_right;
				}

				delete minRight;
				return true;
			}
		}
		return false;
	}

private:
	Node* _root = nullptr;	//根节点

	void __MidTraval(Node* root)	//此接口做私有
	{
		if (root == nullptr)
		{
			return;
		}

		__MidTraval(root->_left);
		cout << root->_key << " ";
		__MidTraval(root->_right);
	}


	bool __findR(Node* root, const K& key)	//此接口作私有
	{
		if (root == nullptr)
		{
			return false;
		}

		if (key < root->_key)
		{
			return __findR(root->_left, key);
		}
		else if (key > root->_key)
		{
			return __findR(root->_right, key);
		}
		return true;
	}

	bool __insertR(Node*& root, const K& key)	//此接口作私有
	{
		if (root == nullptr)
		{
			root = new Node(key);	//注意引用传参，root相当于root->left或root->right的别名
			return true;
		}


		if (key < root->_key)
		{
			return __insertR(root->_left, key);
		}
		else if (key > root->_key)
		{
			return __insertR(root->_right, key);
		}
		return false;
	}

	void Destructor(Node* root)	//此函数作私有
	{
		if (root == nullptr)
		{
			return;
		}

		// 后序删除
		Destructor(root->_left);
		Destructor(root->_right);
		delete root;
	}


	Node* Copy(Node* root)	//此接口作私有
	{
		if (root == nullptr)
		{
			return nullptr;
		}

		Node* ret = new Node(root->_key);
		ret->_left = Copy(root->_left);
		ret->_right = Copy(root->_right);
		return ret;
	}

};



namespace KV
{

	// 节点
	template <class Key,class Val>
	struct BST_node
	{
		BST_node<Key,Val>* _left;		//左子树
		BST_node<Key,Val>* _right;	//右子树
		Key _key;
		Val _val;
		BST_node(const Key& key,const Val& val)
			:_key(key), _val(val),_left(nullptr), _right(nullptr)
		{}
	};

	template <class Key,class Val>
	class BST
	{
		typedef BST_node<Key,Val> Node;
	public:
		bool insert(const Key& key,const Val& val)
		{
			if (_root == nullptr)
			{
				_root = new Node(key,val);
				return true;
			}

			Node* prev = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (key < cur->_key)
				{
					prev = cur;
					cur = cur->_left;
				}
				else if (key > cur->_key)
				{
					prev = cur;
					cur = cur->_right;
				}
				else
				{
					return false;
				}
			}

			cur = new Node(key,val);
			if (key < prev->_key)
			{
				prev->_left = cur;
			}
			else if (key > prev->_key)
			{
				prev->_right = cur;
			}
			return true;
		}


		Node* find(const Key& key)
		{
			if (_root == nullptr)
			{
				return nullptr;
			}

			Node* cur = _root;
			while (cur)
			{
				if (key < cur->_key)
				{
					cur = cur->_left;
				}
				else if (key > cur->_key)
				{
					cur = cur->_right;
				}
				else
				{
					// 找到了
					return cur;
				}
			}
			return nullptr;
		}


		bool erase(const Key& key)
		{
			if (_root == nullptr)
			{
				return false;
			}

			Node* prev = _root;
			Node* cur = _root;
			while (cur)
			{
				if (key < cur->_key)
				{
					prev = cur;
					cur = cur->_left;
				}
				else if (key > cur->_key)
				{
					prev = cur;
					cur = cur->_right;
				}
				else
				{
					if (cur->_left == nullptr)
					{
						if (_root == cur)
						{
							_root = _root->_right;
						}
						else if (prev->_left == cur)
						{
							prev->_left = cur->_right;
						}
						else if (prev->_right == cur)
						{
							prev->_right = cur->_right;
						}

						delete cur;
						return true;
					}

					else if (cur->_right == nullptr)
					{
						if (_root == cur)
						{
							_root = _root->_left;
						}
						else if (prev->_left == cur)
						{
							prev->_left = cur->_left;
						}
						else if (prev->_right == cur)
						{
							prev->_right = cur->_left;
						}

						delete cur;
						return true;
					}

					else
					{
						Node* prev = _root;
						Node* minRight = cur->_right;

						while (minRight->_left)
						{
							prev = minRight;
							minRight = minRight->_left;
						}

						cur->_key = minRight->_key;
						if (prev->_left == minRight)
						{
							prev->_left = minRight->_right;
						}
						else if (prev->_right == minRight)
						{
							prev->_right = minRight->_right;
						}

						delete minRight;
						return true;
					}
				}
			}
			return false;
		}


		void MidTraval()	
		{
			__MidTraval(_root);
			cout << endl;
		}

	private:
		Node* _root = nullptr;

		void __MidTraval(Node* root)
		{
			if (root == nullptr)
			{
				return;
			}

			__MidTraval(root->_left);
			cout << root->_key << ":" << root->_val << endl;
			__MidTraval(root->_right);
		}
	};
}