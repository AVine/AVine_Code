#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;
#include "BST.h"


void test1()
{
	BST<int> b;

	b.insert(8);
	b.insert(3);
	b.insert(1);
	b.insert(10);
	b.insert(6);
	b.insert(4);
	b.insert(7);
	b.insert(14);
	b.insert(13);

	b.erase(10);
	cout << b.find(7) << endl;
	b.mid_traval();
}

void test2()
{
	BST<int> b;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto& e : a)
	{
		b.insertR(e);
	}

	b.mid_traval();

	//cout << b.findR(3) << endl;
	for (auto& e : a)
	{
		b.eraseR(e);
		b.mid_traval();
	}

}


void test3()
{
	BST<int> b;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto& e : a)
	{
		b.insertR(e);
	}


	BST<int> c;
	c = b;
	c.mid_traval();
	for (auto& e : a)
	{ 
		c.eraseR(e);
		c.mid_traval();
	}

}


void test4()
{
	kv::BST<string, int> b;

	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����",
"ƻ��", "�㽶", "ƻ��", "�㽶" };


	kv::BST_node<string, int>* ret = nullptr;
	for (auto& e : arr)
	{
		ret = b.findR(e);
		if (ret)
		{
			ret->_v++;
		}
		else
		{
			b.insertR(e, 1);
		}
	}
	b.mid_traval();

}
int main()
{
	test4();
	return 0;
}