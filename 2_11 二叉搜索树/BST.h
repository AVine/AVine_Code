#pragma once



template <class K>	
struct BST_node
{
	BST_node<K>* _left;		//左子树
	BST_node<K>* _right;	//右子树
	K _key;		//数据

	BST_node(const K& key)
		:_key(key),_left(nullptr),_right(nullptr)
	{}
};


template <class K>	//节点存储什么样的数据类型
class BST
{
	typedef BST_node<K> Node;
public:
	BST()
		:_root(nullptr)
	{}

	~BST()
	{
		Destructor(_root);
	}

	BST(const BST& t)
	{
		_root = Copy(t._root);
	}

	BST<K>& operator=(BST<K> t)
	{
		swap(_root, t._root);
		return *this;
	}

	bool insertR(const K& key)	//递归插入
	{
		return __insertR(_root, key);
	}

	bool insert(const K& key)
	{
		if (_root == nullptr)
		{
			_root = new Node(key);
			return true;
		}


		// 根节点非空
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				// key值等于子树根节点的值
				return false;
			}
		}

		cur = new Node(key);
		if (key < parent->_key)
		{
			parent->_left = cur;
		}
		else if (key > parent->_key)
		{
			parent->_right = cur;
		}
		return true;
	}

	bool findR(const K& key)	//递归查找
	{
		return __findR(_root, key);
	}

	bool find(const K& key)
	{
		Node* cur = _root;

		while (cur)
		{
			if (key < cur->_key)
			{
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				cur = cur->_right;
			}
			else
			{
				// 相等
				return true;
			}
		}

		return false;

	}


	bool eraseR(const K& key)	//递归删除
	{
		return __eraseR(_root, key);
	}
	bool erase(const K& key)
	{
		if (_root == nullptr)
		{
			return false;
		}

		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				// 等于，就是要删除的节点
				
				//1.左子树为空
				if (cur->_left == nullptr)
				{
					if (parent == nullptr)
					{
						_root = cur->_right;
					}
					else
					{
						if (parent->_right == cur)
						{
							parent->_right = cur->_right;
						}
						else if (parent->_left == cur)
						{
							parent->_left = cur->_right;
						}
					}

					delete cur;
					return true;
				}

				//2.右子树为空
				else if (cur->_right == nullptr)
				{
					if (parent == nullptr)
					{
						_root = cur->_left;
					}
					else
					{
						if (parent->_right == cur)
						{
							parent->_right = cur->_left;
						}
						else if (parent->_left == cur)
						{
							parent->_left = cur->_left;
						}
					}

					delete cur;
					return true;
				}
				else
				{
					//3.左右子树都不为空
					
					Node* parent = cur;
					Node* minRight = cur->_right;
					while (minRight->_left)
					{
						parent = minRight;
						minRight = minRight->_left;	//一直往左走，保证是右子树的最小节点
					}

					cur->_key = minRight->_key;

					if (parent->_left == minRight)
					{
						parent->_left = minRight->_right;
					}
					else if (parent->_right == minRight)
					{
						parent->_right = minRight->_right;
					}

					delete minRight;
					return true;
				}
			}
		}
		return false;
	}


	void mid_traval()
	{
		__mid_traval(_root);
		cout << endl;
	}

private:
	Node* _root = nullptr;

	void __mid_traval(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		__mid_traval(root->_left);
		cout << root->_key << " ";
		__mid_traval(root->_right);
	}

	bool __insertR(Node*& root, const K& key)
	{
		if (root == nullptr)
		{
			root = new Node(key);
			return true;
		}
		else
		{
			if (key > root->_key)
			{
				return __insertR(root->_right, key);
			}
			else if (key < root->_key)
			{
				return __insertR(root->_left, key);
			}
		}
		return false;
	}

	//bool __insertR(Node* root,const K& key,Node* parent)
	//{
	//	if (root == nullptr)
	//	{
	//		Node* newnode = new Node(key);
	//		if (parent == nullptr)
	//		{
	//			_root = newnode;
	//		}
	//		else if (parent != nullptr)
	//		{
	//			if (key < parent->_key)
	//			{
	//				parent->_left = newnode;
	//			}
	//			else if (key > parent->_key)
	//			{
	//				parent->_right = newnode;
	//			}
	//		}
	//	}
	//	else
	//	{
	//		if (key < root->_key)
	//		{
	//			return __insertR(root->_left, key, root);
	//		}
	//		else if (key > root->_key)
	//		{
	//			return __insertR(root->_right, key, root);
	//		}
	//	}
	//	return false;
	//}

	bool __findR(Node* root, const K& key)
	{
		if (root == nullptr)
		{
			return false;
		}
		else
		{
			if (key > root->_key)
			{
				return __findR(root->_right, key);
			}
			else if (key < root->_key)
			{
				return __findR(root->_left, key);
			}
			else
			{
				return true;
			}
		}
	}

	bool __eraseR(Node*& root, const K& key)
	{
		if (root == nullptr)
		{
			return false;
		}
		else
		{
			if (key > root->_key)
			{
				return __eraseR(root->_right, key);
			}
			else if (key < root->_key)
			{
				return __eraseR(root->_left, key);
			}
			else
			{
				Node* del = root;
				if (root->_left == nullptr)
				{
					root = root->_right;

					delete del;
					return true;
				}
				else if (root->_right == nullptr)
				{
					root = root->_left;
					
					delete del;
					return true;
				}
				else
				{
					Node* parent = root;
					Node* minRight = root->_right;
					while (minRight->_left)
					{
						parent = minRight;
						minRight = minRight->_left;
					}

					root->_key = minRight->_key;
					if (parent->_left == minRight)
					{
						parent->_left = minRight->_right;
					}
					else if (parent->_right == minRight)
					{
						parent->_right = minRight->_right;
					}

					delete minRight;
					return true;

					//Node* minRight = root->_right;
					//while (minRight->_left)
					//{
					//	minRight = minRight->_left;
					//}

					//// minRight->_left == nullptr;
					//// minright->_key 就是右子树的最小节点

					//swap(root->_key, minRight->_key);
					//return __eraseR(root->_right, key);
				}
			}
		}
	}

	void Destructor(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		Destructor(root->_left);
		Destructor(root->_right);
		delete root;
	}

	Node* Copy(Node* root)
	{
		if (root == nullptr)
		{
			return nullptr;
		}

		Node* newnode = new Node(root->_key);
		newnode->_left = Copy(root->_left);
		newnode->_right = Copy(root->_right);
		return newnode;
	}
};


namespace kv	//kv模型
{
	template <class K,class Val>
	struct BST_node
	{
		BST_node<K,Val>* _left;		//左子树
		BST_node<K,Val>* _right;	//右子树
		K _key;		//数据
		Val _v;

		BST_node(const K& key,const Val& v)
			:_key(key), _v(v),_left(nullptr), _right(nullptr)
		{}
	};


	template <class K,class Val>	//节点存储什么样的数据类型
	class BST
	{
		typedef BST_node<K,Val> Node;
	public:
		BST()
			:_root(nullptr)
		{}

		bool insertR(const K& key,const Val& val)	//递归插入
		{
			return __insertR(_root, key,val);
		}

		void mid_traval()
		{
			__mid_traval(_root);
			cout << endl;
		}

		Node* findR(const K& key)
		{
			return __findR(_root,key);
		}
	private:
		Node* _root = nullptr;

		void __mid_traval(Node* root)
		{
			if (root == nullptr)
			{
				return;
			}

			__mid_traval(root->_left);
			cout << root->_key << ":" << root->_v << endl;
			__mid_traval(root->_right);
		}

		bool __insertR(Node*& root, const K& key,const Val& val)
		{
			if (root == nullptr)
			{
				root = new Node(key,val);
				return true;
			}
			else
			{
				if (key > root->_key)
				{
					return __insertR(root->_right, key,val);
				}
				else if (key < root->_key)
				{
					return __insertR(root->_left, key,val);
				}
			}
			return false;
		}

		Node* __findR(Node* root, const K& key)
		{
			if (root == nullptr)
			{
				return nullptr;
			}
			else
			{
				if (key > root->_key)
				{
					return __findR(root->_right, key);
				}
				else if (key < root->_key)
				{
					return __findR(root->_left, key);
				}
				else
				{
					return root;
				}
			}
		}
	};
}