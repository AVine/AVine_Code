#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
using namespace std;

#include "__BST.h"

void test1()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };

	BST<int> b;
	for (auto& e : a)
	{
		b.insert(e);
	}

	b.MidTraval();

	for (auto& e : a)
	{
		b.erase(e);
		b.MidTraval();
	}
}

void test2()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };

	BST<int> b;
	for (auto& e : a)
	{
		b.insertR(e);
	}

	b.MidTraval();
	//cout << b.findR(11) << endl;
	for (auto& e : a)
	{
		b.eraseR(e);
		b.MidTraval();
	}
}


void test3()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };

	BST<int> b;
	for (auto& e : a)
	{
		b.insertR(e);
	}

	BST<int> b2;
	b2 = b;
	b2.MidTraval();

	for (auto& e : a)
	{
		b2.eraseR(e);
		b2.MidTraval();
	}
}

void test_count()
{
	KV::BST<string, int> bt;
	string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜",
"苹果", "香蕉", "苹果", "香蕉" };

	for (auto& e : arr)
	{
		KV::BST_node<string, int>* ret = bt.find(e);
		if (ret)	//不为空，证明数据结构已有
		{
			ret->_val++;	//次数++
		}
		else
		{
			bt.insert(e, 1);
		}
	}

	bt.MidTraval();
}


int main()
{
	test_count();

	return 0;
}