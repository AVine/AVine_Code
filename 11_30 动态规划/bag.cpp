#define _CRT_SECURE_NO_WARNINGS 1

/*题目描述：有一背包能装n斤石头
现有w数组，每个元素描述的是某个石头的重量
现有v数组，每个元素描述的都是某个石头的价值
例如：有一能装100斤石头的背包
w[]={20,40,50,60}
v[]={100,1000,10000,1000}
求：背包装的石头不超过n时的最大石头价值*/

#include <vector>
#include <iostream>
using namespace std;


int process1(int n,vector<int>& w,vector<int>& v,int i)
{
	if (n < 0) return 0;
	if (i == w.size()) return 0;

	int p1 = process1(n, w, v, i + 1);		//没拿
	int p2 = 0;
	if(n-w[i] >= 0)
		p2 = v[i]+process1(n - w[i], w, v, i + 1);		//拿了
	return max(p1, p2);
}

int ways1(int n,vector<int>& w,vector<int>& v)
{
	//函数调用
	return process1(n, w, v,0);
}

int process2(int n, vector<int>& w, vector<int>& v, int i, vector<vector<int>>& dp)
{
	if (dp[n][i] != 0) return dp[n][i];
	int ans = 0;
	if (n < 0) ans = 0;
	else if (i == w.size()) ans = 0;
	else
	{
		int p1 = process2(n, w, v, i + 1, dp);		//没拿
		int p2 = 0;
		if (n - w[i] >= 0)
			p2 = v[i] + process2(n - w[i], w, v, i + 1, dp);		//拿了
		ans = max(p1, p2);
	}
	dp[n][i] = ans;

	return ans;
}

int ways2(int n, vector<int>& w, vector<int>& v)
{
	vector<vector<int>> dp(n + 1);		//傻缓存
	for (int i = 0; i < n + 1; i++)
	{
		dp[i].reserve(w.size()+1);
		dp[i].resize(w.size()+1);
	}

	//函数调用
	return process2(n, w, v, 0,dp);
}

int ways3(int n, vector<int>& w, vector<int>& v)		//最终动态规划
{
	vector<vector<int>> dp(n + 1);		//傻缓存
	for (int i = 0; i < n + 1; i++)
	{
		dp[i].reserve(w.size() + 1);
		dp[i].resize(w.size() + 1);
	}

	for (int index = w.size() - 1; index >= 0; index--)
	{
		for (int row = 0; row <= n; row++)
		{
			int p1 = dp[row][index + 1];
			int p2 = 0;
			if (row - w[index] >= 0)
				p2 = v[index] + dp[row - w[index]][index + 1];
			dp[row][index] = max(p1, p2);
		}
	}
	return dp[n][0];		//n index
}


int main()
{
	int n = 11;
	vector<int> w{ 3,2,4,7 };
	vector<int> v{ 5,6,3,19 };

	int ret1 = ways1(n, w, v);
	int ret2 = ways2(n, w, v);
	int ret3 = ways3(n, w, v);


	cout << ret1 << endl;
	cout << ret2 << endl;
	cout << ret3 << endl;


	return 0;
}