#define _CRT_SECURE_NO_WARNINGS 1



////题目要求：有一个只存正整数的数组，有两个玩家依次从这个数组拿数字
////每次只能从最左或最右拿，两个玩家绝顶聪明，都能拿到最优解
////问谁最后的分数高
//
//#include <iostream>
//#include <vector>
//using namespace std;
//
//
//
//int process(int cur, vector<vector<int>>& r, int k, int n, vector<vector<int>>& dp)
//{
//    if (dp[cur][k] != 0) return dp[cur][k];
//    int ans = 0;
//
//    if (k == 0) return cur == n - 1 ? 1 : 0;
//    else
//    {
//        for (int i = 0; i < r.size(); i++)
//        {
//            if (r[i][0] == cur)
//            {
//                ans += process(r[i][1], r, k - 1, n, dp);
//            }
//            dp[cur][k] = ans;
//        }
//
//        for (int i = 0; i < dp.size(); i++)
//        {
//            for (int j = 0; j < dp[i].size(); j++)
//            {
//                cout << dp[i][j] << " ";
//            }
//            cout << endl;
//        }
//        cout << endl;
//
//    }
//    return ans;
//}
//int numWays(int n, vector<vector<int>>& relation, int k) {
//
//    vector<vector<int>> dp(n);
//    for (int i = 0; i < n; i++)
//    {
//        dp[i].reserve(k + 1);
//        dp[i].resize(k + 1);
//    }
//
//    return process(0, relation, k, n, dp);
//}
//
//
//
//int main()
//{
//    int n = 5, k = 3;
//    vector<vector<int>> vec{ {0, 2},{2, 1},{3, 4},{2, 3},{1, 4},{2, 0},{0, 4} };
//    numWays(5, vec, 3);
//
//    return 0;
//}


//int l(vector<int>& vec, int left, int right);
//
//int f(vector<int>& vec, int left, int right)
//{
//	if (left == right) return vec[left];		//只剩下一张牌时，先手直接拿走
//	
//	int p1 = vec[left] + l(vec, left + 1, right);	//如果拿走左边的，那么先手玩家就要在left+1和right间后手了
//	int p2 = vec[right] + l(vec, left, right - 1);	//如果拿走右边的，那么先手玩家就要在left和right-1间后手了
//	return max(p1, p2);		//返回最优解
//
//}
//
//int l(vector<int>& vec, int left, int right)
//{
//	if (left == right) return 0;		//如果只剩下一张牌，先手拿走，后手只能为0
//	
//	int p1 = f(vec, left + 1, right);	//如果先手拿走了左边的，那么后手就可以在left+1和right间先手了
//	int p2 = f(vec, left, right - 1);	//如果先手拿走了右边的，那么后手就可以在left和right-1间先手了
//
//	return min(p1, p2);		//但是作为后手，拿不到最优解
//
//}
//
//int win1(vector<int>& vec)
//{
//	int fast = f(vec,0,vec.size() - 1);		//先手玩家
//	int last = l(vec, 0, vec.size() - 1);		//后手玩家
//	return max(fast, last);
//}
//
//
//
//
//int l2(vector<int>& vec, int left, int right, vector<vector<int>>& fdp, vector<vector<int>>& ldp);
//
//int f2(vector<int>& vec, int left, int right, vector<vector<int>>& fdp, vector<vector<int>>& ldp)
//{
//	if (fdp[left][right] != 0) return fdp[left][right];
//	int ans = 0;
//	if (left == right) ans = vec[left];		//只剩下一张牌时，先手直接拿走
//	else
//	{
//		int p1 = vec[left] + l2(vec, left + 1, right, fdp, ldp);	//如果拿走左边的，那么先手玩家就要在left+1和right间后手了
//		int p2 = vec[right] + l2(vec, left, right - 1, fdp, ldp);	//如果拿走右边的，那么先手玩家就要在left和right-1间后手了
//		ans = max(p1, p2);
//	}
//	fdp[left][right] = ans;
//	return ans;
//}
//
//int l2(vector<int>& vec, int left, int right, vector<vector<int>>& fdp, vector<vector<int>>& ldp)
//{
//	if (ldp[left][right] != 0) return ldp[left][right];
//	int ans = 0;
//	if (left == right) ans = 0;		//如果只剩下一张牌，先手拿走，后手只能为0
//	else
//	{
//		int p1 = f2(vec, left + 1, right, fdp, ldp);	//如果先手拿走了左边的，那么后手就可以在left+1和right间先手了
//		int p2 = f2(vec, left, right - 1, fdp, ldp);	//如果先手拿走了右边的，那么后手就可以在left和right-1间先手了
//		ans = min(p1, p2);
//	}
//	ldp[left][right] = ans;
//	return ans;
//}
//
//int win2(vector<int>& vec)
//{
//	vector<vector<int>> fdp(vec.size());		//先手缓存
//	vector<vector<int>> ldp(vec.size());		//后手缓存
//	for (int i = 0; i < vec.size(); i++)
//	{
//		fdp[i].reserve(vec.size());
//		fdp[i].resize(vec.size(),0);
//		ldp[i].reserve(vec.size());
//		ldp[i].resize(vec.size(),0);
//	}
//
//	int fast = f2(vec, 0, vec.size() - 1,fdp,ldp);		//先手玩家
//	int last = l2(vec, 0, vec.size() - 1,fdp,ldp);		//后手玩家
//	return max(fast, last);
//}
//
//int win3(vector<int>& vec)
//{
//	vector<vector<int>> fdp(vec.size());		//先手缓存
//	vector<vector<int>> ldp(vec.size());		//后手缓存
//	for (int i = 0; i < vec.size(); i++)
//	{
//		fdp[i].reserve(vec.size());
//		fdp[i].resize(vec.size(), 0);
//		ldp[i].reserve(vec.size());
//		ldp[i].resize(vec.size(), 0);
//	}
//
//	//对角线填表
//	for (int col = 1; col < vec.size(); col++)
//	{
//		int left = 0, right = col;
//		while (right < vec.size())
//		{
//			fdp[left][right] = max(vec[left] + ldp[left + 1][right], vec[right] + ldp[left][right - 1]);
//			ldp[left][right] = min(fdp[left + 1][right], fdp[left][right - 1]);
//			++left;
//			++right;
//		}
//	}
//	return max(fdp[0][vec.size() - 1], ldp[0][vec.size() - 1]);
//}
//
//int main()
//{
//	vector<int> vec{ 50,100,20,10 };
//	int ret = win1(vec);
//	int ret2 = win2(vec);
//	int ret3 = win3(vec);
//
//
//	cout << ret << endl;
//	cout << ret2 << endl;
//	cout << ret3 << endl;
//
//	return 0;
//}




///*题目要求：
//有一个机器人，在给定的1~N的范围上移动，总共可以移动K步(在1不能往左走，在N不能往右走)
//其目标为M，问：有几种移动到M的方案？*/
//
//#include <iostream>
//#include <vector>
//using namespace std;
//
////当前位置，还可以走几步，目标，范围
//int ways1(int cur, int K, int M, int N)		//暴力递归，存在多种重复结果
//{
//	if (K == 0)		//步数用完
//		return cur == M ? 1 : 0;
//
//	//两个边界
//	if (cur == 1)
//		return ways1(2, K - 1, M, N);
//	if (cur == N)
//		return ways1(N - 1, K - 1, M, N);
//
//	return ways1(cur + 1, K - 1, M, N) + ways1(cur - 1, K - 1, M, N);
//}
//
//void test1()
//{
//	int ret = ways1(2, 9, 9, 13);
//	cout << ret << endl;
//}
//
//
////当前位置，还可以走几步，目标，范围,傻缓存
//int ways2(int cur, int K, int M, int N,vector<vector<int>>& dp)
//{
//	if (dp[cur][K] != 0) return dp[cur][K];		//如果重复了，直接从缓存拿
//
//	int cnt = 0;
//	if (K == 0)	
//		cnt = cur == M ? 1 : 0;		//确定方案了，存下来
//
//	else if (cur == 1)
//		cnt = ways2(2, K - 1, M, N,dp);
//	else if (cur == N)
//		cnt = ways2(N - 1, K - 1, M, N,dp);
//	else 
//		cnt =  ways2(cur + 1, K - 1, M, N,dp) + ways2(cur - 1, K - 1, M, N,dp);
//	dp[cur][K] = cnt;		//存
//	return cnt;
//}
//
//void test2()
//{
//	int N = 5, K = 5, M = 4;
//	vector<vector<int>> dp(N+1);		//傻缓存
//	for (int i = 0; i < N + 1; i++)
//	{
//		dp[i].reserve(K + 1);
//		dp[i].resize(K + 1, 0);
//	}
//	int ret = ways2(3, K, M, N,dp);
//	cout << ret << endl;
//}
//
////最终动态规划
//int ways3(int cur, int K, int M, int N, vector<vector<int>>& dp)
//{
//	dp[M][0] = 1;
//	for (int col = 1; col <= K; ++col)
//	{
//		dp[1][col] = dp[2][col - 1];
//
//		for (int row = 2; row < N; ++row)
//		{
//			dp[row][col] = dp[row - 1][col - 1] + dp[row + 1][col - 1];
//		}
//		dp[N][col] = dp[N-1][col - 1];
//	}
//	return dp[cur][K];
//}
//
//void test3()
//{
//	int N = 5, K = 6, M = 4;
//	vector<vector<int>> dp(N + 1);
//	for (int i = 0; i < N + 1; i++)
//	{
//		dp[i].reserve(K + 1);
//		dp[i].resize(K + 1, 0);
//	}
//	int ret = ways3(4, K, M, N, dp);
//	cout << ret << endl;
//
//}
//int main()
//{
//	test3();
//
//	return 0;
//}