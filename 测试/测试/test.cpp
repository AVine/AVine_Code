#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>

void func(char para[100])
{
	void* p = malloc(100);
	printf("%d, %d\n", sizeof(para), sizeof(p));
}


int main()
{
	char str[100];
	func(str);
	/*int value = 1024;
	char condition = *((char*)(&value));
	if (condition) value += 1; condition = *((char*)(&value));
	if (condition) value += 1; condition = *((char*)(&value));*/
	//printf("%d %d", value, condition);

	return 0;
}