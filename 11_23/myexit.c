#include <stdio.h>
#include <stdlib.h>
int Accumulation(int from,int to)
{
    int sum = 0;
    for(int i=from;i<to;i++)        //故意少加一个数
    {
        sum += i;
    }
    if(sum != 5050) exit(1);
    return sum;
}
int main()
{
    int sum = Accumulation(1,100);
    return 0;
}
