#include <stdio.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
int main()
{
    pid_t id = fork();
    if(id == 0)
    {
        int cnt = 0;
        while(cnt < 5)
        {
            printf("我是子进程，我正在执行一些程序...PID:%d\n",getpid());
            ++cnt;
            sleep(1);
        }
        exit(1);
    }
    else if(id > 0)
    {
        int status = 0;
        while(1)
        { 
            pid_t ret = waitpid(id,&status,WNOHANG);      //0默认为阻塞等待
            //varpid_t ret = wait(NULL);
            if(ret > 0)
            {
                printf("等待子进程成功，PID:%d,退出码:%d,信号:%d\n",ret,(status>>8)&0xff,status & 0x7f);
                break;
            }
            else if(ret == 0)
            {
                printf("非阻塞等待，父进程可以执行其他程序...\n");
            }
            sleep(1);
        }
    }
    return 0;
}
