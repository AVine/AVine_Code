#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <assert.h>


char command[1024]={0};
char* myargv[64]={0};
int main()
{
    while(1)
    {
        printf("[用户名@主机名 当前路径] #");
        fflush(stdout);         //刷新缓冲区
        fgets(command,sizeof(command)-1,stdin);     //fgets会自动添加'\0'，所以-1
        command[strlen(command)-1]=0;       //覆盖最后一个'\0'
        
        myargv[0]=strtok(command," ");      //字符串切割
        int i=1;
        while(myargv[i++]=strtok(NULL," "));
        
        if(myargv[0] != NULL && strcmp(myargv[0],"cd") == 0)        //内建命令
        {
            if(myargv[1] != NULL) chdir(myargv[1]);
            continue;
        }
        
        pid_t id = fork();
        if(id == 0)
        {
            execvp(myargv[0],myargv);       //执行哪个指令，如何执行
            exit(1);        //如果替换失败
        }
        else if(id > 0)
        {
            int status = 0;
            pid_t ret = waitpid(id,&status,0);
            if(ret > 0)
            {
                if(((status >> 8)& 0xff) != 0)
                {
                    printf("进程替换失败!\n");
                }
            }
        }
        
    }
}
