#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
using namespace std;

double div(double x, double y)
{
	//try
	//{
		if (y == 0)
		{
			string s("div by zero condition");
			throw s;//出现异常现在当前层找cathc，找不到去上一层调用链。main还找不到就报错
		}
		else
		{
			return x / y;
		}
	//}
	//catch (...)
	//{
	//	cout << "div" << endl;
	//}
	//异常捕获完之后正常走后面的程序
}

void func()
{
	//int* p = new int[10];//中间层动态开辟了一块空间
	double x = 0, y = 0;
	cin >> x >> y;
	//try
	//{
		cout << div(x, y) << endl;
	//}
	//catch (...)
	//{
	//	delete[] p;//产生异常先防止内存泄漏(这种写法非常low)
	//	throw;//重新抛出
	//}
}
int main()
{
	//出现了try就必须有catch
	try
	{
		func();
	}
	catch (const char* str)
	{
		cout << str << endl;
	}
	catch (...)
	{
		cout << "未知异常" << endl;
	}

	cout << "异常捕获完成继续走下面的代码" << endl;
	return 0;
}