#pragma once

#include <vector>
#include <algorithm>

namespace ly
{
	template <class K>
	struct Greater
	{
		bool operator()(const K& left, const K& right)
		{
			return left > right;
		}
	};

	template <class K>
	struct Less
	{
		bool operator()(const K& left, const K& right)
		{
			return left < right;
		}
	};

	template <class K, class Container = std::vector<K>,class Compare = Less<K>>
	class priority_queue
	{
	public:
		// 迭代器区间构造
		template <class Iterator>
		priority_queue(const Iterator& left,const Iterator& right)
			:_con(left,right)
		{
			// 调整
			for (int i = _con.size() - 1; i >= 0; i--)
			{
				adjustDown(i,_con.size());
			}
		}

		void sort()
		{
			int heapSize = _con.size();
			while (heapSize)
			{
				std::swap(_con[0], _con[heapSize - 1]);
				adjustDown(0, --heapSize);
			}
		}

		void push(const K& k)
		{
			_con.push_back(k);
			adjustUp(_con.size() - 1, _con.size());
		}


		void adjustUp(int child, int heapSize)
		{
			Compare com;
			int parent = (child - 1) / 2;

			while (com(_con[parent],_con[child]))
			{
				std::swap(_con[child], _con[parent]);
				child = parent;
				parent = (child - 1) / 2;
			}
		}

		void adjustDown(int parent,int heapSize)
		{
			Compare com;
			int child = parent * 2 + 1;	//左孩子
			
			while (child < heapSize)
			{
				if (child + 1 < heapSize && _con[child + 1] > _con[child])
				{
					++child;
				}

				// 如果父节点小于孩子节点
				if (com(_con[parent], _con[child]))
				{
					std::swap(_con[parent], _con[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}

	private:
		Container _con;
	};
}
