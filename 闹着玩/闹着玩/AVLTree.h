#define _CRT_SECURE_NO_WARNINGS 1

#pragma once




template<class K,class V>
struct AVLTreeNode
{
	pair<K, V> _kv;
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;

	int _bf;	//平衡因子

	AVLTreeNode(const pair<K,V>& kv)
		:_kv(kv),_left(nullptr),_right(nullptr)
		,_parent(nullptr),_bf(0)
	{}
};


template<class K,class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	bool insert(const pair<K,V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (kv.first < cur->_kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (kv.first > cur->_kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;
			}
		}

		// 找到了插入的位置
		cur = new Node(kv);
		if (kv.first < parent->_kv.first)
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		else if (kv.first > parent->_kv.first)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}

		while (parent)
		{
			if (parent->_left == cur)
			{
				parent->_bf--;
			}
			else if (parent->_right == cur)
			{
				parent->_bf++;
			}

			if (parent->_bf == 0)
			{
				break;
			}
			else if (parent->_bf == -1 || parent->_bf == 1)
			{
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				// 旋转
				if (parent->_bf == 2 && cur->_bf == 1)	//右右
				{
					RotateL(parent);	//左单旋
				}
				else if(parent->_bf == -2 && cur->_bf == -1)	//左左
				{
					RotateR(parent);	//右单旋
				}
				else if (parent->_bf == -2 && cur->_bf == 1)	//左右
				{
					RotateLR(parent);	//左右双旋
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					RotateRL(parent);
				}
				else
				{
					assert(false);
				}

				break;
			}
			else
			{
				assert(false);
			}
		}
		return true;
	}


	void RotateL(Node* parent)
	{
		Node* cur = parent->_right;
		Node* curL = cur->_left;

		parent->_right = curL;
		if (curL)
		{
			curL->_parent = parent;
		}
		cur->_left = parent;

		Node* oldParent = parent->_parent;

		parent->_parent = cur;

		if (oldParent == nullptr)
		{
			_root = cur;
			_root->_parent = nullptr;
		}
		else
		{
			if (oldParent->_left == parent)
			{
				oldParent->_left = cur;
				cur->_parent = oldParent;
			}
			else if (oldParent->_right == parent)
			{
				oldParent->_right = cur;
				cur->_parent = oldParent;
			}
		}

		parent->_bf = cur->_bf = 0;
	}

	void RotateR(Node* parent)
	{
		Node* cur = parent->_left;
		Node* curR = cur->_right;

		parent->_left = curR;
		if (curR)
		{
			curR->_parent = parent;
		}
		cur->_right = parent;

		Node* oldParent = parent->_parent;
		
		parent->_parent = cur;

		if (oldParent == nullptr)
		{
			_root = cur;
			_root->_parent = nullptr;
		}
		else
		{
			if (oldParent->_left == parent)
			{
				oldParent->_left = cur;
				cur->_parent = oldParent;
			}
			else if (oldParent->_right == parent)
			{
				oldParent->_right = cur;
				cur->_parent = oldParent;
			}
		}

		parent->_bf = cur->_bf = 0;
	}


	void RotateLR(Node* parent)
	{
		Node* cur = parent->_left;
		Node* curR = cur->_right;

		int bf = curR->_bf;

		RotateL(parent->_left);
		RotateR(parent);

		if (bf == -1)	//新增节点插在curR的左侧
		{
			curR->_bf = 0;
			cur->_bf = 0;
			parent->_bf = 1;
		}
		else if (bf == 1)	//新增节点插在curR的右侧
		{
			curR->_bf = 0;
			cur->_bf = -1;
			parent->_bf = 0;
		}
		else if (bf == 0)	//curR本身就是新增节点
		{
			curR->_bf = cur->_bf = parent->_bf = 0;
		}
	}

	void RotateRL(Node* parent)
	{
		Node* cur = parent->_right;
		Node* curL = cur->_left;

		int bf = curL->_bf;

		RotateR(parent->_right);
		RotateL(parent);

		if (bf == -1)	//新增节点插在curL的左侧
		{
			curL->_bf = 0;
			cur->_bf = 1;
			parent->_bf = 0;
		}
		else if (bf == 1)	//新增节点插在curL的右侧
		{
			curL->_bf = 0;
			parent->_bf = -1;
			cur->_bf = 0;
		}
		else if(bf == 0)	//curL本身就是新增节点
		{
			curL->_bf = cur->_bf = parent->_bf = 0;
		}
	}

	bool isAVLTree()
	{
		return isAVLTree(_root);
	}

	bool isAVLTree(Node* root)
	{
		if (root == nullptr)
		{
			return true;
		}

		int leftHeight = Height(root->_left);
		int rightHeight = Height(root->_right);
		
		if (root->_bf != (rightHeight - leftHeight))
		{
			cout << root->_kv.first << "平衡因子异常:" << root->_bf << endl;
			return false;
		}

		return abs(rightHeight - leftHeight) < 2
			&& isAVLTree(root->_left)
			&& isAVLTree(root->_right);
	}


	int Height(Node* root)
	{
		if (root == nullptr)
		{
			return 0;
		}

		int lh = Height(root->_left);
		int rh = Height(root->_right);

		return lh > rh ? lh + 1 : rh + 1;
	}

	
	void inorder()
	{
		inorder(_root);
		cout << endl;
	}

	void inorder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		inorder(root->_left);
		cout << root->_kv.first << " ";
		inorder(root->_right);
	}

	void level()
	{
		vector<vector<string>> vec;
		if (_root == nullptr)
		{
			return;
		}
		
		queue<Node*> q;
		q.push(_root);

		while (!q.empty())
		{
			int size = q.size();
			vector<string> tmp;
			while (size--)
			{
				Node* front = q.front();
				q.pop();
				
				if (front)
				{
					tmp.push_back(to_string(front->_kv.first));
				}
				else
				{
					tmp.push_back("nullptr");
				}
				
				if (front)
				{
					if (front->_left)
					{
						q.push(front->_left);
					}
					else
					{
						q.push(nullptr);
					}

					if (front->_right)
					{
						q.push(front->_right);
					}
					else
					{
						q.push(nullptr);
					}
				}

			}
			vec.push_back(tmp);


		}

		for (int i = 0; i < vec.size(); i++)
		{
			for (int j = 0; j < vec[i].size(); j++)
			{
				cout << vec[i][j] << " ";
			}
			cout << endl;
		}
	}


private:
	Node* _root = nullptr;
};