#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

class A
{
public:
	virtual ~A()
	{
		cout << "A::~A()" << endl;
	}
};

class B : public A
{
public:
	~B()
	{
		cout << "B:~B()" << endl;
	}
};

int main()
{
	// 如果基类和派生类的析构函数不构成多态系统
	// 那么在清理资源时就会产生内存泄漏
	A* pa = new A;
	A* pb = new B;

	delete pa;
	delete pb;
	return 0;
}



//#include "__priority_queue.h"
//using namespace ly;
//
//
//void test1()
//{
//	std::vector<int> vec{ 7,8,2,4,6,9,10 };
//	priority_queue<int> heap(vec.begin(),vec.end());
//	heap.push(13);
//	heap.sort();
//
//}
//int main()
//{
//	test1();
//	return 0;
//}