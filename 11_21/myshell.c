#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <assert.h>

#define NUM 1024

char command[NUM];      //c99数组
char* myargv[64];       //存储指令参数
int main()
{
    while(1)
    {
        printf("[用户名@主机名 %s]",getenv("PWD"));
        fflush(stdout);

        char* s = fgets(command,sizeof(command),stdin);
        command[strlen(command)-1]=0;       //清除 \n 
        

        myargv[0] = strtok(command," ");
        int i = 1;
        while(myargv[i++] = strtok(NULL," "));      //切割空格
        

        pid_t id = fork();
        if(id == 0)
        {
           execvp(myargv[0],myargv);
           exit(1);
        }
        waitpid(id,NULL,0);
    }
    return 0;
}
