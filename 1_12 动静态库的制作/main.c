#include <stdio.h>
#include "Add.h"
#include "Sub.h"

int main()
{
    int x = 10,y = 20;
    int res = Sub(x,y);
    printf("%d - %d = %d\n",x,y,res);
    res = Add(x,y);
    printf("%d + %d = %d\n",x,y,res);
    return 0;
}
