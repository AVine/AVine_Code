#define _CRT_SECURE_NO_WARNINGS 1


#include <vector>

namespace ly
{
	//使用模板泛型化
	template<class T,class Container = std::vector<T>>
	class stack
	{
	public:
		//成员只有 _con，这个_con还是vector实例化之后的类型
		//所以不需要显式定义构造函数，因为默认生成的够用

		void push(const T& val)
		{
			_con.push_back(val);
		}

		void pop()
		{
			_con.pop_back();
		}

		T& top()
		{
			return _con[_con.size() - 1];
		}

		size_t size()
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.size() == 0;
		}
	private:
		Container _con;

	};
}