#define _CRT_SECURE_NO_WARNINGS 1

#include "stack.h"
#include "queue.h"

#include <iostream>
using std::cout;
using std::endl;
using namespace ly;


void test1()
{
	stack<int> s1;
	s1.push(1);
	s1.push(2);
	s1.push(3);
	s1.push(4);
	s1.push(5);

	while (!s1.empty())
	{
		cout << s1.top() << endl;
		s1.pop();
	}

}

void test2()
{
	queue<int> q1;
	q1.push(1);
	q1.push(2);
	q1.push(3);
	q1.push(4);
	q1.push(5);

	while (!q1.empty())
	{
		cout << q1.back() << endl;
		q1.pop();
	}
}


void test_stack()
{
	stack<int> st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);
	st.push(5);

	while (!st.empty())
	{
		cout << st.top() << " ";
		st.pop();
	}
	cout << endl;

}

void test_queue()
{
	queue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	q.push(5);
	q.push(6);


	while (!q.empty())
	{
		cout << q.front() << " ";
		q.pop();
	}
	cout << endl;
}
int main()
{
	test_queue();
	return 0;
}