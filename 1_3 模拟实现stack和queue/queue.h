#pragma once


#include <list>

namespace ly
{
	template<class T,class Container = std::list<T>>
	class queue
	{
	public:
		//不需要构造函数

		void push(const T& val)
		{
			_con.push_back(val);
		}

		void pop()
		{
			_con.pop_front();
		}

		T& front()
		{
			return _con.front();
		}

		T& back()
		{
			return _con.back();
		}

		size_t size()
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.size() == 0;
		}
	private:
		Container _con;
	};
}