
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <cassert>

// 实现父进程向管道写入 "i am father"
// 子进程从管道中读取父进程发送的数据
int main()
{
    int fds[2] = {0};   // 用来保存管道读端、写端的文件描述符
    int n = pipe(fds);    // 创建管道文件的系统调用
    assert(n == 0);   // pipe返回0证明创建管道成功
    // 管道文件创建创建成功后，会将读、写端的文件描述符写入fds
    // 其中,fds[0]表写端,fds[1]表读端

    pid_t id = fork();  // 创建子进程
    assert(id >= 0);

    if(id == 0) // 子进程
    {
        close(fds[1]);  // 子进程关闭写端

        while(true)
        {
            char buffer[1024] = {0};
            int s = read(fds[0],buffer,sizeof(buffer)-1);   // 从管道读数据
            if(s > 0)
            {
                buffer[s] = 0;
                std::cout <<  buffer << std::endl;
            }
            else if(s == 0)
            {
                std::cout << "未从管道读到任何数据!" << std::endl;
            }
        }

        close(fds[0]);
        exit(0);
    }
    else  // 父进程
    {
        close(fds[0]);  // 父进程关闭读端

        while(true)
        {
            char buffer[1024] = {0};
            snprintf(buffer,sizeof(buffer),"i am father");
            write(fds[1],buffer,strlen(buffer));    //向管道写入信息
            sleep(1);   
        }
    }

    n = waitpid(id,nullptr,0);  // 阻塞等待子进程退出
    assert(n == id);

    close(fds[1]);
    return 0;
}