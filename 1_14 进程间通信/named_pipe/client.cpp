// client.cpp
#include "command.h"

// client做写端

int main()
{

    // 如果server端没有打开管道文件
    // 那么open将阻塞
    int wfd = open(PIPE_NAME,O_WRONLY);//只写方式打开文件
    assert(wfd > 0);
    (void)wfd;

    char buffer[1024] = {0};
    while(true)
    {
        // 从stdin中读取数据到buffer里面来
        // 大小-1更加安全(虽然fgets会自动补\0)
        std::cout << "client say #";
        fgets(buffer,sizeof(buffer)-1,stdin);
        if(strlen(buffer) > 0)
        {
            // 事实上fgets一定会读到至少一个字符(\n)
            buffer[strlen(buffer)-1] = 0;//将读上来的回车吃掉
        }

        // 向管道中写入信息
        int n = write(wfd,buffer,strlen(buffer));
        assert(n > 0);
        (void)n;
    }

    close(wfd);

    return 0;
}