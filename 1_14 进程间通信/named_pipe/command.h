// command.h
#pragma once

// 编写打开管道文件的方式
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <cstdio>
#include <cassert>

// 默认为本目录下的named_pipe文件
#define PIPE_NAME "named_pipe"

// 创建管道文件
bool createNamedPipe(const std::string& path)
{
    umask(0);   //使得权限是我们想要的
    int n = mkfifo(path.c_str(),0600);// 只允许拥有者读写
    if(n < 0)
    {
        // 返回值为-1则打开管道文件失败
        std::cout << "strerror:" << strerror(errno) << std::endl;
        return true;
    }
    else if(n == 0)
    {
        return true;
    }
}

// 关闭管道文件
bool closeNamedPipe(const std::string& path)
{
    int n = unlink(path.c_str()); // 关闭管道文件
    assert(n == 0);
    (void)n;
}