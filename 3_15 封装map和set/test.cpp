#define _CRT_SECURE_NO_WARNINGS 1

#include <map>
#include <string>
#include <iostream>
using namespace std;
#include "Map.h"
#include "RBTree.h"
#include "Set.h"

void test_map()
{
	ly::map<string, int> m;
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����",
"ƻ��", "�㽶", "ƻ��", "�㽶" };
	for (auto& e : arr)
	{
		m[e]++;
	}

	auto it = m.begin();
	while (it != m.end())
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;
}

void test_set()
{
	ly::set<int> s;
	s.insert(1);
	s.insert(4);
	s.insert(2);
	s.insert(3);
	s.insert(5);

	auto it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}
int main()
{
	test_set();
	return 0;
}