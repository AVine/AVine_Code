#pragma once

#include "RBTree.h"
namespace ly
{
	template<class K>
	class set
	{
	public:
		struct KeyOfSet//提供给红黑树的仿函数
		{
			K operator()(const K& k)
			{
				return k;
			}
		};

		//加上typename表示是某类中的类型而非静态变量
		//需要注意，通常我们不希望改变set中K值
		//所以set使用的迭代器都是const迭代器
		typedef typename RBTree<K, K, KeyOfSet>::const_iterator iterator;
		typedef typename RBTree<K, K, KeyOfSet>::const_iterator const_iterator;

		iterator begin() const
		{
			return _rb.begin();
		}

		iterator end() const
		{
			return _rb.end();
		}

		pair<iterator, bool> insert(const K& k)
		{
			/*又因为set使用的const迭代器
			而底层红黑树的insert接口返回的是普通迭代器
			而普通迭代器是不能类型转换成const迭代器的*/
			//return _rb.insert(k);

			//所以需要在底层的迭代器上做更改
			//使得普通迭代器能够构造出const迭代器
			//所以，第一步：先使用普通迭代器的pair对象接收返回值
			pair<typename RBTree<K, K, KeyOfSet>::const_iterator, bool> p = _rb.insert(k);
			//第二步：再用普通迭代器构造出const迭代器
			return pair<iterator, bool>(p.first, p.second);
		}


	private:
		RBTree<K, K, KeyOfSet> _rb;//红黑树对象
	};
}
