#pragma once

#include "RBTree.h"
namespace ly
{
	template<class K, class V>
	class map
	{
	public:
		struct KeyOfMap//提供给红黑树的仿函数
		{
			K operator()(const pair<const K, V>& kv)
			{
				return kv.first;
			}
		};

		//加上typename表示是某类中的类型而非静态变量
		typedef typename RBTree<K, pair<const K, V>, KeyOfMap>::iterator iterator;
		typedef typename RBTree<K, pair<const K, V>, KeyOfMap>::const_iterator const_iterator;

		iterator begin()
		{
			return _rb.begin();
		}

		iterator end()
		{
			return _rb.end();
		}

		const_iterator begin() const
		{
			return _rb.begin();
		}

		const_iterator end() const
		{
			return _rb.end();
		}

		pair<iterator, bool> insert(const pair<const K, V>& kv)
		{
			return _rb.insert(kv);
		}

		V& operator[](const K& k)
		{
			//只通过insert接口实现[]运算符重载
			//1.先使用临时变量接收insert的返回值
			pair<iterator, bool> p = insert(make_pair(k, V()));
			//2.p中的first就是迭代器，指向了insert的节点
			return p.first->second;
		}

	private:
		RBTree<K, pair<const K, V>, KeyOfMap> _rb;//红黑树对象
	};
}
