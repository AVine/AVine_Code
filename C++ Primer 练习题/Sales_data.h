#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
#include <fstream>
using namespace std;
class Sales_data
{
public:
	//Sales_data() = default;		//合成的默认构造函数

	Sales_data(istream& in)
	{
		read(in, *this);
	}

	Sales_data(const string& s)
		:bookNo(s)
	{}

	Sales_data(const string& s, size_t n, double p)
		:bookNo(s), units_sold(n), revenue(p* n)	//初始化顺序与成员声明有关；与初始化列表无关
	{}

	Sales_data()
		:bookNo(""), units_sold(0), revenue(0)
	{}

	friend istream& read(istream& in, Sales_data& s);

	friend ostream& print(ostream& out, const Sales_data& s);

	Sales_data& combine(const Sales_data& ans)
	{
		units_sold += ans.units_sold;
		revenue += ans.revenue;
		return *this;
	}

	string isbn() const
	{
		return bookNo;
	}


private:	//隐藏实现细节
	string bookNo;		//ISBN编号
	size_t units_sold = 0;		//销量
	double revenue = 0;		//单价
};

istream& read(istream& in, Sales_data& s)
{
	double price = 0;
	in >> s.bookNo >> price;
	s.units_sold = 1;
	s.revenue = price * s.units_sold;
	return in;
}

ostream& print(ostream& out, const Sales_data& s)
{
	out << s.bookNo << "销售数:" << s.units_sold;
	out << "	销售额:" << s.revenue << endl;
	return out;
}

Sales_data add(const Sales_data& s1, const Sales_data& s2)
{
	Sales_data sum = s1;
	sum.combine(s2);
	return sum;
}