#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string>
using namespace std;

//练习 6.54/55/56


int Add(int x, int y)
{
	return x + y;
}
int Sub(int x, int y)
{
	return x - y;
}
int Mul(int x, int y)
{
	return x * y;
}
int Div(int x, int y)
{
	return x / y;
}

enum Sel
{
	add,sub,mul,divi
};
int main()
{
	using ptr = int (*)(int, int);

	//函数指针数组
	int (*arr[4])(int, int) = {Add,Sub,Mul,Div};
	vector<ptr> vec(begin(arr),end(arr));
	
	int ret1 = vec[add](3, 5);
	cout << ret1 << endl;

	int ret2 = vec[sub](3, 5);
	cout << ret2 << endl;

	int ret3 = vec[mul](3, 5);
	cout << ret3 << endl;

	int ret4 = vec[divi](3, 5);
	cout << ret4 << endl;
	return 0;
}







//练习 6.51
//void f()
//{
//	cout << "f()" << endl;
//}
//void f(int)
//{
//	cout << "f(int)" << endl;
//}
//void f(int, int)
//{
//	cout << "f(int,int)" << endl;
//}
//void f(double, double)
//{
//	cout << "f(double,double)" << endl;
//}
//int main()
//{
//	// f(2.56, 42); //a
//	f(42);		//b
//	f(42, 0);	//c
//	f(2.56, 3.14);	//d
//
//	return 0;
//}

//constexpr函数
//constexpr int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int sum = 0;
//	for (int i = 1; i <= 100; i+=2)
//	{
//		sum += Add(i, i + 1);
//		cout << sum << endl;
//	}
//	return 0;
//}


//练习 6.44
//inline bool isshorter(const string& s1, const string& s2)
//{
//	return s1.size() <= s2.size();
//}
//
//int main()
//{
//	bool ret = isshorter("hello", "nice");
//	cout << ret << endl;
//	return 0;
//}



//不要把函数声明写在局部域中
//#include <stdio.h>
//
//int main()
//{
//	//int printf = 0;
//	void printf();
//
//	printf("%d", 0);
//	return 0;
//}




//const_cast
//string& shortstring(const string& s1, const string& s2)	//底层const
//{
//	const string& t = s1.size() <= s2.size() ? s1 : s2;
//	return const_cast<string&>(t);	//起到强制类型转换的作用
//}
//
//int main()
//{
//	string s1 = "hello";
//	string s2 = "nice";
//	shortstring(s1, s2);
//	return 0;
//}


//练习 6.38
//int odd[] = { 1,3,5,7,9 };
//int even[] = { 0,2,4,6,8 };
//
////int (&arrPtr(int i))[end(odd) - begin(odd)]
//
//using ret = int[end(odd) - begin(odd)];
////ret& arrPtr(int i)
//
////auto arrPtr(int i) -> ret(&)
////auto arrPtr(int i) -> int (&) [5]
//
//
//decltype(odd)& arrPtr(int i)
//{
//	return (i % 2 == 0) ? odd : even;
//}
//
//int main()
//{
//	int(&ret)[end(odd) - begin(odd)] = arrPtr(2);
//	for (auto e : ret)
//		cout << e << " ";
//	cout << endl;
//	return 0;
//}



//练习 6.37
////typedef string str[10];		//将 string[10] -> str
//using str = string[10];
//
////str& func()
////auto func() -> str(&)
////auto func() -> string (&)[10]		//这三个效果最好
//
//string s[10];
//decltype(s)& func()		//效果最差
//{
//	static string str[10];
//	for (auto e : str)
//		cout << e << " ";
//	cout << endl;
//		return str;
//}
//int main()
//{
//	string (&ret)[10] = func();
//
//	for (int i = 0; i < 10; i++)
//		ret[i] += "hello";
//	func();
//	return 0;
//}



//练习 6.36
//string(&func())[10]
//{
//	static string str[10];
//
//	for (auto e : str)
//		cout << e << " ";
//	cout << endl;
//	return str;
//}
//int main()
//{
//	string (&ret)[10] = func();
//	for (int i = 0; i < 10; i++)
//		ret[i] += "hello";
//	func();
//	return 0;
//}

//练习 6.33
//void show_vector(vector<int>& vec,int i)
//{   
//	if (i < 0) return;
//	show_vector(vec, i - 1);
//	cout << vec[i] << " ";
//}
//int main()
//{
//	vector<int> vec = { 1,2,3,4,5,6,7,8,9,10 };
//	show_vector(vec,vec.size()-1);
//	return 0;
//}


//练习 6.30
//bool str_subrange(const string& str1, const string& str2)
//{
//	if (str1.size() == str2.size())
//		return str1 == str2;
//	auto size = str1.size() < str2.size() ? str1.size() : str2.size();
//	for (int i = 0; i != size; ++i)
//	{
//		if (str1[i] != str2[i]);
//			//return false;
//	}
//}
//
//int main()
//{
//	string s1 = "hello world";
//	string s2 = "i like coding";
//	bool ret = str_subrange(s1, s2);
//	cout << ret << endl;
//	return 0;
//}


//练习 6.27
//int sum_of_elem(initializer_list<int> il)
//{
//	int sum = 0;
//	for (auto& e : il)
//		sum += e;
//	return sum;
//}
//int main()
//{
//	initializer_list<int> il{ 1,2,3,4,5,6,7,8,9,10 };
//	int ret = sum_of_elem(il);
//	cout << ret << endl;
//	return 0;
//}

//练习6.2
//int main(int argc,char** argv)
//{
//	string s;
//	for (int i = 1; i < argc; i++)
//	{
//		s += argv[i];
//	}
//	cout << s << endl;
//	return 0;
//}

//练习 6.23
//void print(int i, int* j,int size)	//显示传递一个表示数组大小的形参
//{
//	cout << i << endl;
//	for (int i = 0; i < size; i++)
//		cout << j[i] << " ";
//	cout << endl;
//}
//
//void print(int i, int j[2])	//指定数组的大小
//{
//	cout << i << endl;
//	for (int i = 0; i < 2; i++)
//		cout << j[i] << " ";
//	cout << endl;
//}
//
//void print(int i,int* begin,int* end)	//使用标准库
//{
//	cout << i << endl;
//	while (begin != end)
//	{
//		cout << *begin << " ";
//		++begin;
//	}
//	cout << endl;
//}
//
//int main()
//{
//	int i = 0, j[2] = { 0,1 };
//	print(i, j, 2);
//	print(i, j);
//	print(i, begin(j),end(j));
//
//	return 0;
//}

//练习 6.22
//void Swap(int** p1, int** p2)	//1.使用二级指针
//{
//	int* tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//void Swap(int* &p1, int* &p2)	//2.使用引用
//{
//	int* tmp = p1;
//	p1 = p2;
//	p2 = tmp;
//}
//int main()
//{
//	int x = 3, y = 6;
//	int* px = &x, * py = &y;
//	Swap(&px, &py);
//	//Swap(px, py);
//	cout << *px << ":" << *py << endl;
//	return 0;
//}



//练习 6.21
//int Max(int x,int* p)
//{
//	return x > (*p) ? x : *p;
//}
//int main()
//{
//	int n = 3;
//	int ret = Max(5, &n);
//	cout << ret << endl;
//	return 0;
//}


//练习6.18
//class matrix
//{};
//
//bool compare(const matrix&, const matrix& );
//vector<int>::iterator change_val(int, vector<int>::iterator);
//int main()
//{
//	return 0;
//}


//练习 6.17
//bool Ispower(const string& s)
//{
//	for (auto e : s)
//	{
//		if (e >= 'A' && e <= 'Z') return true;
//	}
//	return false;
//}
//
//void Tolower(string& s)
//{
//	for (auto& e : s)
//	{
//		if (isupper(e)) e += 32;
//	}
//}
//int main()
//{
//	string s1 = "hello worLd!";
//	if (Ispower(s1))
//	{
//		cout << "yes" << endl;
//		Tolower(s1);
//		cout << s1 << endl;
//	}
//	return 0;
//}

//练习 6.13
//用一个返回值为void类型的函数，实现加法运算
//int func(int& x, int& y, int& sum)		//引用是C++的特性
//{
//	sum = x * y;
//	return x + y;
//}
//int main()
//{
//	int x = 3, y = 4;
//	int sum = 0;
//	int ret = func(x, y, sum);	//引用是
//	cout << ret << endl;
//
//	cout << sum << endl;
//	return 0;
//}



//练习 6.12
//void Swap(int& p1, int& p2)
//{
//	int tmp = p1;
//	p1 = p2;
//	p2 = tmp;
//}
//int main()
//{
//	int x = 3, y = 5;
//	Swap(x, y);
//	cout << x << ":" << y << endl;		//针对自定义类型
//	return 0;
//}


//练习 6.11
//void reset(int& n)	//函数三要素 返回值 函数名 参数列表
//					//代码必须放在代码块里面
//{
//	n = 20;
//}
//int main()
//{	
//	int num = 0;
//	reset(num);
//	if(num)
//		cout << num << endl;
//	return 0;
//}





//练习 6.10
//void Swap(int* p1, int* p2)	//形参拥有了指针的值(指向了同一个对象)
//{							//但是，形参和实参是不同的指针变量(相互独立)
//	int tmp = *p1;			//形参的改变，不影响实参(针对变量本身而言)
//	*p1 = *p2;
//	*p2 = tmp;
//}
//int main()
//{
//	int x = 3, y = 5;
//	int* p1 = &x, * p2 = &y;
//	Swap(p1, p2);
//	cout << x << ":" << y << endl;
//	return 0;
//}


//#include "fib.h"//拿到了函数的声明，也就是告诉编译器有这个函数，具体在哪编译器自己找
//
//int main()
//{
//	int ret = fib(50);	//编译器要找函数定义了(链接)
//	cout << ret << endl;
//	return 0;
//}

//练习 6.7
//int test2()
//{
//	static int count;		//计数器
//	++count;
//	return count;
//}
//
//int main()
//{
//	//统计函数被调用的次数
//	srand((unsigned int)time(NULL));
//	int n = rand();
//	cout << n << endl;
//	int ret = 0;
//	for (int i = 0; i < n; i++)
//	{
//		ret = test2();
//	}
//	cout << ret << endl;
//	return 0;
//}



//练习 6.6
//int  test1(int val)		//形参
//{
//	static int ret;		//静态局部变量
//	ret += val;
//	return ret;
//}
//int main()
//{
//	//求 1~100 的和
//	int sum = 0;		//局部变量
//	for (int i = 1; i <= 100; i++)
//	{
//		sum = test1(i);
//	}
//	cout << sum << endl;
//	return 0;
//}