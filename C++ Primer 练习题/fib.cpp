#define _CRT_SECURE_NO_WARNINGS 1

#include "fib.h"
//斐波那契数列递归调用了多少次函数
int fib(int n)
{
	static int count;
	++count;
	if (n <= 1) return count;
	fib(n - 1);
	fib(n - 2);
}