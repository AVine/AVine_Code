#define _CRT_SECURE_NO_WARNINGS 1

#include <string>
#include <iostream>
using namespace std;


// 练习15.3/15.5/15.6/15.7
class Quote
{
public:
	Quote(const string& isbn = "", const double& price = 0)
		:_isbn(isbn),_price(price)
	{}

	virtual ~Quote()	//基类应该定义一个虚析构函数
	{}

	string isbn()
	{
		return _isbn;
	}

	virtual double net_price(size_t n)	//对于返回销售价的函数来说，我们希望派生类有自己的定义
	{
		return n * _price;
	}

private:
	string _isbn;
protected:
	double _price;	//单价
};

class Bulk_quote : public Quote
{
public:
	Bulk_quote(const string& isbn = "", const double& price = 0,
			   const size_t minCnt = 0,const double& disCount = 1)
		:Quote(isbn,price),_minCnt(minCnt),_disCount(disCount)
	{}

	double net_price(size_t n)
	{
		if (n >= _minCnt)
		{
			return n * _price * _disCount;
		}
		else
		{
			return n * _price;
		}
	}

protected:
	size_t _minCnt;
	double _disCount;

};

class mytest : public Quote
{
public:
	mytest(const string& isbn = "", const double& price = 0,
		const size_t maxCnt = 0, const double& disCount = 1)
		:Quote(isbn, price), _maxCnt(maxCnt), _disCount(disCount)
	{}

	double net_price(size_t n)
	{
		if (n <= _maxCnt)
		{
			return n * _price * _disCount;
		}
		else
		{
			return _maxCnt * _price * _disCount + (n - _maxCnt) * _price;
		}
	}
protected:
	size_t _maxCnt;
	double _disCount;
};

void print_total(Quote& q,size_t n)
{
	string ISBN = q.isbn();
	double ret = q.net_price(n);
	cout << "购买" << n << "本ISBN:" << ISBN << "花费" << ret << "元" << endl;
}

int main()
{
	Quote q("20110244",20);
	Bulk_quote bq("20110244", 20, 10, 0.8);
	mytest m("20110244", 20, 10, 0.8);

	print_total(q, 10);
	print_total(bq, 10);
	print_total(m, 11);


	Quote q1(bq);

	return 0;
}