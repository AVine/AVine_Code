#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
#include <vector>
using namespace std;


int a = 0;
int main()
{
	int a = 1;
	{
		int a = 2;
		cout << ::a << endl;
	}
	return 0;
}


//使用友元类 将学生信息用vector存储
//class Student;
//
//class Storage
//{
//public:
//	void push(const Student& s)
//	{
//		_vec.push_back(s);
//	}
//	
//	friend void display(const Storage& s);
//private:
//	vector<Student> _vec;
//};
//
//
//class Student
//{
//public:
//	Student(string name,size_t age)
//		:_name(name),_age(age)
//	{}
//
//	friend class Storage;
//
//	friend void print(const Student& s);
//private:
//	string _name;
//	size_t _age = 0;
//};
//
//void print(const Student& s)
//{
//	cout << "Name:" << s._name;
//	cout << " Age:" << s._age << endl;
//}
//
//void display(const Storage& s)
//{
//	for (auto& e : s._vec)
//	{
//		print(e);
//	}
//}
//
//int main()
//{
//	Storage st;
//	Student s1("ly", 21);
//	st.push(s1);
//
//	Student s2("lym", 22);
//	st.push(s2);
//
//	Student s3("zxy", 19);
//	st.push(s3);
//
//	display(st);
//	return 0;
//}





//typedef double Money;
//string bal;
//class Account
//{
//public:
//	Money balance()
//	{
//		return bal;
//	}
//private:
//	typedef double Money;
//	Money bal;
//};
//
//int main()
//{
//	return 0;
//}


//练习 7.32
//class Screen;
//
//class Window_mgr
//{
//public:
//	void clear(size_t pos);	//Screen的友元，如果定义在此类里，那么就屏蔽了外部作用域
//
//private:
//	//vector<Screen> _screens{Screen(24,80,' ')};	//不完全类型只能定义指针或引用
//	vector<Screen> _screens;
//
//};
//
//class Screen
//{
//public:
//	using pos = size_t;
//
//	friend void Window_mgr::clear(size_t pos);
//
//	Screen() = default;		//合成的默认构造函数
//
//	Screen(pos width, pos height)
//		:_width(width), _height(height), _content(width* height, ' ')
//	{
//		++_cnt;
//	}
//
//	Screen(pos width, pos height, const char& c)
//		:_width(width), _height(height), _content(width* height, c)
//	{
//		++_cnt;
//
//	}
//
//	Screen& move(const pos& wdes, const pos& hdes)
//	{
//		_cur = wdes * _width + hdes;
//		++_cnt;
//		return *this;
//	}
//
//	Screen& set(const char& c)
//	{
//		_content[_cur] = c;
//		++_cnt;
//
//		return *this;
//	}
//
//	Screen& display(ostream& out)
//	{
//		do_display(out);
//		++_cnt;
//
//		return *this;
//	}
//
//	const Screen& display(ostream& out) const
//	{
//		do_display(out);
//		++_cnt;
//
//		return *this;
//	}
//	
//private:
//	void do_display(ostream& out) const
//	{
//		out << _content << endl;
//	}
//
//	mutable pos _cnt = 0;		//可变数据成员(不受常量对象限制)
//	pos _cur = 0;
//	pos _width = 0, _height = 0;
//	string _content;
//};
//
//void Window_mgr::clear(size_t pos)
//{
//	Screen& s = _screens[pos];
//	s._content = string(s._width*s._height,'0');
//	
//}
//
//int main()
//{
//	Window_mgr w;
//	
//	Screen s0(5, 5, 'A');
//	s0.display(cout);
//
//	Screen s1(3, 3, 'B');
//	s1.display(cout);
//
//	Screen s2(4, 4, 'C');
//	s2.display(cout);
//
//	return 0;
//}




//struct X	//声明友元的类
//{
//	friend void f()
//	{
//		cout << "hello world" << endl;
//	}
//
//	X()		//声明友元的类 的成员函数
//	{
//		f();		//调用友元函数，这个函数也必须被声明		
//	}
//};




//练习 7.31
//class Y;	//声明而不定义类
//class X
//{
//	Y* py;		//不完全类型可以定义指针或引用
//};
//
//class Y
//{
//	X x;
//};
//
//int main()
//{
//	X x1;
//	Y y1;
//	return 0;
//}




//练习 7.23
//class Screen
//{
//public:
//	using pos = size_t;
//
//	Screen() = default;		//合成的默认构造函数
//
//	Screen(pos width, pos height)
//		:_width(width), _height(height), _content(width* height, ' ')
//	{
//		++_cnt;
//	}
//
//	Screen(pos width, pos height, const char& c)
//		:_width(width), _height(height), _content(width* height, c)
//	{
//		++_cnt;
//
//	}
//
//	Screen& move(const pos& wdes, const pos& hdes)
//	{
//		_cur = wdes * _width + hdes;
//		++_cnt;
//		return *this;
//	}
//
//	Screen& set(const char& c)
//	{
//		_content[_cur] = c;
//		++_cnt;
//
//		return *this;
//	}
//
//	Screen& display(ostream& out)
//	{
//		do_display(out);
//		++_cnt;
//
//		return *this;
//	}
//
//	const Screen& display(ostream& out) const
//	{
//		do_display(out);
//		++_cnt;
//
//		return *this;
//	}
//	
//private:
//	void do_display(ostream& out) const
//	{
//		out << _content << endl;
//	}
//
//	mutable pos _cnt = 0;		//可变数据成员(不受常量对象限制)
//	pos _cur = 0;
//	pos _width = 0, _height = 0;
//	string _content;
//};
//
//
//int main()
//{
//	Screen myScreen(5, 5, 'X');
//	myScreen.move(4, 0).set('#').display(cout);
//
//	myScreen.display(cout);
//
//	const Screen s(10, 10, 'A');
//	s.display(cout);
//	return 0;
//}


//练习 7.11/7.12/7.13/7.14/7.21
//extern struct Sales_data;
//
//istream& read(istream& in, Sales_data& s);

//class Sales_data
//{
//public:
//	//Sales_data() = default;		//合成的默认构造函数
//
//	Sales_data(istream& in)
//	{
//		read(in, *this);
//	}
//
//	Sales_data(const string& s)
//		:bookNo(s)
//	{}
//	
//	Sales_data(const string& s,size_t n,double p)
//		:bookNo(s),units_sold(n),revenue(p*n)	//初始化顺序与成员声明有关；与初始化列表无关
//	{}
//
//	Sales_data()
//		:bookNo(""),units_sold(0),revenue(0)
//	{}
//
//	friend istream& read(istream& in, Sales_data& s);
//
//	friend ostream& print(ostream& out, const Sales_data& s);
//
//	Sales_data& combine(const Sales_data& ans)
//	{
//		units_sold += ans.units_sold;
//		revenue += ans.revenue;
//		return *this;
//	}
//
//	string isbn() const
//	{
//		return bookNo;
//	}
//
//
//private:	//隐藏实现细节
//	string bookNo;		//ISBN编号
//	size_t units_sold = 0;		//销量
//	double revenue = 0;		//总收入
//};
//
//istream& read(istream& in, Sales_data& s)
//{
//	double price = 0;
//	in >> s.bookNo >> price;
//	s.units_sold=1;
//	s.revenue = price * s.units_sold;
//	return in;
//}
//
//ostream& print(ostream& out, const Sales_data& s)
//{
//	out << s.bookNo << "销售数:" << s.units_sold;
//	out << "	销售额:" << s.revenue << endl;
//	return out;
//}
//
//Sales_data add(const Sales_data& s1,const Sales_data& s2)
//{
//	Sales_data sum = s1;
//	sum.combine(s2);
//	return sum;
//}
//
//int main()
//{
//	Sales_data total(cin);
//	Sales_data trans;
//	while (read(cin, trans))
//	{
//		if (trans.isbn() == total.isbn())
//		{
//			total.combine(trans);
//		}
//		else
//		{
//			print(cout, total);
//			total = trans;
//		}
//	}
//	return 0;
//}


//int main()
//{
//	Sales_data s1;
//	print(cout, s1);
//
//	Sales_data s2(cin);
//	print(cout, s2);
//
//	Sales_data s3("1");
//	print(cout, s3);
//
//	Sales_data s4("2", 2, 20);
//	print(cout, s4);
//
//	return 0;
//}






//练习 7.6/7.7
//struct Sales_data
//{
//	string bookNo;		//ISBN编号
//	size_t units_sold = 0;		//销量
//	double revenue = 0;		//总收入
//
//	Sales_data& combine(const Sales_data& ans)
//	{
//		units_sold += ans.units_sold;
//		revenue += ans.revenue;
//		return *this;
//	}
//
//	string isbn() const
//	{
//		return bookNo;
//	}
//};
//
//istream& read(istream& in, Sales_data& s)
//{
//	double price = 0;
//	in >> s.bookNo >> price;
//	s.units_sold=1;
//	s.revenue = price * s.units_sold;
//	return in;
//}
//
//ostream& print(ostream& out, const Sales_data& s)
//{
//	out << s.bookNo << "销售数:" << s.units_sold;
//	out << "	销售额:" << s.revenue << endl;
//	return out;
//}
//
//Sales_data add(const Sales_data& s1,const Sales_data& s2)
//{
//	Sales_data sum = s1;
//	sum.combine(s2);
//	return sum;
//}
//
//
//int main()
//{
//	Sales_data total;
//
//	if (read(cin,total))	//输入ISBN和单价
//	{
//
//		Sales_data trans;
//	
//		while (read(cin,trans))
//		{
//
//			if (total.isbn() == trans.isbn())
//			{
//				total.combine(trans);
//			}
//			else
//			{
//				print(cout,total);
//				total = trans;
//			}
//		}
//		print(cout, trans);
//	}
//	else
//	{
//		cout << "没有输入!" << endl;
//		return 1;
//	}
//	return 0;
//}




//练习 7.4/7.5/7.8/7.15
//extern struct Person;
//
//istream& read(istream& in, Person& p);

//class Person
//{
//public:
//	//Person() = default;
//
//	Person(istream& in)
//	{
//		read(in, *this);
//	}
//
//	Person(const char* name = "ly", const char* address = "湖南")
//		:_name(name),_address(address)
//	{}
//
//	friend istream& read(istream& in, Person& p);
//
//	friend ostream& print(ostream& out, Person& p);
//
//	string name() const
//	{
//		return _name;
//	}
//	string address() const
//	{
//		return _address;
//	}
//
//private:
//	string _name;
//	string _address;
//
//};
//
//istream& read(istream& in, Person& p)
//{
//	in >> p._name >> p._address;
//	return in;
//}
//
//ostream& print(ostream& out, Person& p)
//{
//	out << p._name << " " << p._address << endl;
//	return out;
//}
//int main()
//{
//	Person p1;
//	print(cout, p1);
//
//	Person p2(cin);
//	print(cout, p2);
//
//	return 0;
//}


//int main()
//{
//	Person p;
//	read(cin, p);
//	print(cout, p);
//	return 0;
//}







//练习 7.1/7.2/7.3
//struct Sales_data
//{
//	string bookNo;		//ISBN编号
//	size_t units_sold = 0;		//销量
//	double revenue = 0;		//总收入
//
//	Sales_data& combine(Sales_data& ans)
//	{
//		units_sold += ans.units_sold;
//		revenue += ans.revenue;
//		return *this;
//	}
//
//	string isbn() const
//	{
//		return bookNo;
//	}
//};
//
//int main()
//{
//	Sales_data total;
//	size_t price;
//
//	if (cin >> total.bookNo >> price)	//输入ISBN和单价
//	{
//		++total.units_sold;
//		total.revenue += price;
//
//		Sales_data trans;
//		
//		while (cin >> trans.bookNo >> price)
//		{
//			trans.revenue = price;
//			trans.units_sold = 1;
//
//			if (total.isbn() == trans.isbn())
//			{
//				total.combine(trans);
//			}
//			else
//			{
//				cout << total.bookNo << "销售数:" << total.units_sold << "	销售额:" << total.revenue << endl;
//
//				total.bookNo = trans.bookNo;
//				total.units_sold = trans.units_sold;
//				total.revenue = trans.revenue;
//			}
//		}
//
//		cout << trans.bookNo << "销售数:" << trans.units_sold << "	销售额:" << trans.revenue << endl;
//
//	}
//	else
//	{
//		cout << "没有输入!" << endl;
//		return 1;
//	}
//	return 0;
//}