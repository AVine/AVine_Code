#define _CRT_SECURE_NO_WARNINGS 1


#include <string>
#include <iostream>
using namespace std;

class X
{
public:
};

class Y
{
public:
	X _x;	//在Y类中定义X类的对象
};

//class A
//{
//public:
//	int _a = 1;
//};
//
//class B :  virtual public A
//{
//public:
//	int _b = 2;
//};
//
//class C : virtual public A
//{
//public:
//	int _c = 3;
//};
//
//class D : public B,public C
//{
//public:
//	int _d = 4;
//};
//
//int main()
//{
//	D d;
//
//	B b;
//	C c;
//
//	return 0;
//}

//class Person
//{
//public:
//	static int _count;
//};
//
//int Person::_count = 0;
//
//class Student : public Person
//{
//public:
//	int _num;
//};
//
//int main()
//{
//	// 静态成员在整个继承体系中只有一个
//	Person::_count = 3;
//	cout << Student::_count << endl;
//	return 0;
//}

//class Student;
//class Person
//{
//public:
//	//基类的友元
//	friend void Display(const Person& p, const Student& s);
//
//protected:
//	string _name; // 姓名
//};
//
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._stuNum << endl;//友元关系不能被继承，所以不能访问派生类的protected/private成员
//}



//class Person	
//{
//public:
//	Person(const string& name = "")
//		:_name(name)
//	{}
//
//	Person(const Person& p)
//	{
//		_name = p._name;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		_name = p._name;
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "Person::~Person()" << endl;
//	}
//protected:
//	string _name;
//};
//
//class Student : public Person		
//{
//public:
//	Student(const string& name = "",int age = 0)
//		:Person(name),_age(age)		//基类成员通过调用基类的构造函数完成初始化
//	{}
//
//	Student(const Student& s)
//		:Person(s)		//基类的引用仅能看到派生类对象的基类部分
//	{
//		_age = s._age;
//	}
//
//	Student& operator=(const Student& s)
//	{
//		Person::operator=(s);	
//		_age = s._age;
//		return *this;
//	}
//
//	~Student()
//	{
//		//只需要清理派生类的成员即可
//		cout << "Student::~Student" << endl;
//	}//派生类的析构函数调用结束后会自动调用基类的析构函数
//	
//protected:
//	int _age = 0;
//};
//
//int main()
//{
//	return 0;
//}



// 基类的fun函数和派生类的fun函数构成隐藏关系
//class A
//{
//public:
//	void fun()
//	{
//		cout << "func()" << endl;
//	}
//};
//class B : public A
//{
//public:
//	void fun(int i)
//	{
//		cout << "func(int i)->" << i << endl;
//	}
//};
//
//int main()
//{
//	B b;
//	b.fun(1);	//直接访问的是派生类的fun函数
//	b.A::fun();
//	
//	return 0;
//}

//// 基类的_num和派生类的_num构成隐藏关系
//class Person
//{
//protected:
//	string _name = "小李子"; // 姓名
//	int _num = 111; // 身份证号
//};
//class Student : public Person
//{
//public:
//	void Print()
//	{
//		cout << " 姓名:" << _name << endl;
//		cout << " 身份证号:" << Person::_num << endl;
//		cout << " 学号:" << _num << endl;	//直接访问的是派生类的_num
//	}
//protected:
//	int _num = 999; // 学号
//};
//void Test()
//{
//	Student s1;
//	s1.Print();
//};



//class Person	//此类作为基类
//{
//public:
//	void print()
//	{
//		cout << "name:" << _name << endl;
//	}
//protected:
//	string _name;
//};
//
//class Student : public Person		//Student类由Person类继承而来
//{
//public:
//	void print()
//	{
//		cout << "name:" << _name << endl;
//		cout << "age:" << _age << endl;
//	}
//protected:
//	int _age = 0;
//};
//
//int main()
//{
//	Student s;
//	Person p = s;	//拷贝构造
//
//	Person p2;
//	p2 = s;		//赋值运算符重载
//
//	Person* ptr = &s;
//	Person& rp = s;
//
//	Student* pptr = &p2;	//错误，基类对象不能赋值给派生类对象
//
//	return 0;
//}



//class Person
//{
//public:
//	Person(const string& name="",const string& id="",const string& addr="")
//		:_name(name),_id(id),_addr(addr)
//	{}
//
//	void print()
//	{
//		cout << "name:" << _name << " ";
//		cout << "id:" << _id << " ";
//		cout << "addr:" << _addr << endl;;
//	}
//
//	string _name;
//	string _id;
//	string _addr;
//};
//
//class Teacher : private Person
//{
//public:
//	Teacher(const string& name = "", const string& id = "", const string& addr = "",const string& sub="")
//		:Person(name,id,addr),_sub(sub)
//	{}
//
//	void print()
//	{
//		cout << "name:" << _name << " " ;
//		cout << "id:" << _id << " ";
//		cout << "addr:" << _addr << " ";
//		cout << "sub:" << _sub << endl;
//	}
//
//	void print2(Teacher t)
//	{
//		cout << t._addr << endl;
//	}
//	string  _sub;
//};
//
//int main()
//{
//	Person p1("张三", "2011", "湖南");
//
//	Teacher t1("李四", "2022", "湖北","物理");
//	//t1._name = "小明";
//	t1.print();
//
//	
//	return 0;
//}