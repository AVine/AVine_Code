#define _CRT_SECURE_NO_WARNINGS 1

#pragma once

#include <string.h>
#include <assert.h>
#include "reverse_iterator.h"

namespace ly
{
	template <class T>
	class vector
	{
	public:

		typedef T* iterator;
		typedef const T* const_iterator;

		typedef ReverseIterator<iterator, T&, T*> reverse_iterator;
		typedef ReverseIterator<const_iterator, const T&, const T*> const_reverse_iterator;


		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		const_reverse_iterator rbegin() const
		{
			return const_reverse_iterator(end());
		}

		const_reverse_iterator rend() const
		{
			return const_reverse_iterator(begin());
		}

		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		const_iterator begin() const
		{
			return _start;
		}

		const_iterator end() const
		{
			return _finish;
		}




		vector()
			:_start(nullptr),_finish(nullptr),_end_of_storage(nullptr)
		{}


		vector(size_t n, const T& val = T())
			:_start(nullptr), _finish(nullptr), _end_of_storage(nullptr)
		{
			resize(n, val);
		}

		vector(int n, const T& val = T())
			:_start(nullptr), _finish(nullptr), _end_of_storage(nullptr)
		{
			resize(n, val);
		}


		~vector()	//析构函数
		{
			delete[] _start;
			_start = _finish = _end_of_storage = nullptr;
		}

		template <class InputIterator>
		vector(InputIterator first, InputIterator last)
			:_start(nullptr), _finish(nullptr), _end_of_storage(nullptr)
		{
			reserve(last - first);

			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}


		/*传统写法(拷贝构造)*/
		//vector(const vector<T>& v)
		//{
		//	reserve(v.capacity());
		//	for (auto& e : v)
		//	{
		//		push_back(e);
		//	}
		//}


		/*现代写法(拷贝构造)*/
		vector(const vector<T>& v)
			:_start(nullptr),_finish(nullptr),_end_of_storage(nullptr)
		{
			vector<T> tmp(v.begin(), v.end());
			swap(tmp);
		}

		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_end_of_storage, v._end_of_storage);
		}


		//允许自己给自己赋值 —— 没有语法错误
		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}



		size_t capacity() const
		{
			return _end_of_storage - _start;
		}

		size_t size() const
		{
			return _finish - _start;
		}


		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t oldSize = size();	//记录

				T* tmp = new T[n];

				if (_start != nullptr)
				{
					//memcpy(tmp, _start, sizeof(T) * oldSize);	//浅拷贝

					for (size_t i = 0; i < oldSize; i++)
					{
						tmp[i] = _start[i];
					}

					delete[] _start;
				}

				_start = tmp;
				_finish = _start + oldSize;
				_end_of_storage = _start + n;
			}
		}

		void resize(size_t n,const T& val = T())
		{
			if (n > capacity())
			{
				reserve(n);
				while (_finish < _start + n)
				{
					*_finish = val;
					++_finish;
				}
			}

			else if (n >= size() && n <= capacity())
			{
				while (_finish < _start + n)
				{
					*_finish = val;
					++_finish;
				}
			}

			else if (n < size())
			{
				_finish = _start + n;
			}
		}


		void push_back(const T& val)
		{
			//判断是否需要扩容
			if (_finish == _end_of_storage)
			{
				//扩容

				size_t newCapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newCapacity);
			}

			*_finish = val;
			++_finish;
		}

		void pop_back()
		{
			assert(!empty());
			--_finish;
		}


		T& operator[](size_t pos)
		{
			return *(_start + pos);
		}

		const T& operator[](size_t pos) const
		{
			return *(_start + pos);
		}


		bool empty() const
		{
			return _finish == _start;
		}


		void clear()
		{
			resize(0);
		}

		iterator insert(iterator pos, const T& val)
		{
			assert(pos <= _finish && pos >= _start);
			//判断是否需要扩容

			if (_finish == _end_of_storage)
			{
				size_t oldPos = pos - _start;

				size_t newCapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newCapacity);

				pos = _start + oldPos;
			}

			auto end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				--end;
			}

			*pos = val;
			++_finish;

			return pos;
		}


		iterator erase(iterator pos)
		{
			assert(!empty());

			assert(pos < _finish);

			auto del = pos + 1;
			while (del < _finish)
			{
				*(del - 1) = *del;
				++del;
			}

			--_finish;

			return pos;
			
		}

	private:
		iterator _start;
		iterator _finish;
		iterator _end_of_storage;
		
	};
}