#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
#include <algorithm>
#include <vector>
#include "vector.h"
using namespace ly;
using std::cout;
using std::endl;


void test1()
{
	vector<int> vec;
	vec.push_back(1);
	vec.push_back(2);
	vec.push_back(3);
	vec.push_back(4);
	auto it = std::find(vec.begin(), vec.end(), 2);

	it = vec.insert(it, 20);		//���ݻᵼ�µ�����ʧЧ(�������)
	cout << *it << endl;

	//vec.pop_back();
	//vec.pop_back();

	for (auto& e : vec)
		cout << e << " ";
	cout << endl;

	//cout << vec.size() << endl;
	//vec.resize(10);
	//cout << vec.size() << endl;

	//for (int i = 0; i < vec.size(); i++)
	//{
	//	cout << vec[i];
	//}
	//cout << endl;
}



void test2()
{
	std::vector<int> vec;
	vec.push_back(1);
	vec.push_back(2);
	vec.push_back(3);
	vec.push_back(4);
	vec.push_back(5);

	auto it = vec.begin();
	while (it != vec.end())
	{
		if (*it % 2 == 0)
		{
			it = vec.erase(it);
		}
		else
		{
			++it;
		}
	}

	//auto it = std::find(vec.begin(), vec.end(), 3);
	//it = vec.erase(it);

	////cout << *it << endl;
	////*it = 50;

	for (auto& e : vec)
		cout << e << " ";
	cout << endl;

}


void test3()
{
	vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);


	vector<int> v2(v1.begin(),v1.end());
	for (auto& e : v2)
		cout << e << " ";
	cout << endl;

	vector<int> v3;
	v3 = v2;
	for (auto& e : v2)
		cout << e << " ";
	cout << endl;
}


void test4()
{
	vector<vector<int>> vec;
	vector<int> v(5, 1);

	vec.push_back(v);
	vec.push_back(v);
	vec.push_back(v);
	vec.push_back(v);
	vec.push_back(v);

	cout << vec.capacity() << endl;
	
	//cout << vec[0].size() << endl;

	/*for (size_t i = 0; i < vec.size(); i++)
	{
		vec[i].resize(5,0);
	}*/


	for (size_t i = 0; i < vec.size(); i++)
	{
		for (size_t j = 0; j < vec[i].size(); j++)
		{
			cout << vec[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;

}

void test5()
{
	int arr[] = { 1,2,3,4,5,6,7 };
	vector<int> vec1(std::begin(arr), std::end(arr));

	auto it1 = vec1.begin();
	while (it1 != vec1.end())
	{
		cout << *it1 << " ";
		++it1;
	}
	cout << endl;

	auto rit1 = vec1.rbegin();
	while (rit1 != vec1.rend())
	{
		cout << *rit1 << " ";
		++rit1;
	}
	cout << endl;

	const vector<int> vec2(std::begin(arr), std::end(arr));

	auto it2 = vec2.begin();
	while (it2 != vec2.end())
	{
		cout << *it2 << " ";
		++it2;
	}
	cout << endl;

	auto rit2 = vec2.rbegin();
	while (rit2 != vec2.rend())
	{
		cout << *rit2 << " ";
		++rit2;
	}
	cout << endl;
}

int main()
{
	test5();
	return 0;
}