#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
int AddFromTo(int from,int to)
{
    int ret = 0;
    for(int i=from;i<to;i++)
    {
        ret+=i;
    }
    return ret;
}

int main()
{
    pid_t id = fork();
    if(id < 0)
    {
        printf("创建子进程失败\n");
        exit(1);
    }
    else if(id == 0)
    {
        int cnt = 5;
        while(cnt)      //父子进程共同等待5秒，然后子进程退出
        {
            printf("我是子进程,pid:%d,ppid:%d,cnt:%d\n",getpid(),getppid(),cnt--);
            sleep(1);
        }
        exit(12);
    }
    else 
    {
        sleep(7);      //子进程退出后，父进程再等待5秒
       // pid_t ret = wait(NULL);     //等待期间能看到子进程是僵尸状态，5秒之后回收子进程
        int status = 0;
        pid_t ret = waitpid(id,&status,0);
        if(id > 0)
        {
            printf("wait succese:%d,sig numbers:%d,child exit code:%d\n",ret,(status & 0x7F), (status>>8)&0xFF);
        }
        sleep(5);
        exit(0);
    }


//   int ret= AddFromTo(1,100);
//   printf("ret = %d",ret);
//   if(ret == 5050)_exit(0);
//   _exit(1);

}
