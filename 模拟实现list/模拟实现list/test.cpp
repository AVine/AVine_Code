#define _CRT_SECURE_NO_WARNINGS 1

#include "list.h"
#include <iostream>
#include <algorithm>
#include <list>
using std::cout;
using std::endl;

using namespace ly;

void test1()
{
	list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);

	
	lt.pop_back();
	//auto it = lt.find(lt.begin(), lt.end(),3);

	//lt.insert(it, 30);
	//
	//auto it2 = lt.find(lt.begin(), lt.end(), 5);

	//lt.erase(it2);


	auto begin = lt.begin();
	while (begin != lt.end())
	{
		cout << *begin << " ";
		++begin;
	}
	cout << endl;
}


void test2()
{
	list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);

	list<int> lt2(lt);

	list<int> lt3;
	lt3 = lt2;

	auto begin = lt3.begin();
	while (begin != lt3.end())
	{
		cout << *begin << " ";
		++begin;
	}
	cout << endl;
}



//class test
//{
//public:
//	test()
//	{}
//private:
//	int* p;
//};


void test3()
{

	list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	lt1.push_back(5);

	list<int> lt2(lt1);

	list<int> lt3;
	lt3 = lt2;

	auto begin = lt3.begin();
	while (begin != lt3.end())
	{
		cout << *begin << " ";
		++begin;
	}
	cout << endl;
}


struct Pos
{
	int _x;
	int _y;
	Pos(int x = 0, int y = 0)
		:_x(x),_y(y)
	{}

};

std::ostream& operator<<(std::ostream& out, const Pos& p)
{
	out << p._x << ":" << p._y;
	return out;
}


void print(const list<Pos>& lt)
{
	auto it = lt.begin();	//迭代器是一个像指针一样的东西

	//it->_x++;
	cout << it->_x << endl;	//规定
	cout << it.operator->()->_x << endl;
	cout << (&(*it))->_x << endl;
}
void test4()
{
	list<Pos> lt;
	lt.push_back(Pos(1, 1));
	lt.push_back(Pos(2, 2));
	lt.push_back(Pos(3, 3));
	lt.push_back(Pos(4, 4));
	lt.push_back(Pos(5, 5)); 

	auto it = lt.begin();	//迭代器是一个像指针一样的东西
	
	it->_x++;
	cout << it->_x << endl;	//规定
	cout << it.operator->()->_x << endl;
	cout << (&(*it))->_x << endl;
	

	//print(lt);


	/*auto begin = lt.begin();
	while (begin != lt.end())
	{
		cout << *begin << " ";
		++begin;
	}
	cout << endl;*/

}

int main()
{
	test4();
	return 0;
}