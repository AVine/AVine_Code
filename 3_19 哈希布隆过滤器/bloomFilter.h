#pragma once



struct BKDRHash
{
	size_t operator()(const string& key)
	{
		size_t hash = 0;
		for (auto ch : key)
		{
			hash *= 131;
			hash += ch;
		}
		return hash;
	}
};

struct APHash
{
	size_t operator()(const string& key)
	{
		unsigned int hash = 0;
		int i = 0;

		for (auto ch : key)
		{
			if ((i & 1) == 0)
			{
				hash ^= ((hash << 7) ^ (ch) ^ (hash >> 3));
			}
			else
			{
				hash ^= (~((hash << 11) ^ (ch) ^ (hash >> 5)));
			}

			++i;
		}

		return hash;
	}
};

struct DJBHash
{
	size_t operator()(const string& key)
	{
		unsigned int hash = 5381;

		for (auto ch : key)
		{
			hash += (hash << 5) + ch;
		}

		return hash;
	}
};


template <size_t N,
size_t X = 7,
class T = string,
class HashFunc1 = BKDRHash,
class HashFunc2 = APHash,
class HashFunc3 = DJBHash
>
class bloomFilter
{
public:
	void set(const T& s)
	{
		//哈希函数映射多个位置
		size_t hash1 = HashFunc1()(s) % (N*X);
		size_t hash2 = HashFunc2()(s) % (N * X);
		size_t hash3 = HashFunc3()(s) % (N * X);

		//借助位图完成映射
		_bf.set(hash1);
		_bf.set(hash2);
		_bf.set(hash3);
	}

	bool test(const T& s)
	{
		//如果某一个映射位置为0，说明一定不存在
		//但三个映射都为1，说明不一定存在
		//所以布隆过滤器是有一定的误判率的
		size_t hash1 = HashFunc1()(s) % (N * X);
		if (!_bf.test(hash1))
		{
			return false;//任意一个映射为0，说明不存在
		}

		size_t hash2 = HashFunc2()(s) % (N * X);
		if (!_bf.test(hash2))
		{
			return false;
		}

		size_t hash3 = HashFunc3()(s) % (N * X);
		if (!_bf.test(hash3))
		{
			return false;
		}

		return true;//如果三个映射都为1，说明存在
	}
private:
	bitset<N*X> _bf;
};