#define _CRT_SECURE_NO_WARNINGS 1

#include <bitset>
#include <string>
#include <iostream>
#include <vector>
using namespace std;

#include "bloomFilter.h"

void test1()
{
	bloomFilter<10> bf;
	string str[] = { "苹果","梨子","菠萝","香蕉","苹果1","1苹果","苹1果" };
	for (auto& s : str)
	{
		bf.set(s);
	}
	for (auto& s : str)
	{
		cout << bf.test(s) << endl;
	}
	cout << endl;

	srand(time(0));
	for (const auto& s : str)
	{
		cout << bf.test(s + to_string(rand())) << endl;//有一定的误判率
	}
}

void test2()
{
	srand(time(0));
	const size_t N = 100000;
	bloomFilter<N> bf;

	vector<string> v1;
	string url = "https://www.cnblogs.com/-clq/archive/2012/05/31/2528153.html";

	for (size_t i = 0; i < N; ++i)
	{
		v1.push_back(url + to_string(i));
	}

	for (auto& str : v1)
	{
		bf.set(str);
	}

	// v2跟v1是相似字符串集，但是不一样
	vector<string> v2;
	for (size_t i = 0; i < N; ++i)
	{
		std::string url = "https://www.cnblogs.com/-clq/archive/2012/05/31/2528153.html";
		url += to_string(999999 + i);
		v2.push_back(url);
	}

	size_t n = 0;
	for (auto& e : v2)
	{
		if (bf.test(e))
		{
			++n;
		}
	}
	cout << "误判率:" << (double)n / (double)N << endl;
}
int main()
{
	test2();
	return 0;
}