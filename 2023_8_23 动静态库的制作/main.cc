#include "mylib/include/Add.h"
#include "mylib/include/Sub.h"

int main()
{
    int a1 = 10,b1 = 20;
    int sum = Add(a1,b1);
    std::cout << "sum = " << sum << std::endl;

    int a2 = 22,b2 = 13;
    int sub = Sub(a2,b2);
    std::cout << "sub = " << sub << std::endl;
    return 0;
}