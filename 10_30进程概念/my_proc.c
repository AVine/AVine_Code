
#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>


int main()

{
    
    pid_t id = fork();  //子进程接收的返回值为 0，父进程接收的返回值为 子进程pid

    if(id == 0)
    {
        while(1)
        {
            printf("我是子进程，pid为:%d\n",getpid());
            sleep(1);
        }
    }
    else if(id != -1)
    {
        
        while(1)
        {
            printf("我是父进程(本来的进程),pid为:%d\n",getpid());
            sleep(1);
        }
    }


    //while(1)
    //{
    //    printf("我是进程，我的pid:%d,父进程pid:%d\n",getpid(),getppid());
    //    sleep(1);
    //}


    //while(1)
    //{
    //    printf("我是一个进程\n");
    //    sleep(1);
    //}
    return 0;
}
