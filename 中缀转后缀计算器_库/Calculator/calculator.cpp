#define _CRT_SECURE_NO_WARNINGS 1

#include "calculator.h"

void ly::calculator::transform()
{
	std::stack<char> st;
	std::string tmp;

	for (auto& e : _s)
	{
		if (isdigit(e))
		{
			tmp += e;
			continue;
		}

		if (e == '+' || e == '-' || e == '*' || e == '/')
		{
			if (!st.empty() && (e == '+' || e == '-') && (st.top() == '+' || st.top() == '-'))
			{
				tmp += st.top();
				st.pop();
				st.push(e);
			}
			else if (!st.empty() && (e == '+' || e == '-') && (st.top() == '*' || st.top() == '/'))
			{
				tmp += st.top();
				st.pop();
				st.push(e);
			}
			else if (!st.empty() && (e == '*' || e == '/') && (st.top() == '*' || st.top() == '/'))
			{
				tmp += st.top();
				st.pop();
				st.push(e);
			}
			else
			{
				st.push(e);
			}
		}

	}

	while (!st.empty())
	{
		tmp += st.top();
		st.pop();
	}

	_s = tmp;
}


double ly::calculator::calculate()
{
	std::stack<double> st;
	for (auto& e : _s)
	{
		if (st.empty() || isdigit(e))
		{
			st.push(e - '0');
		}
		else if (!st.empty() && !isdigit(e))
		{
			double right = st.top();
			st.pop();
			double left = st.top();
			st.pop();

			switch (e)      //ѡ�����
			{
			case '+':st.push(left + right); break;
			case '-':st.push(left - right); break;
			case '*':st.push(left * right); break;
			case '/':st.push(left / right); break;
			}
		}
	}

	return st.top();
}

ly::calculator::calculator(const std::string& s = "")
	:_s(s)
{}