#pragma once


#include <string>
#include <stack>
namespace ly
{
	class calculator
	{
	public:
		calculator(const std::string& s);

		void transform();

		double calculate();
	private:
		std::string _s;
	};
}