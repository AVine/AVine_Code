#define _CRT_SECURE_NO_WARNINGS 1


#include "calculator.h"
#include <iostream>
using namespace ly;

int main()
{
	std::string s;
	std::cin >> s;
	calculator c(s);
	c.transform();

	double ret = c.calculate();
	std::cout << ret << std::endl;
	return 0;
}