#include "dropbar.h"
#include "ui_dropbar.h"
#include <QSpinBox>
#include <QSlider>

DropBar::DropBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DropBar)
{
    ui->setupUi(this);
    void (QSpinBox:: *spinBoxSignal)(int) = &QSpinBox::valueChanged;
    connect(ui->spinBox,spinBoxSignal,ui->horizontalSlider,&QSlider::setValue);

    void (QSlider:: *sliderSlot)(int) = &QSlider::valueChanged;
    connect(ui->horizontalSlider,sliderSlot,ui->spinBox,&QSpinBox::setValue);
}

int DropBar::GetNum()// 获取spinBox的值
{
    return ui->spinBox->value();
}
void DropBar::SetNum(int n)// 设置spinBox的到一半
{
     ui->spinBox->setValue(n);
}


DropBar::~DropBar()
{
    delete ui;
}
