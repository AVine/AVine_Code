#ifndef DROPBAR_H
#define DROPBAR_H

#include <QWidget>

namespace Ui {
class DropBar;
}

class DropBar : public QWidget
{
    Q_OBJECT

public:
    explicit DropBar(QWidget *parent = nullptr);
    ~DropBar();

    int GetNum();// 获取spinBox的值
    void SetNum(int n);// 设置spinBox的到一半

private:
    Ui::DropBar *ui;
};

#endif // DROPBAR_H
