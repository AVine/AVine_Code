#include "widget.h"
#include "ui_widget.h"
#include <QPushButton>
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    connect(ui->btn1,&QPushButton::clicked,[=](){
        qDebug() << ui->widget->GetNum();
    });

    connect(ui->btn2,&QPushButton::clicked,[=](){
        ui->widget->SetNum(50);
    });
}

Widget::~Widget()
{
    delete ui;
}

