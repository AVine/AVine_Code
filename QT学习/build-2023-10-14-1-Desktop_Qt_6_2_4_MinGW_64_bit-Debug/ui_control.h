/********************************************************************************
** Form generated from reading UI file 'control.ui'
**
** Created by: Qt User Interface Compiler version 6.2.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTROL_H
#define UI_CONTROL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_control
{
public:
    QPushButton *pushButton;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QListWidget *listWidget;
    QTreeWidget *treeWidget;

    void setupUi(QWidget *control)
    {
        if (control->objectName().isEmpty())
            control->setObjectName(QString::fromUtf8("control"));
        control->resize(1108, 524);
        pushButton = new QPushButton(control);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(40, 80, 80, 24));
        groupBox = new QGroupBox(control);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(130, 170, 61, 92));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        radioButton = new QRadioButton(groupBox);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));

        verticalLayout->addWidget(radioButton);

        radioButton_2 = new QRadioButton(groupBox);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));

        verticalLayout->addWidget(radioButton_2);

        groupBox_2 = new QGroupBox(control);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(40, 170, 73, 92));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        radioButton_3 = new QRadioButton(groupBox_2);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));

        verticalLayout_2->addWidget(radioButton_3);

        radioButton_4 = new QRadioButton(groupBox_2);
        radioButton_4->setObjectName(QString::fromUtf8("radioButton_4"));

        verticalLayout_2->addWidget(radioButton_4);

        groupBox_3 = new QGroupBox(control);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(40, 280, 97, 120));
        verticalLayout_3 = new QVBoxLayout(groupBox_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        checkBox = new QCheckBox(groupBox_3);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setTristate(true);

        verticalLayout_3->addWidget(checkBox);

        checkBox_2 = new QCheckBox(groupBox_3);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));

        verticalLayout_3->addWidget(checkBox_2);

        checkBox_3 = new QCheckBox(groupBox_3);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));

        verticalLayout_3->addWidget(checkBox_3);

        listWidget = new QListWidget(control);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(240, 80, 211, 321));
        treeWidget = new QTreeWidget(control);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        treeWidget->setHeaderItem(__qtreewidgetitem);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        treeWidget->setGeometry(QRect(540, 90, 441, 311));

        retranslateUi(control);

        QMetaObject::connectSlotsByName(control);
    } // setupUi

    void retranslateUi(QWidget *control)
    {
        control->setWindowTitle(QCoreApplication::translate("control", "Form", nullptr));
        pushButton->setText(QCoreApplication::translate("control", "\346\231\256\351\200\232\346\214\211\351\222\256", nullptr));
        groupBox->setTitle(QCoreApplication::translate("control", "\346\200\247\345\210\253", nullptr));
        radioButton->setText(QCoreApplication::translate("control", "\347\224\267", nullptr));
        radioButton_2->setText(QCoreApplication::translate("control", "\345\245\263", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("control", "\345\251\232\345\220\246", nullptr));
        radioButton_3->setText(QCoreApplication::translate("control", "\345\267\262\345\251\232", nullptr));
        radioButton_4->setText(QCoreApplication::translate("control", "\346\234\252\345\251\232", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("control", "\350\260\203\346\237\245\351\227\256\345\215\267", nullptr));
        checkBox->setText(QCoreApplication::translate("control", "\351\235\236\345\270\270\345\245\275\345\220\203", nullptr));
        checkBox_2->setText(QCoreApplication::translate("control", "\344\270\200\350\210\254\350\210\254", nullptr));
        checkBox_3->setText(QCoreApplication::translate("control", "\351\232\276\345\220\203", nullptr));
    } // retranslateUi

};

namespace Ui {
    class control: public Ui_control {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTROL_H
