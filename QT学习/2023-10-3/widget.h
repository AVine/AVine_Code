#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class Widget : public QWidget// 我们自己的窗口类
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);// 构造函数
    ~Widget();// 析构函数
};
#endif // WIDGET_H
