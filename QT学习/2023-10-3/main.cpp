#include "widget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);// 应用程序对象
    Widget w;// 空白窗口对象
    w.show();// 显示该窗口
    return a.exec();// 事件监听循环
}
