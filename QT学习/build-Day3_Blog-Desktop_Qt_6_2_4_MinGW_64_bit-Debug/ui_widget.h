/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 6.2.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <dropbar.h>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    DropBar *widget;
    QWidget *widget1;
    QVBoxLayout *verticalLayout;
    QPushButton *btn1;
    QPushButton *btn2;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(555, 442);
        widget = new DropBar(Widget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(120, 70, 301, 101));
        widget1 = new QWidget(Widget);
        widget1->setObjectName(QString::fromUtf8("widget1"));
        widget1->setGeometry(QRect(220, 190, 82, 56));
        verticalLayout = new QVBoxLayout(widget1);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        btn1 = new QPushButton(widget1);
        btn1->setObjectName(QString::fromUtf8("btn1"));

        verticalLayout->addWidget(btn1);

        btn2 = new QPushButton(widget1);
        btn2->setObjectName(QString::fromUtf8("btn2"));

        verticalLayout->addWidget(btn2);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Widget", nullptr));
        btn1->setText(QCoreApplication::translate("Widget", "\350\216\267\345\217\226\345\200\274", nullptr));
        btn2->setText(QCoreApplication::translate("Widget", "\350\256\276\347\275\256\345\200\274", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
