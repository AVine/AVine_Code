/********************************************************************************
** Form generated from reading UI file 'dropbar.ui'
**
** Created by: Qt User Interface Compiler version 6.2.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DROPBAR_H
#define UI_DROPBAR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DropBar
{
public:
    QHBoxLayout *horizontalLayout;
    QSpinBox *spinBox;
    QSlider *horizontalSlider;

    void setupUi(QWidget *DropBar)
    {
        if (DropBar->objectName().isEmpty())
            DropBar->setObjectName(QString::fromUtf8("DropBar"));
        DropBar->resize(367, 62);
        horizontalLayout = new QHBoxLayout(DropBar);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        spinBox = new QSpinBox(DropBar);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));

        horizontalLayout->addWidget(spinBox);

        horizontalSlider = new QSlider(DropBar);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(horizontalSlider);


        retranslateUi(DropBar);

        QMetaObject::connectSlotsByName(DropBar);
    } // setupUi

    void retranslateUi(QWidget *DropBar)
    {
        DropBar->setWindowTitle(QCoreApplication::translate("DropBar", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DropBar: public Ui_DropBar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DROPBAR_H
