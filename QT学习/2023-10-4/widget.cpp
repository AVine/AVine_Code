#include "widget.h"
#include <QDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
{

}

Widget::~Widget()// 因为Widget是基类，所以它是先被构造的，所以要后释放。所以调用该析构时，会先释放派生类
{
    qDebug() << "Widget::~Widget()";
}

