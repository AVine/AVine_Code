#include "widget.h"

#include <QApplication>
#include <QPushButton>
#include <QDebug>
class MyPushButton : public QPushButton// 派生类
{
public:
    MyPushButton(QWidget *parent = nullptr)
        :QPushButton(parent)
    {
        qDebug() << "MyPushButton(QWidget *parent = nullptr)";
    }

    ~MyPushButton()
    {
        qDebug() << "~MyPushButton()";
    }
private:
};

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    QPushButton *btn1 = new QPushButton;
    //btn->show();
    btn1->setParent(&w);
    btn1->setText("Hello Button!");

    QPushButton *btn2 = new QPushButton("Second Button!",&w);
    btn2->move(100,100);

    w.setWindowTitle("第一个程序");

    MyPushButton *myBnt = new MyPushButton(&w);
    myBnt->move(200,0);
    myBnt->setText("我的按钮");

    w.show();
    w.resize(600,400);
    return a.exec();
}
