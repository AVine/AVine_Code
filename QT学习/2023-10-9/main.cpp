#include "mainwindow.h"

#include <QApplication>
#include <QMenuBar>// 菜单栏头文件
#include <QMenu>
#include <QToolBar>
#include <QAction>
#include <QPushButton>
#include <QStatusBar>
#include <QLabel>
#include <QDockWidget>
#include <QTextEdit>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.resize(600,400);
    /**********************创建菜单栏*******************************/
    QMenuBar *menuBar = w.menuBar();// 在窗口当中创建一个菜单栏
    //QMenuBar *menuBar = new menuBar(&w);// 等同于上面
    w.setMenuBar(menuBar);// 设置到窗口中
    QMenu * fileMenu = menuBar->addMenu("文件");// 添加一个菜单项
    fileMenu->addAction("新建");// 为某个菜单项添加子选项
    fileMenu->addSeparator();
    fileMenu->addAction("打开");
    /**************************************************************/

    /************************创建工具栏****************************/
    QToolBar *toolBar = new QToolBar(&w);
    w.addToolBar(Qt::LeftToolBarArea,toolBar);// 默认在左边
    toolBar->setAllowedAreas(Qt::LeftToolBarArea);// 设置允许停靠的位置
    toolBar->setFloatable(false);// 禁止悬浮
    toolBar->addAction("新建");
    toolBar->addSeparator();
    toolBar->addAction("打开");
    QPushButton *btn = new QPushButton("按钮",&w);
    toolBar->addWidget(btn);
    /*************************************************************/

    /*********************创建状态栏****************************/
    //QStatusBar *statusBar = w.statusBar();
    QStatusBar *statusBar = new QStatusBar(&w);
    w.setStatusBar(statusBar);
    QLabel *statusLabel = new QLabel("此时状态",&w);
    statusBar->addWidget(statusLabel);
    QLabel *statusLabel2 = new QLabel("右侧状态",&w);
    statusBar->addPermanentWidget(statusLabel2);
    /*********************************************************/

    /***********************创建铆接部件******************************/
    QDockWidget *dockWidget = new QDockWidget("部件1",&w);
    w.addDockWidget(Qt::BottomDockWidgetArea,dockWidget);
    dockWidget->setFloating(false);
    /***************************************************************/

    /*************************创建核心部件******************************/
    QTextEdit *textEdit = new QTextEdit(&w);
    w.setCentralWidget(textEdit);
    /****************************************************************/
    w.show();
    return a.exec();
}
