#ifndef STAFF_H
#define STAFF_H

#include <QObject>

class Staff : public QObject// 员工类
{
    Q_OBJECT
public:
    explicit Staff(QObject *parent = nullptr)
        :QObject(parent)
    {}

signals:
    void IsEndOfMonth();// 到月底了
    void IsEndOfMonth(int paid);// 指定一个参数
};

#endif // STAFF_H
