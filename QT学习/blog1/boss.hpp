#ifndef BOSS_H
#define BOSS_H

#include <QObject>
#include <QDebug>
class Boss : public QObject
{
    Q_OBJECT
public:
    explicit Boss(QObject *parent = nullptr)
        :QObject(parent)
    {}

public slots:
    void GetPaid()//
    {
        qDebug() << "老板发工资了!";
    }

    void GetPaid(int paid)
    {
        qDebug() << "老板发工资了!发了" << paid << "元!";
    }

};

#endif // BOSS_H
