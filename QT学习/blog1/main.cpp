#include "widget.h"

#include <QApplication>
#include <QPushButton>
#include <QString>
#include <QDebug>
#include "staff.hpp"
#include "boss.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    Staff *st = new Staff(&w);
    Boss *bs = new Boss(&w);
    QPushButton *btn = new QPushButton("我的按钮",&w);
    /*指向int类型参数的信号和槽*/
    void (Staff:: *staffSignal)(int) = &Staff::IsEndOfMonth;
    void (Boss:: *bossSlot)(int) = &Boss::GetPaid;
    QObject::connect(st,staffSignal,bs,bossSlot);
    auto func = [=]()
    {
        emit st->IsEndOfMonth(100);
    };
    QObject::connect(btn,&QPushButton::clicked,func);// 使用lambda表达式时可以不指定接收信号对象

    w.show();
    return a.exec();
}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    Staff *st = new Staff(&w);
//    Boss *bs = new Boss(&w);
//    QPushButton *btn = new QPushButton("我的按钮",&w);
//    /*指向无参的信号和槽*/
////    void (Staff:: *staffSignal)() = &Staff::IsEndOfMonth;
////    void (Boss:: *bossSlot)() = &Boss::GetPaid;

//    /*指向int类型参数的信号和槽*/
//    void (Staff:: *staffSignal)(int) = &Staff::IsEndOfMonth;
//    void (Boss:: *bossSlot)(int) = &Boss::GetPaid;

//    QObject::connect(btn,&QPushButton::clicked,st,staffSignal);// 信号连接信号
//    QObject::connect(st,staffSignal,bs,bossSlot);
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    Staff *st = new Staff(&w);
//    Boss *bs = new Boss(&w);
//    QPushButton *btn = new QPushButton("我的按钮",&w);
//    QObject::connect(btn,&QPushButton::clicked,st,&Staff::IsEndOfMonth);// 信号连接信号
//    QObject::connect(st,&Staff::IsEndOfMonth,bs,&Boss::GetPaid);
//    w.show();
//    return a.exec();
//}


//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Staff *st = new Staff;
//    Boss *bs = new Boss;
//    QObject::connect(st,&Staff::IsEndOfMonth,bs,&Boss::GetPaid);// 注册一个信号和槽
//    emit st->IsEndOfMonth();// 员工发送"月底到了"的信号
//    return a.exec();
//}


//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    QPushButton *btn = new QPushButton("我的按钮",&w);
//    Widget::connect(btn,&QPushButton::clicked,&w,&Widget::close);// 注册信号和槽
//    w.show();
//    return a.exec();
//}


//class MyPushButton : QPushButton// 继承自QPushButton类
//{
//public:
//    MyPushButton(const QString &text, QWidget *parent = nullptr)
//        :QPushButton(text,parent)
//    {}

//    ~MyPushButton()
//    {
//        qDebug() << "~MyPushButton()";
//    }
//private:
//};

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    MyPushButton *btn = new MyPushButton("自定义按钮",&w);
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget *wid = new Widget;
//    QPushButton *btn = new QPushButton("我的按钮",wid);
//    wid->show();
//    delete wid;
//    delete btn;
//    return 0;
//}


//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    QPushButton *btn = new QPushButton("我的按钮",&w);
//    btn->resize(200,300);// 设置按钮的大小
//    btn->move(200,300);// 移动到(200,300)位置
//    w.show();
//    return a.exec();
//}


//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    QPushButton *btn = new QPushButton("我的按钮",&w);// 构造函数当中可以指定按钮的文字和"父亲"
//    //btn->setParent(&w);// 另一种方法指定"父亲"
//    //btn->setText("我的按钮");// 另一种方法指定按钮的文字
//    w.show();
//    return a.exec();
//}


//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    QPushButton *btn = new QPushButton;// 创建一个按钮对象
//    btn->show();// 单独打开一个窗口显示
//    w.show();
//    return a.exec();
//}
