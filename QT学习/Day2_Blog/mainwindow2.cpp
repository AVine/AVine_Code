#include "mainwindow2.h"
#include "ui_mainwindow2.h"
#include <QDebug>
#include <QListWidget>

MainWindow2::MainWindow2(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow2)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnCount(2);
    ui->tableWidget->setRowCount(3);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList()<<"角色"<<"技能");

    QStringList nameList;
    nameList<<"鸣人"<<"佐助"<<"卡卡西";

    QStringList tecList;
    tecList<<"螺旋丸"<<"天照"<<"雷切";

    for(int row=0;row<3;row++)
    {
        int col = 0;
        ui->tableWidget->setItem(row,col++,new QTableWidgetItem(nameList[row]));
        ui->tableWidget->setItem(row,col++,new QTableWidgetItem(tecList[row]));
    }
}

//MainWindow2::MainWindow2(QWidget *parent) :
//    QMainWindow(parent),
//    ui(new Ui::MainWindow2)
//{
//    ui->setupUi(this);
//    ui->tableWidget->setColumnCount(2);
//    ui->tableWidget->setRowCount(3);
//    ui->tableWidget->setHorizontalHeaderLabels(QStringList()<<"角色"<<"技能");

//    // 角色数据
//    static QTableWidgetItem mingren("鸣人");
//    static QTableWidgetItem kai("凯");
//    static QTableWidgetItem kakaxi("卡卡西");

//    // 技能数据
//    static QTableWidgetItem mingrenTec("螺旋丸");
//    static QTableWidgetItem kaiTec("八门遁甲");
//    static QTableWidgetItem kakaxiTec("雷切");

//    ui->tableWidget->setItem(0,0,&mingren);
//    ui->tableWidget->setItem(1,0,&kai);
//    ui->tableWidget->setItem(2,0,&kakaxi);

//    ui->tableWidget->setItem(0,1,&mingrenTec);
//    ui->tableWidget->setItem(1,1,&kaiTec);
//    ui->tableWidget->setItem(2,1,&kakaxiTec);
//}

//MainWindow2::MainWindow2(QWidget *parent) :
//    QMainWindow(parent),
//    ui(new Ui::MainWindow2)
//{
//    ui->setupUi(this);
//    ui->treeWidget->setHeaderLabels(QStringList()<<"所属组织"<<"技能");

//    // 创建顶层节点
//    QTreeWidgetItem *muye = new QTreeWidgetItem(ui->treeWidget,QStringList()<<"木叶村");
//    QTreeWidgetItem *xiao = new QTreeWidgetItem(ui->treeWidget,QStringList()<<"晓组织");
//    QTreeWidgetItem *ying = new QTreeWidgetItem(ui->treeWidget,QStringList()<<"鹰小队");
//    ui->treeWidget->addTopLevelItem(muye);
//    ui->treeWidget->addTopLevelItem(xiao);
//    ui->treeWidget->addTopLevelItem(ying);

//    // 追加子节点
//    QTreeWidgetItem *mingren = new QTreeWidgetItem(muye,QStringList()<<"鸣人"<<"螺旋丸");
//    QTreeWidgetItem *you = new QTreeWidgetItem(xiao,QStringList()<<"鼬"<<"月读");
//    QTreeWidgetItem *zuozhu = new QTreeWidgetItem(ying,QStringList()<<"佐助"<<"千鸟");
//    muye->addChild(mingren);
//    xiao->addChild(you);
//    ying->addChild(zuozhu);
//}

//MainWindow2::MainWindow2(QWidget *parent) :
//    QMainWindow(parent),
//    ui(new Ui::MainWindow2)
//{
//    ui->setupUi(this);
//    ui->listWidget->addItems(QStringList()<<"万里长城永不倒"<<"黄河两岸水滔滔");
//}

//MainWindow2::MainWindow2(QWidget *parent) :
//    QMainWindow(parent),
//    ui(new Ui::MainWindow2)
//{
//    ui->setupUi(this);
//    QListWidgetItem *listItem1 = new QListWidgetItem("在远方的她此刻可知道",ui->listWidget);
//    listItem1->setTextAlignment(Qt::AlignHCenter);// 设置水平居中
//    ui->listWidget->addItem(listItem1);
//}

//MainWindow2::MainWindow2(QWidget *parent) :
//    QMainWindow(parent),
//    ui(new Ui::MainWindow2)
//{
//    ui->setupUi(this);
////    connect(ui->checkBox_4,&QCheckBox::clicked,[](int x){// 接收一个int类型的参数
////        qDebug() << "用户选择了非常好:" << x;
////    });
//}

MainWindow2::~MainWindow2()
{
    delete ui;
}
