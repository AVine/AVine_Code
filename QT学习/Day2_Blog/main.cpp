#include "mainwindow.h"
#include "mainwindow2.h"
#include <QApplication>
#include <QMenuBar>
#include <QToolBar>
#include <QPushButton>
#include <QStatusBar>
#include <QLabel>
#include <QDockWidget>
#include <QTextEdit>
#include <QDialog>
#include <QMessageBox>
#include <QDebug>
#include <QColorDialog>
#include <QFileDialog>
#include <QFontDialog>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow2 w;
    w.show();
    return a.exec();
}


//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;

//    QMenuBar *mBar = new QMenuBar(&w);// 创建菜单栏
//    w.setMenuBar(mBar);// 设置菜单栏到窗口中
//    QMenu *m1 = mBar->addMenu("文件");// 添加菜单
//    QAction *a1 = m1->addAction("新建");// 为菜单添加菜单项

//    QObject::connect(a1,&QAction::triggered,[&](){
//        QFontDialog fLog;
//        bool flag;
//        QFont ret = fLog.getFont(&flag,QFont("仿宋",30));// 静态成员函数这么访问是没有问题的
//        qDebug() << "字体:" << ret.family() << "是否加粗:" << ret.bold();
//        fLog.exec();
//    });
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;

//    QMenuBar *mBar = new QMenuBar(&w);// 创建菜单栏
//    w.setMenuBar(mBar);// 设置菜单栏到窗口中
//    QMenu *m1 = mBar->addMenu("文件");// 添加菜单
//    QAction *a1 = m1->addAction("新建");// 为菜单添加菜单项

//    QObject::connect(a1,&QAction::triggered,[&](){
//        QFileDialog fLog;
//        QString fileName = fLog.getOpenFileName(nullptr,"","","*.h");
//        qDebug() << fileName;
//        fLog.exec();
//    });
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;

//    QMenuBar *mBar = new QMenuBar(&w);// 创建菜单栏
//    w.setMenuBar(mBar);// 设置菜单栏到窗口中
//    QMenu *m1 = mBar->addMenu("文件");// 添加菜单
//    QAction *a1 = m1->addAction("新建");// 为菜单添加菜单项

//    QObject::connect(a1,&QAction::triggered,[&](){
//        QColorDialog  cLog;
//        QColor clo = cLog.getColor();
//        qDebug() << "R = " << clo.red() << " G = " << clo.green() << " B = " << clo.blue();
//        cLog.exec();
//    });
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;

//    QMenuBar *mBar = new QMenuBar(&w);// 创建菜单栏
//    w.setMenuBar(mBar);// 设置菜单栏到窗口中
//    QMenu *m1 = mBar->addMenu("文件");// 添加菜单
//    QAction *a1 = m1->addAction("新建");// 为菜单添加菜单项

//    QObject::connect(a1,&QAction::triggered,[&](){
//        QMessageBox::StandardButton ret = QMessageBox::question(&w,"提问","您是否确定?",
//                            QMessageBox::Yes | QMessageBox::No,QMessageBox::No);
//        if(ret == QMessageBox::Yes)
//        {
//            qDebug() << "用户选择了Yes!";
//        }
//        else if(ret == QMessageBox::No)
//        {
//            qDebug() << "用户选择了No!";
//        }
//        else
//        {
//            qDebug() << "当前对话框出错!";
//        }

//    });
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;

//    QMenuBar *mBar = new QMenuBar(&w);// 创建菜单栏
//    w.setMenuBar(mBar);// 设置菜单栏到窗口中
//    QMenu *m1 = mBar->addMenu("文件");// 添加菜单
//    QAction *a1 = m1->addAction("新建");// 为菜单添加菜单项

//    QObject::connect(a1,&QAction::triggered,[&](){
//        QMessageBox::information(&w,"消息","您有一条消息!",QMessageBox::Cancel);
//    });
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;

//    QMenuBar *mBar = new QMenuBar(&w);// 创建菜单栏
//    w.setMenuBar(mBar);// 设置菜单栏到窗口中
//    QMenu *m1 = mBar->addMenu("文件");// 添加菜单
//    QAction *a1 = m1->addAction("新建");// 为菜单添加菜单项

//    QObject::connect(a1,&QAction::triggered,[&](){
//        QMessageBox::critical(&w,"错误","出错了!");
//    });
//    w.show();
//    return a.exec();
//}


//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;

//    QMenuBar *mBar = new QMenuBar(&w);// 创建菜单栏
//    w.setMenuBar(mBar);// 设置菜单栏到窗口中
//    QMenu *m1 = mBar->addMenu("文件");// 添加菜单
//    QAction *a1 = m1->addAction("新建");// 为菜单添加菜单项

//    QObject::connect(a1,&QAction::triggered,[&](){
//        QDialog *dLog = new QDialog(&w);// 创建在堆上，如果是在栈上出了当前作用域对象就会销毁
//        dLog->setAttribute(Qt::WA_DeleteOnClose);// 设置对话框关闭后自动销毁，否则会造成内存泄漏
//        dLog->resize(200,100);
//        dLog->show();
//    });
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;

//    QMenuBar *mBar = new QMenuBar(&w);// 创建菜单栏
//    w.setMenuBar(mBar);// 设置菜单栏到窗口中
//    QMenu *m1 = mBar->addMenu("文件");// 添加菜单
//    QAction *a1 = m1->addAction("新建");// 为菜单添加菜单项

//    QObject::connect(a1,&QAction::triggered,[=](){
//        QDialog dLog;
//        dLog.resize(200,100);
//        dLog.exec();
//    });
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;
//    QPushButton *btn = new QPushButton(&w);
//    btn->resize(500,500);
//    btn->setIconSize(QSize(100,100));
//    btn->setIcon(QIcon(":/ResourceFile/Kai.png"));
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;
//    QTextEdit *tEdit = new QTextEdit(&w);
//    w.setCentralWidget(tEdit);

//    QDockWidget *dWid = new QDockWidget(&w);
//    w.addDockWidget(Qt::BottomDockWidgetArea,dWid);// 默认在下方
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;
//    QStatusBar *sBar = new QStatusBar(&w);// 指定父亲避免内存泄漏
//    QLabel *lb = new QLabel("当前状态",&w);
//    QLabel *lb2 = new QLabel("未来状态",&w);
//    sBar->addWidget(lb);
//    sBar->addPermanentWidget(lb2);// 设置到右边
//    w.setStatusBar(sBar);
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;
//    QToolBar *tBar = new QToolBar;
//    QToolBar *tBar2 = new QToolBar;
//    w.addToolBar(Qt::LeftToolBarArea,tBar);// 默认停靠在左方
//    w.addToolBar(tBar2);// 添加一个新的工具栏

//    tBar->setAllowedAreas(Qt::LeftToolBarArea | Qt::RightToolBarArea);// 只允许停靠在左方和右方
//    tBar->setFloatable(false);// 禁止悬浮
//    tBar->setMovable(false);// 禁止被拖拽
//    tBar->addAction("编辑");
//    tBar->addSeparator();// 添加分割线
//    tBar->addAction("帮助");

//    QPushButton *btn = new QPushButton(&w);
//    btn->setText("按钮");
//    tBar->addWidget(btn);// 添加控件
//    w.show();
//    return a.exec();
//}


//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;
//    QMenuBar *mBar = new QMenuBar;// 可以直接new出来
//    w.setMenuBar(mBar);// 设置到窗口当中
//    QMenu *m1 = mBar->addMenu("文件");
//    m1->addAction("新建");
//    m1->addSeparator();// 添加分割线
//    m1->addAction("打开");

//    QMenuBar *mBar2 = new QMenuBar;
//    w.setMenuBar(mBar2);// 设置新的菜单栏
//    w.show();
//    return a.exec();
//}
