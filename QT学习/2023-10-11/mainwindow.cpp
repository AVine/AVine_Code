#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDialog>
#include <QMessageBox>
#include <QDebug>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->actionnew->setIcon(QIcon(":/Image/boy.gif"));
    ui->actionopen->setIcon(QIcon(":/Image/Luffy.png"));
    connect(ui->actionTool,&QAction::triggered,[=](){
//        QDialog dlg(this);
//        dlg.resize(400,200);
//        dlg.exec();// 模态对话框

//        QDialog dlg2(this);// 创建在栈上，出了作用域对象就会直接销毁
//        QDialog *dlg2 = new QDialog(this);// 可能会导致内存泄漏
//        dlg2->setAttribute(Qt::WA_DeleteOnClose);// 关闭对话框时对象自动释放，避免内存泄漏
//        dlg2->resize(400,200);
//        dlg2->show();// 非模态对话框

        QMessageBox::StandardButton buttonMsg = QMessageBox::information(this,"消息","测试功能",QMessageBox::Cancel | QMessageBox::Yes);// 消息对话框
        if(buttonMsg == QMessageBox::Yes)// 返回值是枚举类型，直接比较就可以了
        {
            qDebug() << "用户点击的是Yes";
        }
        else qDebug() << "用户点击的是Cancel";
    });

}

MainWindow::~MainWindow()
{
    delete ui;
}

