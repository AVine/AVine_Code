#ifndef STUDENT_H
#define STUDENT_H

#include <QObject>
#include <QString>
class Student : public QObject
{
    Q_OBJECT
public:
    explicit Student(QObject *parent = nullptr);
    void GoOutOfClass();// 要声明和实现
    void GoOutOfClass(QString str);
signals:

};

#endif // STUDENT_H
