#include "widget.h"

#include <QApplication>
#include <QPushButton>  // 包含头文件
#include "student.h"
#include "teacher.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    QPushButton *btn = new QPushButton(&w);
    btn->move(250,100);
    btn->resize(100,100);
    btn->setText("打开一个窗口");

    Widget ww;
    bool buttonState = false;
    QObject::connect(btn,&QPushButton::clicked,[&](){
        if(buttonState == false)// 没打开新窗口
        {
            btn->setText("点击关闭窗口");
            ww.show();
            buttonState = true;
        }
        else
        {
            btn->setText("打开一个窗口");
            ww.close();
            buttonState = false;
        }
    });

    w.resize(600,400);
    w.show();
    return a.exec();
}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    Student *stu = new Student(&w);
//    Teacher *tec = new Teacher(&w);

//    void (Teacher::*tecSignal)() = &Teacher::ClassOver;
//    void (Student::*stuSlot)() = &Student::GoOutOfClass;
//    QObject::connect(tec,SIGNAL(&Teacher::ClassOver(QString)),stu,SLOT(&Student::GoOutOfClass(QString)));

//    QPushButton *btn = new QPushButton(&w);
//    btn->setText("PushButton");
//    btn->resize(200,100);

//    QObject::connect(btn,&QPushButton::clicked,tec,tecSignal);
//    w.resize(600,400);
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Student *stu = new Student;
//    Teacher *tec = new Teacher;
//    void (Teacher::*tecSignal)(QString) = &Teacher::ClassOver;
//    void (Student::*stuSlot)(QString) = &Student::GoOutOfClass;
//    QObject::connect(tec,tecSignal,stu,stuSlot);
//    emit tec->ClassOver("下课啦!");// emit关键字，发送信号
//    delete stu;delete tec;
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Student *stu = new Student;
//    Teacher *tec = new Teacher;
//    QObject::connect(tec,&Teacher::ClassOver,stu,&Student::GoOutOfClass);
//    emit tec->ClassOver();// emit关键字，发送信号
//    delete stu;delete tec;
//    return a.exec();
//}


//#include "mytest.h"
//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    QPushButton *btn = new QPushButton(&w);
//    btn->setText("PushButton");
//    btn->move(200,100);
//    btn->resize(100,100);
//    QObject::connect(btn,&QPushButton::clicked,&w,&Widget::close);// 注册信号
//    w.show();
//    return a.exec();
//}

//#include "mytest.h"
//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    MyTest *mbtn = new MyTest(&w);
//    w.resize(600,400);
//    w.show();
//    return a.exec();
//}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    QPushButton *btn = new QPushButton;
//    btn->setParent(&w);// 依附于已经存在的窗口
//    btn->setText("First Button");// 显示文字
//    btn->resize(200,200);// 设置一下按钮的大小
//    btn->move(200,100);// 移动到指定为止
//    //btn->show();// 显示
//    w.resize(600,400);// 固定一下窗口的大小
//    w.show();
//    return a.exec();
//}
