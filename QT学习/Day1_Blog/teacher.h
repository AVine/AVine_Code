#ifndef TEACHER_H
#define TEACHER_H

#include <QObject>
#include <QString>
class Teacher : public QObject
{
    Q_OBJECT
public:
    explicit Teacher(QObject *parent = nullptr);

signals:
    void ClassOver();// 下课，只声明不实现
    void ClassOver(QString str);
};

#endif // TEACHER_H
