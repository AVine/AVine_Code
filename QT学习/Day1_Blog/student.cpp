#include "student.h"
#include <QDebug>
Student::Student(QObject *parent)
    : QObject{parent}
{

}
void Student::GoOutOfClass()
{
    qDebug() << "学生走出教室!";
}

void Student::GoOutOfClass(QString str)
{
    qDebug() << "老师说:" << str;
}
