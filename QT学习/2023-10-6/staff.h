#ifndef STAFF_H
#define STAFF_H

#include <QObject>

class Staff : public QObject
{
    Q_OBJECT
public:
    explicit Staff(QObject *parent = nullptr);

signals:
    void IsEndOfMonth();// 到月底了
    void IsEndOfMonth(int paid);// 到月底了，应该发多少钱
};

#endif // STAFF_H
