#ifndef BOSS_H
#define BOSS_H

#include <QObject>

class Boss : public QObject
{
    Q_OBJECT
public:
    explicit Boss(QObject *parent = nullptr);

public slots:
    void GetPaid();
    void GetPaid(int paid);
};

#endif // BOSS_H
