#include "widget.h"
#include "boss.h"
#include "staff.h"
#include <QApplication>
#include <QPushButton>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    QPushButton *btn = new QPushButton("我的按钮",&w);
//    QObject::connect(btn,&QPushButton::clicked,&w,&QWidget::close);

    Boss *boss = new Boss(&w);
    Staff *staff = new Staff(&w);
    //QObject::connect(staff,&Staff::IsEndOfMonth,boss,&Boss::GetPaid);// 无参
    //emit staff->IsEndOfMonth();// 员工触发信号

    void (Staff:: *staffSignal)(int) = &Staff::IsEndOfMonth;// 自动去找正确的函数
    void (Boss:: *bossSlot)(int) = &Boss::GetPaid;
    QObject::connect(staff,staffSignal,boss,bossSlot);
    //QObject::connect(btn,&QPushButton::clicked,staff,staffSignal);
    QObject::connect(btn,&QPushButton::clicked,[=](){
        emit staff->IsEndOfMonth(5000);
    });
    w.show();
    return a.exec();
}
