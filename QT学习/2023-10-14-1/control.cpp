#include "control.h"
#include "ui_control.h"
#include <QDebug>
#include <QStringList>
control::control(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::control)
{
    ui->setupUi(this);
    connect(ui->radioButton_2,&QRadioButton::clicked,[=](){
        qDebug() << "女";
    });

    connect(ui->checkBox,&QCheckBox::stateChanged,[=](int state){
        qDebug() << state;
    });

    QListWidgetItem *item = new QListWidgetItem("床前明月光");
    ui->listWidget->addItem(item);
    item->setTextAlignment(Qt::AlignHCenter);// 居中

    QStringList items;
    items << "疑是地上霜" << "举头望明月" << "低头思故乡";
    ui->listWidget->addItems(items);

    ui->treeWidget->setHeaderLabels(QStringList() << "人物" << "简介");

    QTreeWidgetItem *treeItem1 = new QTreeWidgetItem(QStringList() << "朱元璋");
    QTreeWidgetItem *treeItem2 = new QTreeWidgetItem(QStringList() << "李鸿章");
    QTreeWidgetItem *treeItem3 = new QTreeWidgetItem(QStringList() << "周星驰");

    ui->treeWidget->addTopLevelItem(treeItem1);
    ui->treeWidget->addTopLevelItem(treeItem2);
    ui->treeWidget->addTopLevelItem(treeItem3);

    QTreeWidgetItem *treeItem1Child = new QTreeWidgetItem(QStringList() << "明朝" << "开国皇帝");
    treeItem1->addChild(treeItem1Child);
    QTreeWidgetItem *treeItem2Child = new QTreeWidgetItem(QStringList() << "清朝" << "辅国忠诚");
    treeItem2->addChild(treeItem2Child);
    QTreeWidgetItem *treeItem3Child = new QTreeWidgetItem(QStringList() << "现代" << "著名演员");
    treeItem3->addChild(treeItem3Child);
}

control::~control()
{
    delete ui;
}
