#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <windows.h>

int main()
{
	int cnt = 0;
	char rotate[4] = { '|','/','-','\\' };
	char proc[101] = { 0 };
	while (cnt <= 100)
	{
		printf("[%-100s][%d%%][%c]\r", proc,cnt,rotate[cnt%4]);
		fflush(stdout);
		Sleep(40);
		proc[cnt++] = '>';
	}
	return 0;
}