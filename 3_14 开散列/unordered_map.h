#pragma once

#include "HashBucket.h"
#include <map>
using std::pair;
namespace ly
{
	//                          通常这个函数由上层提供
	template <class K,class V,class HashFunc = Func<K>>
	class unordered_map
	{
	public:
		struct KeyOfMap
		{
			K operator()(const pair<K, V>& kv)
			{
				return kv.first;
			}
		};
		typedef typename HashBucket<K, pair<K, V>, KeyOfMap, HashFunc>::iterator iterator;
		typedef typename HashBucket<K, pair<K, V>, KeyOfMap, HashFunc>::const_iterator const_iterator;

		template<class Iterator>
		unordered_map(Iterator begin, Iterator end)
		{
			while (begin != end)
			{
				insert(make_pair(begin->first, begin->second));
				++begin;
			}
		}

		unordered_map(const unordered_map& um)
		{
			unordered_map tmp(um.begin(), um.end());
			swap(tmp);
		}

		unordered_map()
		{}

		void swap(unordered_map& um)
		{
			std::swap(_hb.get_tables(), um._hb.get_tables());
			std::swap(_hb.get_n(), um._hb.get_n());
		}

		iterator begin()
		{
			return _hb.begin();
		}
		iterator end()
		{
			return _hb.end();
		}

		const_iterator begin() const
		{
			return _hb.begin();
		}
		const_iterator end() const
		{
			return _hb.end();
		}
		
		pair<iterator, bool> insert(const pair<K, V>& kv)
		{
			return _hb.insert(kv);
		}

		V& operator[](const K& k)
		{
			pair<iterator, bool> p = insert(make_pair(k, V()));
			return p.first->second;
		}
	private:
		HashBucket<K, pair<K, V>,KeyOfMap,HashFunc> _hb;
	};
}