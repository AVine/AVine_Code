#pragma once

namespace ly
{

	template <class T>
	struct Func
	{
		size_t operator()(const T& t)
		{
			return (size_t)t;
		}
	};

	template<>
	struct Func<string>
	{
		size_t operator()(const string& s)
		{
			size_t hash = 0;
			for (auto& e : s)
			{
				hash *= 131;
				hash += e;
			}
			return hash;
		}
	};


	template <class K, class V>
	struct HashNode
	{
		HashNode<K, V>* _next;
		pair<K, V> _kv;
		HashNode(const pair<K, V>& kv)
			:_kv(kv), _next(nullptr)
		{}
	};


	template<class K, class V>
	class HashBucket
	{
	public:
		typedef Func<K> Func;
		typedef HashNode<K, V> Node;
		HashBucket()
			:_n(0)
		{
			_tables.resize(10);//默认大小为10
		}

		~HashBucket()
		{
			clear();
		}

		void clear()
		{
			for (auto cur : _tables)
			{
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
			}
		}

		bool insert(const pair<K, V>& kv)
		{
			if (find(kv.first))
			{
				return false;
			}

			if (_n == _tables.size())//当负载因子等于1时
			{
				vector<Node*> newTables;
				newTables.resize(_tables.size() * 2, nullptr);
				for (auto cur : _tables)
				{
					while (cur)//把cur看成新节点
					{
						Node* next = cur->_next;
						//size_t hashi = cur->_kv.first % newTables.size();
						size_t hashi = Func()(cur->_kv.first) % newTables.size();
						cur->_next = newTables[hashi];
						newTables[hashi] = cur;
						cur = next;
					}
				}
				_tables.swap(newTables);
			}

			//不需要扩容时，头插
			//size_t hashi = kv.first % _tables.size();//不能确保每个K都是数字类型
			size_t hashi = Func()(kv.first) % _tables.size();
			Node* newnode = new Node(kv);
			newnode->_next = _tables[hashi];
			_tables[hashi] = newnode;
			++_n;//增加有效数据个数
			return true;
		}

		Node* find(const K& k)
		{
			size_t hashi = Func()(k) % _tables.size();
			Node* cur = _tables[hashi];
			while (cur)
			{
				if (cur->_kv.first == k)
				{
					return cur;
				}
				else
				{
					cur = cur->_next;
				}
			}
			return nullptr;
		}

		bool erase(const K& k)
		{
			size_t hashi = Func()(k) % _tables.size();
			Node* cur = _tables[hashi];
			Node* prev = nullptr;
			while (cur)
			{
				if (cur->_kv.first == k)
				{
					if (prev != nullptr)
					{
						prev->_next = cur->_next;
						delete cur;
					}
					else
					{
						_tables[hashi] = cur->_next;
						delete cur;
					}
					--_n;
					return true;
				}
			}
			return false;
		}
	private:
		vector<Node*> _tables;
		size_t _n;//有效数据个数
	};
}