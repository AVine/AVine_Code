#pragma once

#include <string>
#include <vector>
using std::string;
using std::vector;
using std::make_pair;
namespace ly
{

	template <class T>
	struct Func
	{
		size_t operator()(const T& t)
		{
			return (size_t)t;
		}
	};

	template<>
	struct Func<string>
	{
		size_t operator()(const string& s)
		{
			size_t hash = 0;
			for (auto& e : s)
			{
				hash *= 131;
				hash += e;
			}
			return hash;
		}
	};


	template <class KV>
	struct HashNode
	{
		HashNode<KV>* _next;
		KV _data;
		HashNode(const KV& data)
			:_data(data),_next(nullptr)
		{}
	};



	template<class K, class KV, class KOfKV, class HashFunc>
	class HashBucket;//声明

	//template<class KV>
	template<class K, class KV, class KOfKV, class HashFunc>
	struct HashIterator
	{
		typedef HashNode<KV> Node;
		typedef HashIterator<K, KV, KOfKV, HashFunc> Self;
		typedef HashBucket<K, KV, KOfKV, HashFunc> Tables;
		typedef Func<K> Func;
		Node* _node;
		Tables* _ht;
		/*如果const对象，传参是传不进来的
		就算加上const兼容，那么成员的定义是非const，也不能转换
		所以const迭代器不能在一个模板上实现
		必须单独实现*/
		HashIterator(Node* _node,Tables* ht)
			:_node(_node),_ht(ht)
		{}

		KV& operator*()
		{
			return _node->_data;
		}

		KV* operator->()
		{
			return &_node->_data;
		}

		Self& operator++()
		{
			if (_node->_next)//如果下一个节点不为空
			{
				_node = _node->_next;
			}
			else
			{
				//如果下一个节点为空，就要想办法跳到下一个有效位置
				KOfKV kof;
				size_t hashi = Func()(kof(_node->_data)) % _ht->_tables.size();
				++hashi;
				while (hashi < _ht->_tables.size())
				{
					if (_ht->_tables[hashi])
					{
						_node = _ht->_tables[hashi];
						break;
					}
					else
					{
						++hashi;
					}
				}
				if (hashi == _ht->_tables.size())
				{
					_node = nullptr;
				}
			}
			return *this;
		}

		bool operator!=(const Self& it)
		{
			return _node != it._node;
		}
	};

	template<class K, class KV, class KOfKV, class HashFunc>
	struct HashConstIterator
	{
		typedef HashNode<KV> Node;
		typedef HashConstIterator<K, KV, KOfKV, HashFunc> Self;
		typedef HashBucket<K, KV, KOfKV, HashFunc> Tables;
		typedef HashIterator<K, KV, KOfKV, HashFunc> iterator;
		typedef Func<K> Func;
		const Node* _node;
		const Tables* _ht;

		HashConstIterator(const Node* _node, const Tables* ht)
			:_node(_node), _ht(ht)
		{}

		/*与set同样的问题，支持普通迭代器向const迭代器的转换*/
		HashConstIterator(const iterator& it)
			:_node(it._node), _ht(it._ht)
		{}

		const KV& operator*()
		{
			return _node->_data;
		}

		const KV* operator->()
		{
			return &_node->_data;
		}

		Self& operator++()
		{
			if (_node->_next)//如果下一个节点不为空
			{
				_node = _node->_next;
			}
			else
			{
				//如果下一个节点为空，就要想办法跳到下一个有效位置
				KOfKV kof;
				size_t hashi = Func()(kof(_node->_data)) % _ht->_tables.size();
				++hashi;
				while (hashi < _ht->_tables.size())
				{
					if (_ht->_tables[hashi])
					{
						_node = _ht->_tables[hashi];
						break;
					}
					else
					{
						++hashi;
					}
				}
				if (hashi == _ht->_tables.size())
				{
					_node = nullptr;
				}
			}
			return *this;
		}

		bool operator!=(const Self& it)
		{
			return _node != it._node;
		}
	};


	template<class K,class KV,class KOfKV,class HashFunc>
	class HashBucket
	{
	public:
		typedef Func<K> Func;
		typedef HashNode<KV> Node;

		template<class K, class KV, class KOfKV, class HashFunc>
		friend struct HashIterator;
		template<class K, class KV, class KOfKV, class HashFunc>
		friend struct HashConstIterator;

		typedef HashIterator<K, KV, KOfKV, HashFunc> iterator;
		typedef HashConstIterator<K, KV, KOfKV, HashFunc> const_iterator;

		iterator begin()
		{
			//begin应该指向第一个有效位置
			for (auto cur : _tables)
			{
				if (cur)
				{
					return iterator(cur, this);
				}
			}
			return iterator(nullptr, this);
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

		const_iterator begin() const
		{
			//begin应该指向第一个有效位置
			for (auto cur : _tables)
			{
				if (cur)
				{
					return const_iterator(cur, this);
				}
			}
			return const_iterator(nullptr, this);
		}

		const_iterator end() const
		{
			return const_iterator(nullptr, this);
		}


		HashBucket()
			:_n(0)
		{
			_tables.resize(10);//默认大小为10
		}

		~HashBucket()
		{
			clear();
		}

		void clear()
		{
			for (auto cur : _tables)
			{
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
			}
		}

		std::pair<iterator,bool> insert(const KV& data)
		{
			KOfKV kof;
			auto ret = find(kof(data));
			if (ret != end())
			{
				return make_pair(ret, false);
			}
			

			if (_n == _tables.size())//当负载因子等于1时
			{
				vector<Node*> newTables;
				newTables.resize(_tables.size() * 2, nullptr);
				for (auto cur : _tables)
				{
					while (cur)//把cur看成新节点
					{
						Node* next = cur->_next;
						//size_t hashi = cur->_kv.first % newTables.size();
						size_t hashi = Func()(kof(cur->_data)) % newTables.size();
						cur->_next = newTables[hashi];
						newTables[hashi] = cur;
						cur = next;
					}
				}
				_tables.swap(newTables);
			}

			//不需要扩容时，头插
			//size_t hashi = kv.first % _tables.size();//不能确保每个K都是数字类型
			size_t hashi = Func()(kof(data)) % _tables.size();
			Node* newnode = new Node(data);
			newnode->_next = _tables[hashi];
			_tables[hashi] = newnode;
			++_n;//增加有效数据个数
			return make_pair(iterator(newnode,this), true);
		}

		iterator find(const K& k)
		{
			KOfKV kof;
			size_t hashi = Func()(k) % _tables.size();
			Node* cur = _tables[hashi];
			while (cur)
			{
				if (kof(cur->_data) == k)
				{
					return iterator(cur,this);
				}
				else
				{
					cur = cur->_next;
				}
			}
			return iterator(nullptr,this);
		}

		bool erase(const K& k)
		{
			size_t hashi = Func()(k) % _tables.size();
			Node* cur = _tables[hashi];
			Node* prev = nullptr;
			while (cur)
			{
				if (cur->_kv.first == k)
				{
					if (prev != nullptr)
					{
						prev->_next = cur->_next;
						delete cur;
					}
					else
					{
						_tables[hashi] = cur->_next;
						delete cur;
					}
					--_n;
					return true;
				}
			}
			return false;
		}

		vector<Node*>& get_tables()
		{
			return _tables;
		}

		size_t& get_n()
		{
			return _n;
		}
	private:
		vector<Node*> _tables;
		size_t _n;//有效数据个数
	};
}