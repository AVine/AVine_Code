#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;
#include "unordered_map.h"
#include "unordered_set.h"

void test()
{
	ly::unordered_map<string, int> um;
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����",
"ƻ��", "�㽶", "ƻ��", "�㽶" };
	for (auto& e : arr)
	{
		um[e]++;
	}
	const ly::unordered_map<string, int> um2(um);
	um["ƻ��"] = 88;

	auto it = um2.begin();
	while (it != um2.end())
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;
}

void test2()
{
	ly::unordered_set<string> us;
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����",
"ƻ��", "�㽶", "ƻ��", "�㽶" };

	for (auto& e : arr)
	{
		us.insert(e);
	}

	auto it = us.begin();
	while (it != us.end())
	{
		cout << *it << endl;
		++it;
	}
	cout << endl;
}

int main()
{
	test2();
	return 0;
}