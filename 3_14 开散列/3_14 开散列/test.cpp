#define _CRT_SECURE_NO_WARNINGS 1

#include <map>
#include <vector>
#include <string>
#include <iostream>
using namespace std;
#include "unordered_map.h"

void test1()
{
	ly::unordered_map<string, int> um;
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����",
"ƻ��", "�㽶", "ƻ��", "�㽶" };
	for (auto& e : arr)
	{
		um[e]++;
	}

	auto it = um.begin();
	while (it != um.end())
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;
}

int main()
{
	test1();
	return 0;
}