#pragma once

#include <string>
#include <vector>
namespace ly
{
	template <class T>
	struct Func
	{
		size_t operator()(const T& t)
		{
			return (size_t)t;
		}
	};

	template <>
	struct Func<std::string>
	{
		size_t operator()(const std::string& t)
		{
			size_t hash = 0;
			for (auto& e : t)
			{
				hash *= 131;
				hash += e;
			}
			return hash;
		}
	};

	template <class Value>
	struct HashNode //其实并不知道Value到底是K还是KV
	{
		HashNode<Value>* _next;//链式存储
		Value _data;
		HashNode(const Value& data)
			:_data(data), _next(nullptr)
		{}
	};

	template <class Key, class KeyValue, class KeyOfValue, class HashFunc>
	struct HashIterator
	{
		template <class Key, class KeyValue, class KeyOfValue, class HashFunc>
		friend class HashTable;

		typedef Func<Key> Func;
		typedef HashNode<KeyValue> Node;
		typedef HashIterator<Key, KeyValue, KeyOfValue, HashFunc> Self;
		typedef HashTable<Key, KeyValue, KeyOfValue, HashFunc> Tables;
		Node* _node;
		Tables* _ht;
		KeyOfValue kof;

		HashIterator(Node* node, Tables* ht)
			:_node(node), _ht(ht)
		{}

		KeyValue& operator*()
		{
			return _node->_data;
		}

		KeyValue* operator->()
		{
			return &_node->_data;
		}

		Self& operator++()
		{
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else
			{
				//当前桶遍历结束，要找到下一个桶的位置
				size_t hashi = Func()(kof(_node->_data)) % _ht->_tables.size();
				++hashi;
				while (hashi < _ht->_tables.size())
				{
					if (_ht->_tables[hashi])
					{
						_node = _ht->_tables[hashi];
						break;
					}
					else
					{
						++hashi;
					}
				}
				if (hashi == _ht->_tables.size())
				{
					_node = nullptr;
				}
			}
			return *this;
		}

		bool operator!=(const Self& s) const
		{
			return _node != s._node;
		}
	};


	//套路与 map、set一样
	template <class Key, class KeyValue, class KeyOfValue, class HashFunc>
	class HashTable
	{
		typedef HashNode<KeyValue> Node;
		typedef Func<Key> Func;
	public:

		template <class Key, class KeyValue, class KeyOfValue, class HashFunc>
		friend struct HashIterator;
		typedef HashIterator<Key, KeyValue, KeyOfValue, HashFunc> iterator;
		iterator begin()
		{
			for (auto cur : _tables)
			{
				if (cur)
				{
					return iterator(cur, this);
				}
			}
			return iterator(nullptr, this);
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

		HashTable()
			:_n(0)
		{
			_tables.resize(10);//STL使用素数。这里简化不适用
		}

		~HashTable()
		{
			clear();
			_n = 0;
		}

		void clear()
		{
			for (auto cur : _tables)
			{
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = nullptr;
					cur = next;
				}
			}
		}

		size_t size()
		{
			return _n;
		}

		std::pair<iterator, bool> insert(const KeyValue& data)
		{
			iterator it = find(kof(data));
			if (it != end())
			{
				return make_pair(it, false);
			}

			if (_n == _tables.size())//当负载因子为1时，扩容
			{
				std::vector<Node*> newTables;
				newTables.resize(_tables.size() * 2, nullptr);
				for (int i = 0; i < _tables.size(); i++)
				{
					Node* cur = _tables[i];
					while (cur)//直接把cur当成新节点
					{
						Node* next = cur->_next;
						size_t hashi = Func()(kof(cur->_data)) % newTables.size();
						cur->_next = newTables[hashi];
						newTables[hashi] = cur;
						cur = next;
					}
				}
				_tables.swap(newTables);
			}

			size_t hashi = Func()(kof(data)) % _tables.size();
			Node* newnode = new Node(data);
			newnode->_next = _tables[hashi];
			_tables[hashi] = newnode;
			++_n;
			return make_pair(iterator(newnode, this), true);
		}

		iterator find(const Key& k)
		{
			size_t hashi = Func()(k) % _tables.size();
			Node* cur = _tables[hashi];
			while (cur)
			{
				if (kof(cur->_data) == k)
				{
					return iterator(cur, this);
				}
				cur = cur->_next;
			}
			return end();
		}


		bool erase(const Key& k)
		{
			size_t hashi = Func()(k) % _tables.size();
			Node* cur = _tables[hashi];
			Node* prev = nullptr;
			while (cur)
			{
				if (kof(cur->_data) == k)
				{
					if (prev == nullptr)//如果是头删
					{
						_tables[hashi] = cur->_next;
						delete cur;
					}
					else
					{
						prev->_next = cur->_next;
						delete cur;
					}
					--_n;
					return true;
				}
				else
				{
					prev = cur;
					cur = cur->_next;
				}

			}
			return false;
		}


	private:
		std::vector<Node*> _tables;
		size_t _n;	//有效数据个数
		KeyOfValue kof;//仿函数对象
	};
}