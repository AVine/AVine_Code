#pragma once

#include "OpenHash.h"//哈希表开散列

namespace ly
{

	// 因为哈希函数的缘故。映射位置是用数字映射的
		// 所以当要把自定义类型放到哈希表里面时
		// 需要一种算法提取出数字(Func)
	template <class Key, class Value, class HashFunc = Func<Key>>
	class unordered_map
	{
	public:
		struct KeyOfMap
		{
			Key operator()(const std::pair<Key, Value>& kv)
			{
				return kv.first;
			}
		};

		typedef typename HashTable<Key, std::pair<Key, Value>, KeyOfMap, HashFunc>::iterator iterator;
		iterator begin()
		{
			return _ht.begin();
		}
		iterator end()
		{
			return _ht.end();
		}

		bool insert(const std::pair<Key, Value>& kv)
		{
			return _ht.insert(kv);
		}

		Value& operator[](const Key& k)
		{
			std::pair<iterator, bool> ret = _ht.insert(make_pair(k, Value()));
			return ret.first->second;
		}
	private:
		HashTable<Key, std::pair<Key, Value>, KeyOfMap, HashFunc> _ht;
	};

}