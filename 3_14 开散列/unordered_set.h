#pragma once

#include "HashBucket.h"
#include <map>
using std::pair;
namespace ly
{
	//                          通常这个函数由上层提供
	template <class K,class HashFunc = Func<K>>
	class unordered_set
	{
	public:
		struct KeyOfSet
		{
			K operator()(const K& k)
			{
				return k;
			}
		};
		typedef typename HashBucket<K,K, KeyOfSet, HashFunc>::const_iterator iterator;

		template<class Iterator>
		unordered_set(Iterator begin, Iterator end)
		{
			while (begin != end)
			{
				insert(*begin);
				++begin;
			}
		}

		unordered_set(const unordered_set& um)
		{
			unordered_set tmp(um.begin(), um.end());
			swap(tmp);
		}

		unordered_set()
		{}

		void swap(unordered_set& um)
		{
			std::swap(_hb.get_tables(), um._hb.get_tables());
			std::swap(_hb.get_n(), um._hb.get_n());
		}

		iterator begin() const 
		{
			return _hb.begin();
		}
		iterator end() const
		{
			return _hb.end();
		}

		/*与set同样的问题，需要支持普通迭代器向const迭代器的转换*/
		pair<iterator, bool> insert(const K& k)
		{
			pair<typename HashBucket<K, K, KeyOfSet, HashFunc>::iterator, bool> p = _hb.insert(k);
			return pair<iterator, bool>(p.first, p.second);
		}

	private:
		HashBucket<K, K, KeyOfSet, HashFunc> _hb;
	};
}