#include <stdio.h>

#define FILE_NAME "bite"

int main()
{
    FILE* fp = fopen(FILE_NAME,"r");        //只读
    char buffer[64]={0};
    while(fgets(buffer,sizeof(buffer)-1,fp) != NULL)
    {
        printf("%s",buffer);        //不吃回车
    }
    //FILE* fp = fopen(FILE_NAME,"w");        //只写
    //int cnt = 5;
    //while(cnt)
    //{
    //    fprintf(fp,"%s:%d\n","linux so easy!",cnt--);
    //}
    fclose(fp);
    return 0;
}
