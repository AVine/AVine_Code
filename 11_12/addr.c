#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int addr = 100;
int main()
{
    pid_t id = fork();
    if(id < 0)
    {
            printf("错误\n");
            return 1;
    }
    else if(id == 0)
    {
        int cnt=0;
        while(1)
        {
            printf("子进程,pid:%d,ppid:%d,addr:%d,&addr:%p\n",getpid(),getppid(),addr,&addr);
            sleep(1);
            ++cnt;
            if(cnt == 10)
            {
                addr = 500;
                printf("子进程修改全局变量啦!\n");
            }
        }
    }
    else 
    {
        while(1)
        {
            
            printf("父进程,pid:%d,ppid:%d,addr:%d,&addr:%p\n",getpid(),getppid(),addr,&addr);
            sleep(2);
        }
    }
    return 0;
}
