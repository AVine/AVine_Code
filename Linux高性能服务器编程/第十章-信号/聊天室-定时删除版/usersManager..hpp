#pragma once

#include <arpa/inet.h>
#include "timerManager.hpp"
#include <memory>
#include <unordered_map>
#include <iostream>

#define BUFFER_SIZE 2048
class users
{
public:
    void set_sockfd(int sockfd) {_sockfd = sockfd;}
    void set_address(const sockaddr_in &address) {_client_address = address;}
    void set_timer(timer *tm) {_tm = tm;}
protected:
    int _sockfd;
    sockaddr_in _client_address;
    // char read_buffer[BUFFER_SIZE] = {0};
    // char write_buffer[BUFFER_SIZE] = {0};
    timer *_tm = nullptr; 
};


typedef std::shared_ptr<users> users_ptr;
class user_namager
{
public:
    void create_user(int sockfd,const sockaddr_in &address,timer *tm)
    {
        users_ptr up(new users);
        up->set_sockfd(sockfd);
        up->set_address(address);
        up->set_timer(tm);
        _sockfd_users.insert(std::make_pair(sockfd,up));
    }

    void del_user(int sockfd)
    {
        _sockfd_users.erase(sockfd);
    }

    users_ptr get_by_sockfd(int sockfd)
    {
        auto it = _sockfd_users.find(sockfd);
        if(it == _sockfd_users.end())
        {
            std::cout << "找不到sockfd对应的用户！返回空指针!" << std::endl;
            return users_ptr();
        }
        return it->second;
    }
protected:
    std::unordered_map<int,users_ptr> _sockfd_users;
};