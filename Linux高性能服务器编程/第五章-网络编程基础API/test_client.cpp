#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <iostream>
#include <functional>

// server
int main()
{
    // 0. 带外数据信号重定向
    // 1. 创建套接字
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    // 2. 连接
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    socklen_t len = sizeof(server);
    server.sin_family = AF_INET;
    server.sin_port = htons(9090);
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    connect(sockfd, (const struct sockaddr *)&server, len);
    //3. 读写数据
    char normal_dada[64] = "123";
    char urg_data = 'c';
    send(sockfd,normal_dada,strlen(normal_dada),0);
    send(sockfd,&urg_data,1,MSG_OOB);
    while(true);

    close(sockfd);
    return 0;
}
