#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>

// work_path:自定义工作目录;close:是否需要重定向到/dev/null
void Daemon(const char *work_path = "./",bool null = true)
{
    //1. 孤儿进程化
    if(fork() > 0) exit(0);
    //2. 创建一个新的回话
    setsid();
    //3. 改变工作目录
    int n = chdir(work_path);
    if(n == -1)
    {
        std::cout << "改变工作目录失败！" << std::endl;
    }
    std::cout << "当前工作目录:" << getcwd(nullptr,1024) << std::endl;
    //4. 判断是否需要重定向到/dev/null
    if(null)
    {
        close(0);
        close(1);
        close(2);

        open("/dev/null",O_RDWR);
        open("/dev/null",O_RDWR);
        open("/dev/null",O_RDWR);
    }
}