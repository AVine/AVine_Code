
//1. 测试syslog函数
// #include <syslog.h>
// int main()
// {
//     openlog("ttt",LOG_CONS,LOG_USER);
//     syslog(LOG_INFO,"测试一下我在干什么");
//     closelog();
//     return 0;
// }


//2. 测试真实用户和有效用户
// #include <sys/types.h>
// #include <unistd.h>
// #include <iostream>

// int main()
// {
//     int uid = getuid();
//     int euid = geteuid();
//     std::cout << "uid: " << uid << " euid: " << euid << std::endl;
//     return 0;
// }


//3. 测试用户切换
// #include <sys/types.h>
// #include <unistd.h>
// #include <iostream>

    
// bool exchage_user(uid_t uid,gid_t gid)
// {
//     //1. 不能再切换到root用户
//     if(uid == 0 && gid == 0)
//     {
//         std::cout << "目标用户又是root用户！阻止切换！" << std::endl;
//         return false;
//     }
//     //2. 判断当前用户是否合法：要么是root、要么是目标用户
//     uid_t cur_uid = getuid();
//     gid_t cur_gid = getgid();
//     if((cur_gid != 0 || cur_uid != 0) && (cur_gid != gid || cur_uid != uid))
//     {
//         std::cout << "当前用户不合法！" << std::endl;
//         return false;
//     }
//     //3. 判断当前用户是否已经是目标用户
//     if(cur_uid == uid && cur_gid == gid) 
//     {
//         std::cout << "当前用户已经是目标用户！无须切换！" << std::endl;
//         return false;
//     }
//     //4. 切换用户
//     if(setgid(gid) == -1 || setuid(uid) == -1)
//     {
//         return false;
//     }
//     return true;
// }
// int main()
// {
//     //1. 程序启动成功，当前的用户是root，切换到普通用户
//     std::cout << "程序启动成功！启动用户uid: " << getuid() << " 当前gid:" << getgid() << std::endl;
//     bool ret = exchage_user(1000,1000);//   切换到普通用户去
//     if(ret == false)
//     {
//         std::cout << "切换用户失败！" << std::endl;
//     }
//     std::cout << "当前用户uid:" << getuid() << " 当前gid: " << getgid() << std::endl;
//     return 0;
// }


//4. 测试系统资源限制
// #include <sys/resource.h>
// #include <iostream>
// int main()
// {
//     struct rlimit rl;
//     getrlimit(RLIMIT_CORE,&rl);
//     std::cout << "软上限:" << rl.rlim_cur << " 硬上限:" << rl.rlim_max << std::endl;
//     rl.rlim_cur = 1024;
//     setrlimit(RLIMIT_CORE,&rl);
//     std::cout << "软上限:" << rl.rlim_cur << " 硬上限:" << rl.rlim_max << std::endl;
//     return 0;
// }


//5. 测试工作目录
// #include <unistd.h>
// #include <iostream>

// int main()
// {
//     char *ret = getcwd(nullptr,1024);
//     std::cout << ret << std::endl;
//     return 0;
// }


#include "mydaemon.hpp"
//6. 测试守护进程
int main()
{
    Daemon("/");
    while(true);
    return 0;
}