#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <iostream>
#include <functional>

// client
int main()
{
    // 0. 带外数据信号重定向
    // 1. 创建套接字
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    // 2. 连接
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    socklen_t len = sizeof(server);
    server.sin_family = AF_INET;
    server.sin_port = htons(9090);
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    connect(sockfd, (const struct sockaddr *)&server, len);
    //3. 读写数据
    char buffer[4096] = {0};
    sprintf(buffer,"你好,吃了吗?");
    write(sockfd,buffer,strlen(buffer));
    char read_buf[1024] = {0};
    int n = read(sockfd,read_buf,sizeof(read_buf)-1);
    if(n > 0)
    {
        read_buf[n] = 0;
        std::cout << read_buf << std::endl;
    }
    
    close(sockfd);
    return 0;
}
