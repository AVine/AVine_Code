#pragma once


enum Corlor	//枚举颜色
{
	RED,BLACK
};

template <class K>	//为了方便，使用K模型
struct RBTreeNode
{
	RBTreeNode<K>* _left;
	RBTreeNode<K>* _right;
	RBTreeNode<K>* _parent;
	K _key;
	Corlor _cor;

	RBTreeNode(const K& key)
		:_left(nullptr),_right(nullptr),_parent(nullptr),
		_key(key),_cor(RED)
	{}
};


template <class K>
class RBTree
{
	typedef RBTreeNode<K> Node;
public:
	bool insert(const K& key)
	{
		if (_root == nullptr)
		{
			_root = new Node(key);
			_root->_cor = BLACK;	//根节点的颜色必须是黑色
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;
			}
		}

		cur = new Node(key);
		cur->_cor = RED;	//新插入的节点颜色为红色
		if (key < parent->_key)
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		else if (key > parent->_key)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}

		// 此时如果新插入的节点破坏了红黑树的性质，就必须做一些微调
		while (parent && parent->_cor == RED)
		{
			// 调整动作
			Node* grandfather = parent->_parent;
			if (grandfather->_left == parent)
			{
				Node* uncle = grandfather->_right;
				if (uncle && uncle->_cor == RED)
				{
					// 情况1

					parent->_cor = uncle->_cor = BLACK;
					grandfather->_cor = RED;
					
					cur = grandfather;
					parent = cur->_parent;
				}
				else	// unle不存在或uncle存在且为黑
				{
					if (parent->_left == cur)
					{
						// 情况2
						RotateR(grandfather);
						parent->_cor = BLACK;
						grandfather->_cor = RED;
					}
					else if(parent->_right == cur)
					{
						// 情况3
						RotateL(parent);
						RotateR(grandfather);
						cur->_cor = BLACK;
						grandfather->_cor = RED;
					}

					break;	//关键
				}
			}
			else if (grandfather->_right == parent)	//镜像即可
			{
				Node* uncle = grandfather->_left;
				if (uncle && uncle->_cor == RED)
				{
					parent->_cor = uncle->_cor = BLACK;
					grandfather->_cor = RED;

					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					if (parent->_right == cur)
					{
						// 情况2
						RotateL(grandfather);
						parent->_cor = BLACK;
						grandfather->_cor = RED;
					}
					else if (parent->_left == cur)
					{
						// 情况3
						RotateR(parent);
						RotateL(grandfather);
						cur->_cor = BLACK;
						grandfather->_cor = RED;
					}
					break;	//关键
				}
			}
		}

		_root->_cor = BLACK;	//确保根节点颜色为黑色
		return true;
	}


	bool isRBTree()
	{
		if (_root == nullptr)
		{
			return true;	//空树也可以是红黑树
		}

		if (_root->_cor != BLACK)
		{
			cout << "根节点不为黑色!" << endl;
			return false;	//根节点颜色不为黑色
		}

		// 计算任意一条路径的黑色节点数量
		int black_cnt = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_cor == BLACK)
			{
				++black_cnt;
			}
			cur = cur->_left;
		}

		return __isRBTree(_root, 0, black_cnt);
	}

	bool __isRBTree(const Node* root, int cnt, int key)
	{
		if (root == nullptr)
		{
			if (cnt != key)
			{
				cout << "每条路径的黑色节点数量不相等!" << endl;
				return false;
			}
			return true;
		}

		if (root->_cor == BLACK)
		{
			++cnt;
		}
		
		// 顺便判断是否存在连续的红节点
		Node* parent = root->_parent;
		if (parent && parent->_cor == RED && root->_cor == RED)
		{
			cout << "存在连续的红节点!" << endl;
			return false;
		}

		return __isRBTree(root->_left, cnt, key) && __isRBTree(root->_right, cnt, key);
	}

	void RotateL(Node* parent)
	{
		Node* cur = parent->_right;
		Node* curL = cur->_left;	//cur的左树

		parent->_right = curL;	//cur的左树变成parent的右树
		if (curL)
		{
			curL->_parent = parent;
		}

		Node* oldParent = parent->_parent;	//记录parent的父节点
		parent->_parent = cur;	//cur作为parent的父节点
		cur->_left = parent;	//parent作为cur的左树

		if (oldParent == nullptr)
		{
			_root = cur;	//直接让cur作为根节点(因为parent的旧父节点为空)
			cur->_parent = nullptr;
		}
		else
		{
			if (oldParent->_left == parent)
			{
				oldParent->_left = cur;
				cur->_parent = oldParent;
			}
			else if (oldParent->_right == parent)
			{
				oldParent->_right = cur;
				cur->_parent = oldParent;
			}
		}
	}

	void RotateR(Node* parent)
	{
		Node* cur = parent->_left;
		Node* curR = cur->_right;


		parent->_left = curR;	//cur的右树作为parent的左树
		if (curR)
		{
			curR->_parent = parent;
		}

		Node* oldParent = parent->_parent;
		parent->_parent = cur;
		cur->_right = parent;	//parent作为cur的右树

		if (oldParent == nullptr)
		{
			_root = cur;
			cur->_parent = nullptr;
		}
		else
		{
			if (oldParent->_left == parent)
			{
				oldParent->_left = cur;
				cur->_parent = oldParent;
			}
			else if (oldParent->_right == parent)
			{
				oldParent->_right = cur;
				cur->_parent = oldParent;
			}
		}
	}



	void level()
	{
		vector<vector<Node*>> vec;
		if (_root == nullptr)
		{
			return;
		}

		queue<Node*> q;
		q.push(_root);

		while (!q.empty())
		{
			int size = q.size();
			vector<Node*> tmp;
			while (size--)
			{
				Node* front = q.front();
				q.pop();

				if (front)
				{
					tmp.push_back(front);
				}
				else
				{
					tmp.push_back(nullptr);
				}

				if (front)
				{
					if (front->_left)
					{
						q.push(front->_left);
					}
					else
					{
						q.push(nullptr);
					}

					if (front->_right)
					{
						q.push(front->_right);
					}
					else
					{
						q.push(nullptr);
					}
				}

			}
			vec.push_back(tmp);


		}

		for (int i = 0; i < vec.size(); i++)
		{
			for (int j = 0; j < vec[i].size(); j++)
			{
				if (vec[i][j])
				{
					cout << vec[i][j]->_key << ":" << vec[i][j]->_cor << " ";
				}

				if (vec[i][j] == nullptr)
				{
					cout << "N" << " ";
				}
			}
			cout << endl;
		}
	}


private:
	Node* _root = nullptr;
};