#pragma once


enum Color
{
	RED,BLACK
};

template <class K>
struct RBTreeNode
{
	RBTreeNode<K>* _left;
	RBTreeNode<K>* _right;
	RBTreeNode<K>* _parent;

	Color _cor;
	K _key;

	RBTreeNode(const K& key)
		:_left(nullptr),_right(nullptr),_parent(nullptr),
		_cor(RED),_key(key)
	{}
};


template <class K>
class RBTree
{
	typedef RBTreeNode<K> Node;
public:
	
	bool insert(const K& key)
	{
		if (_root == nullptr)
		{
			_root = new Node(key);
			_root->_cor = BLACK;	//根节点为黑
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;
			}
		}

		cur = new Node(key);
		cur->_cor = RED;	//新增节点为红
		if (key < parent->_key)
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		else if (key > parent->_key)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}	//至此，插入完成

		// 处理
		while (parent && parent->_cor == RED)	//新增节点为红。若父节点也为红则需要处理
		{
			Node* gradFather = parent->_parent;
			if (gradFather->_left == parent)
			{
				Node* uncle = gradFather->_right;
				if (uncle && uncle->_cor == RED)	//如果叔叔存在且为红，情况一
				{
					parent->_cor = BLACK;
					uncle->_cor = BLACK;
					gradFather->_cor = RED;

					//情况一可以衍生出情况二或情况三，所以需要迭代
					cur = gradFather;
					parent = cur->_parent;
				}
				else
				{
					if (parent->_left == cur)	//gradFather、parent、cur为直线：情况二
					{
						RotateR(gradFather);
						parent->_cor = BLACK;
						gradFather->_cor = RED;
					}
					else if (parent->_right == cur)	//折线：情况三
					{
						RotateL(parent);
						RotateR(gradFather);
						cur->_cor = BLACK;
						gradFather->_cor = RED;
					}

					break;
				}
			}
			else if (gradFather->_right == parent)
			{
				Node* uncle = gradFather->_left;
				if (uncle && uncle->_cor == RED)
				{
					parent->_cor = BLACK;
					uncle->_cor = BLACK;
					gradFather->_cor = RED;

					cur = gradFather;
					parent = cur->_parent;
				}
				else
				{
					if (parent->_right == cur)	//情况二
					{
						RotateL(gradFather);
						parent->_cor = BLACK;
						gradFather->_cor = RED;
					}
					else if (parent->_left == cur)	//情况三
					{
						RotateR(parent);
						RotateL(gradFather);
						cur->_cor = BLACK;
						gradFather->_cor = RED;
					}

					break;
				}
			}
		}

		_root->_cor = BLACK;
		return true;
	}


	void RotateR(Node* parent)
	{
		Node* cur = parent->_left;
		Node* curR = cur->_right;


		parent->_left = curR;	//cur的右树作为parent的左树
		if (curR)
		{
			curR->_parent = parent;
		}

		Node* oldParent = parent->_parent;
		parent->_parent = cur;
		cur->_right = parent;	//parent作为cur的右树

		if (oldParent == nullptr)
		{
			_root = cur;
			cur->_parent = nullptr;
		}
		else
		{
			if (oldParent->_left == parent)
			{
				oldParent->_left = cur;
				cur->_parent = oldParent;
			}
			else if (oldParent->_right == parent)
			{
				oldParent->_right = cur;
				cur->_parent = oldParent;
			}
		}
	}


	void RotateL(Node* parent)
	{
		Node* cur = parent->_right;
		Node* curL = cur->_left;	//cur的左树

		parent->_right = curL;	//cur的左树变成parent的右树
		if (curL)
		{
			curL->_parent = parent;
		}

		Node* oldParent = parent->_parent;	//记录parent的父节点
		parent->_parent = cur;	//cur作为parent的父节点
		cur->_left = parent;	//parent作为cur的左树

		if (oldParent == nullptr)
		{
			_root = cur;	//直接让cur作为根节点(因为parent的旧父节点为空)
			cur->_parent = nullptr;
		}
		else
		{
			if (oldParent->_left == parent)
			{
				oldParent->_left = cur;
				cur->_parent = oldParent;
			}
			else if (oldParent->_right == parent)
			{
				oldParent->_right = cur;
				cur->_parent = oldParent;
			}
		}
	}


	void level()
	{
		vector<vector<Node*>> vec;
		if (_root == nullptr)
		{
			return;
		}

		queue<Node*> q;
		q.push(_root);

		while (!q.empty())
		{
			int size = q.size();
			vector<Node*> tmp;
			while (size--)
			{
				Node* front = q.front();
				q.pop();

				if (front)
				{
					tmp.push_back(front);
				}
				else
				{
					tmp.push_back(nullptr);
				}

				if (front)
				{
					if (front->_left)
					{
						q.push(front->_left);
					}
					else
					{
						q.push(nullptr);
					}

					if (front->_right)
					{
						q.push(front->_right);
					}
					else
					{
						q.push(nullptr);
					}
				}

			}
			vec.push_back(tmp);


		}

		for (int i = 0; i < vec.size(); i++)
		{
			for (int j = 0; j < vec[i].size(); j++)
			{
				if (vec[i][j])
				{
					cout << vec[i][j]->_key << ":" << vec[i][j]->_cor << " ";
				}
				
				if(vec[i][j] == nullptr)
				{
					cout << "N" << " ";
				}
			}
			cout << endl;
		}
	}




	void inorder()
	{
		inorder(_root);
	}

	void inorder(const Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		inorder(root->_left);
		cout << root->_key << " ";
		inorder(root->_right);
	}


	bool isRBTree()
	{
		if (_root == nullptr)	//空默认为黑
		{
			return true;
		}

		if (_root->_cor == RED)	//根节点必须是黑色
		{
			cout << "根节点不是黑色！" << endl;
			return false;
		}


		//统计任意一条路径的黑色节点数量(每条路径黑色节点数量必须相等)
		int black_size = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_cor == BLACK)
			{
				++black_size;
			}

			cur = cur->_left;
		}

		return isRBTree(_root, 0, black_size);
	}

	bool isRBTree(const Node* root, int k, int val)
	{
		if (root == nullptr)
		{
			if (k != val)
			{
				cout << "每条路径的黑色节点数量不相等！" << endl;
				cout << k << ":" << val << endl;
				return false;
			}
			return true;
		}

		if (root->_cor == BLACK)
		{
			++k;
		}

		// 顺便判断：不能存在连续的红色节点
		Node* parent = root->_parent;
		if (parent && parent->_cor == RED && root->_cor == RED)
		{
			cout << "存在连续的红色节点！" << endl;
			return false;
		}

		return isRBTree(root->_left, k, val) && isRBTree(root->_right, k, val);
	}
private:
	Node* _root = nullptr;
};