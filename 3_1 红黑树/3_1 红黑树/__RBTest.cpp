#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
#include <queue>
#include <algorithm>
#include <string>
using namespace std;
#include "__RBTree.h"


void test1()
{
	RBTree<int> rb;
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16,14 };
	for (auto& e : a)
	{
		rb.insert(e);
	}

	rb.level();
	//rb.inorder();
	cout << rb.isRBTree() << endl;
}


void test2()
{
	RBTree<int> rb;
	srand(time(nullptr));

	for (int i = 0; i < 100; i++)
	{
		int n = rand();
		rb.insert(n);
	}

	//rb.level();
	//cout << rb.isRBTree() << endl;

}
int main()
{
	test1();
	return 0;
}