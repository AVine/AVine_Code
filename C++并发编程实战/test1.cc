#include <thread>
#include <iostream>
#include <string>
#include <unistd.h>
#include "ScopeThread.hpp"

//class Functor
//{
//public:
//	void operator()()
//	{
//		std::cout << "Functor::operator()" << std::endl;
//	}
//};
//
//int main()
//{
//	//std::thread t(Functor());// 如果给的是匿名对象就会解析成函数声明
//	std::thread t{ Functor() };// 解决方案：用列表初始化
//	//t.detach();
//	if (t.joinable())
//	{
//		std::cout << "true" << std::endl;
//		t.join();
//	}
//	else
//	{
//		std::cout << "false" << std::endl;
//	}
//	return 0;
//}


//void change_data(int &data)
//{
//	data = 88;
//}
//
//void output_data(const int& data)
//{
//	std::cout << "data = " << data << std::endl;
//}
//int main()
//{
//	int data = 99;
//	std::thread t(change_data,data);
//	t.join();
//	output_data(data);
//	return 0;
//}

#include "ScopeThread.hpp"
#include <vector>

// void ThreadHandle(int id)
// {
// 	int n = 3;
// 	while (n--)
// 	{
// 		std::cout << id << ": " << "ThreadHandle(int id)" << std::endl;
// 		sleep(1);
// 	}
// }
// int main()
// {
// 	std::vector<ScopeThread> threads;
// 	for (int i = 0; i < 3; i++)
// 	{
// 		threads.push_back(std::thread(ThreadHandle, i + 1));
// 	}
// 	return 0;
// }


// int main()
// {
// 	size_t num = std::thread::hardware_concurrency();
// 	std::cout << num << std::endl;
// 	return 0;
// }

// #include <functional>
// void ThreadOneHandle()
// {
// 	while(true)
// 	{
// 		std::cout << std::this_thread::get_id() << std::endl;
// 		sleep(1);
// 	}
// }

// void test(int arr[10])
// {
// 	std::cout << sizeof(arr) << std::endl;
// }
// int main()
// {
// 	//std::vector<int> vec(std::string());
// 	std::vector<int> func(std::string p());
// 	int arr[10] = {0};
// 	test(arr);
// 	return 0;
// }
// std::string p()
// {
// 	return "";
// }


class Functor
{
public:
	void operator()()
	{
		std::cout << "我是线程的初始函数" << std::endl;
	}
};

int main()
{
	std::thread t{Functor()};// 强制高速编译器这是一个构造函数!
	t.join();
	return 0;
}
