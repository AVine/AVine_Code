#include <iostream>
#include <mutex>
#include <thread>
#include "ScopeThread.hpp"
#include <unistd.h>
// class Object
// {
// public:
//     void swap(Object &o1,Object &o2)
//     {
//         if(&o1 == &o2) return;// 相同的对象交换没有意义，甚至可能引起死锁
//         std::lock(o1._mutex,o2._mutex);// 一次性把两个锁锁住就可以避免死锁
//         std::unique_lock<std::mutex> lock1(o1._mutex,std::adopt_lock);
//         sleep(1);
//         std::unique_lock<std::mutex> lock2(o2._mutex,std::adopt_lock);
//         // 做一些交换
//         std::cout << "两个对象进行了交换!" << std::endl;
//     }
// private:
//     std::mutex _mutex;// 互斥元
// };
// int main()
// {
//     Object o1,o2;
//     ScopeThread st{std::thread([&](){o1.swap(o1,o2);})};
//     o1.swap(o2,o1);
//     return 0;
// }