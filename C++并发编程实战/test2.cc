#include "ThreadsafeStack.hpp"
#include <iostream>

int main()
{
    ThreadsafeStack<int> st;
    st.push(1);
    st.push(2);
    st.push(3);
    st.push(4);
    st.push(5);
    st.push(6);

    int x = 0;
    try
    {
        st.pop(x);
    }
    catch(std::exception &ex)
    {
        std::cout << "抛出异常: " << ex.what() << std::endl;
    }
    std::cout << "x = " << x << std::endl;

    std::shared_ptr<int> ptr;
    try
    {
        ptr = st.pop();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    st.push(7);
    st.push(8);

    if(ptr)
    {
        std::cout << "ptr->val = " << *ptr << std::endl;
    }
    else std::cout << "所指对象不存在!" << std::endl;
    
    return 0;
}