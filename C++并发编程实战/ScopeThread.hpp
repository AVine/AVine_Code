#pragma once
#include <thread>
#include <iostream>

class ScopeThread
{
public:	
	ScopeThread(std::thread &&t)// thread类本身就是移动构造
		:_t(std::move(t))
	{
		if (_t.joinable() == false)
		{
			std::cout << "没有关联的线程" << std::endl;
		}
	}

	ScopeThread(ScopeThread&& st)
		:_t(std::move(st._t))
	{}

	ScopeThread(ScopeThread& st)
		:_t(std::move(st._t))
	{}

	~ScopeThread()
	{
		if (_t.joinable())
		{
			_t.join();
		}
	}
private:
	std::thread _t;
};