#include <iostream>
#include <sstream>
#include <jsoncpp/json/json.h>

int main()
{
    //1. 创建json::Value对象
    Json::Value root;
    //2. 将内容放到Value对象当中
    root["姓名"] = "张三";
    root["年龄"] = 20;
    root["成绩"].append(88);
    root["成绩"].append(82);
    root["成绩"].append(75);
    //3. 创建Json::streamWriterBuilder对象
    Json::StreamWriterBuilder swb;
    //4. 创建Json::streamWriter对象
    Json::StreamWriter *sw = swb.newStreamWriter();
    //5. 将结构化数据送到io流中
    std::stringstream ss;
    sw->write(root,&ss);
    std::cout << ss.str() << std::endl;
    return 0;
}