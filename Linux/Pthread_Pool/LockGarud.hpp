#pragma once

#include <pthread.h>


class Mutex
{
public:
    Mutex(pthread_mutex_t* lock = nullptr)
        :_lock(lock)
    {}

    void lock()
    {
        if(_lock != nullptr)
        {
            pthread_mutex_lock(_lock);
        }
    }

    void unlock()
    {
        if(_lock != nullptr)
        {
            pthread_mutex_unlock(_lock);
        }
    }
private:
    pthread_mutex_t* _lock;
};

class LockGarud//RAII风格的锁
{
public:
    LockGarud(pthread_mutex_t* lock = nullptr)
        :_mtx(lock)
    {
        _mtx.lock();
    }

    ~LockGarud()
    {
        _mtx.unlock();
    }
private:
    Mutex _mtx;
};