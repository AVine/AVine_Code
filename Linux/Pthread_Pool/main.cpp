#include "Pthread.hpp"
#include "LockGarud.hpp"
#include <iostream>
#include "pthread_pool.hpp"

#include <unistd.h>
#include "task.hpp"

int main()
{
    pthread_pool<task>::getInstance()->run();
    int left = 0,right = 0;
    char op = 0;
    while(true)
    {
        std::cout << "左操作数# ";
        std::cin >> left;
        std::cout << "右操作数# ";
        std::cin >> right;
        std::cout << "操作符# ";
        std::cin >> op;

        task pt(left,right,op,task_math);
        // t.push(pt);// 外部推送任务
        pthread_pool<task>::getInstance()->push(pt);
        usleep(1234);
    }
    while(true);
    return 0;
}