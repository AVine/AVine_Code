#pragma once

#include <string>
#include <pthread.h>
#include <functional>
#include <assert.h>

class Pthread
{
private:
    static void* start_routine(void* args)
    {
        Pthread* _this = static_cast<Pthread*>(args);
        return _this->run();
    }

    void* run()
    {
        _func(_args);
        return nullptr;
    }
public:
    typedef std::function<void*(void*)> func_t;

    Pthread()
    {
        char buffer[64] = {0};
        snprintf(buffer,sizeof(buffer),"pthread %d",id++);
        _name = buffer;
    }

    void start(func_t func,void* args = nullptr)
    {
        _func = func;
        _args = args;
        pthread_create(&_tid,nullptr,start_routine,this);
    }

    void join()
    {
        pthread_join(_tid,nullptr);
    }

    std::string& name()
    {

        return _name;
    }
private:
    std::string _name;
    void* _args;//参数
    pthread_t _tid;
    func_t _func;//函数

    static size_t id;
};
size_t Pthread::id = 1;