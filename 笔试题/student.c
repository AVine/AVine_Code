#include<stdio.h>
#define MAX_STUDENT 50

int desion_value = 0;
typedef struct stu_data
{
	int number;
	float score;
}stu_data;

typedef struct student
{
	stu_data data_arr[MAX_STUDENT];
	int size;
}student;


void input_data(student* stud)
{
	int i = 0;
	stu_data temp_data;
	printf("请输入学号和成绩:> ");
	scanf("%d", &temp_data.number);
	desion_value = temp_data.number;
	if (desion_value == -1)
	{
		return;
	}
	scanf("%f",&temp_data.score);
	for (i = 0; i < stud->size; i++)
	{
		if (temp_data.number == stud->data_arr[i].number)
		{
			break;
		}		
	}
	if (i != stud->size)
	{
		printf("输入错误，该学号已存在,成绩更新\n");
	}
	stud->data_arr[stud->size++] = temp_data;
}

void sort_data(student* stud)
{
	stu_data  temp_data;
	int i, j;
	if (stud->size == 0)
	{
		printf("系统中还没有录入过书籍的信息\n\n");
		return;
	}
	for (i = 0; i < stud->size - 1; i++)
	{
		for (j = 0; j < stud->size - i - 1; j++)//冒泡排序 
		{
			if (stud->data_arr[j].score < stud->data_arr[j+1].score)
			{
				temp_data = stud->data_arr[j];
				stud->data_arr[j] = stud->data_arr[j + 1];
				stud->data_arr[j + 1] = temp_data;
			}
		}
	}
}

void show_data(student* stud)
{
	int i;
	if (stud->size == 0)
	{
		printf("系统中还没有录入信息\n\n");
		return;
	}
	printf("%-10s\t%-10s\n", "学号", "成绩");
	for (i = 0; i < stud->size; i++)
	{
		printf("%-10d\t%-10f\n",stud->data_arr[i].number,stud->data_arr[i].score);
	}
	printf("\n\n");
}

int main()
{
	student temp = {0};
	do
	{
		input_data(&temp);
		if (temp.size == MAX_STUDENT)
		{
			printf("已输入到达50个学生信息，输入完毕");
			break;
		}
	} while (desion_value != -1);
	sort_data(&temp);
	show_data(&temp);
	return 0;
}
