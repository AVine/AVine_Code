#include <stdio.h>
#include <stdlib.h>

struct Student
{
    int _id;// 学号
    double _score;// 成绩
};

#define MAX_NUM 50
struct Student stu_arr[MAX_NUM + 1];// 存储学生的数组(+1是为了方便映射)


int Compare(const void *left,const void *right)
{
    return ((struct Student *)right)->_score - ((struct Student *)left)->_score;
}

void ShowStudent()// 打印所有学生信息
{
    qsort(stu_arr + 1,MAX_NUM + 1,sizeof(struct Student),Compare);// 按成绩排序

    printf("result:\n");
    for(int i=1;i<=MAX_NUM;i++)
    {
        if(stu_arr[i]._id != 0)
        {
            printf("%d %.0lf\n",stu_arr[i]._id,stu_arr[i]._score);
        }
    }
}

int StuArrFull()// 判断stu_arr是否已满
{
    for(int i=1;i<=MAX_NUM;i++)
    {
        if(stu_arr[i]._id == 0) return 0;// 数组还没有满
    }
    return 1;// 没有id为0，表示数组已满
}

void InputsStudent()// 输入学生信息
{
    while(!StuArrFull())// 当数组未满时继续输入
    {
        int id = 0;
        double score = 0;

        scanf("%d",&id);// 输入学号
        if(id == -1) break;// 成绩输入完毕
        if(id > 50)
        {
            printf("输入的学号不合法!\n");
            break;
        }

        scanf("%lf",&score);// 输入成绩
        
        /*这里具有覆盖前面已存在的id的功能了*/
        stu_arr[id]._id = id;
        stu_arr[id]._score = score;
    }
    ShowStudent();// 打印一下
}

int main()
{
    InputsStudent();
    return 0;
}