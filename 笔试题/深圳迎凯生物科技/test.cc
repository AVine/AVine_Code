// 字符串求和："$123$%$456(789##"求数字之和

#include <vector>
#include <string>
#include <iostream>
#include <map>

// int Getsum(char *str)
// {
//     int sum = 0;
//     std::string s(str);
//     int left = 0,right = 0;
//     std::vector<std::string> vec;
//     while(right < s.size())
//     {
//         if(!isdigit(s[right]) && left == right)
//         {
//             ++right;++left;
//             continue;
//         }
//         while(isdigit(s[right])) ++right;
//         vec.push_back(s.substr(left,right));
//         left = right;
//     }
//     for(auto &e:vec)
//     {
//         sum += std::stoi(e);
//     }
//     return sum;
// }
// int main()
// {
//     char str[] = "$123$%$456(789##";
//     int ret = Getsum(str);
//     std::cout << "sum = " << ret << std::endl;
//     return 0;
// }


// 删除map当中的元素
int main()
{
    std::map<int,std::string> m;
    m.insert(std::make_pair(1,"11"));
    m.insert(std::make_pair(2,"22"));
    m.insert(std::make_pair(3,"33"));
    m.insert(std::make_pair(4,"44"));
    m.insert(std::make_pair(5,"55"));

    auto it = m.begin();
    while(it != m.end())
    {
        if(it->first == 2 || it->first == 3 || it->first == 4)
        {
            it = m.erase(it);
        }
        else it++;
    }

    for(auto &e:m)
    {
        std::cout << e.first << " ";
    }
    std::cout << std::endl;
    return 0;
}