

#include <vector>
#include <algorithm>
#include <iostream>

int Partation(std::vector<int>& v, int left, int right)
{
	int keyi = left;
	while (left < right)
	{
		while (left < right && v[right] >= v[keyi]) --right;// 一定是右边先走，让右边先停下来，right指向的位置要么无效，要么一定比key小
		while (left < right && v[left] <= v[keyi]) ++left;
		if (left < right) std::swap(v[left], v[right]);
	}
	// left == right，就说明区间构造完毕，left以左都是小于等于，right以右都是大于等于
	int meeti = left;
	std::swap(v[meeti], v[keyi]);
	return meeti;
}

void QuickSort(std::vector<int> &v,int left,int right)
{
	if (left >= right) return;
	int keyi = Partation(v, left, right);
	QuickSort(v, left, keyi - 1);
	QuickSort(v, keyi + 1, right);
}


int main()
{
	std::vector<int> v({ 45,1,478,324,875,132,6,36,87 });
	QuickSort(v,0,v.size()-1);
	for (auto& e : v)
	{
		std::cout << e << " ";
	}
	std::cout << std::endl;
	return 0;
}