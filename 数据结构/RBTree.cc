
#include <utility>
#include <iostream>

enum RBTreeColour
{
	RED,
	BLACK
};
template <class K,class V>
class RBTreeNode
{
public:
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;

	RBTreeColour _col;// 颜色
	std::pair<K, V> _data;

	RBTreeNode(const std::pair<K, V>& data)
		:_left(nullptr),_right(nullptr),_parent(nullptr),_col(BLACK),_data(data)
	{}
};

template <class K,class V>
class RBTree
{
using Node = RBTreeNode<K, V>;
public:
	RBTree()
		:_root(nullptr)
	{}

	bool insert(const std::pair<K, V>& data)
	{
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;// 根节点为黑色
			return true;
		}

		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (data.first < cur->_data.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (data.first > cur->_data.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;// 不考虑等于的情况
			}
		}
		
		cur = new Node(data);
		cur->_col = RED;// 新插入的节点颜色为红色
		if (cur->_data.first < parent->_data.first)
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		else if (cur->_data.first > parent->_data.first)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}

		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;// 祖父节点一定不为空，自己推
			if (parent == grandfather->_left)
			{
				Node* uncle = grandfather->_right;
				if (uncle && uncle->_col == RED)// 如果叔叔存在且为红
				{
					parent->_col = uncle->_col = BLACK;// 父亲和叔叔为黑
					grandfather->_col = RED;// 祖父为红
					
					// 向上反复
					cur = grandfather;
					parent = cur->_parent;
				}
				else// 如果叔叔不存在或者为黑
				{
					if (cur == parent->_left)// 祖父、父亲、cur都在一条线
					{
						// 右旋
						RotateR(grandfather);
						grandfather->_col = RED;
						parent->_col = BLACK;
					}
					else if(cur == parent->_right)// 不在一条线
					{
						// 先左旋然后右旋
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;// 不用继续向上反复了，因为旋转后子树的根节点一定为黑
				}
			}
			else
			{
				Node* uncle = grandfather->_left;
				if (uncle && uncle->_col == RED)// 如果叔叔存在且为红
				{
					parent->_col = uncle->_col = BLACK;// 父亲和叔叔为黑
					grandfather->_col = RED;// 祖父为红

					// 向上反复
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					if (cur == parent->_right)
					{
						// 左旋
						RotateL(grandfather);
						grandfather->_col = RED;
						parent->_col = BLACK;
					}
					else if(cur == parent->_left)
					{
						//右旋然后左旋
						RotateR(parent);
						RotateL(grandfather);
						grandfather->_col = RED;
						cur->_col = BLACK;
					}
					break;
				}
			}
		}

		_root->_col = BLACK;// 最后确保根节点颜色为黑
		return true;
	}

	void RotateR(Node* parent)
	{
		Node* cur = parent->_left;
		Node* curR = cur->_right;

		parent->_left = curR;
		if (curR) curR->_parent = parent;

		Node* oldparent = parent->_parent;
		parent->_parent = cur;
		cur->_right = parent;

		if (oldparent == nullptr)
		{
			_root = cur;
			_root->_parent = nullptr;
		}
		else
		{
			if (oldparent->_left == parent)
			{
				oldparent->_left = cur;
				cur->_parent = oldparent;
			}
			else if(oldparent->_right == parent)
			{
				oldparent->_right = cur;
				cur->_parent = oldparent;
			}
		}
	}

	void RotateL(Node* parent)
	{
		Node* cur = parent->_right;
		Node* curL = cur->_left;

		parent->_right = curL;
		if (curL) curL->_parent = parent;

		Node* oldparent = parent->_parent;
		parent->_parent = cur;
		cur->_left = parent;

		if (oldparent == nullptr)
		{
			_root = cur;
			_root->_col = BLACK;
		}
		else
		{
			if (oldparent->_left == parent)
			{
				oldparent->_left = cur;
				cur->_parent = oldparent;
			}
			else if(oldparent->_right == parent)
			{
				oldparent->_right = cur;
				cur->_parent = oldparent;
			}
		}
	}

	void inorder()
	{
		inorder(_root);
	}

	void inorder(Node* root)
	{
		if (root == nullptr) return;
		inorder(root->_left);
		std::cout << root->_data.first << "--" << root->_data.second << std::endl;
		inorder(root->_right);
	}

	bool IsRBTree()
	{
		return IsRBTree(_root);
	}

	bool IsRBTree(Node* root)
	{
		if (root == nullptr) return true;
		if (root->_col != BLACK) return false;

		int baseval = 0;
		Node* left = root;
		while (left)
		{
			if (left->_col == BLACK) ++baseval;
			left = left->_left;
		}
		return check(root,0,baseval);
	} 

	bool check(Node* root,int blackNum,const int &baseval)
	{
		if (root == nullptr)
		{
			if (blackNum != baseval)
			{
				std::cout << "违反规则：出现了黑色节点数量不等于最左路径的路径" << std::endl;
				return false;
			}
			return true;
		}
		if (root->_col == BLACK) ++blackNum;
		if (root->_col == RED && root->_parent && root->_parent->_col == RED)
		{
			std::cout << "违反规则：出现连续的红节点" << std::endl;
			return false;
		}
		return check(root->_left, blackNum,baseval) && check(root->_right, blackNum, baseval);
	}
private:
	Node* _root;// 根节点
};

int main()
{
	//int a[] = { 32,3,1,546,85,12,45,82,60,46,32,66 };
	//int a[] = { 63,86,478,21,94,60,36,26,5,553,35 };
	int a[] = { 96,720,57,47,58,12,65,69,62,72,721 };
	RBTree<int,int> t;
	for (auto& e : a)
	{
		t.insert(std::make_pair(e, e));
	}
	t.inorder();
	std::cout << t.IsRBTree() << std::endl;
	return 0;
}