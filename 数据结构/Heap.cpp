
#include <string.h>
#include <algorithm>
#include <iostream>

template <class T>
class Heap
{
protected:
	void adjust_up(int child_pos)
	{
		int parent_pos = (child_pos - 1) / 2;
		while (child_pos > 0)// 子节点非根节点时都可以向上调整
		{
			if (_a[child_pos] < _a[parent_pos])
			{
				std::swap(_a[child_pos], _a[parent_pos]);
				child_pos = parent_pos;
				parent_pos = (child_pos - 1) / 2;
			}
			else break;
		}
	}

	void adjust_down(int parent_pos)
	{
		int child_pos = parent_pos * 2 + 1;
		while (child_pos < _size)
		{
			if (child_pos + 1 < _size && _a[child_pos + 1] < _a[child_pos])
			{
				++child_pos;
			}
			if (_a[child_pos] < _a[parent_pos])
			{
				std::swap(_a[child_pos], _a[parent_pos]);
				parent_pos = child_pos;
				child_pos = parent_pos * 2 + 1;
			}
			else break;
		}
	}

public:

	void print()
	{
		for (int i = 0; i < _size; i++)
		{
			std::cout << _a[i] << " ";
		}
		std::cout << std::endl;
	}

	void reserve(int capacity)
	{
		if (capacity > _capacity)
		{
			T* tmp = new T[capacity];
			if (_a)
			{
				memcpy(tmp, _a, _size * sizeof(T));
				delete[] _a;
			}
			_a = tmp;
			_capacity = capacity;
		}
	}


	void push(const T &val)
	{
		if (_size == _capacity)
		{
			int new_capacity = _capacity == 0 ? 4 : _capacity * 4;
			reserve(new_capacity);
		}
		_a[_size++] = val;

		adjust_up(_size - 1);
	}



	void pop()
	{
		if (!empty())
		{
			_a[0] = _a[_size - 1];
			--_size;
			adjust_down(0);
		}
	}
	const T &top()
	{}

	bool empty()
	{
		return _size == 0;
	}
protected:
	T* _a = nullptr;
	int _size = 0;
	int _capacity = 0;
};


int main()
{
	int arr[] = { 23,15,23,66,72,90,45,91 };
	Heap<int> h1;
	for (auto& e : arr)
	{
		h1.push(e);
	}
	h1.print();

	h1.pop();
	h1.print();
	return 0;
}