#include <vector>
#include <algorithm>
#include <iostream>

template <class T>
struct Less
{
	bool operator()(const T& left, const T& right)
	{
		return left < right;
	}
};

template <class T>
struct Greater
{
	bool operator()(const T& left, const T& right)
	{
		return left > right;
	}
};
template <class T,class Container = std::vector<T>,class Compare = Less<T>>
class PriorityQueue
{
protected:
	void adjust_down(int parent_pos)
	{
		Compare com;
		int child_pos = parent_pos * 2 + 1;
		while (child_pos < _con.size())
		{
			//if (child_pos + 1 < _con.size() && _con[child_pos + 1] > _con[child_pos])
			if (child_pos + 1 < _con.size() && com(_con[child_pos], _con[child_pos + 1]))
			{
				++child_pos;
			}
			if (com(_con[parent_pos] , _con[child_pos]))
			{
				std::swap(_con[child_pos], _con[parent_pos]);
				parent_pos = child_pos;
				child_pos = parent_pos * 2 + 1;
			}
			else break;
		}
	}

	void adjust_up(int child_pos)
	{
		Compare com;
		int parent_pos = (child_pos - 1) / 2;
		while (child_pos > 0)
		{
			//if (_con[child_pos] > _con[parent_pos])
			if (com (_con[parent_pos], _con[child_pos]))

			{
				std::swap(_con[child_pos], _con[parent_pos]);
				child_pos = parent_pos;
				parent_pos = (child_pos - 1) / 2;
			}
			else break;
		}
	}
public:
	PriorityQueue() {}
	template <class it>
	PriorityQueue(it first, it second)
		:_con(first, second)
	{
		// 建堆
		//std::make_heap(_con.begin(), _con.end());
		for (int i = (_con.size() - 2) / 2; i >= 0; --i)
		{
			adjust_down(i);
		}
	}

	void push(const T& val)
	{
		_con.push_back(val);
		//std::push_heap(_con.begin(), _con.end());// 向上调整
		adjust_up(_con.size() - 1);
	}

	void pop()
	{
		std::swap(_con[0], _con[_con.size() - 1]);
		_con.pop_back();
		//std::pop_heap(_con.begin(), _con.end());
		adjust_down(0);
	}

	const T& top()
	{
		return _con[0];
	}

	bool empty()
	{
		return _con.empty();
	}

protected:
	std::vector<T> _con;
};


int main()
{
	std::vector<int> vec = { 345,123,456,12,9,47,67,75,678,257,864,132 };
	//PriorityQueue<int> pq(vec.begin(),vec.end());
	PriorityQueue<int,std::vector<int>,Greater<int>> pq(vec.begin(), vec.end());

	while (!pq.empty())
	{
		std::cout << pq.top() << " ";
		pq.pop();
	}
	std::cout << std::endl;
	return 0;
}