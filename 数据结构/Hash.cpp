
#include <vector>
#include <utility>
#include <string>
#include <iostream>
using namespace std;

//template <class T>
//struct HashFunc
//{
//	size_t operator()(const T &val)
//	{
//		return (size_t)val;
//	}
//};
//template <>
//struct HashFunc<std::string>
//{
//	size_t operator()(const std::string& val)
//	{
//		size_t sum = 0;
//		for (auto& e : val)
//		{
//			sum *= 131;
//			sum += e;
//		}
//		return sum;
//	}
//};
//
//
//template <class K,class V>
//struct HashNode
//{
//	std::pair<K, V> _node;
//	HashNode<K, V>* _next;
//	HashNode(const std::pair<K, V>& kv) :_node(kv) {}
//};
//
//template <class K,class V,class Hash = HashFunc<K>>
//class HashTable
//{
//	typedef HashNode<K, V> Node;
//public:
//	HashTable()
//		:_tables(10)
//	{}
//
//	~HashTable()
//	{
//		std::cout << "~HashTable()" << std::endl;
//		for (int i = 0; i < _tables.size(); i++)
//		{
//			Node* cur = _tables[i];
//			Node* next = nullptr;
//			while (cur)
//			{
//				next = cur->_next;
//				delete cur;
//				cur = next;
//			}
//			_tables[i] = nullptr;
//		}
//		_n = 0;
//	}
//
//	Node* find(const K& key)
//	{
//		Hash trans_to_i;
//		int hashi = trans_to_i(key) % _tables.size();
//		Node* cur = _tables[hashi];
//		while (cur)
//		{
//			if (cur->_node.first == key) return cur;
//			cur = cur->_next;
//		}
//		return nullptr;
//	}
//
//	void insert(const std::pair<K,V> &kv)
//	{
//		if (find(kv.first)) return;// 去重
//		Hash trans_to_i;
//
//		if (_n*10 / _tables.size() == 10)// 需要扩容
//		{
//			//HashTable<K, V> newTables;
//			//newTables._tables.resize(_tables.size() * 2);
//			//for (int i = 0; i < _tables.size(); i++)
//			//{
//			//	Node* cur = _tables[i];
//			//	while (cur)
//			//	{
//			//		newTables.insert(cur->_node);
//			//		cur = cur->_next;
//			//	}
//			//}
//			//_tables = std::move(newTables._tables);
//
//			HashTable<K, V> newTables;
//			newTables._tables.resize(_tables.size() * 2);
//			for (int i = 0; i < _tables.size(); i++)
//			{
//				Node* cur = _tables[i];
//				Node* next = nullptr;
//				while (cur)
//				{
//					next = cur->_next;
//					size_t hashi = trans_to_i(cur->_node.first) % newTables._tables.size();
//
//					// 头插
//					cur->_next = newTables._tables[hashi];
//					newTables._tables[hashi] = cur;
//
//					cur = next;
//				}
//				_tables[i] = nullptr;// 一定要置空，不置空就会重复析构
//			}
//			_tables.swap(newTables._tables);
//		}
//		int hashi = trans_to_i(kv.first) % _tables.size();
//		Node* newNode = new HashNode<K,V>(kv);
//		newNode->_next = _tables[hashi];
//		_tables[hashi] = newNode;
//		++_n;
//	}
//protected:
//	std::vector<Node *> _tables;
//	size_t _n;// 有效数据
//};
//
//int main()
//{
//	HashTable<int, int> ht;
//	int arr[] = { 11,21,1,3,13,23,5,15,25,16 };
//	for (auto& e : arr)
//	{
//		ht.insert(std::make_pair(e, e));
//	}
//	
//	//ht.~HashTable();
//	ht.insert(std::make_pair(6, 6));
//
//	//std::cout << ht.find(1)->_node.first << std::endl;
//	return 0;
//}



//class Solution{
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @return int整型
//     */
//    int InversePairs(vector<int>&nums) {
//        // write code here
//        vector<int> tmp(nums.size());
//        int ret = merge_sort(nums,0,nums.size() - 1,tmp);
//        for (int i = 0; i < nums.size(); i++)
//        {
//            cout << nums[i] << " ";
//        }
//        cout << endl;
//        return ret;
//    }
//
//    int merge_sort(vector<int> &nums,int begin,int end,vector<int> &tmp)
//    {
//        if (begin >= end) return 0;
//        int mid = (begin + end) / 2;
//        int left_num = merge_sort(nums,begin,mid,tmp);
//        int right_num = merge_sort(nums,mid + 1,end,tmp);
//
//        int begin1 = begin,end1 = mid;
//        int begin2 = mid + 1,end2 = end;
//        int i = begin;
//        int ret = 0;
//        while (begin1 <= end1 && begin2 <= end2)
//        {
//            if (nums[begin1] <= nums[begin2]) tmp[i++] = nums[begin1++];
//            else
//            {
//                ++ret;
//                tmp[i++] = nums[begin2++];
//            }
//        }
//
//        while (begin1 <= end1)
//        {
//            tmp[i++] = nums[begin1++];
//        }
//
//        while (begin2 <= end2)
//        {
//            tmp[i++] = nums[begin2++];
//        }
//
//        for (int i = begin; i <= end; i++) nums[i] = tmp[i];
//        return ret + left_num + right_num;
//    }
//};
//
//
//int main()
//{
//    Solution s;
//    vector<int> nums = { 1,2,3,4,5,6,7,0 };
//    s.InversePairs(nums);
//    return 0;
//}



class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 比较版本号
     * @param version1 string字符串
     * @param version2 string字符串
     * @return int整型
     */
    int compare(string version1, string version2) {
        // write code here
        int num1 = 0, num2 = 0;
        int v1 = 0, v2 = 0;
        int s1 = version1.size(), s2 = version2.size();
        if (s1 < s2) version1.resize(s2, '0');
        else version2.resize(s1, '0');
        while (v1 < version1.size() && v2 < version2.size())
        {
            if (version1[v1] != '.') num1 = num1 * 10 + (version1[v1] - '0');
            //else num1 = 0;
            if (version2[v2] != '.') num2 = num2 * 10 + (version2[v2] - '0');
            //else num2 = 0;
            if (num1 < num2) return -1;
            else if (num1 > num2) return 1;
            ++v1, ++v2;
        }
        return 0;
    }
};

int main()
{
    Solution s;
    s.compare("1.0.1", "1");
    return 0;
}