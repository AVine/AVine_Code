
#include <iostream>
#include <algorithm>

template <class T>
void adjust_down(T *_a,int parent_pos,int _size)
{
	int child_pos = parent_pos * 2 + 1;
	while (child_pos < _size)
	{
		if (child_pos + 1 < _size && _a[child_pos + 1] > _a[child_pos])
		{
			++child_pos;
		}
		if (_a[child_pos] > _a[parent_pos])
		{
			std::swap(_a[child_pos], _a[parent_pos]);
			parent_pos = child_pos;
			child_pos = parent_pos * 2 + 1;
		}
		else break;
	}
}

template <class T>
void heap_sort(T *arr,int size)
{
	//1. 将要排序的数组看成堆
	for (int i = (size - 1 - 1) / 2; i >= 0; i--)
	{
		adjust_down(arr, i, size);
	}
	//2. 排序
	for (int i = 1; i < size; i++)
	{
		std::swap(arr[0], arr[size - i]);
		adjust_down(arr, 0, size - i);
	}

}
int main()
{
	int arr[] = { 123,42,78,67,245,546,75,675,7,36,12,674,3547,36413,345 };
	heap_sort(arr, sizeof(arr) / sizeof(int));
	for (int i = 0; i < sizeof(arr) / sizeof(int); i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
	return 0;
}