#include <stdio.h>
#include <string.h>
#define FILE_NAME "log.txt"


#define ONE (1<<0)
#define TOW (1<<1)
#define THREE (1<<2)
#define FOUR (1<<3)


void show(int flag)
{
    if(flag & ONE) printf("ONE");
    if(flag & TOW) printf("TOW");
    if(flag & THREE) printf("THREE");
    if(flag & FOUR) printf("FOUR");
    printf("\n");
}
int main()
{
    FILE* fp = fopen(FILE_NAME,"w");
    if(fp == NULL) perror("fopen:");
    
    show(ONE);
    printf("-----------------------------\n");
    show(ONE | TOW);
    printf("-----------------------------\n");
    show(ONE | TOW | THREE);
    printf("-----------------------------\n");
    show(ONE | TOW | THREE | FOUR);
    printf("-----------------------------\n");





    
 //   char buff[64]={0};
 //   while(fgets(buff,sizeof(buff)-1,fp) != NULL)
 //   {
 //       buff[strlen(buff)-1]=0;
 //       printf("%s\n",buff);
 //   }
    //int cnt = 5;
    //while(cnt)
    //{
    //    fprintf(fp,"%s%d\n","write count:",cnt--);
    //}
    return 0;
}
