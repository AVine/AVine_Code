#define _CRT_SECURE_NO_WARNINGS 1



#include "string.h"
using namespace ly;



void test1()
{
	string s1("hello");
	string s2;


	//cout << s1.c_str() << endl;

	for (int i = 0; i < s1.size(); i++)
	{
		cout << s1[i];
	}
	cout << endl;

	//cout << s1[6] << endl;

	for (auto& e : s1)
	{
		cout << e;
	}
	cout << endl;

	

}

void test2()
{
	string s1("hello ");
	string s2("world");

	//s1.append("world");

	//cout << s1.c_str() << endl;

	//s1.push_back('!');

	//cout << s1.c_str() << endl;

	//char str[] = "nice";

	s1 += s2;
	cout << s1.c_str() << endl;

	//s1 += '!';
	//cout << s1.c_str() << endl;

	//s1.insert(s1.size(), "hello");
	//cout << s1.c_str() << endl;

	//s1.insert(s1.size(), 1,'c');
	//cout << s1.c_str() << endl;

	s1.erase(6, 3);
	cout << s1.c_str() << endl;




}


void test3()
{
	//string s1;

	///*s1.resize(7);
	//cout << s1.c_str() << endl;*/

	//s1.resize(20,'a');
	//cout << s1.c_str() << endl;
	//cout << s1.size() << endl;


	string s1("hello world");
	//s1.replace(6,4, "nice");
	//cout << s1.c_str() << endl;

	string s2("world");
	size_t ret = s1.find("ld");
	cout << ret << endl;


	cout << s1.find('e') << endl;
}


void test4()
{
	//string s1("hello world");

	//string s2(s1);

	//string s3;

	//s3 = s2;

	//cout << s1 << endl;
	//cout << s2 << endl;  
	//cout << s3 << endl;

	//cin >> s3;
	//cout << s3 << endl;

	string s1;
	//s1 = "nice";

	string s2("abcdeasdsad");

	s1 = s2;
	//cin >> s1;
	cout << s1 << endl;


	getline(cin, s1);
	cout << s1 << endl;

}

void test5()
{
	string s1("hello world");

	string s2(s1);
	cout << s2 << endl;

	string s3;
	s3 = s2;
	cout << s3 << endl;

	s3.erase(6, 20);
	cout<< s3 << endl;
}

int main()
{
	test5();
	return 0;
}