#pragma once


#include <assert.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;


namespace ly
{
	class string
	{
	public:
		typedef char* iterator;	//迭代器
		typedef const char* const_iterator;

		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return (_str + _size);
		}

		const_iterator begin() const
		{
			return _str;
		}

		const_iterator end() const
		{
			return (_str + _size);
		}


		string(const char* str = "")
		{
			size_t len = strlen(str);

			_size = _capacity = len;

			_str = new char[_capacity + 1];
			
			strcpy(_str, str);
			
		}


		//传统写法
		//string(const string& s)		//深拷贝
		//{
		//	_str = new char[s._capacity + 1];

		//	strcpy(_str, s._str);

		//	_size = s._size;
		//	_capacity = s._capacity;
		//}

		
		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		//现代写法(拷贝构造)
		string(const string& s)
			:_str(nullptr),_size(0),_capacity(0)
		{
			string tmp(s._str);	//构造
			swap(tmp);

		}


		//传统写法
		/*string& operator=(const string& s)
		{
			if (this != &s)
			{
				char* tmp = new char[s._capacity + 1];
				
				strcpy(tmp, s._str);
				
				delete[] _str;

				_str = tmp;

				_size = s._size;
				_capacity = s._capacity;
			}

			return *this;
		}*/


		//现代写法(赋值运算符重载)
		string& operator=(string s)
		{
			swap(s);
			return *this;
		}

		~string()
		{
			delete[] _str;
			_size = _capacity = 0;
		}
		
		size_t size() const
		{
			return _size;
		}


		size_t capacity() const
		{
			return _capacity;
		}

		
		const char* c_str() const
		{
			return _str;
		}

		char& operator[](size_t pos)
		{
			assert(pos <= _size && pos >= 0);
			return _str[pos];
		}

		const char& operator[](size_t pos) const
		{
			assert(pos <= _size && pos >= 0);
			return _str[pos];
		}


		void reserve(size_t n)
		{
			if (n > _capacity)
			{

				/*异地扩容*/
				char* tmp = new char[n + 1] {0};	//开辟一块新的空间
				strcpy(tmp, _str);		//拷贝

				delete[] _str;		//释放旧空间
				_str = tmp;		//更新指向

				_capacity = n;
			}
		}


		string& append(const string& s)
		{

			if (_size + s._size > _capacity)
			{
				reserve(_size + s._size);
			}

			strcpy(_str + _size, s.c_str());

			_size = _size + s._size;

			return *this;
		}


		void push_back(char c)
		{
			if (_size == _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				
				//reserve(_capacity * 2);	//如果 _capacity == 0 呢？

				reserve(newcapacity);
			}

			_str[_size++] = c;
		}


		string& operator+=(const string& s)
		{
			append(s);
			return *this;
		}

		string& operator+=(char c)
		{
			push_back(c);
			return *this;
		}


		string& insert(size_t pos, const string& s)
		{
			if (_size + s._size > _capacity)
			{
				reserve(_size + s._size);
			}

			size_t len = s.size();
			size_t end = _size + len;

			while (end > pos + len - 1)
			{
				_str[end] = _str[end - len];
				--end;
			}

			strncpy(_str + pos, s.c_str(), len);

			_size = _size + s._size;
			return *this;

		}


		string& insert(size_t pos, size_t n, char c)
		{
			if (_size + n > _capacity)
			{
				reserve(_size + n);
			}

			size_t len = n;
			size_t end = _size + len;

			while (end > pos + len - 1)
			{
				_str[end] = _str[end - len];
				--end;
			}

			while (n--)
			{
				_str[pos++] = c;
			}

			_size = _size + n;

			return *this;
		}


		string& erase(size_t pos = 0, size_t len = npos)
		{
			assert(pos >= 0 && pos < _size);

			if (len == npos || len >= _size - pos)
			{
				_str[pos] = '\0';
				_size -= _size - pos;

				return *this;
			}
			

			size_t end = pos + len;

			while (end <= _size)
			{
				_str[end - len] = _str[end];
				++end;
			}


			return *this;


		}


		void resize(size_t n,char c = '\0')
		{
			if (n < _size)
			{
				_str[n] = '\0';
			}
			else if (n > _size && n <= _capacity)
			{
				while (_size < n)
				{
					_str[_size++] = c;
				}
			}
			else if (n > _capacity)
			{
				reserve(n);

				while (_size < n)
				{
					_str[_size++] = c;
				}
			}

			_size = n;
		}


		void clear() 
		{
			//_size = _capacity = 0;	//不可将容量置0

			_size = 0;
			_str[0] = '\0';
		}


		string& replace(size_t pos, size_t len, const string& s)
		{

			assert(len <= _size - pos);


			if (_size - len + s._size > _capacity)
			{
				reserve(_size - len + s._size);
			}

			erase(pos, len);
			insert(pos, s);

			return *this;
		}


		size_t find(const string& s, size_t pos = 0) const
		{
			assert(pos < _size);

			const char* ret = strstr(_str, s.c_str());

			if (ret == nullptr)
			{
				return npos;
			}
			else
			{
				return ret-_str;
			}
		}


		size_t find(char c, size_t pos = 0) const
		{
			assert(pos < _size);

			while (pos < _size)
			{
				if (_str[pos] == c)
				{
					return pos;
				}
				++pos;
			}

			return npos;
		}

	private:
		char* _str;
		size_t _size;		//有效元素个数
		size_t _capacity;		//存储有效元素容量

		static const size_t npos = -1;		//只有整形常量才可写缺省值
	};


	std::ostream& operator<<(std::ostream& out, const string& s)
	{
		for (size_t i = 0; i < s.size(); i++)
		{
			out << s[i];
		}
		return out;
	}

	std::istream& operator>>(std::istream& in, string& s)
	{

		s.clear();

		//char ch = in.get();

		//while (ch != '\n' && ch != ' ')
		//{
		//	s += ch;	//势必会多次开辟空间
		//	ch = in.get();
		//}


		/*优化版本，减少开辟空间的次数*/
		char buff[128] = { 0 };
		int index = 0;
		
		char ch = in.get();
		while (ch != '\n' && ch != ' ')
		{
			if (index == 127)
			{
				s += buff;
				index = 0;

				memset(buff, 0, 128);
			}
			buff[index++] = ch;

			ch = in.get();
		}

		if (index > 0)
		{
			//buff[index] = '\0';
			s += buff;
		}

		return in;
	}


	std::istream& getline(std::istream& in, string& s)
	{
		s.clear();

		char buff[128] = { 0 };
		int index = 0;

		char ch = in.get();
		while (ch != '\n')
		{
			if (index == 127)
			{
				s += buff;
				index = 0;

				memset(buff, 0, 128);
			}
			buff[index++] = ch;

			ch = in.get();
		}

		if (index > 0)
		{
			//buff[index] = '\0';
			s += buff;
		}

		return in;
	}
}





