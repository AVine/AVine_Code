
//server.cpp
#include "command.h"
#include <unistd.h>

int main()
{
    //第一步，获取内核唯一key值
    key_t key = getKey();
    
    //第二步，使用key值创建共享内存
    int shmId = createShm(key);

    //第三步，与进程地址空间挂接
    char* shmAddr = (char*)attachProcess(shmId);

    //插曲：通信之前先创建命名管道
    createPipe();

    //创建好管道之后，以只读方式打开管道
    int pfd = open(PIPE_NAME,O_RDONLY);
    assert(pfd >= 0);

    char ch[10];
    while(true)//若从管道读到数据，则读取内存
    {
        int n = read(pfd,ch,sizeof(ch));
        if(n > 0)
        {
            //第四步，通信(读)
            std::cout << "server get #" << shmAddr << std::endl;
            sleep(1);
        }
        else if(n == 0)
        {
            std::cout << "pipe writed close" << std::endl;
            break;
        }
    }

    //第五步，去关联
    dettachProcess(shmAddr);

    //第六步，释放内存
    freeMemory(shmId);

    close(pfd);
    freePipe();
    return 0;
}