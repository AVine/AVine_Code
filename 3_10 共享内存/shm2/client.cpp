
// client.cpp
#include "command.h"
#include <unistd.h>
#include <cstdio>

int main()
{
    //第一步，获取内核唯一key值
    key_t key = getKey();
    
    //第二步，使用key值创建共享内存
    int shmId = getShm(key);

    //第三步，与进程地址空间挂接
    char* shmAddr = (char*)attachProcess(shmId);
    
    //打开管道文件
    int pfd = open(PIPE_NAME,O_WRONLY);
    assert(pfd >= 0);

    char sig = 'y';
    while(true)
    {
        //Ctrl+C是直接终止掉当前进程，剩下的指令不再执行
        //但是当Ctrl+C作为输入的话，cin应该会抛异常
        std::cin >> shmAddr;
        //插曲：写好之后，向管道发送一个信号
        write(pfd,&sig,sizeof(sig));
    }
    close(pfd);
    freePipe();
    //第五步，去关联
    dettachProcess(shmAddr);

    return 0;
}