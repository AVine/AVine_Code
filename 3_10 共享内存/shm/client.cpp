
//client作输入端

#include "command.h"
#include <unistd.h>

int main()
{
    key_t k = getKey();//首先获取唯一的key值
    printf("key: 0x%x\n", k);
    int shmid = getShm(k);//共享内存由server端创建，此时只需要获取其标识符即可
    printf("shmid: %d\n", shmid);

    char *start = (char*)attachShm(shmid);//然后通过其标识符挂接
    printf("attach success, address start: %p\n", start);

    const char* message = "hello server, 我是另一个进程，正在和你通信";
    pid_t id = getpid();
    int cnt = 1;


    //因为获得了内存了直接地址
    //所以像正常使用malloc一样使用这块空间
    while(true)
    {
        sleep(5);
        snprintf(start, MAX_SIZE, "%s[pid:%d][消息编号:%d]", message, id, cnt++);
    }

    detachShm(start);//最后不忘去关联

    //done

    return 0;
}