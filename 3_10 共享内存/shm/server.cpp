
#include "command.h"

#include <unistd.h>

int main()
{
    key_t k = getKey();//首先获取唯一key值
    printf("key: 0x%x\n", k);
    int shmid = getShm(k);//server端负责创建共享内存(这里写错了)
    printf("shmid: %d\n", shmid);

    char *start = (char*)attachShm(shmid);//然后挂接
    printf("attach success, address start: %p\n", start);

    const char* message = "hello server, 我是另一个进程，正在和你通信";
    pid_t id = getpid();
    int cnt = 1;
    
    //通信
    while(true)
    {
        sleep(5);
        snprintf(start, MAX_SIZE, "%s[pid:%d][消息编号:%d]", message, id, cnt++);
    }

    detachShm(start);//去关联

    //最后不忘释放内存
    //done

    return 0;
}