

#include <iostream>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <sys/ipc.h>
#include <sys/shm.h>

#define PATHNAME "."
#define PROJ_ID  0x66

#define MAX_SIZE 4097  

//要想让两个毫无关系的进程使用共享内存的方式进行通信
//先在内存当中建立一块由用户指定大小的内存块
//然后再想办法让这块内存地址映射到进程的虚拟地址

//在内存创建一块内存之前，首先要考虑如何让两个进程看到这块内存
//在内存创建一块内存是很简单的，例如malloc申请一块内存，然后返回它的起始地址
//但是我们要考虑，一个进程使用malloc得到地址后，另一个进程怎么办？
//所以我们需要指定一个路径、一个内存块的id(这俩感觉没啥用)
//然后通过系统调用ftok从内核中获取一个唯一标识符
//因为我们可以通过头文件的方式使得两个进程共享宏定义
//两个进程获得同一个唯一标识符(key)后，就可以使用这个key
//在内存创建一块共享内存了
key_t getKey()
{
    key_t k = ftok(PATHNAME, PROJ_ID);//使用ftok系统调用获取key 
    if(k < 0)//如果其key值为0，表获取失败
    {
        std::cerr << errno << ":" << strerror(errno) << std::endl;
        exit(1);
    }
    return k;//返回给调用此函数的进程
}


int getShmHelper(key_t k, int flags)//然后通过key值在内存创建一块共享内存
{
    //我们需要指定这块内存的大小，以及设置一些标志位
    //IPC_CREAT:如果内存当中还没有共享内存，创建；如果有，获取这块内存的标识符
    //IPC_EXCL:如果内存当中还没有共享内存，创建；如果有，报错
    //需要注意，IPC_EXCL不能够单独使用，必须IPC_CREAT|IPC_EXCL
    //这个标志位适用于必须创建一个新的共享内存的情况
    //shmget的返回值是用户使用的，仅仅是告诉我们已经创建好了
    //但是还没有通过页表映射到虚拟地址，让进程看见
    int shmid = shmget(k, MAX_SIZE, flags);
    if(shmid < 0)//如果标识符为-1，表创建失败
    {
        std::cerr << errno << ":" << strerror(errno) << std::endl;
        exit(2);
    }
    return shmid;
}

int getShm(key_t k)//如果当另一个进程创建好了共享内存，那么调用此接口的进程只需要获取其标识符即可
{
    return getShmHelper(k, IPC_CREAT);
}

int createShm(key_t k)//调用此接口的进程，是希望创建一个新的共享内存
{
    return getShmHelper(k, IPC_CREAT | IPC_EXCL | 0600);
}


//创建好共享内存之后，需要将物理地址映射到进程的虚拟地址
//这个过程称为挂接
void *attachShm(int shmid)//需要通过标识符映射
{
    //shmat负责映射。后两个参数暂时没啥用
    //shmat和malloc非常类似，都是获取内存地址
    void *mem = shmat(shmid, nullptr, 0); 
    if((long long)mem == -1L)//Linux操作系统是64位的，注意转换
    {
        std::cerr <<"shmat: "<< errno << ":" << strerror(errno) << std::endl;
        exit(3);
    }
    return mem;
}

//当进程退出之前，需要取消映射
//这个过程称为去关联
void detachShm(void *start)
{
    if(shmdt(start) == -1)//通过指定的内存地址去关联
    {
        std::cerr <<"shmdt: "<< errno << ":" << strerror(errno) << std::endl;
    }
}

//最后不要忘记释放这块共享内存
void delShm(int shmid)
{
    //shmctl并不是释放内存的接口，它是一个控制接口
    //可以通过设置标志位达到释放内存的效果
    if(shmctl(shmid, IPC_RMID, nullptr) == -1)
    {
        std::cerr << errno << " : " << strerror(errno) << std::endl;
    }
}