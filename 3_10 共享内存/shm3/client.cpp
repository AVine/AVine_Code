//client.cpp

#include "command.h"
#include <unistd.h>
//假设client端负责写
int main()
{
    key_t key = getKey();//获取IPC资源唯一标识符
    int shmid = getShm(key);//创建共享内存
    char* p = (char*)attachShm(shmid);//挂接

    //通信
    while(true)
    {
        strcpy(p,"hello i am client!");
        sleep(3);
    }

    deAttachShm(p);//去关联
    return 0;
}