//server.cpp

#include "command.h"
#include <unistd.h>

//假设server端负责创建、删除内存；负责读
int main()
{
    key_t key = getKey();//获取IPC资源唯一标识符
    int shmid = createShm(key);//创建共享内存
    char* p = (char*)attachShm(shmid);//挂接

    //通信
    while(true)
    {
        std::cout << "client -> server:" << p << std::endl;
        sleep(1);
    }

    deAttachShm(p);//去关联
    delShm(shmid);//删除
    return 0;
}