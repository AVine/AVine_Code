#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
int main()
{
    pid_t id = fork();
    while(1)
    {
    if(id == 0)
    {
        printf("我是子进程,pid:%d,ppid:%d\n",getpid(),getppid());
        sleep(1);
    }
    else if(id > 0)
    {
        printf("我是父进程,pid:%d,ppid:%d\n",getpid(),getppid());
        sleep(2);
    }
    }
    return 0;
}
