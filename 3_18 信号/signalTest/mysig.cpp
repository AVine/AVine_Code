
#include <iostream>
using namespace std;
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

void sigcb(int signo)
{
    cout << "捕捉到信号:" << signo << endl;
}
int main()
{
    signal(SIGINT,sigcb);
    while(true)
    {
        cout << "我是一个进程 pid:" << getpid() << endl;
        sleep(1);
    }
    return 0;
}