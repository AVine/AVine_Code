
// #include <iostream>
// #include <vector>
// using namespace std;
// #include <signal.h>
// #include <unistd.h>

// static vector<int> sig(31,1);//31个信号

// static void myHandler(int signo)
// {
//     cout << "get signo:" << signo << endl;
// }

// static void show_pending(const sigset_t& sig)
// {
//     // 左边是高位，先打印高位
//     for(int i=31;i>=0;i--)
//     {
//         if(sigismember(&sig,i))//如果i号信号在pending信号级中
//         {
//             cout << "1";
//         }
//         else 
//         {
//             cout << "0";
//         }
//     }
//     cout << endl;
// }

// int main()
// {
//     for(int i=1;i<sig.size();i++)
//     {
//         sig[i] = sig[i-1] + 1;//1~31号信号
//     }

//     sigset_t block,oblock,pending;

//     //初始化
//     sigemptyset(&block);
//     sigemptyset(&oblock);
//     sigemptyset(&pending);
    
//     for(auto& e:sig)
//     {
//         sigaddset(&block,e);//将1~31号信号设置到用户层信号级中
//     }

//     //将1~31号信号覆盖pcb的block信号级
//     sigprocmask(SIG_SETMASK,&block,&oblock);

//     for(auto& e:sig)
//     {
//         signal(e,myHandler);
//     }

//     while(true)
//     {
//         sigpending(&pending);//获取pending信号级
//         show_pending(pending);
//         sleep(1);
//     }
//     return 0;
// }


#include <iostream>
#include <cstdio>
using namespace std;
#include <signal.h>
#include <unistd.h>

//倒计时程序
void Count(int cnt)
{
    while(cnt)
    {
        printf("cnt:%2d\r",cnt--);
        fflush(stdout);
        sleep(1);
    }
}

void sigcb(int signo)
{
    cout << "get signo:" << signo << endl;
    Count(5); //倒计时5秒
}

int main()
{
    struct sigaction act,oact;//设置结构体对象

    //初始化
    act.sa_flags = 0;
    act.sa_handler = sigcb;
    sigemptyset(&act.sa_mask);
    //sigaddset(&act.sa_mask,3);//屏蔽3号信号

    sigaction(2,&act,&oact);//对2号信号捕捉

    while(true);
    return 0;
}