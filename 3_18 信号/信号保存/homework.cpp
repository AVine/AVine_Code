

#include <iostream>
#include <vector>
using namespace std;
#include <signal.h>
#include <unistd.h>

static vector<int> sig = {2,40};//屏蔽2号和40号信号

void myHandler(int signo)
{
    cout << signo << "号信号" << endl;
}

void show_pending(const sigset_t& sig)
{
    for(int i=63;i>=1;--i)
    {
        if(sigismember(&sig,i))//检测1~31号是否被设置到pending中
        {
            cout << "1";
        }
        else 
        {
            cout << "0";
        }
    }
    cout << endl;
}
int main()
{
    for(auto& e:sig)
    {
        signal(e,myHandler);//设置2、40号信号的自定义行为
    }
    
    sigset_t block,oblock,pending;//用户层位图
    sigemptyset(&oblock);
    sigemptyset(&block);
    
    for(auto& e:sig)
    {
        sigaddset(&block,e);//添加信号
    }

    sigprocmask(SIG_SETMASK,&block,&oblock);//屏蔽

    int cnt = 30;
    while(true)
    {
        sigpending(&pending);
        show_pending(pending);
        sleep(1);
        if(cnt == 0)
        {
            sigprocmask(SIG_SETMASK,&oblock,&block);//取消屏蔽
        }
        --cnt;
    }

    return 0;
}