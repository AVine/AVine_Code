
#include <iostream>
#include <vector>
using namespace std;
#include <signal.h>
#include <unistd.h>

static vector<int> vec = {2};//假设我们要对2号信号屏蔽 

void show_pending(const sigset_t& sig)
{
    for(int i=31;i>=1;--i)
    {
        if(sigismember(&sig,i))//检测1~31号是否被设置到pending中
        {
            cout << "1";
        }
        else 
        {
            cout << "0";
        }
    }
    cout << endl;
}

void myHandler(int signo)
{
    cout << signo << "号信号被取消屏蔽" << endl;
}

int main()
{
    for(auto& sig:vec)
    {
        signal(sig,myHandler);
    }

    sigset_t block,oblock,pending;//用户层位图

    //初始化
    sigemptyset(&block);
    sigemptyset(&oblock);
    sigemptyset(&pending);

    //将我们的要求的信号，添加到位图里面来
    for(auto& sig:vec)
    {
        sigaddset(&block,sig);
    }

    //通过系统调用将用户层位图设置到pcb里面去
    sigprocmask(SIG_SETMASK,&block,&oblock);

    int cnt = 10;
    while(true)
    {
        //我们要将pcb中的pending集提取到用户层
        sigpending(&pending);
        show_pending(pending);
        sleep(1);

        if(cnt == 0)
        {
            //当计数器到位后，我们想取消屏蔽
            sigprocmask(SIG_SETMASK,&oblock,&block);
            cout << "信号屏蔽取消" << endl;
        }
        --cnt;
    }
    return 0;
}