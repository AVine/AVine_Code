
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
int quit = 0;

//倒计时程序
void Count(int cnt)
{
    while(cnt--)
    {
        printf("cnt:%2d\r",cnt);
        fflush(stdout);
        sleep(1);
    }
    printf("\n");
}

void myHandler(int signo)
{
    printf("get signo:%d\n",signo);
    printf("quit %d->",quit);
    quit = 1;
    printf("%d\n",quit);
    Count(5);
}

int main()
{
    signal(2,myHandler);
    while(!quit);
    printf("正常退出程序!\n");
    return 0;
}