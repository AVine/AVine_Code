
#include <iostream>
using namespace std;
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

void mySig(int signo)
{
    cout << "收到信号：" << signo << endl;
    sleep(1);
}
int main()
{
    signal(8,mySig);
    while(true)
    {
        cout << "正在运行...pid:" << getpid() << endl;
        sleep(1);
        int a = 3;
        a /= 0;
    }
    return 0;
}

// int main()
// {
//     int a = 3;
//     a /= 0;
//     return 0;
// }