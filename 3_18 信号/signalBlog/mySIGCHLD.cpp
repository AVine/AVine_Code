
#include <iostream>
using namespace std;
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

// void myHandler(int signo)
// {
//     cout << "get signo:" << signo << endl;
// }
int main()
{
    signal(17,SIG_IGN);
    pid_t id = fork();
    if(id == 0)
    {
        cout << "i am a child..." << endl;
        sleep(3);
        exit(1);
    }
    while(1);//让父进程不退出，否则父进程一定先比子进程退出
    
    //waitpid(id,nullptr,0);
    return 0;
}