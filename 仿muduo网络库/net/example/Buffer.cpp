#include "../Buffer.hpp"
#include <iostream>


int main()
{
    std::string str = "hello!!";
    Buffer buf;
    // buf.Write(str);
    // std::cout << buf.ReadableSpaceSize() << std::endl;// 测试写入之后，可读数据个数

    // std::string out;
    // buf.Read(out);
    // std::cout << out << std::endl;
    // std::cout << buf.ReadableSpaceSize() << std::endl;// 测试读出之后，可读数据个数

    for(int i=1;i<=300;i++)
    {
        std::string str = "hello!!" + std::to_string(i) + "\n";
        buf.Write(str);
    }
    std::cout << buf.ReadableSpaceSize() << std::endl;// 测试写入之后，可读数据个数

    std::string out;
    buf.Read(out,false);
    std::cout << out << std::endl;
    std::cout << buf.ReadableSpaceSize() << std::endl;// 测试读出之后，可读数据个数
    return 0;
}