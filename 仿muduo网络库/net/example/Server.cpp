#include "../Server.hpp"
#include <iostream>

void connected_handle(const ConnectionPtr &cp)
{
    std::cout << cp->GetSockfd() << "号连接已连接!" << std::endl;
}

void message_handle(const ConnectionPtr &cp,Buffer *buf)
{
    std::string str;
    buf->Read(str);
    DEBUG_LOG("接收到新消息: %s",str.c_str());

    cp->Send((void *)str.c_str(),str.size());
}

int main()
{
    TcpServer server(9090);
    server.SetThreadCount(2);
    server.SetConnectedCallBack(connected_handle);
    server.SetMessageCallBack(message_handle);
    server.Start();
    return 0;
}