#include "../TimerWheel.hpp"
#include <iostream>
#include <unistd.h>

class Test
{
public:
    Test()
    {
        std::cout << "Test()" << std::endl;
    }

    ~Test()
    {
        std::cout << "~Test()" << std::endl;
    }
};

void callback(Test *t)
{
    delete t;
}

int main()
{
    TimerWheel tw;
    Test *t = new Test();
    tw.AddTimer(1,std::bind(callback,t),5);
    //tw.TimerDel(1);   
    while(true)
    {
        //tw.TimerRefresh(1);
        std::cout << "------------------" << std::endl;
        tw.tick();
        sleep(1);
    }
    return 0; 
}