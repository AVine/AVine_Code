#include <sys/epoll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

int main()
{
    int listenfd = socket(AF_INET,SOCK_STREAM,0);
    
    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(9090);
    server.sin_addr.s_addr = INADDR_ANY;
    int n = bind(listenfd,(const sockaddr *)&server,sizeof(server));

    n = listen(listenfd,5);
    struct sockaddr_in client;
    socklen_t len = sizeof(client);

    int connfd = accept(listenfd,(sockaddr *)&client,&len);
    printf("接收到连接: %d\n",connfd);

    int epfd = epoll_create(1);
    epoll_event ev;
    ev.data.fd = connfd;
    ev.events = EPOLLIN;
    n = epoll_ctl(epfd,EPOLL_CTL_MOD,connfd,&ev);
    if(n == -1)
    {
        printf("%s\n",strerror(errno));
        abort();
    }
    epoll_event events[1024] = {0};
    while(true)
    {
        int ret = epoll_wait(epfd,events,sizeof(events),-1);
        if(ret > 0 )
        {
            printf("有事件触发!");
        }
    }
    

    close(listenfd);
    return 0;
}