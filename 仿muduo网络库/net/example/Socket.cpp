#include "../Server.hpp"

#include <assert.h>

int main()
{
    Socket s;
    int ret = s.Create();
    assert(ret >= 0);

    ret = s.Bind(9090);
    assert(ret != -1);

    ret = s.Listen();
    assert(ret != -1);

    int connfd = s.Accept();
    assert(connfd != -1);

    Socket ss(connfd);
    char buffer[1024] = {0};
    ret = ss.Recv(buffer,sizeof(buffer));
    if(ret > 0)
    {
        buffer[ret] = 0;
        DEBUG_LOG("get new message: %s",buffer);

        ret = ss.Send(buffer,strlen(buffer),0);
        if(ret > 0)
        {
            DEBUG_LOG("send message: %s",buffer);
        }
    }

    s.Close();
    ss.Close();
    return 0;
}