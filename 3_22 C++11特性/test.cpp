#define _CRT_SECURE_NO_WARNINGS 1


//右值引用：解决左值引用无法解决返回值问题

#include <cassert>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
#include <map>
#include <array>
#include <functional>
using namespace std;
using namespace placeholders;

int func(int x, int y)
{
	return x + y;
}

int sub(int x, int y)
{
	return x - y;
}

struct Func
{
	int operator()(int x, int y)
	{
		return x + y;
	}
};

struct Test
{
	int add(int x, int y)
	{
		return x + y;
	}
};

int main()
{
	//包装器
	auto FUNC = [](int x, int y) {return x + y; };//lambda表达式
	function<int(int, int)> f1 = func;//函数指针
	cout << f1(1, 2) << endl;
	
	function<int(int, int)> f2 = Func();//仿函数
	cout << f2(1, 2) << endl;

	function<int(int, int)> f3 = FUNC;//lambda表达式
	cout << f3(1, 2) << endl;

	function<int(Test,int, int)> f4 = &Test::add;//非静态成员函数必须加&
	cout << f4(Test(), 1, 2) << endl;

	function<int(int, int)> f5 = std::bind(sub, _1, _2);
	cout << f5(1, 2) << endl;

	function<int(int, int)> f6 = std::bind(sub, _2, _1);
	cout << f6(1, 2) << endl;

	function<int(int, int)> f7 = std::bind(&Test::add,Test(), _1, _2);
	cout << f7(1, 2) << endl;
	return 0;
}

//namespace ly
//{
//	class string
//	{
//	public:
//		typedef char* iterator;
//		iterator begin()
//		{
//			return _str;
//		}
//		iterator end()
//		{
//			return _str + _size;
//		}
//		string(const char* str = "")
//			:_size(strlen(str))
//			, _capacity(_size)
//		{
//			//cout << "string(char* str)" << endl;
//			_str = new char[_capacity + 1];
//			strcpy(_str, str);
//		}
//		// s1.swap(s2)
//		void swap(string& s)
//		{
//			::swap(_str, s._str);
//			::swap(_size, s._size);
//			::swap(_capacity, s._capacity);
//		}
//		// 拷贝构造
//		string(const string& s)
//			:_str(nullptr)
//		{
//			cout << "string(const string& s) -- 深拷贝" << endl;
//			string tmp(s._str);
//			swap(tmp);
//		}
//
//		//右值引用分为两种：字面值常量 --> 纯右值
//		//				自定义类型 --> 将亡值
//		string(string&& s)//传入将亡值
//		{
//			cout << "string(const string& s) -- 移动构造" << endl;
//			swap(s);//直接转移资源，不需要拷贝了
//		}
//		// 赋值重载
//		string& operator=(const string& s)
//		{
//			//cout << "string& operator=(string s) -- 深拷贝" << endl;
//			string tmp(s);
//			swap(tmp);
//			return *this;
//		}
//		string& operator=(string&& s)
//		{
//			cout << "string& operator=(string s) -- 移动赋值" << endl;
//			swap(s);
//			return *this;
//		}
//		~string()
//		{
//			delete[] _str;
//			_str = nullptr;
//		}
//		char& operator[](size_t pos)
//		{
//			assert(pos < _size);
//			return _str[pos];
//		}
//		void reserve(size_t n)
//		{
//			if (n > _capacity)
//			{
//				char* tmp = new char[n + 1];
//				strcpy(tmp, _str);
//				delete[] _str;
//				_str = tmp;
//				_capacity = n;
//			}
//		}
//		void push_back(char ch)
//		{
//			if (_size >= _capacity)
//			{
//				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
//				reserve(newcapacity);
//			}
//			_str[_size] = ch;
//			++_size;
//			_str[_size] = '\0';
//		}
//		//string operator+=(char ch)
//		string& operator+=(char ch)
//		{
//			push_back(ch);
//			return *this;
//		}
//		const char* c_str() const
//		{
//			return _str;
//		}
//	private:
//		char* _str = nullptr;
//		size_t _size = 0;
//		size_t _capacity = 0; // 不包含最后做标识的\0
//	};
//
//	string to_string(int value)
//	{
//		bool flag = true;
//		if (value < 0)
//		{
//			flag = false;
//			value = 0 - value;
//		}
//		string str;
//		while (value > 0)
//		{
//			int x = value % 10;
//			value /= 10;
//			str += ('0' + x);
//		}
//		if (flag == false)
//		{
//			str += '-';
//		}
//			std::reverse(str.begin(), str.end());
//		return str;
//	}
//
//	void test1()
//	{
//		//string ret = to_string(-1234);//本来是两次拷贝构造，但是新编译器会优化成一次拷贝构造
//
//		//这种方式编译器无法进行优化
//		//string ret;
//		//ret = to_string(-1234);
//
//		//即使编译器能够把两次拷贝构造优化成一次拷贝构造，但是当对象过大时，效率还是比较低
//		//使用右值引用间接地解决这个问题(左值引用无法优化传值返回的问题)
//		//string ret = to_string(-1234);
//
//		//string ret;
//		//ret = to_string(-1234);
//
//		string s1("111111");
//		string s2(s1);//调用左值引用版本的拷贝构造
//
//		string s3;
//		s3 = string("22222");//调用右值引用版本的赋值重载
//
//		string s4;//调用右值引用版本的赋值重载
//		s4 = move(s1);//s1本来是个左值，但是move之后把s1看成右值，所以这里直接发生资源转移/交换
//
//		//总结：右值引用能够解决两个问题：
//		//1.解决左值引用无法解决的传值返回引发的深拷贝
//		//2.解决左值引用引用右值作为拷贝构造/赋值重载的形参无法优化的深拷贝(右值引用能够避免不必要的深拷贝)
//	}
//
//	void test2()
//	{
//		string ret = to_string(-1234);
//	}
//}
//
//
//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//
//
//	//析构、拷贝构造、赋值重载都不实现的话，编译器会生成一个默认移动构造(没有实现移动构造的话)
//	//析构、拷贝构造、赋值重载都不实现的话，编译器会生成一个默认移动赋值(没有实现移动赋值的哈)
//
//	
//	Person(const Person& p)
//	:_name(p._name)
//	,_age(p._age)
//	{}
//
//	Person& operator=(const Person& p)
//	{
//		if(this != &p)
//		{
//			_name = p._name;
//			_age = p._age;
//		}
//		return *this;
//	}
//
//	~Person()
//	{}
//
//	//但是显式定义了析构、拷贝构造、赋值重载中的任意一个，就不会生成默认的
//	//但是可以通过default关键字强制生成
//	Person(Person&& p) = default;
//	Person& operator=(Person&& p) = default;
//private:
//	ly::string _name;
//	int _age;
//};
//
//
//int main()
//{
//	Person p1("张三", 20);
//	Person p2(move(p1));//默认移动构造对内置类型值拷贝、对自定义类调用自己的移动构造
//	p1 = Person("李四", 21);//移动赋值
//	return 0;
//}



//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//
//template <class T>
//void func(T&& t)//函数模板的万能引用
//{
//	//Fun(t);//但是万能引用不管引用左值还是右值，它都是左值
//
//	//所以需要用到完美转发
//	Fun(std::forward<T>(t));
//}
//
//int main()
//{
//	func(10);
//	int x = 3;
//	func(x);//万能引用既可以引用左值也可以引用右值
//	
//	const int y = 6;
//	func(y);
//	func(move(y));//还能引用const左值和const右值
//	return 0;
//}



//string func(const string& s)//该函数的返回值不能用引用
//{
//	string tmp(s);
//	return tmp;//返回值是一个局部对象，出了该函数栈帧便销毁，所以返回值为引用没有意义
//}
//int main()
//{
//	string str = "hello";
//	string ret = func(str);
//	return 0;
//}

//int main()
//{
//	int x = 3;
//	int&& r1 = x;//右值引用不能引用左值
//	int&& r2 = move(x);//但是右值引用可以引用move后的左值
//	//move:将左值变成右值
//	return 0;
//}


//int sum(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	//不能取右值的地址!
//	const int* p = &10;
//	const int* p2 = &sum(1, 2);
//	return 0;
//}

//int sum(int x, int y)
//{
//	return x + y;
//}

//int main()
//{
//	//p、b、c都是左值，因为它们都可以被取地址
//	int* p = new int(0);
//	int b = 1;
//	const char c = 'a';
//
//	//以下都是对左值的引用
//	int*& rp = p;
//	int& rb = b;
//	const char& rc = c;
//
//	//以下都是右值，因为它们都不能被取地址
//	double x = 1.1, y = 2.2;
//	10;
//	x + y;
//	sum(x, y);
//
//	//以下都是对右值的引用
//	int&& r1 = 10;
//	double&& r2 = x + y;
//	int&& r3 = sum(x , y);
//	return 0;
//}






//int main()
//{
//	//int arr1[5] = { 1,2,3,4,5 };
//	//array<int, 5> arr2 = { 1,2,3,4,5 };
//	//arr1[100];//普通数组可以运行
//	//arr2[100];//引发断言错误
//
//	vector<int> vec(5, 0);
//	vec[100];
//	return 0;
//}




//template <class T1,class T2>
//decltype(T1() * T2()) func(T1 t1, T2 t2)//如果没有decltype，如何确定func函数的返回值?
//{
//	//计算t1*t2,并返回
//	return t1 * t2;
//}
//int main()
//{
//	//decltype(1 + 1) a;//1+1的结果是int类型，所以a也是int类型
//
//	decltype('a' * 13) ret = func('a', 13);//传入一个char类型和int类型
//	cout << ret << endl;
//
//	decltype('b' * 3.14) ret2 = func('b', 3.14);//如果没有decltype，如何确定接收返回值变量的类型?
//	cout << ret2 << endl;
//	return 0;
//}


//void test(auto x)//auto不可用作推导函数参数
//{}
//int main()
//{
//	auto w = 3;//自动推断出w的类型为int
//	auto d = 3.14;//自动推断出d的类型为d
//	
//	map<int, int> m;
//	auto it = m.begin();//自动推断出it为map的迭代器
//
//	auto arr[] = { 1,2,3,4,5 };//auto不可用作推导数组
//	return 0;
//}

//namespace ly
//{
//	template <class T>
//	class vector
//	{
//	public:
//		typedef T* iterator;
//		vector(std::initializer_list<T> il)
//			:_start(new T[il.size()])//指向起始位置
//			,_finish(_start + il.size())//指向有效数据结尾位置(最后一个元素的下一个位置)
//			,_endofstorage(_start + il.size())//指向容量结束位置
//		{
//			iterator it = _start;
//			for (auto e : il)
//			{
//				*it = e;
//				++it;
//			}
//		}
//
//		vector<T>& operator=(std::initializer_list<T> il)
//		{
//			vector<T> tmp(il);
//			std::swap(_start,tmp._start);
//			std::swap(_finish,tmp._finish);
//			std::swap(_endofstorage,tmp._endofstorage);
//			return *this;
//		}
//	private:
//		iterator _start;//起始位置
//		iterator _finish;//有效数据结束位置
//		iterator _endofstorage;//容量结束位置
//	};
//}
//
//int main()
//{
//	ly::vector<int> vec = { 1,2,3,4,5 };
//	return 0;
//}

//int main()
//{
//	auto t = {1,2,3,4,5 };
//	cout << typeid(t).name() << endl;
//	return 0;
//}



//class Date
//{
//public:
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	//C++11允许花括号对所有对象进行初始值设定
//	int x{ 3 };
//	int y = { 3 };
//	Date d1{ 2023,3,25 };
//	Date d2 = { 2022,3,25 };
//	int arr1[]{ 1,2,3,4,5 };
//	int arr2[] = { 2,3,4,5,6 };
//
//	//vector有支持这么多参数的构造函数吗？
//	vector<int> v1{ 1,2,3,4,5 };
//	vector<int> v2 = { 2,3,4,5,6 };
//	return 0;
//}



//struct Point
//{
//	int _x;
//	int _y;
//};
//int main()
//{
//	//C++98允许使用花括号对数组或结构体进行初始值设定
//	Point p{ 1,2 };
//	int arr[] = { 1,2,3,4 };
//	return 0;
//}



//namespace ly
//{
//	class string
//	{
//	public:
//		typedef char* iterator;
//		iterator begin()
//		{
//			return _str;
//		}
//		iterator end()
//		{
//			return _str + _size;
//		}
//		string(const char* str = "")
//			:_size(strlen(str))
//			, _capacity(_size)
//		{
//			//cout << "string(char* str)" << endl;
//			_str = new char[_capacity + 1];
//			strcpy(_str, str);
//		}
//		// s1.swap(s2)
//		void swap(string& s)
//		{
//			::swap(_str, s._str);
//			::swap(_size, s._size);
//			::swap(_capacity, s._capacity);
//		}
//		// 拷贝构造
//		string(const string& s)
//			:_str(nullptr)
//		{
//			cout << "string(const string& s) -- 深拷贝" << endl;
//			string tmp(s._str);
//			swap(tmp);
//		}
//
//		//右值引用分为两种：字面值常量 --> 纯右值
//		//				自定义类型 --> 将亡值
//		string(string&& s)//传入将亡值
//		{
//			cout << "string(const string& s) -- 移动构造" << endl;
//			swap(s);//直接转移资源，不需要拷贝了
//		}
//		// 赋值重载
//		string& operator=(const string& s)
//		{
//			//cout << "string& operator=(string s) -- 深拷贝" << endl;
//			string tmp(s);
//			swap(tmp);
//			return *this;
//		}
//		string& operator=(string&& s)
//		{
//			cout << "string& operator=(string s) -- 移动赋值" << endl;
//			swap(s);
//			return *this;
//		}
//		~string()
//		{
//			delete[] _str;
//			_str = nullptr;
//		}
//		char& operator[](size_t pos)
//		{
//			assert(pos < _size);
//			return _str[pos];
//		}
//		void reserve(size_t n)
//		{
//			if (n > _capacity)
//			{
//				char* tmp = new char[n + 1];
//				strcpy(tmp, _str);
//				delete[] _str;
//				_str = tmp;
//				_capacity = n;
//			}
//		}
//		void push_back(char ch)
//		{
//			if (_size >= _capacity)
//			{
//				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
//				reserve(newcapacity);
//			}
//			_str[_size] = ch;
//			++_size;
//			_str[_size] = '\0';
//		}
//		//string operator+=(char ch)
//		string& operator+=(char ch)
//		{
//			push_back(ch);
//			return *this;
//		}
//		const char* c_str() const
//		{
//			return _str;
//		}
//	private:
//		char* _str = nullptr;
//		size_t _size = 0;
//		size_t _capacity = 0; // 不包含最后做标识的\0
//	};
//
//	string to_string(int value)
//	{
//		bool flag = true;
//		if (value < 0)
//		{
//			flag = false;
//			value = 0 - value;
//		}
//		string str;
//		while (value > 0)
//		{
//			int x = value % 10;
//			value /= 10;
//			str += ('0' + x);
//		}
//		if (flag == false)
//		{
//			str += '-';
//		}
//			std::reverse(str.begin(), str.end());
//		return str;
//	}
//
//	void test1()
//	{
//		//string ret = to_string(-1234);//本来是两次拷贝构造，但是新编译器会优化成一次拷贝构造
//
//		//这种方式编译器无法进行优化
//		//string ret;
//		//ret = to_string(-1234);
//
//		//即使编译器能够把两次拷贝构造优化成一次拷贝构造，但是当对象过大时，效率还是比较低
//		//使用右值引用间接地解决这个问题(左值引用无法优化传值返回的问题)
//		//string ret = to_string(-1234);
//
//		//string ret;
//		//ret = to_string(-1234);
//
//		string s1("111111");
//		string s2(s1);//调用左值引用版本的拷贝构造
//
//		string s3;
//		s3 = string("22222");//调用右值引用版本的赋值重载
//
//		string s4;//调用右值引用版本的赋值重载
//		s4 = move(s1);//s1本来是个左值，但是move之后把s1看成右值，所以这里直接发生资源转移/交换
//
//		//总结：右值引用能够解决两个问题：
//		//1.解决左值引用无法解决的传值返回引发的深拷贝
//		//2.解决左值引用引用右值作为拷贝构造/赋值重载的形参无法优化的深拷贝(右值引用能够避免不必要的深拷贝)
//	}
//
//	void test2()
//	{
//		string ret = to_string(-1234);
//	}
//}
//
//int main()
//{
//	ly::test1();
//	return 0;
//}