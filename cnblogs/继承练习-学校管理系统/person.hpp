#pragma once
#include <string>
#include <list>

namespace mystd
{
	class Person
	{
	public:
		Person(const std::string &name = "", const std::string &age = "", const std::string &sex = "")
			:_name(name), _age(age), _sex(sex)
		{}

		Person(const Person &p)
			:_name(p._name), _age(p._age), _sex(p._sex)
		{}
	protected:
		std::string _name;
		std::string _age;
		std::string _sex;
	};

	class personList // 充当一个基类，没什么用
	{
	public:
		virtual void push(){}
	};
}/*namespace mystd ends here*/