#include "system.hpp"
#include <memory>

int main()
{
	std::unique_ptr<mystd::System> ups(new mystd::System());
	ups->start();
	return 0;
}