#pragma once
#include "person.hpp"
#include "teacher.hpp"
#include "student.hpp"
#include "util.hpp"
#include <list>
#include <string>

namespace mystd
{
	class System
	{
	private:
		typedef char SELECT;
	public:
		System(){}

		void start()// 系统启动运行
		{
			while (true)
			{
				Util::loadMenu();// 加载菜单
				SELECT select = Util::selectAction();// 选择操作
				switch (select)
				{
				case '1':
					std::cout << _tl << std::endl;
					break;
				case '2':
					std::cout << _sl << std::endl;
				break;
				case '3':
					Util::pushPerson(_tl);
				break;
				case '4':
					Util::pushPerson(_sl);
				break;
				default:
					std::cout << "选择不存在" << std::endl;
				break;
				}
			}
		}
	protected:
		studentList _sl;
		teacherList _tl;
		personList _pl;
	};
}