#pragma once
#include "person.hpp"
#include <string>
#include <list>
#include <iostream>
namespace mystd
{
	class Teacher : public Person
	{
	public:
		Teacher(const std::string &name, const std::string &age, const std::string &sex,
			const std::string &subject, const std::string &Class)
			:Person(name, age, sex), _subject(subject), _class(Class)
		{}

		friend std::ostream &operator<<(std::ostream &out, const Teacher & s);

	protected:
		std::string _subject;
		std::string _class;
	};

	std::ostream &operator<<(std::ostream &out, const Teacher & t)
	{
		out << t._name << " " << t._age << " " << t._sex << " " << t._subject << " " << t._class;
		return out;
	}

	class teacherList : public personList
	{
	public:
		void push()
		{
			std::string name, age, sex, subject, Class;
			std::cout << "插入老师的姓名>";
			std::cin >> name;
			getchar();

			std::cout << "插入老师的年龄>";
			std::cin >> age;
			getchar();

			std::cout << "插入老师的性别>";
			std::cin >> sex;
			getchar();

			std::cout << "插入所授的学科>";
			std::cin >> subject;
			getchar();

			std::cout << "插入学生的班级>";
			std::cin >> Class;
			getchar();

			_list.push_back(Teacher(name, age, sex, subject, Class));
		}

		friend std::ostream &operator<<(std::ostream &out, const teacherList & tl);

	private:
		std::list<Teacher> _list;
	};

	std::ostream &operator<<(std::ostream &out, const teacherList & tl)
	{
		for (auto &e : tl._list)
		{
			out << e << std::endl;
		}
		return out;
	}
}