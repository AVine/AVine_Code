#pragma once
#include "person.hpp"
#include <string>
#include <list>
#include <iostream>

namespace mystd
{
	class Student : public Person
	{
	public:
		Student(const std::string &name, const std::string &age, const std::string &sex,
				const std::string &id, const std::string &Class)
			:Person(name, age, sex), _id(id), _class(Class)
		{}
		friend std::ostream &operator<<(std::ostream &out, const Student & s);

	protected:
		std::string _id;
		std::string _class;
	};

	std::ostream &operator<<(std::ostream &out, const Student & s)
	{
		out << s._name << " " << s._age << " " << s._sex << " " << s._id << " " << s._class;
		return out;
	}

	class studentList : public personList
	{
	public:
		void push()
		{
			std::string name, age, sex, id, Class;
			std::cout << "插入学生的姓名>";
			std::cin >> name;
			getchar();

			std::cout << "插入学生的年龄>";
			std::cin >> age;
			getchar();

			std::cout << "插入学生的性别>";
			std::cin >> sex;
			getchar();

			std::cout << "插入学生的学号>";
			std::cin >> id;
			getchar();

			std::cout << "插入学生的班级>";
			std::cin >> Class;
			getchar();

			_list.push_back(Student(name, age, sex, id, Class));
		}
		friend std::ostream &operator<<(std::ostream &out, const studentList & sl);

	private:
		std::list<Student> _list;
	};
	
	std::ostream &operator<<(std::ostream &out, const studentList & sl)
	{
		for (auto &e : sl._list)
		{
			out << e << std::endl;
		}
		return out;
	}
}