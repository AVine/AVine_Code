

#include <iostream>
#include <string>// string类的头文件
using namespace std;


class A
{
public:
	/*发生构造、拷贝构造时打印一下*/
	A()
	{
		cout << "A()" << endl;
	}
	A(const A& a)
	{
		cout << "A(const A& a)" << endl;
	}
};

A func()
{
	return A();
}
int main()
{
	A ret = func();
	return 0;
}

//////////////////////////////////////////////////////////////////
//class A
//{
//public:
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//};
//
//A func(const A& a)
//{
//	/*如果单纯为了返回一个对象，这样写就行了*/
//	return A();
//	//A ret;
//	//return ret;
//}
//
//int main()
//{
//	/*如果只是为了传参，那么没必要这么写*/
//	//A a;
//	//func(a);
//
//	/*匿名对象即可(减少代码量[手动狗头])*/
//	func(A());
//	return 0;
//}
//

///////////////////////////////////////////////////////////////////
//class A
//{
//	/*将内部类限制为私有，这样就是A类"专供了"*/
//private:
//	class B
//	{
//	public:
//		void func(const A& aa)
//		{
//			cout << aa._x << " " << aa._y << endl;
//		}
//	};
//
//private:
//	int _x = 1;
//	int _y = 2;
//};
//
//int main()
//{
//	return 0;
//}

//////////////////////////////////////////////////////////////////
//class A
//{
//	/*在类中声明B类是A类的友元类*/
//	friend class B;
//public:
//private:
//	int _x = 1;
//	int _y = 2;
//};
//
//class B
//{
//public:
//	/*
//	 *_aa对象调用它的默认构造
//	 *直接在A类之外使用A类的私有成员 
//	*/
//	B()
//	{
//		_z = _aa._x;
//	}
//private:
//	A _aa;
//	int _z;
//};
//
//int main()
//{
//	return 0;
//}


////////////////////////////////////////////////////////////////////////////////
//class A
//{
//public:
//	A()
//	{
//		++N;
//	}
//	A(const A& a)
//	{
//		++N;
//	}
//	
//	static int GetN()// 静态成员函数
//	{
//		return N;
//	}
//private:
//	static int N;
//};
//
//int A::N = 0;
//
//int main()
//{
//	A a1;
//	A a2 = a1;
//
//	cout << a1.GetN() << endl;
//	cout << a2.GetN() << endl;
//
//	A* pa = nullptr;
//	cout << pa->GetN() << endl;
//
//	cout << A::GetN() << endl;
//	return 0;
//}

/////////////////////////////////////////////////////////////////////////////
//void func(string s)
//{}
//int main()
//{
//	func("hello");// 只需要这么去传
//	
//	// 不需要这样写了
//	//string s("hello");
//	//func(s);
//	return 0;
//}

///////////////////////////////////////////////////////////////////////////
//class Date
//{
//public:
//	// 限制了隐式类型转换的发生
//	explicit Date(int year = 1, int month = 1, int day = 1)
//		:_year(year), _month(month), _day(day)
//	{}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1 = { 2023, 5, 3 };
//	Date d2 = 2024;
//	return 0;
//}



/////////////////////////////////////////////////////////////////////////
//class Date
//{
//public:
//	Date()
//		:_month(5), _day(_month), _year(2023)
//	{
//		cout << _year << ":" << _month << ":" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _day;
//	int _month;
//};
//int main()
//{
//	const int x = 100;
//	x = 300;// 错误
//	return 0;
//}

/////////////////////////////////////////////////////////////////////////////
//class A
//{
//public:
//	A(int a)
//		:_a(a)
//	{}
//private:
//	int _a;
//};
//
//#include <iostream>
//#include <cstdlib>
//using namespace std;
//class Test
//{
//public:
//	Test()
//		:_y(30)
//		, _a((int*)malloc(100))// 只是演示一下可以这么完，因为这里相当于int* _a = (int*)malloc(100)
//	{
//		cout << "_x = " << _x << " _y = " << _y << endl;
//	}
//private:
//	int _x = 3;
//	int _y = 5;
//	int* _a;
//};
//
//int main()
//{
//	Test t1;
//	return 0;
//}

/////////////////////////////////////////////////////////////////////////////////
//#include <iostream>
//using namespace std;
//
//class A
//{
//public:
//	A(int a)// A类没有默认构造
//		:_a(a)
//	{}
//private:
//	int _a;
//};
//
//class Test
//{
//public:
//	Test()
//		:_x(3)// 初始化列表没有显式初始化对象，调用默认构造，但是A类没有默认构造，报错
//	{}
//private:
//	const int _x;
//	A _aa;// 自定义类型对象
//};

//class Date
//{
//public:
//	Date(int year = 1,int month = 1,int day = 1)
//	{
//		cout << _year << ":" << _month << ":" << _day << endl;
// 	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Test t1;
//	return 0;
//}


////////////////////////////////////////////////////////////////////////////
/*class Date
{
public:
	Date(int year = 1,int month = 1,int day = 1)
		:_year(year), _month(month), _day(day)
	{}
private:
	int _year;
	int _month;
	int _day;
};*/
//int main()
//{
//	return 0;
//}