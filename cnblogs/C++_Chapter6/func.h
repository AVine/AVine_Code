#pragma once

template <class T>
class Stack
{
public:
	/*类模板中只有声明*/
	Stack(int capacity = 4);
	void push(const T &in);
private:
	T *_elem;
	int _top;
	int _capacity;
};

///*函数模板的声明*/
//template<class T>
//T Add(const T &x, const T &y);



