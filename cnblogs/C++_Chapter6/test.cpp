#include <iostream>
using namespace std;


int Add(int x, int y)
{
	return x + y;
}

template <class T>
T Add(T x, T y)
{
	return x + y;
}

int main()
{
	/*此时函数模板与函数同时存在，但是这里调用例子中
	*实参直接与函数的参数匹配，所以直接调用函数而非调用函数模板*/
	cout << Add(1, 2) << endl;

	/*显式实例化生成一份真实的函数
	 *看起来实例化的函数与已经存在的函数冲突
	 *实际上并没有发生冲突，还可以正常调用*/
	cout << Add<int>(1, 2) << endl;
	return 0;
}