

#include <iostream>
using namespace std;





#include "func.h"
int main()
{
	Stack<int> st;
	st.push(1);
	return 0;
}

////////////////////////////////////////////////////////

///*只有声明，定义在另一个源文件当中*/
//template<class T>
//T Add(const T &x, const T &y);
//
//int main()
//{
//	cout << Add(1, 2) << endl;
//	return 0;
//}





//class A
//{
//public:
//	A(){}
//	A(const A &a)
//	{
//		cout << "A(const A & a)" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//};
//
//A func()
//{
//	A a;
//	return a;
//}
//
//int main()
//{
//	const A ra = func();
//	return 0;
//}

//template <class T>
//class Stack
//{
//public:
//	Stack(int capacity = 4)
//		:_top(0), _capacity(capacity)
//	{
//		_elem = new T[_capacity];
//	}
//
//	void push(const T &in)
//	{
//		/*不考虑扩容......*/
//		_elem[_top++] = in;
//	}
//private:
//	T *_elem;
//	int _top;
//	int _capacity;
//};
//
//int main()
//{
//	/*实例化出不同的Stack类，用来存储不同的数据类型
//	 *这几个实例化出来的类的类型是不相同的！*/
//	Stack<int> st1;
//	Stack<double> st2;
//	Stack<char> st3;
//
//	/*错误！他们不是相同的类型！*/
//	//st1 = st2;
//	return 0;
//}

/////////////////////////////////////////////////////////
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//template <class T1,class T2>
//T1 Add(T1 x, T2 y)
//{
//	return x + y;
//}
//
//int main()
//{
//	/*这两个调用都可以调用普通函数，但是要发生隐式类型转换
//	 *但是上面的函数模板有两个参数，所以对应两个类型
//	 *所以调用函数模板是最合适的*/
//	cout << Add(1.1, 5) << endl;
//	cout << Add(5, 3.4) << endl;
//	return 0;
//}

//int main()
//{
//	/*此时函数模板与函数同时存在，但是这里调用例子中
//	*实参直接与函数的参数匹配，所以直接调用函数而非调用函数模板*/
//	cout << Add(1, 2) << endl;
//
//	/*显式实例化生成一份真实的函数
//	 *看起来实例化的函数与已经存在的函数冲突
//	 *实际上并没有发生冲突，还可以正常调用*/
//	cout << Add<int>(1, 2) << endl;
//	return 0;
//}

/////////////////////////////////////////////////////////////////
//template <typename T>
//T Add(const T &x, const T &y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int a = 1, b = 3;
//	double c = 3.1, d = 4.6;
//
//	/*显式实例化，Add<int>确定了函数模板的参数类型
//	 *所以即可生成对应的真实函数
//	 *所以此时可以发生隐式类型转换，因为此时就是在调用真实的函数*/
//	cout << Add<int>(a, c) << endl;
//	return 0;
//}

//int main()
//{
//	/*让编译器根据实参的类型推演出函数模板的参数类型
//	 *然后再生成具体的真实函数*/
//	int a = 1, b = 3;
//	cout << Add(a , b) << endl;
//
//	double c = 3.1, d = 4.6;
//	cout << Add(c , d) << endl;
//
//	return 0;
//}

//////////////////////////////////////////////////////////////////////////
//template <class T1,class T2,......,class Tn>
//返回类型 函数名(参数列表)
//{}

//template <class T>
//void Swap(T &left, T &right)
//{
//	T tmp = left;
//	left = right;
//	right = tmp;
//}
//
//int main()
//{
//	int x = 3, y = 5;
//	Swap(x, y);
//
//	char chl = 'a', chr = 'b';
//	Swap(chl, chr);
//
//	double d = 1.1, b = 4.3;
//	Swap(d, b);
//	return 0;
//}

//void Swap(int &left, int &right)
//{
//	int tmp = left;
//	left = right;
//	right = tmp;
//}
//
//void Swap(char &left, char &right)
//{
//	char tmp = left;
//	left = right;
//	right = tmp;
//}
//
//void Swap(double &left, double &right)
//{
//	double tmp = left;
//	left = right;
//	right = tmp;
//}
//
///*......*/



