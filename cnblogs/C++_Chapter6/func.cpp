
#include "func.h"

/*声明与定义分离*/
template <class T>
Stack<T>::Stack(int capacity = 4)
:_top(0), _capacity(capacity)
{
	_elem = new T[_capacity];
}

template <class T>
void Stack<T>::push(const T &in)
{
	/*不考虑扩容......*/
	_elem[_top++] = in;
}

/*显式实例化*/
template class Stack<int>;


///*函数模板的定义*/
//template <class T>
//T Add(const T &x, const T &y)
//{
//	return x + y;
//}
//
///*显式实例化*/
//template int Add(const int &x, const int &y);
//template double Add(const double &x, const double &y);