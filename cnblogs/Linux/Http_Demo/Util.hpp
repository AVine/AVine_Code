#pragma once

#include <string>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

class Util
{
public:
    static std::string getRequestLine(std::string &in, const std::string &sep)
    {
        auto pos = in.find(sep);
        if (pos == std::string::npos)
            return "";
        std::string line = in.substr(0, pos);
        // in.erase(0, pos + sep.size());
        return line;
    }

    static bool readFile(const std::string &resource, std::string &out, int &contentSize)
    {
        std::ifstream in(resource, std::ios_base::binary);
        if (!in.is_open())
            return false;

        /*如果请求到是无效资源*/
        if (contentSize == 0)
        {
            in.seekg(0, in.end);
            int size = in.tellg();
            in.seekg(0, in.beg);
            contentSize = size;
        }

        vector<char> v(contentSize);
        in.read(v.data(), contentSize);
        std::string tmp(v.begin(), v.end());
        out += tmp;
        return true;
    }
};