#include "httpServer.hpp"
#include "httpProtocol.hpp"

/*按道理来说不加这个两个头文件也行，因为httpServer.hpp已经有了
 *但是不加不会报错*/
#include <sys/types.h>
#include <sys/socket.h>
#include <unordered_map>
typedef function<bool(httpRequest &, httpResponse &)> func_t;

class Task
{
public:
    Task(const int &sock, const func_t &func/*,const std::unordered_map<std::string,func_t> &hashCallBack*/)
        : _sock(sock), _func(func)/*,_hashCallBack(hashCallBack)*/
    {
    }
    Task() {}
    void operator()()
    {
        char buffer[4096];
        httpRequest request;
        httpResponse response;
        int n = recv(_sock, buffer, sizeof(buffer), 0);
        if (n > 0)
        {
            buffer[n] = 0;
            request.inbuffer = buffer;
            request.parse();
            //_hashCallBack[request.path](request,response);
            _func(request, response);
            send(_sock, response.outbuffer.c_str(), response.outbuffer.size(), 0);
        }
        else
        {
            logMessage(FATAL, "recv failed,return value[%d]", n);
            return;
        }
    }

private:
    int _sock;
    func_t _func;
    //std::unordered_map<std::string, func_t> _hashCallBack;
};