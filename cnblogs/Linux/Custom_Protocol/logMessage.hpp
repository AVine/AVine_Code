#pragma once

#include <stdarg.h>
/*本文件负责实现日志的记录功能
 *日志分为5个等级*/
#define DEBUG 0
#define NORMAL 1
#define WARNING 2
#define ERROR 3
#define FATAL 4

#include <iostream>
#include <string>
using namespace std;

#include <stdio.h>

const char *level_string(int level)
{
    if (level == DEBUG)
        return "DEBUG";
    if (level == NORMAL)
        return "NORMAL";
    if (level == WARNING)
        return "WARNING";
    if (level == ERROR)
        return "ERROR";
    if (level == FATAL)
        return "FATAL";
}

void logMessage(int level, const char *format, ...)
{
    va_list arg;
    va_start(arg, format); /*确定可变参数的起始位置*/

    /*可变参数之前加上固定前缀:[日志等级][时间]
     *利用time()接口和localtime()接口得到时间
     *其中年+1900、月+1*/
    char logPrev[1024];
    time_t timep = time(nullptr);
    struct tm *ptm = localtime(&timep);
    snprintf(logPrev, sizeof(logPrev), "[%s][%d-%d-%d->%d:%d:%d]", level_string(level),
             ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);

    /*将可变参数的内容写入logContext当中*/
    char logContext[1024];
    vsnprintf(logContext, sizeof(logContext), format, arg);

    cout << logPrev << logContext << endl;
}