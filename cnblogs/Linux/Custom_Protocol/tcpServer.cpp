
#include <memory>
#include "tcpServer.hpp"
using namespace server;

void Usage(char *command)
{
    cout << "\nUsage:\n\t" << command << "\ttcp_port\n"
         << endl;
}

bool calculateTask(const Request &rqs, Response &rps)
{
    switch (rqs._oper)
    {
    case '+':
        rps._result = rqs._left + rqs._right;
        break;
    case '-':
        rps._result = rqs._left - rqs._right;
        break;
    case '*':
        rps._result = rqs._left * rqs._right;
        break;
    case '/':
    {
        if (rqs._right == 0)
        {
            rps._exitCode = DIV_ZERO;
            rps._result = -1;
        }
        else
            rps._result = rqs._left / rqs._right;
    }
    break;
    case '%':
    {
        if (rqs._right == 0)
        {
            rps._exitCode = MOD_ZERO;
            rps._result = -1;
        }
        else
            rps._result = rqs._left % rqs._right;
    }
    break;
    default:
    {
        rps._exitCode = OPER_ERROR;
        rps._result = -1;
    }
        break;
    }
    return true;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(START_ERROR);
    }
    uint16_t port = (uint16_t)atoi(argv[1]);
    unique_ptr<tcpServer> upts(new tcpServer(port));

    upts->start(calculateTask);

    return 0;
}