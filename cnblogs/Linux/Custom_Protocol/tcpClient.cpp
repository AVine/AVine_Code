
#include <memory>
#include "tcpClient.hpp"
using namespace client;

void Usage(char *command)
{
    cout << "\nUsage:\n\t" << command << "\ttcp_ip\ttcp_port\n" << endl;
}
int main(int argc,char *argv[])
{
    if(argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }
    string serverIp = argv[1];
    uint16_t port = (uint16_t)atoi(argv[2]);
    unique_ptr<tcpClient> uptc(new tcpClient(serverIp,port));
    uptc->start();
    
    return 0;
}