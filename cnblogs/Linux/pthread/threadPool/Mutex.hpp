#pragma once

#include <pthread.h>

namespace mutex
{
    /*Mutex类负责将有关于锁的接口封装起来*/
    class Mutex
    {
    public:
        Mutex(pthread_mutex_t *pmutex = nullptr):_pmutex(pmutex) 
        {
            if(_pmutex != nullptr) pthread_mutex_lock(_pmutex);
        }
        ~Mutex()
        {
            if(_pmutex != nullptr) pthread_mutex_unlock(_pmutex);
        }
    private:
        pthread_mutex_t *_pmutex;
    };

    /*LockGuard负责给外部使用
     *即RAII风格的锁*/
    class LockGuard
    {
    public:
        /*LockGuard的构造函数当中了定义了Mutex对象
         *Mutex对象被定义时会调用构造函数
         *Mutex类的构造函数会执行上锁操作*/
        LockGuard(pthread_mutex_t *pmutex):_mutex(pmutex)
        {}
        ~LockGuard()
        {
            _mutex.~Mutex();
        }
    private:
        Mutex _mutex;
    };
}/*namespace ly ends here*/