
#include "singletonPthreadPool.hpp"
using namespace threadpool;

#include "Task.hpp"
#include <iostream>
#include <memory>
using namespace std;
#include <unistd.h>

int main()
{
    threadPool<culculateTask<int>>::getInstance()->start();
    printf("0x%x\n", threadPool<culculateTask<int>>::getInstance());
    int left = 0, right = 0;
    char oper;
    while (true)
    {
        cout << "Input left number# ";
        cin >> left;
        cout << "Input right number# ";
        cin >> right;
        cout << "Input oper# ";
        cin >> oper;
        culculateTask<int> task(left, right, oper);
        threadPool<culculateTask<int>>::getInstance()->push(task);
        printf("0x%x\n", threadPool<culculateTask<int>>::getInstance());
        usleep(1234);
    }
    return 0;
}