#pragma once
#include <fcntl.h>


class util
{
public:
    static void set_noblocking(int sockfd)
    {
        int flags = fcntl(sockfd,F_GETFL,0);
        fcntl(sockfd,F_SETFL,flags | O_NONBLOCK);
    }
};