#pragma once
#include <stdlib.h>

enum error_list
{
    SOCKET_ERR = 1,
    BIND_ERR,
    LISTEN_ERR,
    EPOLL_CREATE_ERR,
    EPOLL_WAIT_ERR
};