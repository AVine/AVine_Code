#pragma once

#include <stdio.h>
#include <string.h>

#define DEBUG 0
#define NORMAL 1
#define WARNING 2
#define ERROR 3
#define FATAL 4

void level_to_string(int level,char *out)
{
    if(level == DEBUG) strcpy(out,"[DEBUG]");
    else if(level == NORMAL) strcpy(out,"[NORMAL]");
    else if(level == WARNING) strcpy(out,"[WARNING]");
    else if(level == ERROR) strcpy(out,"[ERROR]");
    else if(level == FATAL) strcpy(out,"[FATAL]");
}

#define LOG(level,format,...) {\
    char err_str[20] = {0};\
    level_to_string(level,err_str);\
    fprintf(stdout,"%s:" format "\n",err_str,##__VA_ARGS__);\
}
