#include "processPool.hpp"
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <functional>

void test1()
{
    std::cout << "test1()" << std::endl;
}

void test2()
{
    std::cout << "test2()" << std::endl;
}

void test3()
{
    std::cout << "test3()" << std::endl;
}

void test4()
{
    std::cout << "test4()" << std::endl;
}

int main()
{
    srand((unsigned int)time(nullptr));
    task_pool<std::function<void()>> tp;
    tp.push(test1);
    tp.push(test2);
    tp.push(test3);
    tp.push(test4);

    process_pool<std::function<void()>> pp(&tp);
    pp.start();
    return 0;
}