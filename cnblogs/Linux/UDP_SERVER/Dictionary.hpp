

#include <unordered_map>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

class Dictionary
{
public:
    void loadFile()
    {
        ifstream in("./dict.txt");

        if(!in.is_open())
        {
            cerr << "open file fail" << endl;
            exit(-1);
        }

        /*哈希表的key为英文，value为中文
         *打开文件之后将key和value存入哈希表*/
        string line;
        string key,value;
        while(getline(in,line))
        {
            if(cutString(line,key,value,":"))
            {
                _dict.insert(make_pair(key,value));
            }
        }
    }

    string translate(const string &str)
    {
        auto it = _dict.find(str);
        string response;
        if(it != _dict.end())
        {
            response = it->second;
        }
        else 
        {
            response = "unknow";
        }
        return response;
    }
private:
    /*以哈希表作为字典的容器*/
    unordered_map<string,string> _dict;

    /*字符串分割*/
    bool cutString(const string &line,string &key,string &value,const string &sep)
    {
        auto pos = line.find(":");
        if(pos != string::npos)
        {
            key = line.substr(0,pos);
            value = line.substr(pos+1);
            return true;
        }
        return false;
    }
}; 