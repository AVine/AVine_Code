#include "udpClient.hpp"
#include <iostream>
#include <string>
#include <memory>
using namespace std;
using namespace client;

void Usage(char *command)
{
    cout << "\nUsage:\n\t" << command << "\t" << "server_ip\t" << "server_port\n" << endl; 
}

/*客户端启动时必须绑定两个选项
 *意在指明与哪个服务器建立通信*/
int main(int argc,char *argv[])
{
    if(argc != 3)
    {
        Usage(argv[0]);
        exit(START_ERROR);
    }
    string serverIp = argv[1];
    uint16_t serverPort = atoi(argv[2]);
    unique_ptr<udpClinet> upuc(new udpClinet(serverIp,serverPort));
    upuc->start();
    return 0;
}