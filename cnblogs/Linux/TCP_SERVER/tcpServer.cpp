
#include <memory>
#include "tcpServer.hpp"
using namespace server;
#include "Daemon.hpp"

void Usage(char *command)
{
    cout << "\nUsage:\n\t" << command << "\ttcp_port\n" << endl;
}
int main(int argc,char *argv[])
{
    if(argc != 2)
    {
        Usage(argv[0]);
        exit(START_ERROR);
    }
    uint16_t port = (uint16_t)atoi(argv[1]);
    unique_ptr<tcpServer> upts(new tcpServer(port));

    //Daemon();/*守护进程化*/
    upts->start();
    
    return 0;
}