#pragma once

#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void Daemon()
{
    /*守护进程化之前要忽略一些异常信息
     *否则服务器有可能"无缘无故"挂掉了
     *这里选择忽略管道信号，因为有可能服务器正准备向客户端发送消息时
     *客户端异常，导致向空文件写入*/
    signal(SIGPIPE, SIG_IGN);

    /*守护进程化，但是需要守护进程化的进程不能是组长*/
    if (fork() > 0)
        exit(0);
    /*走到这里一定是子进程，但是它的父进程退出了，所以是孤儿进程*/
    setsid(); /*守护进程化*/

    /*守护进程是脱离终端的，需要重定向(关闭)以前进程默认打开的文件
     *日志的输出都往"黑洞"里面写*/
    int fd = open("/dev/null", O_RDWR);
    if (fd > 0)
    {
        dup2(fd, 0);
        dup2(fd, 1);
        dup2(fd, 2);

        close(fd);
    }
    else
    {
        close(0);
        close(1);
        close(2);
    }
}