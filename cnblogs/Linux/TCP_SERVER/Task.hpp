#pragma once

#include <functional>
using namespace std;

class Task
{
public:
    typedef function<void(int)> func_t;
    Task(int sock,func_t func)
        :_sock(sock),_func(func)
    {}
    Task(){}
    void operator()()
    {
        _func(_sock);
    }
private:
    int _sock;
    func_t _func;
};