#include "select_server.hpp"
#include <memory>

int main()
{
    std::unique_ptr<server> svr(new server(9090));
    svr->start();
    return 0;
}