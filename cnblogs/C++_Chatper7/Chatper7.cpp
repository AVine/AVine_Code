
#include <string>/*string类所在的头文件*/
#include <iostream>
#include <stack>
#include <algorithm>
using namespace std;

//class Test
//{
//public:
//	~Test()
//	{
//		cout << "~Test()" << endl;
//	}
//};
//
//Test t;
//Test func()
//{
//	return t;
//}
//int main()
//{
//	const Test &rt = func();
//	return 0;
//}


#include "String.hpp"
using namespace mystd;

void test1()
{
	//String s;
	//int cap = s.capacity();
	//for (int i = 0; i < 1000; i++)
	//{
	//	s.push_back('a');
	//	if (cap != s.capacity())
	//	{
	//		cout << "new capacity: " << s.capacity() << endl;
	//		cap = s.capacity();
	//	}
	//}

	//String s2("");
	//cout << s2.size() << endl;
	//s2.insert(0, 1,'x');
	//cout << s2.c_str() << ":" << s2.size() << endl;
	//s2 += "hello world";
	//s2.insert(4, 1, 'w');
	//cout << s2.c_str() << endl;

	/*String s = "hello world";
	s.erase(5);
	cout << s.c_str() << endl;
	String s1;
	s1 = s;
	s += " world";
	cout << s1.c_str() << endl;*/

	//String s = "hello world";
	//size_t pos = s.find(' ');
	//cout << pos << endl;
	//cout << (s + "nonono").c_str() << endl;
	//cout << (s + "nonono")<< endl;

	//s.push_back('\0');
	//s.push_back('\0');
	//s.push_back('\0');

	//s += "yesyes";
	//cout << s << endl;

	String s = "hello world";
	cin >> s;
	cout << s << endl;
}

int main()
{
	test1();
	return 0;
}

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//		:_year(year), _month(month), _day(day)
//	{}
//	
//	static bool operator==(const Date &d1, const Date &d2)
//	{
//		return d1._year == d2._year && d1._month == d2._month && d1._day == d2._day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1(2023, 5, 16);
//	Date d2(2023, 5, 16);
//
//	Date::operator==(d1, d2);/*这样的写法完全没有问题，但是编译器报错*/
//	return 0;
//}

//int main()
//{
//	string s = "hello";
//	s.resize(10);/*多的内容以'\0'填充*/
//	s += "world";
//	cout << s.c_str();
//	return 0;
//}


//int main()
//{
//	string s1 = "test iterator";
//	const string s2 = "test const_iterator";
//	//string::reverse_iterator rit1 = s1.rbegin();
//	auto rit1 = s1.rbegin();/*方便写法*/
//	
//	//string::const_reverse_iterator rit2 = s2.rbegin();
//	auto rit2 = s2.rbegin();/*方便写法*/
//
//	while (rit1 != s1.rend())
//	{
//		cout << *rit1;
//		++rit1;
//	}
//	cout << endl;
//
//	while (rit2 != s2.rend())
//	{
//		cout << *rit2;
//		++rit2;
//	}
//	cout << endl;
//}



//int main()
//{
//	string s = "test iterator";
//
//	/*迭代器类型是string类当中的内嵌类型*/
//	string::iterator it = s.begin();
//	while (it != s.end())
//	{/*像指针一样*/
//		(*it)++;
//		cout << *it;
//		++it;
//	}
//	cout << endl;
//	return 0;
//}

//////////////////////////////////////////////////////////
//int main()
//{
//	string s = "test elements";
//	s.at(15);
//
//	return 0;
//}

/////////////////////////////////////////////////////////////////
//int main()
//{
//	string s1 = "test resize";
//	s1.resize(20,'x');
//	cout << s1 << " " << s1.size() << " " << s1.capacity() << endl;
//	return 0;
//}

//int main()
//{
//	string s1 = "test resize";
//	s1.resize(14);
//	cout << s1 << " " << s1.size() << endl;
//
//	s1.push_back('x');
//	cout << s1 << endl;/*以string的视角来看，并不会影响输出*/
//
//	cout << s1.c_str() << endl;/*以字符串的视角来看，碰到'\0'就停止*/
//	return 0;
//}


//int main()
//{
//	string s1 = "test resize";
//	s1.resize(4);
//	cout << s1 << " " << s1.size() << endl;
//	return 0;
//}

///////////////////////////////////////////////////////////////

//int main()
//{
//	string s;
//	s.reserve(1000);/*预留容量*/
//	size_t oldCap = s.capacity();
//	for (int i = 0; i < 1000; i++)
//	{
//		s.push_back('x');
//		if (oldCap != s.capacity())
//		{
//			cout << "发生了扩容: " << s.capacity() << endl;
//			oldCap = s.capacity();
//		}
//	}
//	return 0;
//}



//int main()
//{
//	string s1 = "test capacity";
//	s1.reserve(40);
//	cout << s1.capacity() << endl;
//
//	cout << s1.max_size() << endl;/*永远都是固定值*/
//	
//	s1.shrink_to_fit();
//	cout << s1.capacity() << endl;/*VS会做一些内存对齐*/
//	cout << s1.size() << endl;
//	return 0;
//}

//int main()
//{
//	string s1 = "test capacity";
//
//	/*获取s1对象有效字符串的长度
//	 *与strlen()等价*/
//	cout << s1.size() << endl;
//	cout << s1.length() << endl;
//
//	/*获取s1对象的容量*/
//	cout << s1.capacity() << endl;
//
//	s1.reserve(1000);/*将s1的容量预留至1000*/
//	cout << s1.capacity() << endl;/*VS可能会做一些内存对齐*/
//
//	s1.resize(20, 'x');/*将s1对象的有效字符扩展到20个*/
//	/*扩展的部分用'x'填充
//	 *如果单纯使用s1.resize(20)，那么扩展的部分用'\0'填充*/
//	cout << s1 << " " << s1.size() << endl;
//
//	s1.resize(5);/*有效字符缩减至5个，相当于删除字符*/
//	cout << s1 << " " << s1.size() << endl;
//
//	cout << s1.empty() << endl;/*判空。非0为真，即为空串；0为假，表示非空串*/
//
//	s1.clear();/*将有效字符清空。不会影响容量,但是会影响有效字符串长度*/
//	cout << s1 << " " << s1.size() << endl;
//	return 0;
//}

//int main()
//{
//	string s1;/*默认构造，构造一个空串*/
//	//basic_string<char> s1;/*等价*/
//
//	string s2("hello");/*以常量字符串构造string对象，对象当中保存一份常量字符串的拷贝*/
//	string s3(s2);/*拷贝构造，很明显是深拷贝*/
//
//	/*从s2对象的字符串的第2个(下标为1)字符开始
//	 *向后拷贝3个字符并初始化s4对象，s4 = "ell"*/
//	string s4(s2, 1, 3);
//
//	/*如果第三个参数使用缺省值，那么从s2对象的第2个(下标为1)字符开始
//	 *向后所有的字符初始化s5对象，s5 = "ello"
//	 *该构造函数的第三个参数的缺省值为npos，其定义为size_t npos = -1，即42亿多
//	 *也就是说，从起始位置开始向后的字符个数如果超过了字符串长度，那么最多到'\0'处*/
//	string s5(s2, 1);
//
//	string s6("nice to meet you", 7);/*将字符串常量的前7七个作为s6的初始化数据*/
//
//	string s7(10, 'a');/*将10个'a'组成的字符串初始化s7*/
//
//	string s8(s6.begin(), s6.end());/*用s6字符串的起始位置到结束位置的字符串构造s8*/
//	return 0;
//}