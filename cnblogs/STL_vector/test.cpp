#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;
#include "Vector.hpp"
using namespace mystd;

struct Test
{
	~Test(){ cout << "~Test()" << endl; }
};

void test1()
{
	//Vector<int> v;
	//cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
	//v.push_back(1);
	//v.push_back(2);
	//v.push_back(3);
	//v.push_back(4);
	//v.push_back(5);
	//cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
	//v.pop_back();
	//v.pop_back();
	//v.pop_back();
	//cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
	//for (auto &e : v) cout << e << " ";
	//cout << endl;

	Vector<Test> v;
	cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
	v.push_back(Test());
	v.push_back(Test());
	v.push_back(Test());
	v.push_back(Test());
	v.push_back(Test());
	cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
	v.pop_back();
	v.pop_back();
	v.pop_back();
	cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
}

void test2()
{
	Vector<int> v;
	v.resize(10,22);
	cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
	for (auto &e : v) cout << e << " ";
	cout << endl;
	v.resize(5,32);
	cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
	for (auto &e : v) cout << e << " ";
	cout << endl;
}

void test3()
{
	Vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
	for (auto &e : v) cout << e << " ";
	cout << endl;

	auto it = find(v.begin(), v.end(), 3);
	v.insert(it, 30);
	cout << "size:" << v.size() << " capacity:" << v.capacity() << endl;
	for (auto &e : v) cout << e << " ";
	cout << endl;
}

int main()
{
	test3();
	return 0;
}

///////////////////////////////////////////////////////////
//int main()
//{
//	vector<int> v;
//	v.reserve(10);/*这里虽然扩容了，但是有效数据依然为0*/
//	for (int i = 0; i < 10; i++)
//	{
//		v[i] = i;
//	}
//	return 0;
//}

////////////////////////////////////////////////////////////
//int main()
//{
//	string s = "hello world";/*这种写法会发生隐式类型转换*/
//	vector<char> v(s.begin(),s.end());
//	return 0;
//}

///////////////////////////////////////////////////////////
//int main()
//{
//	vector<int> v1;
//	vector<int> v2;
//	swap(v1, v2);
//	return 0;
//}


////////////////////////////////////////////////////////////////
//int main()
//{
//	vector<char> v;
//	size_t curCap = v.capacity();
//	cout << "当前容量: " << curCap << endl;
//	for (int i = 0; i < 1000; i++)
//	{
//		v.push_back('c');
//		if (curCap != v.capacity())
//		{
//			cout << "发生扩容，当前容量: " << v.capacity() << endl;
//			curCap = v.capacity();
//		}
//	}
//	return 0;
//}

////////////////////////////////////////////////////////////
//class Date
//{};
//int main()
//{
//	vector<int> v1;
//	vector<Date> v2;
//
//	v1.resize(100);
//	v2.resize(200);
//	return 0;
//}