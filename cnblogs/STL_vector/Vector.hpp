#pragma once

#include <string.h>
#include <assert.h>
namespace mystd
{
	template <class T>
	class Vector
	{
	public:
		typedef T * iterator;
		typedef const T * const_iterator;
		iterator begin() { return _start; }
		iterator end() { return _finish; }
		const_iterator begin() const { return _start; }
		const_iterator end() const { return _finish; }
	public:
		Vector() :_start(nullptr), _finish(nullptr), _end_of_storage(nullptr)
		{}

		/*[]运算符重载，可读可写接口，重载两个版本*/
		T &operator[](size_t pos)
		{
			assert(pos < _finish);
			return _start[pos];
		}
		const T &operator[](size_t pos) const
		{
			assert(pos < _finish);
			return _start[pos];
		}

		/*容量控制函数*/
		void reserve(size_t n)
		{
			if (n > capacity())
			{
				T *tmp = new T[n];
				size_t oldSize = size();
				if (_start)
				{
					memcpy(tmp, _start, sizeof(T)* capacity());// 双层深拷贝问题TODO
					delete[] _start;
				}
				_start = tmp;
				_finish = _start + oldSize;
				_end_of_storage = _start + n;
			}
		}

		/*有效元素个数控制函数*/
		void resize(size_t n, T val = T())
		{
			if (n > capacity()) reserve(n);// 预先扩容减少频繁扩容
			if (n > size())
			{
				size_t cnt = n - size();
				while (cnt--) push_back(val);
			}
			else if (n <= size())
			{
				size_t cnt = size() - n;
				while (cnt--) pop_back();
			}
		}

		/*尾插*/
		void push_back(const T& val)
		{
			if (_finish >= _end_of_storage)// 需要扩容
			{
				size_t newCapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newCapacity);
			}
			*_finish = val;
			++_finish;
		}

		/*插入,要注意迭代器失效问题
		 *如果发生了扩容，pos就指向了一个无效位置，需要更新pos*/
		iterator insert(iterator pos, const T &val)
		{
			assert(pos >= _start && pos <= _finish);// 说不定有尾插
			if (_finish >= _end_of_storage)
			{
				size_t offset = pos - _start;// 先记录未扩容之前的偏移量
				size_t newCapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newCapacity);
				pos = _start + offset;// 再根据偏移量计算出新的pos位置
			}
			auto end = _finish;
			while (end > pos)
			{
				*end = *(end-1);
				--end;
			}
			*pos = val;
			++_finish;
			return pos;
		}

		/*尾删*/
		void pop_back()
		{
			assert(_finish > _start);
			_finish->~T();// 如果元素是自定义类型，最好显式调用析构
			--_finish;
		}

		/*返回有效数据个数*/
		size_t size() const
		{
			return _finish - _start;
		}

		/*返回当前容量*/
		size_t capacity() const
		{
			return _end_of_storage - _start;
		}
	private:
		/*三个核心成员*/
		iterator _start;
		iterator _finish;
		iterator _end_of_storage;
	};
}/*namespace mystd ends here*/