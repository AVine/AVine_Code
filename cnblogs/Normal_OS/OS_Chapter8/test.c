#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
    // Linux下一切皆文件
    int fd = open("./hello.txt",O_RDONLY);
    if(fd != -1)
    {
        // 这里只是个举例
        char buffer[64];
        int n = read(fd,buffer,sizeof(buffer));
        if(n > 0)
        {
            buffer[n] = 0;
            printf("%s\n",buffer);
        }
    }
    return 0;
}