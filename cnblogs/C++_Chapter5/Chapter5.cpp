﻿#include <iostream>
#include <string>// string类的头文件
#include <cstdlib>
using namespace std;

int main()
{
	char *pa = new char[1024 * 1024 * 1024];
	/*该程序永不退出并且不释放pa指向的空间
	 *那么就会导致内存泄漏*/
	while (1)
	{}
	return 0;
}


////////////////////////////////////////////////////////////////////

///*free是以宏实现的*/
//#define   free(p)               _free_dbg(p, _NORMAL_BLOCK)
//
//void operator delete(void *pUserData)
//{
//	_CrtMemBlockHeader * pHead;
//	RTCCALLBACK(_RTC_Free_hook, (pUserData, 0));
//	if (pUserData == NULL)
//		return;
//	_mlock(_HEAP_LOCK);  /* block other threads */
//	__TRY
//		        /* get a pointer to memory block header */
//				pHead = pHdr(pUserData);
//	         /* verify block type */
//	_ASSERTE(_BLOCK_TYPE_IS_VALID(pHead->nBlockUse));
//
//	/*调用free*/
//	_free_dbg(pUserData, pHead->nBlockUse);
//	__FINALLY
//		_munlock(_HEAP_LOCK);  /* release other threads */
//	__END_TRY_FINALLY
//		return;
//}



//void *__CRTDECL operator new(size_t size) _THROW1(_STD bad_alloc)
//{
//	void *p;
//	/*调用malloc申请空间*/
//	while ((p = malloc(size)) == 0)
//	if (_callnewh(size) == 0)
//	{
//		/*如果申请失败，抛异常*/
//		static const std::bad_alloc nomem;
//		_RAISE(nomem);
//	}
//	return (p);
//}

//class A
//{
//public:
//	A()
//	{
//		cout << "A()" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//};
//int main()
//{
//	/*使用malloc()开辟的空间没有调用构造函数*/
//	A *pa = (A *)malloc(sizeof(A));
//
//	/*使用定位new表达式显式调用构造函数*/
//	new(pa)A();
//
//	/*delete则正常按照规则执行*/
//	delete pa;
//	return 0;
//}

//int main()
//{
//	/*使用try对可能会出现异常的地方尝试捕获异常*/
//	try
//	{
//		while (true)
//		{
//			int *p = new int[1024 * 1024];
//			if (p)
//			{
//				cout << p << endl;
//			}
//			else
//			{
//				cout << "申请失败" << endl;
//				break;
//			}
//		}
//	}
//	/*当出现异常后，异常直接被抛出，new的异常类型为exception*/
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//
//	return 0;
//}


////////////////////////////////////////////////////////////////
//class A
//{
//public:
//	A()
//	{
//		cout << "A()" << endl;
//	}
//};
//
//int main()
//{
//	A *p1 = new A[10];
//	delete p1;
//	return 0;
//}



///////////////////////////////////////////////////////////////////////
//class A
//{
//public:
//	/*使用malloc()不会调用构造函数，所以_a成员无法初始化*/
//	A(int a = 0)
//		:_a(new int[10])
//	{
//		cout << "A()" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int *_a;
//};
//
//int main()
//{
//	/*malloc()没有调用构造，那么A类当中的成员无法正确初始化*/
//	A *p1 = (A *)malloc(sizeof(A));
//	free(p1);
//	return 0;
//}

////////////////////////////////////////////////////////////
//int main()
//{
//	/*动态开辟一个int类型的空间*/
//	int *p1 = new int;
//	delete p1;
//
//	/*动态开辟一个int类型空间，并初始化为0*/
//	int *p2 = new int(0);
//	delete p2;
//	/*动态开辟十个int类型的空间，即开辟一个数组*/
//	int *p3 = new int[10];
//	delete[] p3;
//
//	/*动态开辟一个数组，并将前4个整数初始化为1、2、3、4，随后的都被初始化为0*/
//	int *p4 = new int[10]{1, 2, 3, 4};
//	delete[] p4;
//
//	return 0;
//}

////////////////////////////////////////////////////////

//int globalVar = 1;
//static int staticGlobalVar = 1;
//void Test()
//{
//	static int staticVar = 1;
//	int localVar = 1;
//	int num1[10] = { 1, 2, 3, 4 };
//	char char2[] = "abcd";
//	const char* pChar3 = "abcd";
//	int* ptr1 = (int*)malloc(sizeof(int)* 4);
//	int* ptr2 = (int*)calloc(4, sizeof(int));
//	int* ptr3 = (int*)realloc(ptr2, sizeof(int)* 4);
//	free(ptr1);
//	free(ptr3);
//}