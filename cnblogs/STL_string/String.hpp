#pragma once

#include <string.h>
#include <assert.h>
#include <iostream>

namespace mystd
{
	class String
	{/*毕竟是模拟实现，所以就不用模板模拟basic_string<char>了*/
	public:
		/*原生指针就可以充当迭代器
		 *原生指针具有指向、修改、插入...的能力
		 *只要自定义类型有了迭代器，在外部就可以使用范围for遍历了
		 *但是一定要注意，使用范围for的类型，其迭代器一定要配套*/
		typedef char * iterator;
		typedef const char * const_iterator;
		iterator begin() {return _str;}
		iterator end() {return _str + _size;}
		const_iterator begin() const{return _str;}
		const_iterator end() const{return _str + _size;}

	public:
		/*构造、拷贝、赋值、析构*/
		String(const char *str = "");
		String(const String &str);
		//String &operator=(const String &str);
		String &operator=(String str);/*现代写法*/
		~String();

		/*有关容量的接口*/
		void reserve(int capacity);
		void resize(size_t n, const char &ch = '\0');

		/*增、删、查、改*/
		void push_back(const char &ch);
		String &insert(size_t pos, size_t n, const char &ch);
		String &insert(size_t pos, const String &s);
		String &append(size_t n, const char &ch);
		String &append(const String &s);
		String &operator+=(const char &ch);
		String &operator+=(const String &s);
		String &erase(size_t pos = 0, size_t len = npos);
		size_t find(const char &ch, size_t pos = 0) const;
		size_t find(const String &str, size_t pos = 0) const;
		String operator+(const String &str);

		/*通用接口*/
		char &operator[](size_t pos);
		const char &operator[](size_t pos) const;/*可读可写接口，要重载两个版本*/
		size_t size() const;
		size_t capacity() const;
		void clear();
		const char *c_str() const;/*只读接口*/
		void swap(String &str);

		/*输入与输出*/
		friend ostream &operator<<(ostream &out, const String &str);
		friend istream &operator>>(istream &in, String &str);

	private:
		char *_str;
		size_t _size;
		size_t _capacity;
		
		static const size_t npos = -1;/*常量整数可以在类内直接初始化*/
	};

	String::String(const char *str) /*缺省值给到声明处，定义不要再给了*/
	{
		_capacity = strlen(str) + 1;
		_size = _capacity - 1;
		_str = new char[_capacity];
		strcpy(_str, str);
	}

	String::String(const String &str)
	{
		_capacity = str._capacity;
		_size = str._size;
		_str = new char[_capacity];
		memcpy(_str, str._str,str._capacity);
	}
	

	/*传统写法*/
	//String &String::operator=(const String &str)
	//{
	//	/*统一异地扩容*/
	//	char *newStr = new char[str._capacity];
	//	memcpy(newStr, str._str, str._capacity);
	//	delete[] _str;
	//	_str = newStr;
	//	_capacity = str._capacity;
	//	_size = str._size;
	//	return *this;
	//}

	String &String::operator=(String str)
	{
		swap(str);
		return *this;
	}

	String::~String()
	{
		delete[] _str;
		_str = nullptr;
		_size = _capacity = 0;
	}

	const char *String::c_str() const/*只读接口*/
	{
		return _str;
	}

	void String::swap(String &str)
	{
		std::swap(_str, str._str);
		std::swap(_size, str._size);
		std::swap(_capacity, str._capacity);
	}


	void String::reserve(int capacity)
	{
		if (capacity > _capacity)/*扩容只有往大了扩*/
		{
			/*统一异地扩容*/
			char *newStr = new char[capacity];

			/*这里不能用strcpy，因为strcpy遇到'\0'截止
			*我们说过string类不以'\0'结尾*/
			memcpy(newStr, _str, _capacity);

			delete[] _str;
			_str = newStr;
			_capacity = capacity;
		}

	}

	void String::resize(size_t n, const char &ch)
	{
		/*resize有三种情况：小于当前有效个数->删除
		*大于当前有效个数&&小于当前容量->扩展
		*大于等于当前容量->扩容+扩展*/
		if (n <= _size)
		{
			_size = n;
			_str[_size] = '\0';
		}
		else
		{
			for (int i = _size; i < n; i++)
			{
				push_back(ch);/*复用，push_back()自动检查是否需要扩容*/
			}
		}
	}

	void String::push_back(const char &ch)
	{
		if (_size + 1 == _capacity)
		{
			/*扩容*/
			reserve(_capacity * 2);
		}
		_str[_size++] = ch;
		_str[_size] = '\0';
	}

	String &String::insert(size_t pos, size_t n, const char &ch)
	{
		assert(pos < _size);
		if (_size + n + 1 >= _capacity)
		{
			reserve(_size + n + 1);
		}
		size_t end = _size + n;
		while (end >= pos + n)
		{
			_str[end] = _str[end - n];
			--end;
		}
		while (pos <= end)
		{
			_str[pos++] = ch;
		}
		_size += n;
		return *this;
	}

	String &String::insert(size_t pos, const String &s)
	{
		assert(pos < _size);
		if (_size + s._size + 1 >= _capacity)
		{
			reserve(_size + s._size + 1);
		}
		size_t end = _size + s._size;
		while (end >= pos + s._size)
		{
			_str[end] = _str[end - s._size];
			--end;
		}

		/*插入字符串时不需要插入'\0'*/
		strncpy(_str + pos, s._str, s._size);

		_size += s._size;
		return *this;
	}

	String &String::append(size_t n, const char &ch)
	{
		if (_size + n + 1 >= _capacity)
		{
			reserve(_size + n + 1);
		}
		while (n--) push_back(ch);
		return *this;
	}

	String &String::append(const String &s)
	{
		if (_size + s._size + 1 >= _capacity)
		{
			reserve(_size + s._size + 1);
		}
		memcpy(_str + _size, s._str, s._size + 1);
		_size += s._size;/*最后不忘修改_size*/
		return *this;
	}


	String &String::operator+=(const char &ch)
	{
		push_back(ch);
		return *this;
	}

	String &String::operator+=(const String &s)
	{
		append(s);
		return *this;
	}

	String &String::erase(size_t pos, size_t len)
	{
		assert(pos < _size);
		if (len == npos || pos + len >= _size)
		{
			_str[pos] = '\0';
			_size -= len;
			return *this;
		}
		memcpy(_str + pos, _str + pos + len, _size - (pos + len) + 1);
		_size -= len;
		return *this;
	}

	/*可读可写接口，要重载两个版本*/
	char &String::operator[](size_t pos)
	{
		assert(pos < _size);
		return _str[pos];
	}

	const char &String::operator[](size_t pos) const
	{
		assert(pos < _size);
		return _str[pos];
	}

	size_t String::size() const
	{
		return _size;
	}

	size_t String::capacity() const
	{
		return _capacity;
	}

	void String::clear()
	{
		_size = 0;
		_str[0] = '\0';
	}

	size_t String::find(const char &ch, size_t pos) const
	{
		assert(pos < _size);
		for (size_t i = 0; i < _size; i++)
		{
			if (_str[i] == ch) return i;
		}
		return npos;
	}

	size_t String::find(const String &str, size_t pos) const
	{
		assert(pos < _size);
		char *findStr = strstr(_str, str._str);
		if (findStr == nullptr) return npos;
		return findStr - _str;
	}

	String String::operator+(const String &str)
	{
		String tmp(*this);
		tmp += str;
		return tmp;
	}

	ostream &operator<<(ostream &out, const String &str)
	{
		for (auto &e : str) out << e;
		return out;
	}

	istream &operator>>(istream &in, String &str)
	{
		str.clear();
		char buffer[128];
		size_t i = 0;
		char ch = in.get();
		while (ch != ' ' && ch != '\n')
		{
			if (i == 127)
			{
				str += buffer;
				i = 0;
			}
			buffer[i++] = ch;
			buffer[i] = '\0';

			ch = in.get();
		}
		if (i >= 0)
		{
			buffer[i] = '\0';
			str += buffer;
		}
		return in;
	}
}/*namespace mystd ends here*/