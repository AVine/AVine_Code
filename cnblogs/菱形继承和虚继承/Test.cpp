#include <iostream>
using namespace std;

class Person
{};
class Student : public Person
{};
int main()
{
	return 0;
}


////////////////////////////////////////////////////
//class A
//{
//public:
//	int _a;
//};
//class B
//{
//public:
//	A a;
//};
//int main()
//{
//	return 0;
//}


//////////////////////////////////////////////////
//class A
//{
//public:
//	int _a = 1;
//};
//class B : virtual public A
//{
//public:
//	int _b = 2;
//};
//class C : virtual public A
//{
//public:
//	int _c = 3;
//};
//class D : public B, public C
//{
//public:
//	int _d = 4;
//};
//
//int main()
//{
//	D d;
//	/*都不报错了，他们操作的都是同一个_a*/
//	d._a = 1;
//	d.B::_a = 3;
//	d.C::_a = 8;
//	return 0;
//}


/////////////////////////////////////////////////////////////
//class A
//{
//public:
//	int _a = 1;
//};
//class B : virtual public A // virtual为虚继承关键字
//{
//public:
//	int _b = 2;
//};
//int main()
//{
//	B b;
//	return 0;
//}


//////////////////////////////////////////////////
///*B、C继承自A---D继承自B、C
// *从而构成菱形继承*/
//class A
//{
//public:
//	int _a;
//};
//class B : public A
//{
//public:
//	int _b;
//};
//class C : public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
//
//int main()
//{
//	D d;
//	//d._a = 3; // 报错，_a不明确
//	d.B::_a = 3;
//	d.C::_a = 8;
//	return 0;
//}