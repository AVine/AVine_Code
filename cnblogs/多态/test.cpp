#include <iostream>
using namespace std;

class A
{
public:
	virtual void func1()
	{}
};

class B : virtual public A
{
public:
	// B类重写了A类的虚函数
	virtual void func1()
	{}
};

class C : virtual public A
{
public:
	// C类重写了A类的虚函数
	virtual void func1()
	{}
};

class D : public B,public C
{
public:
	// D类也必须完成重写，因为虚继承没有数据冗余和二义性
	// 所以A类部分只有一份，被放在D类对象的末尾
	// 那么不重写的话，虚表当中放哪个函数？
	virtual void func1()
	{}
};

int main()
{
	B b;
	return 0;
}


//////////////////////////////////////////////////
//class A
//{
//public:
//	virtual void func1()
//	{}
//};
//
//class B
//{
//public:
//	virtual void func1()
//	{}
//};
//
//class C : public A,public B
//{
//public:
//	virtual void func1()
//	{}
//
//	virtual void func2()
//	{}
//};
//int main()
//{
//	C c;
//	return 0;
//}

///////////////////////////////////////////////
//class A
//{
//public:
//	virtual void func1()
//	{}
//};
//
//class B : public A
//{
//public:
//	virtual void func1()
//	{}
//};
//
//class C : public B
//{
//public:
//	virtual void func1()
//	{}
//
//	virtual void func2()
//	{}
//};
//
//int main()
//{
//	B b;
//	C c;
//	return 0;
//}


//class Test
//{
//public:
//	virtual void func(){}
//};
//int main()
//{
//	Test t;
//	
//	int a = 0;
//	cout << "栈: " << (void*)&a << endl;
//
//	static int b = 0;
//	cout << "数据段: " << (void*)&b << endl;
//
//	const char * str = "nice";
//	cout << "代码段: " << (void*)str << endl;
//
//	cout << "虚表指针位置? " << *(void**)&t << endl;
//	return 0;
//}

///////////////////////////////////////////
//class Person
//{
//public:
//	virtual void func1()
//	{}
//	virtual void func2()
//	{}
//};
//
//class Student : public Person
//{
//public:
//	virtual void func1()
//	{}
//};
//int main()
//{
//	Person t;
//	Student s;
//	return 0;
//}

///////////////////////////////////////////////
//class Person
//{
//public:
//	virtual void slefMessage()
//	{
//		cout << "Person" << endl;
//	}
//};
//int main()
//{
//	Person p;
//	cout << sizeof(p) << endl;
//	return 0;
//}


//////////////////////////////////////////////////
//class Person
//{
//public:
//	virtual void slefMessage()
//	{
//		cout << "Person" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual void slefMessage()
//	{
//		cout << "Student" << endl;
//	}
//};
//
//class Teacher : public Person
//{
//public:
//	virtual void slefMessage()
//	{
//		cout << "Teacher" << endl;
//	}
//};
//
//void testPolimorphic(Person &rp)
//{
//	rp.slefMessage();//调用虚函数
//}
//int main()
//{
//	Student s;
//	Teacher t;
//	testPolimorphic(s);
//	testPolimorphic(t);
//	return 0;
//}


///////////////////////////////////////////////
//class A
//{
//public:
//	virtual void func1()
//	{
//		cout << "A::func1()" << endl;
//	}
//};
//class B : public A
//{
//public:
//	//virtual void func1()
//	//{
//	//	cout << "B::func1()" << endl;
//	//}
//};
//class C : public A
//{
//public:
//	//virtual void func1()
//	//{
//	//	cout << "C::func1()" << endl;
//	//}
//};
//class D : public B,public C
//{
//public:
//	virtual void func1()
//	{
//		cout << "D::func1()" << endl;
//	}
//	virtual int Add(int x, int y)
//	{
//		return x + y;
//	}
//};
//int main()
//{
//	D d;
//	return 0;
//}


////////////////////////////////////////////
//class A
//{
//public:
//	virtual void func1()
//	{
//		cout << "A::func1()" << endl;
//	}
//	int _a = 1;
//};
//class B
//{
//public:
//	virtual void func1()
//	{
//		cout << "B::func1()" << endl;
//	}
//	int _b = 2;
//};
//class C : public A,public B
//{
//public:
//	virtual void func1()
//	{
//		cout << "C::func1()" << endl;
//	}
//	virtual int Add(int x, int y)
//	{
//		return x + y;
//	}
//	int _c = 3;
//};
//int main()
//{
//	A a;
//	B b;
//	C c;
//
//	B *bptr = &c;
//	bptr->func1();
//	return 0;
//}

/////////////////////////////////////////////
//class A
//{
//public:
//	virtual void func1()
//	{
//		cout << "A::func1()" << endl;
//	}
//	int _a = 1;
//};
//class B : public A
//{
//public:
//	virtual void func1()
//	{
//		cout << "B::func2()" << endl;
//	}
//
//	virtual int Add(int x, int y)
//	{
//		return x + y;
//	}
//	int _b = 2;
//};
//int main()
//{
//	A a;
//	B b;
//	return 0;
//}