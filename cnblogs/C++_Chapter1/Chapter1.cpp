
#include "func.h"

int main()
{
	func();
	return 0;
}

//inline int test(int n)// 不会报错
//{
//	if (n <= 0)
//	{
//		return 0;
//	}
//	return test(n - 1);
//}
//
//int main()
//{
//	test(3);
//	return 0;
//}

//void func(int arr[])
//{
//	for (auto e : arr)// 错误，arr不是数组
//	{}
//}
//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5 };
//	func(arr);
//	return 0;
//}


//inline int Add(int x, int y)
//{
//	int sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	sum = x + y;
//	return sum;
//}
//
//int main()
//{
//	int sum = Add(1, 2);
//	return 0;
//}

//// 下面哪个宏函数是最合理的？
//#define Add(x,y) x + y 
//#define Add(x,y) (x)+(y) 
//#define Add(x,y) ((x)+(y));
//#define Add(x,y) ((x)+(y))
//
//int main()
//{
//	int sum = Add(1, 3);
//	return 0;
//}



//#include <iostream>
//using namespace std;
//
//void func(int x)
//{
//	cout << "void func(int)" << endl;
//}
//
//void func(void* p)
//{
//	cout << "void func(void*)" << endl;
//}
//
//int main()
//{
//	func(nullptr);
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 6, 7 };
//	for (auto& e : arr)
//	{
//		e++;
//		cout << e << " ";
//	}
//	cout << endl;
//	return 0;
//}



//#include <initializer_list>
//#include <iostream>
//using namespace std;
//
//int main()
//{
//	//auto il[] = { 1, 2, 3, 4, 5 };//C++不给这么玩
//	auto il = { 1, 2, 3, 4, 5 };//我们这么玩
//
//	cout << typeid(il).name() << endl;
//	return 0;
//}




//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int arr[5] = {1,2,3,4,5};
//	cout << sizeof(arr) << endl;// 输出20，很合理
//
//	//int arr2[5] = arr;// 这样的赋值是错的
//	int* parr = arr;// 这样才是对的
//	cout << sizeof(parr) << endl;//32位平台
//	return 0;
//}

//int main()
//{
//	int a1[] = { 1, 2, 3, 4, 5 };
//	//auto* a2[] = { 2, 3, 4, 5, 6 };//错误用法
//
//	auto pa = a1;
//	return 0;
//}



//#include <iostream>
//using namespace std;
//
//void func(auto i = 10)
//{
//	cout << typeid(i).name() << endl;
//}
//int main()
//{
//	func();
//	return 0;
//}

//int main()
//{
//	auto a = 1, b = 2, c = 3;
//	auto x = 1.1, y = 3.14, z = 'c';//错误
//	return 0;
//}

//int main()
//{
//	auto x;//错误，auto修饰的变量必须被初始化
//	auto y = 3;// 正确用法
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	auto a = 10;// 10为int类型，所以a为int类型
//	auto b = a;// a为int类型，所以b为int类型
//	auto c = (double)b;// b类型转换后为double类型，所以c为double类型
//
//	double& rc = c;
//	auto cc = rc;//cc为double类型，并不是double&类型
//
//	auto p1 = &a;//&a为int*类型，所以p1为int*类型
//	auto* p2 = &a;// 与上面等价
//	
//	// 输出各变量类型的名称
//	cout << typeid(a).name() << endl;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(cc).name() << endl;
//	cout << typeid(p1).name() << endl;
//	cout << typeid(p2).name() << endl;
//	return 0;
//}


//#include <vector>
//#include <string>
//
//int main()
//{
//	std::vector<std::string> vec;//创建一个存储string的vector对象
//	std::vector<std::string>::reverse_iterator it1 = vec.rbegin();//it前面是类型
//
//	auto it2 = vec.rbegin();// 使用auto推导it2的类型
//	return 0;
//}


//int main()
//{
//	auto int x = 3;
//	int y = 6;
//	return 0;
//}


//// 下面两个Add函数构成重载，这是没有问题的
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Add(int& x, int& y)
//{
//	return x + y;
//}
//
//int main()
//{
//	Add(1, 2);//匹配第一个Add，因为第二Add的参数是普通引用，引用不了常量
//
//	int x = 3, y = 4;
//	Add(x, y);// 这里就有问题了，x、y都是变量，调用任何一个Add函数都可以，所以存在二义性
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//// 两个重载func函数符合函数重载定义，他们是没有问题的
//void func(int x = 3, int y = 4)
//{
//	cout << "x = " << x << " y = " << y << endl;
//}
//
//void func(const char* str = "hello world")
//{
//	cout << str << endl;
//}
//
//int main()
//{
//	func();// 但是调用时存在二义性
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	cout << "Hello World" << endl;
//	return 0;
//}

///*int main()
//{
//	int a = 10;
//	int& ra = a;
//
//	int b = 3;
//	int* pb = &b;
//	re*/turn 0;
//}


//#include <iostream>
//using namespace std;
//
//int func()// 注意，这里是传值返回
//{
//	int x = 0;
//	++x;
//	return x;
//}
//
//int test()// 这里也是传值返回
//{
//	int x = 100;
//	return x;
//}
//
//int main()
//{
//	const int& ret = func();
//	cout << ret << endl;
//	cout << ret << endl;
//
//	test();
//	cout << ret << endl;
//	return 0;
//}


//void func(int left, int right)
//{
//	// do something...
//}
//int main()
//{
//	const int x = 3;
//	const int y = 7;
//	func(x, y);
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	double x = 3.14;
//	(int)x; // x强制类型转换成int类型，x的性质会发生变化吗？
//	cout << x << endl;// 上一条语句x已经发生类型转换，但是它本身不变
//	return 0;
//}
//
//
//int main()
//{
//	double x = 3.14;
//	const int& rx = x;
//	return 0;
//}

//int main()
//{
//	int x = 3;
//	const int& rx = x;// x被常引用引用
//	x++;// 但是不会修改其权限
//	// x++语句执行完后，x的值为4，rx的值也为4
//	return 0;
//}

//int main()
//{
//	const int y = 7;
//	const int& ry = y;//权限平移
//
//	int z = 3;
//	const int& rz = z;//权限缩小
//	return 0;
//}




//#include <iostream>
//using namespace std;
//#include <time.h>

//// 这个结构体就有 4w字节
//struct A
//{
//	int arr[10000];
//};
//
//struct A a;// 定义一个全局变量
//
//struct A func1()// 传值返回
//{
//	return a;
//}
//
//struct A& func2()// 引用返回
//{
//	return a;
//}
//
//int main()
//{
//	// 计算传值返回的时间差
//	size_t beign1 = clock();
//	for (int i = 0; i < 100000; i++)// 调用10W次传值返回的函数
//	{
//		func1();
//	}
//	size_t end1 = clock();
//
//
//	// 计算引用返回的时间差
//	size_t begin2 = clock();
//	for (int i = 0; i < 100000; i++)
//	{
//		func2();
//	}
//	size_t end2 = clock();
//
//	cout << "struct A func1():" << end1 - beign1 << endl;
//	cout << "struct A& func2():" << end2 - begin2 << endl;
//
//	return 0;
//}





//#include <iostream>
//using namespace std;
//
//int& func()
//{
//	int x = 0;
//	++x;
//	return x;
//}
//
//void test()
//{
//	int x = 100;
//}
//
//int main()
//{
//	const int& ret = func();
//	cout << ret << endl;
//	cout << ret << endl;
//
//	test();
//	cout << ret << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//void swap(int& left, int& right)
//{
//	int tmp = left;
//	left = right;
//	right = tmp;
//}
//int main()
//{
//	int x = 3;
//	int y = 7;
//	swap(x, y);
//	cout << "x = " << x << ",y = " << y << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//int main()
//{
//	int z = 6;
//	int& rz = z;
//	
//	int w = 9;
//	rz = w;// 我们的本意是改变rz的引用实体，但实际上发生的是赋值
//	cout << z << endl;
//	return 0;
//}

//int main()
//{
//	int x = 3;
//
//	// 一个变量可以被引用多次
//	int& rx1 = x;
//	int& rx2 = x;
//
//	// 引用之间可以连续引用
//	int& rx3 = rx1;
//	int& rx4 = rx3;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int a = 3;
//	int& ra = a;// 引用变量引用已经存在的变量a
//
//	ra++;// 对引用变量操作就是对被引用的变量操作
//	cout << "a = " << a << " ra = " << ra << endl;
//	return 0;
//}



//#include <iostream>
//using namespace std;
//
//int add(int x, int y)
//{
//	return x + y;
//}
//
//double add(int x, int y)// 错误
//{
//	return x + y;
//}
//int main()
//{
//	add(3, 5);
//	return 0;
//}


//// 参数的类型不同，可以构成重载
//void func(int x, int y)
//{
//	cout << "func(int,int)" << endl;
//}
//
//void func(char x, char y)
//{
//	cout << "func(char,char)" << endl;
//}
//
//void func(int x, char y)
//{
//	cout << "func(int,char)" << endl;
//}
//
//// 保证参数的个数不同，可以构成重载
//void test(int x, int y)
//{
//	cout << "test(int,int)" << endl;
//}
//
//void test(int x)
//{
//	cout << "test(int)" << endl;
//}
//
//void test()
//{
//	cout << "test()" << endl;
//}
//
//// 参数的顺序不同，可以构成重载
//// 不过一定要注意，是类型的顺序不同!
//void count(int x, double y, char z)
//{
//	cout << "count(int,double,char)" << endl;
//}
//
//void count(double x, int y, char z)
//{
//	cout << "count(double,int,char)" << endl;
//}
//int main()
//{
//	return 0;
//}

//void swap(int* x, int* y)
//{
//	// ...
//}
//
//void swap(double* x, double* y)
//{
//	// ...
//}
//
//void swap(char* x, char* y)
//{
//	// ...
//}
//int main()
//{
//	int a = 3, b = 5;
//	swap(&a, &b);// 调用 void swap(int* x,int* y);
//
//	double x = 7.2, y = 3.4;
//	swap(&x, &y);// 调用 void swap(double* x,double* y);
//
//	char n = 'a', m = 'z';
//	swap(&n, &m);// 调用 void swap(char* x,char* y);
//	return 0;
//}



//int add(int x, int y)
//{
//	return x + y;
//}
//
//double add_double(double x, double y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int a = 1, b = 4;
//	add(a, b);// 两个整数可以调用add函数相加
//
//	double x = 6.6, y = 9.9;
//	add(x, y);// 两个浮点数也可以，但我们不想有任何精度损失应该怎么办？
//
//	// 只能重新定义一个与add函数不冲突的函数了
//	add_double(x, y);
//
//	return 0;
//}

//struct Stack
//{
//	int* a;
//	int size;
//	int capacity;
//};
//
//// 如果我们确实不清楚要把多少个数据存入栈中
//// 初始化时就以4开始，后面慢慢扩容
//void capacity_init(struct Stack* st,int cap = 4)
//{
//	//...做一些扩容的工作
//	st->capacity = cap;
//	// ...
//}
//
//int main()
//{
//	struct Stack st;
//	capacity_init(&st);// 不确定存多少个数据，使用缺省值然后扩容
//	capacity_init(&st, 100);// 如果我们已经确定要存100个数据,就不需要再扩容增加消耗了
//	return 0;
//}

//void func(int x = 6);// 只能在声明中给定缺省参数
//
//int main()
//{
//	func();
//	return 0;
//}
//
//void func(int x)
//{
//	cout << x << endl;
//}



//// 没有缺省值的参数在有缺省值参数的左边
//void func1(int a, int b = 20, int c = 30)// 正确半缺省
//{
//	cout << "a = " << a << " ";
//	cout << "b = " << b << " ";
//	cout << "c = " << c << endl;
//}
//
//// 没有缺省值参数在有缺省值参数的右边
//void func2(int a = 10, int b, int c = 30)// 错误半缺省
//{
//	cout << "a = " << a << " ";
//	cout << "b = " << b << " ";
//	cout << "c = " << c << endl;
//}
//
//// 没有缺省值参数在有缺省值参数的右边
//void func3(int a = 10, int b = 20, int c)// 错误半缺省
//{
//	cout << "a = " << a << " ";
//	cout << "b = " << b << " ";
//	cout << "c = " << c << endl;
//}
//
//int main()
//{
//	/*func();
//	func(100);
//	func(100, 200);
//	func(100, 200, 300);*/
//	return 0;
//}

//void func(int x = 3)
//{
//	cout << x << endl;
//}
//int main()
//{
//	func();// 没有指定实参，func函数的x参数使用它的缺省值
//	func(5);// 指定了实参，func函数的x参数使用这个实参
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int x = 3;
//	x = x << 1;//这里还是位移运算符
//	cout << x << endl;//这里便是流插入运算符
//	return 0;
//}




//#include <stdio.h>
//namespace ly
//{
//	int x = 3;
//	namespace lll
//	{
//		int x = 6;
//	}
//	namespace ly
//	{
//		int x = 4;
//	}
//}
//
//int main()
//{
//	printf("%d\n", ly::x);
//	printf("%d\n", ly::lll::x);
//	printf("%d\n", ly::ly::x);// 双兔傍地走,安能辨我是雄雌?
//	return 0;
//}

//namespace ly
//{
//	int x = 3;
//}
//
//namespace ly
//{
//	int y = 5;
//}
//
//namespace ly
//{
//	int z = 9;
//}
//
//int main()
//{
//	//namespace ly// 错误，局部域不允许定义
//	//{
//	//	int m = 8;
//	//}
//	return 0;
//}


//#include <stdio.h>
//namespace ly
//{
//	int x = 3;
//	void func()
//	{
//		printf("ly::func()\n");
//	}
//}
//
//using ly::x;// 将命名空间中的x"释放"
//int main()
//{
//	printf("%d\n", x);
//	ly::func();// 未释放的成员必须用"::"访问
//	return 0;
//}


//#include <stdio.h>
//namespace ly// 在全局域定义一个命名空间
//{
//	// 在命名空间中可以定义变量、类型、函数...
//	int x = 3;
//	struct Student
//	{};
//	void func()
//	{
//		printf("ly::func()\n");
//	}
//}
//
//// 不会与命名空间的成员发生冲突
//int x = 6;
//struct Student
//{};
//void func()
//{
//	printf("func()\n");
//}
//
//int main()
//{
//	int x = 9;
//	printf("%d\n", x);//使用x时，编译器从当前开始网上查找x
//	printf("%d\n", ::x);//"::"指定在全局域中找x
//	printf("%d\n", ly::x);//使用命名空间中的x
//
//	struct Student s1;// 使用全局域的Student类型定义变量
//	struct ly::Student s2;// 使用全局域的ly命名空间中的Student类型定义变量
//
//	func();// 调用全局域的func函数
//	ly::func();// 调用全局域中ly命名空间中的func函数
//
//	return 0;
//}


//#include <stdio.h>
//#include <stdlib.h>
//
//int rand = 10;
//int main()
//{
//	printf("%d\n", rand);
//	return 0;
//}