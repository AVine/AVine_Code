#pragma once

#include <iostream>
#include <mutex>
//饿汉单例模式
//class singleton
//{
//public:
//	static singleton &get_instance()
//	{
//		return _sln;
//	}
//	
//	void start()
//	{
//		std::cout << "服务器启动！" << std::endl;
//	}
//
//	
//	// 与拷贝有关的函数禁掉
//	singleton(const singleton& sln) = delete;
//	singleton& operator=(const singleton& sln) = delete;
//protected:
//	singleton()
//	{}
//
//	static singleton _sln;
//};
//singleton singleton::_sln;


template <class Lock>
class LockGuard
{
public:
	LockGuard(Lock &lock)
		:_lock(lock)
	{
		_lock.lock();
	}

	~LockGuard()
	{
		_lock.unlock();
	}
protected:
	Lock& _lock;
};

//懒汉单例模式
class singleton
{
public:
	static singleton& get_instance()
	{
		//LockGuard<std::mutex> lock(_mutex);
		if (_psln == nullptr)// 双重判断
		{
			std::unique_lock<std::mutex> lock(_mutex);
			if (_psln == nullptr)
			{
				_psln = new singleton();// new会抛异常导致来不及解锁
			}
		}
		return *_psln;
	}

	void start()
	{
		std::cout << "服务器启动！" << std::endl;
	}

	// 与拷贝有关的函数禁掉
	singleton(const singleton& sln) = delete;
	singleton& operator=(const singleton& sln) = delete;
protected:
	singleton()
	{}

	static singleton *_psln;
	static std::mutex _mutex;
};
singleton *singleton::_psln = nullptr;
std::mutex singleton::_mutex;