#include "smartPtr.h"
#include <iostream>
#include <thread>
using namespace std;

class Test
{
  public:
    Test()
    {
      cout << "Test()" << endl;
    }
    ~Test()
    {
      cout << "~Test()" <<endl;
    }
  protected:
    
};


void test1()
{
  int cnt = 100;
 sharedPtr<Test> sp1(new Test());
  thread t1([&]
      {
        while(cnt--)
          sharedPtr<Test> sp2(sp1);
      });
  
  thread t2([&]
      {
        while(cnt--)
          sharedPtr<Test> sp3(sp1);
      });

  // 如果不进行线程等待，就会出现"主线程先退出导致新线程无法正常工作"

  cout << sp1.use_count() << endl;

  t1.join();
  t2.join();
}

template <class T>
struct listNode
{
 // listNode<T> *next;
  //listNode<T> *prev;
  weakPtr<listNode<T>> next;
  weakPtr<listNode<T>> prev;
  T val;

  ~listNode()
  {
    cout << "~listNode" <<endl;
  }
};

void test2()
{
  // 研究循环引用
  sharedPtr<listNode<int>> n1(new listNode<int>());
  sharedPtr<listNode<int>> n2(new listNode<int>()); 
  n1->next = n2;
  n2->prev= n1;
}


template <class T>
struct deletor 
{
  void operator()(T *ptr)
  {
    delete[] ptr;
  }
};
void test3()
{
  // 研究定制删除器
  //auto del = [](listNode<int> *pl){delete[] pl;};
  deletor<listNode<int>> del;
  sharedPtr<listNode<int>,decltype(del)> sp1(new listNode<int>[10]);
  uniquePtr<listNode<int>,decltype(del)> up1(new listNode<int>[10]);
  //unique_ptr<listNode<int>,decltype(del)> up(new listNode<int>[10]);
}
int main()
{
 //uniquePtr<Test> up1(new Test());
 // uniquePtr<Test> up2(up1);
  test3(); 
  return 0;
}
