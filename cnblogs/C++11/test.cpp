#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <list>
#include <functional>
#include <string>
#include <memory>
#include <thread>
#include <atomic>
#include <mutex>
using namespace std;
#include "test_singleton.hpp"



int main()
{
	singleton::get_instance().start();
	return 0;
}
//////////////////////////////////////////////////
//int main()
//{
//	singleton::get_instance().start();
//	singleton sln = singleton::get_instance();
//	return 0;
//}

//////////////////////////////////////////////////////////////
////1. 设计一个只能在堆上创建的类
//class heap_only
//{
//public:
//	heap_only* new_heap_only()
//	{
//		return new heap_only();
//	}
//protected:
//	//1. 构造函数不能在外部使用
//	heap_only()
//	{}
//	//2. 有关拷贝的函数不能在外部使用
//	heap_only(const heap_only& ho) = delete;
//	heap_only& operator=(const heap_only& ho) = delete;
//};
//
//void heap_only_test()
//{
//	heap_only* ph = nullptr;
//	ph = ph->new_heap_only();
//
//	heap_only ho1;
//	heap_only ho2(*ph);
//}
//int main()
//{
//	heap_only_test();
//	return 0;
//}

//////////////////////////////////////////////////////////
//int val = 0;
////atomic<int> val = 0;
//mutex _mutex;
//void func1()
//{
//	int i = 10000;
//	while (i--)
//	{
//		unique_lock<mutex> lock(_mutex);
//		++val;
//	}
//}
//void func2()
//{
//	int i = 10000;
//	while (i--)
//	{
//		unique_lock<mutex> lock(_mutex);
//		++val;
//	}
//}
//int main()
//{
//	thread t1(func1);
//	thread t2(func2);
//	
//	t1.join();
//	t2.join();
//
//	cout << val << endl;
//	return 0;
//}
//class Test
//{
//public:
//	Test()
//		:_pi(new int[100]),_pd(new double[100])
//	{}
//	~Test()
//	{
//		delete[] _pi;
//		delete[] _pd;
//		_pi = nullptr;
//		_pd = nullptr;
//	}
//
//	Test(const Test& t)	// 左值引用，完成深拷贝
//	{
//		_pi = new int[100];
//		_pd = new double[100];
//		memcpy(_pi, t._pi, sizeof(int) * 100);
//		memcpy(_pd, t._pd, sizeof(double) * 100);
//		cout << "拷贝构造" << endl;
//	}
//
//	Test(Test &&t)	// 左值引用，完成深拷贝
//	{
//		swap(t);
//		cout << "移动构造" << endl;
//	}
//
//	Test& operator=(const Test& t)
//	{
//		Test tmp(t);//现代写法
//		swap(tmp);
//		cout << "赋值重载" << endl;//会多打印一次"拷贝构造"
//		return *this;
//	}
//
//	Test& operator=(Test&& t)// 右值引用版本
//	{
//		this->~Test();
//		swap(t);
//		cout << "移动赋值" << endl;
//		return *this;
//	}
//
//	void swap(Test& t)
//	{
//		std::swap(_pi, t._pi);
//		std::swap(_pd, t._pd);
//	}
//protected:
//	int* _pi = nullptr;
//	double* _pd = nullptr;
//};
//
//Test func111()
//{
//	Test t;
//	//中间干了一些别的事
//	return t;
//}
//
////int main()
////{
////	Test ret = func111();
////	return 0;
////}
//int main()
//{
//	Test t1;
//	Test t2;
//	Test t3;
//	
//	t1 = t2;
//	t1 = move(t3);
//	return 0;
//}

/////////////////////////////////////////////////////
//int main()
//{
//int&& r1 = 10;
//const& r2 = 10;
//int a = 3;
//int&& r3 = a;// 右值引用不能引用左值，编译报错
//int&& r4 = move(a);// 但是可以引用move()以后的左值
//	return 0;
//}
//////////////////////////////////////////////////////////////
////int Add(int x, int y)
////{
////	return x + y;
////}
////
//string func(const string& str)// 做参数时可以减少一次拷贝
//{
//	string ret;
//	//.....
//	ret = "1231231231231";
//	return ret;// 不可避免地发生一次拷贝
//}
//int main()
//{
//	int a = 3;// 能被取地址，左值
//	const int b = 6;//能被取地址，左值
//	10;//不能被取地址，右值
//	1 + 1;//不能被取地址，右值
//	Add(100, 200);//返回值不能被取地址，右值
//	return 0;
//}



////////////////////////////////////////////////////////////
//class Socket
//{
//public:
//	void close()
//	{
//		cout << "close()" << endl;
//	}
//};
//
//int main()
//{
//	auto deletor = [](Socket* ps)
//	{
//		ps->close();
//		delete ps;
//	};
//	unique_ptr<Socket, decltype(deletor)> up(new Socket, deletor);
//	return 0;
//}


//#include <stdarg.h>
//// 模拟实现printf
//
//void Printf(const char* format, ...)
//{
//	va_list args;
//	va_start(args,format);
//	vprintf(format, args);
//	
//}
//
//int main()
//{
//	Printf("nihao: %s\n", "nice to meet you");
//	return 0;
//}
//template <class T, class... Args>
//void LOG(T&& t)
//{
//	cout << t << endl;
//}
//
//template <class T,class... Args>
//void LOG(T&& t, Args&&... args)
//{
//	cout << t << " ";
//	LOG(args...);
//}
//
//int main()
//{
//	LOG("nice to meet you: %s","nihao");
//	return 0;
//}

///////////////////////////////////////////
//template <class T,class... Args>
//void foo(T&& t, Args&&... args)
//{
//	cout << sizeof...(Args) << endl;
//	cout << sizeof...(args) << endl;
//}

//template <class T, class... Args>
//void foo(T&& t)
//{
//	cout << t << endl;
//}
//template <class T, class... Args>
//void foo(T&& t, Args&&... args)
//{
//	cout << t << ",";
//	foo(args...);
//}
//int main()
//{
//	int i = 0;
//	double d = 3.14;
//	string s = "how now brown cow";
//	foo(i, s, 42, d);
//	//foo(s, 42, "hi");
//	//foo(d, s);
//	//foo("hi");
//	return 0;
//}

///////////////////////////////////////////////////
//int main()
//{
//	auto t = bind(printf, "%s\n", std::placeholders::_1);
//	t("nice");
//	return 0;
//}

//////////////////////////////////////////////
//class String
//{
//public:
//	String(const char* str = "")
//	{
//		size_t len = strlen(str);
//		_size = len;
//		_capacity = len + 1;
//		_str = new char[_capacity];
//		strcpy(_str, str);
//	}
//
//	String(const String& s)// 左值->深拷贝
//	{
//		cout << "深拷贝" << endl;
//		_size = s._size;
//		_capacity = s._capacity;
//		_str = new char[_capacity];
//		memcpy(_str, s._str, _capacity);
//	}
//
//	String& operator=(const String& s)
//	{
//		cout << "拷贝赋值" << endl;
//		String tmp(s);
//		swap(tmp);
//		return *this;
//	}
//
//	String(String&& s)// 右值->将亡值->资源转换
//	{
//		cout << "移动拷贝" << endl;
//		swap(s);
//	}
//
//	String& operator=(String&& s)
//	{
//		cout << "移动赋值" << endl;
//		swap(s);
//		return *this;
//	}
//
//	void swap(String& s)
//	{
//		std::swap(_str, s._str);
//		std::swap(_size, s._size);
//		std::swap(_capacity, s._capacity);
//	}
//private:
//	char* _str = nullptr;
//	size_t _size = 0;
//	size_t _capacity = 0;
//};
//
//class Storage : public String
//{
//public:
//	Storage() {}
//	Storage(const Storage& st) = default;
//	Storage(Storage&& st) = default;
//};
//
//int main()
//{
//	Storage st1;
//	Storage st2(st1);
//	Storage st3(move(st1));
//	return 0;
//}

//class Storage
//{
//public:
//	// 析构、拷贝构造、赋值运算符重载都不实现
//	// 默认生成一个移动构造
//	// 移动构造对于自定义类型不处理，自定义类型调用它的移动构造(拷贝)
//	Storage() {}
//	//Storage(Storage &&st)
//	//	:_s(std::forward<String>(st._s))
//	//{}
//
//	Storage(Storage&& st) = default;
//
//	Storage(const Storage& st){}
//protected:
//	String _s;
//};

//int main()
//{
//	Storage st1;
//	Storage st2(st1);
//	Storage str3(move(st1));
//	return 0;
//}

//////////////////////////////////////////////////
//void func(int& x)
//{
//	cout << "左值引用: void func(int& x)" << endl;
//}
//void func(const int& x)
//{
//	cout << "const左值引用: void func(const int& x)" << endl;
//}
//
//void func(const int&& x)
//{
//	cout << "const右值引用: void func(const int&& x)" << endl;
//}
//void func(int&& x)
//{
//	cout << "右值引用: void func(int&& x)" << endl;
//}
//template<class T>
//class Test
//{
//public:
//	// 类模板一定要实例化之后才能使用，那么里面的函数模板也会跟着实例化
//	// 所以想要实现万能引用的函数必须是一个单独的函数模板
//	template<class T>
//	void funcBuilder(T&& x)
//	{
//		func(std::forward<T>(x));
//	}
//};
//int main()
//{
//	Test<int> t;
//	t.funcBuilder(10);
//	int a = 3;
//	t.funcBuilder(a);
//	return 0;
//}

//////////////////////////////////////////////////////////
//void func(int&& x)// 右值引用本质是一个左值
//{
//	cout << "void func(int&& x)" << endl;
//}
//
//void func(int& x)// 右值引用本质是一个左值
//{
//	cout << "void func(int& x)" << endl;
//}

//void func(int& x)
//{
//	cout << "左值引用: void func(int& x)" << endl;
//}
//void func(const int& x)
//{
//	cout << "const左值引用: void func(const int& x)" << endl;
//}
//
//void func(const int&& x)
//{
//	cout << "const右值引用: void func(const int&& x)" << endl;
//}
//void func(int&& x)
//{
//	cout << "右值引用: void func(int&& x)" << endl;
//}
//template <class T>
//void funcBuilder(T&& x)
//{
//	//func(x);
//	func(std::forward<T>(x));
//}
//int main()
//{
//	//func(1);
//	//int a = 3;
//	//func(a);
//	//func(move(a));
//
//	funcBuilder(1);
//	int b = 3;
//	funcBuilder(b);
//	funcBuilder(move(b));
//	const int c = 8;
//	funcBuilder(c);
//	funcBuilder(move(c));
//
//	return 0;
//}

//class String
//{
//public:
//	String(const char* str = "")
//	{
//		size_t len = strlen(str);
//		_size = len;
//		_capacity = len + 1;
//		_str = new char[_capacity];
//		strcpy(_str, str);
//	}
//
//	String(const String& s)// 左值->深拷贝
//	{
//		cout << "深拷贝" << endl;
//		_size = s._size;
//		_capacity = s._capacity;
//		_str = new char[_capacity];
//		memcpy(_str, s._str, _capacity);
//	}
//
//	String& operator=(const String& s)
//	{
//		cout << "拷贝赋值" << endl;
//		String tmp(s);
//		swap(tmp);
//		return *this;
//	}
//
//	String(String&& s)// 右值->将亡值->资源转换
//	{
//		cout << "移动拷贝" << endl;
//		swap(s);
//	}
//
//	String& operator=(String&& s)
//	{
//		cout << "移动赋值" << endl;
//		swap(s);
//		return *this;
//	}
//
//	void swap(String& s)
//	{
//		std::swap(_str, s._str);
//		std::swap(_size, s._size);
//		std::swap(_capacity, s._capacity);
//	}
//private:
//	char* _str = nullptr;
//	size_t _size = 0;
//	size_t _capacity = 0;
//};
//String toString(int val)
//{
//	String s("nice");
//	// do something...
//	return s;
//}
//
//int main()
//{
//	String s1("hello world");
//	String s2(s1);
//	String s3(move(s1));
//	s3 = move(s2);
//
//	//list<String> lt;
//	//lt.push_back("123");
//	return 0;
//}
