
#include <iostream>
using namespace std;

class A
{
public:
	void print()
	{
		cout << _a << endl;
	}
private:
	int _a;
};

int main()
{
	A::print();
	return 0;
}


//#include <iostream>
//using namespace std;
//class A
//{};
//
//class B
//{
//public:
//	void print(){}
//};

//class C
//{
//public:
//	void print(){}
//private:
//	int _c;
//};
//
//int main()
//{
//	A a;
//	B b;
//	cout << &a << endl;
//	cout << &b << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//class A
//{
//public:
//	void print()
//	{
//		cout << _a << endl;
//	}
//private:
//	int _a;
//};
//
//int main()
//{
//	A a1;
//	A a2;
//	A a3;
//	a1.print();
//	a2.print();
//	a3.print();
//	return 0;
//}

//#include <iostream>
//using namespace std;
//class Date// 日期类
//{
//public:
//	void Init(int year = 0, int month = 0, int day = 0)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void print()
//	{
//		cout << _year << ":" << _month << ":" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1, d2;
//	d1.Init(2023, 4, 22);// 隐式传递d1对象的地址
//	d2.Init(2024, 12, 11);// 隐式传递d2对象的地址
//	d1.print();// 隐式传递d1对象的地址
//	d2.print();// 隐式传递d2对象的地址
//	return 0;
//}


//#include <stdlib.h>
//struct Stack
//{
//	void Init();
//
//	int* a;
//	int top;
//	int capacity;
//};
//
//struct Queue
//{
//	void Init();
//
//	int* a;
//	int front;
//	int tail;
//	int capacity;
//};
//
//void Init()
//{
//	a = (int*)malloc(4);
//	top = 0;
//	capacity = 4;
//}
//
//void Init()
//{
//	a = (int*)malloc(4);
//	front = 0;
//	tail = 0;
//	capacity = 4;
//}
//
//int main()
//{
//	return 0;
//}



//#include <iostream>
//using namespace std;
//#include <stdlib.h>
//
//class Stack
//{
//public:
//	void Init()
//	{
//		a = (int*)malloc(4);
//		top = capacity = 0;
//	}
//private:
//	int* a;
//	int top;
//	int capacity;
//};
//
//int main()
//{
//	Stack st;
//	st.Init();
//	cout << st.top << endl;
//	return 0;
//}



//struct Student
//{
//	char* name;
//
//	// 函数当中这样使用成员，是否正确？
//	void Init()
//	{
//		name = "";
//		id = 0;
//		age = 0;
//	}
//
//	char* id;
//	int age;
//};

//struct Student
//{
//	char* name;
//	char* id;
//	int age;
//
//	void eat(){} // 吃饭行为
//	void sleep(){}// 睡觉行为
//	void study(){}// 学习行为
//};
//
//int main()
//{
//	Student::name = "Zhang San";
//	Student::eat();
//	return 0;
//}