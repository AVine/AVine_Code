
#include <iostream>
using namespace std;

class Coords//坐标类
{
public:
	Coords(int x = 0, int y = 0)
	{
		_x = x;
		_y = y;
	}

	Coords* operator&()
	{
		// 这里也可以写断言、异常之类的，反正取地址就终止
		return nullptr;
	}

	const Coords* operator&() const
	{
		return nullptr;
	}
private:
	int _x;
	int _y;
};

int main()
{
	Coords c1;
	const Coords c2;
	cout << &c1 << " " << &c2 << endl;
	return 0;
}


//class Student
//{
//public:
//	Student(char* name = "", int id = 0, int age = 0)
//	{
//		_name = name;
//		_id = id;
//		_age = age;
//	}
//
//	Student& operator=(const Student& s)// 要赋值过来的对象不改变，所以使用常引用
//	{
//		_name = s._name;
//		_id = s._id;
//		_age = s._age;
//
//		//return s;// 如果返回右操作数，返回类型就要设计为常引用
//		return *this;// 返回右操作数，明智之举
//	}
//private:
//	char* _name;
//	int _id;
//	int _age;
//};
//int main()
//{
//	Student s1("张三", 123456, 21);
//	Student s2;
//	Student s3;
//	s3 = s2 = s1;// operator=有返回值就可以连续赋值
//
//	// 假设Student支持++操作
//	//(s3 = s2 = s1)++;// 返回值为普通引用就可以支持++操作
//	return 0;
//}

//int operator+(int x, int y)// C++:以下犯上？
//{
//	return x + y;
//}
//int main()
//{
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)// 默认构造函数
//	{
//		if (year >= 1 && (month >= 1 && month <= 12) && (day >= 1 && day <= getMonthDay(year, month)))
//		{// 只有日期合法才进行初始化
//			_year = year;
//			_month = month;
//			_day = day;
//		}
//		else
//		{
//			cout << "日期初始化数据不合法!" << endl;
//			exit(1);// 直接让该程序退出
//		}
//	}
//
//	void Print() const// const对象专用
//	{
//		cout << _year << ":" << _month << ":" << _day << endl;
//	}
//
//
//	void Print()// 非const对象专用
//	{
//		cout << _year << ":" << _month << ":" << _day << endl;
//	}
//
//	bool operator>(const Date& d) const//*this不改变，可以加const
//	{
//		if (_year > d._year)// 比较年份
//		{
//			return true;
//		}
//		else if (_year == d._year && _month > d._month)// 年份相同比较月份
//		{
//			return true;
//		}
//		else if (_year == d._year && _month == d._month && _day > d._day)// 年份、月份相同比价日
//		{
//			return true;
//		}
//		return false;// 不大于
//	}
//
//	bool operator== (const Date& d) const//*this不改变，加const
//	{
//		if (_year == d._year && _month == d._month && _day == d._day)
//		{
//			return true;
//		}
//		return false;
//	}
//
//	bool operator>=(const Date& d) const//*this不改变，加const
//	{
//		return *this > d || *this == d;
//	}
//
//	bool operator<(const Date& d) const
//	{
//		if (*this >= d)
//		{
//			return false;
//		}
//		return true;
//	}
//
//	bool operator<=(const Date& d) const
//	{
//		if (*this > d)//如果>就不是<=
//		{
//			return false;
//		}
//		return true;
//	}
//
//	bool operator!=(const Date& d) const
//	{
//		if (*this == d)
//		{
//			return false;
//		}
//		return true;
//	}
//
//	Date& operator+=(int day)// *this改变，不能加const
//	{
//		if (day < 0)// 如果天数是负数
//		{
//			return *this -= abs(day);// 取day的绝对值
//		}
//		_day += day;// 直接让天数相加
//		while (_day > getMonthDay(_year, _month))//如果天数大于本月最大天数
//		{
//			_day -= getMonthDay(_year, _month);
//			++_month;// 月份++
//
//			if (_month == 13)// 如果月份超过12月
//			{
//				++_year;
//				_month = 1;
//			}
//		}
//		return *this;
//	}
//
//	Date operator+(int day) const//*this不改变，加const
//	{
//		Date tmp(*this);// tmp为*this的拷贝
//		tmp += day;
//		return tmp;
//	}
//
//	Date& operator-=(int day)
//	{
//		if (day < 0)// 如果天数是负数
//		{
//			return *this += abs(day);
//		}
//
//		_day -= day;// 天数先相减
//		while (_day <= 0)// 当天数不合法的时候
//		{
//			--_month;
//			if (_month == 0)
//			{
//				--_year;
//				_month = 12;
//			}
//			_day += getMonthDay(_year, _month);
//		}
//		return *this;
//	}
//
//	Date operator-(int day) const
//	{
//		Date tmp(*this);
//		tmp -= day;
//		return tmp;
//	}
//
//	Date& operator++()// 前置++
//	{
//		*this += 1;
//		return *this;// 轻轻松松
//	}
//
//	Date operator++(int)// 后置++
//	{
//		Date tmp(*this);
//		*this += 1;
//		return tmp;
//	}
//
//	Date& operator--()// 前置--
//	{
//		*this -= 1;
//		return *this;
//	}
//
//	Date operator--(int)// 后置--
//	{
//		Date tmp(*this);
//		*this -= 1;
//		return tmp;
//	}
//
//	int operator-(const Date& d) const// *this不改变，加const
//	{
//		Date max = *this;
//		Date min = d;
//		int flag = 1;
//
//		//if (*this < d)
//		if (d > *this)
//		{
//			max = d;
//			min = *this;
//			flag = -1;
//		}
//
//		int n = 0;
//		while (min != max)
//		{
//			++n;
//			++min;// 小的日期一直++，直到与max相等。++多少次就是相差多少天
//		}
//		return n;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//
//	int getMonthDay(int year, int month)
//	{
//		int day[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//
//		// 如果是2月又是闰年
//		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
//		{
//			return 29;
//		}
//		return day[month];
//	}
//
//	// 友元函数的声明可以出现在类中的任意位置
//	friend ostream& operator<<(ostream& out, const Date& d);
//	friend istream& operator>>(istream& in, Date& d);
//
//};
//
//
//ostream& operator<<(ostream& out, const Date& d)
//{
//	out << d._year << ":" << d._month << ":" << d._day;
//	return out;// 返回值是为了连续输出的场景
//}
//
//istream& operator>>(istream& in, Date& d)
//{
//	in >> d._year >> d._month >> d._day;
//	return in;// 返回值是为了连续输入的场景
//}
//
//int main()
//{
//	Date d1(2023, 5, 1);
//	//d1.Print();// 非const对象能调用
//
//	const Date d2(2023, 4, 29);
//	//d2.Print();// const对象也能调用
//
//	cout << d1 << " " << d2 << endl;
//	return 0;
//}


//class Date
//{
//public:
//	friend ostream& operator<<(ostream& out, const Date& d);
//	friend istream& operator>>(istream& in, Date& d);
//
//private:
//	int _year = 1;
//	int _month = 1;
//	int _day = 1;
//};
//ostream& operator<<(ostream& out, const Date& d)
//{
//	out << d._year << ":" << d._month << ":" << d._day;
//	return out;// 返回值是为了连续输出的场景
//}
//
//istream& operator>>(istream& in, Date& d)
//{
//	in >> d._year >> d._month >> d._day;
//	return in;// 返回值是为了连续输入的场景
//}


//#include "Date.h"

//void func()
//{}
//int main()
//{
//	return 0;
//}

//int main()
//{
//	Date d1, d2;
//	cin >> d1 >> d2;// 连续输入
//	cout << d1 << " " << d2 << endl;// 连续输出
//	return 0;
//}
//


//
//
//int main()
//{
//	Date d1(2023, 4, 27);
//	Date d2(2024, 4, 27);
//	// 想要比较这两个对象的大小，必须通过函数
//	bool ret = d1 > d2;
//	//bool ret = d1.operator>(d2);//效果与上面等价，但是不会这么用
//	return 0;
//}

//#include <cstdlib>
//#include <cstring>
//using namespace std;
//
//class Stack
//{
//public:
//	~Stack()
//	{
//		free(_a);
//		_a = nullptr;
//		_top = _capacity = 0;
//	}
//
//	Stack(int capacity = 4)
//	{
//		_a = (int*)malloc(sizeof(int)* capacity);
//		 _top = 0;
//		 _capacity = capacity;
//	}
//
//	Stack(const Stack& st)// 拷贝构造->用于初始化
//	{
//		_a = (int*)malloc(sizeof(int)* st._capacity);
//		memcpy(_a, st._a, st._capacity);
//	
//		_top = st._top;
//		_capacity = st._capacity;
//	}
//
//	Stack& operator=(const Stack& st)
//	{
//		if (this != &st)// 如果赋值符号两边不是一个对象才允许赋值
//		{
//			free(_a);// 将以前的空间释放掉
//			_a = (int*)malloc(sizeof(int)* st._capacity);
//			memcpy(_a, st._a, st._capacity);
//
//			_top = st._top;
//			_capacity = st._capacity;
//		}
//
//		return *this;
//	}
//private:  
//	int* _a;
//	int _top;
//	int _capacity;
//};
//
//
//int main()
//{
//	Stack s1(10000);
//	Stack s2;
//	
//	// 这两个"="有什么区别?
//	Stack s3 = s1;
//	s2 = s1;
//	return 0;
//}



//#include <cstdlib>
//using namespace std;
//
//class Stack
//{
//public:
//	void Push(int in)
//	{
//		// 暂时不考虑扩容...
//		_a[_top++] = in;
//	}
//private:
//	int* _a = (int*)malloc(sizeof(int)*4);
//	int _top = 0;
//	int _capacity = 4;
//};
//
//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)// 默认构造函数
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	int _year = 1;
//	int _month = 1;
//	int _day = 1;
//	Stack _st;
//};
//
//int main()
//{
//	Date d1(2023,4,27);
//	Date d2(d1);
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)// 默认构造函数
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	Date(const Date& d)// 建议使用类类型对象的常引用
//	{
//		_year = d._year;
//		_month = d._month;
//		//d._day = _day;// 报错，常量不能被修改
//		_day = d._day;
//	}
//private:
//	int _year = 1;
//	int _month = 1;
//	int _day = 1;
//};
//
//int main()
//{
//	Date d1(2023, 4, 26);
//	Date d2(d1);// 使用d1对象作为初始化数据，初始化d2对象
//	return 0;
//}

//#include <ctime>
//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//	vector<vector<int>> arr(10000, vector<int>(20000));
//
//	size_t begin1 = clock();
//	for (int i = 0; i < arr.size(); i++)
//	{
//		for (int j = 0; j < arr[0].size(); j++)
//		{
//			arr[i][j] = 0;
//		}
//	}
//	size_t end1 = clock();
//
//	size_t begin2 = clock();
//	for (int i = 0; i < arr[0].size(); i++)
//	{ 
//		for (int j = 0; j < arr.size(); j++)
//		{
//			arr[j][i] = 0;
//		}
//	}
//	size_t end2 = clock();
//
//	cout << "begin1 - end1 = " << end1 - begin1 << endl;
//	cout << "begin2 - end2 = " << end2 - begin2 << endl;
//
//	return 0;
//}



//#include <cstdlib>
//#include <iostream>
//using namespace std;
//
//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)// 默认构造
//	{
//		//_year = year;
//		//_month = month;
//		//_day = day;
//	}
//	~Date()
//	{
//		cout << "~Date()" << endl;
//	}
//private:
//	int _year = 1;
//	int _month = 1;
//	int _day = 1;
//};
//class Stack
//{
//public:
//	void Push(int in)
//	{
//		// 暂时不考虑扩容...
//		_a[_top++] = in;
//	}
//private:
//	// 内置类型成员变量可以给出缺省值
//	int* _a = (int*)malloc(sizeof(int)*4);
//	int _top = 0;
//	int _capacity = 4;
//	Date _d;
//};
//
//int main()
//{
//	Stack s1;
//	return 0;
//}