#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

// int main()
// {
	
// 	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	
// 	sockaddr_in sender;
// 	memset(&sender, 0, sizeof(sender));
// 	sender.sin_family = AF_INET;
// 	sender.sin_port = htons(9090);
// 	sender.sin_addr.s_addr = inet_addr("255.255.255.255");

// 	int val = 1;
// 	setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &val, sizeof(val));

// 	FILE* fp = fopen("test.txt", "r");
	
// 	while (!feof(fp))
// 	{
// 		char buffer[1024] = { 0 };
// 		fgets(buffer, sizeof(buffer), fp);
// 		sendto(sockfd, buffer, strlen(buffer), 0, (const sockaddr*)&sender, sizeof(sender));
// 		printf("已发送数据: %s\n", buffer);
// 		sleep(2);
// 	}

	
// 	return 0;
// }

int main()
{
	
	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	
	int ttl = 64;
	setsockopt(sockfd, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl));
	
	sockaddr_in sender;
	memset(&sender, 0, sizeof(sender));
	sender.sin_family = AF_INET;
	sender.sin_port = htons(9090);
	sender.sin_addr.s_addr = inet_addr("224.1.1.2");


	FILE* fp = fopen("test.txt", "r");
	
	while (!feof(fp))
	{
		char buffer[1024] = { 0 };
		fgets(buffer, sizeof(buffer), fp);
		sendto(sockfd, buffer, strlen(buffer), 0, (const sockaddr*)&sender, sizeof(sender));
		printf("已发送数据: %s\n", buffer);
		sleep(2);
	}

	fclose(fp);
	close(sockfd);
	
	return 0;
}
