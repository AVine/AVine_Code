#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#define MULTICAST_ADDR "224.1.1.2"  
#define PORT 12345                  
int main() {
    int sockfd;
    struct sockaddr_in addr;
    char message[] = "Hello, multicast!";

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket");
        exit(1);
    }

    int ttl = 1;
    if (setsockopt(sockfd, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl)) < 0) {
        perror("setsockopt");
        exit(1);
    }

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = inet_addr(MULTICAST_ADDR);

    if (sendto(sockfd, message, sizeof(message), 0, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        perror("sendto");
        exit(1);
    }

    printf("Message sent: %s\n", message);


    close(sockfd);

    return 0;
}

