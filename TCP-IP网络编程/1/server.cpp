#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <iostream>



int main()
{
    int listenfd = socket(AF_INET,SOCK_STREAM,0);
    
    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(9090);
    server.sin_addr.s_addr = INADDR_ANY;
    int n = bind(listenfd,(const sockaddr *)&server,sizeof(server));
    assert(n != -1);

    n = listen(listenfd,5);
    assert(n != -1);
    sleep(10);
    struct sockaddr_in client;
    socklen_t len = sizeof(client);
    int sockfd = accept(listenfd,(sockaddr *)&client,&len);
    assert(sockfd != -1);

    char buffer[64] = {0};
    recv(sockfd,buffer,sizeof(buffer)-1,0);
    std::cout << "收到数据:" << buffer << std::endl;
    return 0;
}

// int main()
// {
//     const char *str1 = "192.168.1.25";
//     const char *str2 = "172.168.2.43";
//     in_addr_t ip1 = inet_addr(str1);
//     in_addr_t ip2 = inet_addr(str2);
//     struct in_addr in;
//     in.s_addr = ip1;
//     char *ret = inet_ntoa(in);
//     printf("%s\n",ret);

//     in.s_addr = ip2;
//     ret = inet_ntoa(in);
//     printf("%s\n",ret);

//     printf("%p\n",&(*ret));
//     static int x = 3;
//     int y = 6;
//     int *z = new int(88);
//     printf("%p\n",&x);
//     printf("%p\n",&y);
//     printf("%p\n",z);


//     return 0;
// }


// int main()
// {
//     int listenfd = socket(AF_INET,SOCK_STREAM,0);
    
//     struct sockaddr_in server;
//     server.sin_family = AF_INET;
//     server.sin_port = htons(9090);
//     server.sin_addr.s_addr = INADDR_ANY;
//     int n = bind(listenfd,(const sockaddr *)&server,sizeof(server));
//     assert(n != -1);

//     n = listen(listenfd,5);
//     assert(n != -1);

//     struct sockaddr_in client;
//     socklen_t len = sizeof(client);
//     int sockfd = accept(listenfd,(sockaddr *)&client,&len);
//     assert(sockfd != -1);

//     const char *str = "hello!";
//     write(sockfd,str,strlen(str));
//     return 0;
// }