#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <iostream>


class server
{
public:
    server(short port)
        :_port(port)
    {
        _listenfd = socket(AF_INET,SOCK_STREAM,0);
        if(_listenfd == -1)
        {
            std::cout << "socket error!" << std::endl;
            exit(-1);
        }
        int val = 1;
        setsockopt(_listenfd,SOL_SOCKET,SO_REUSEADDR,&val,sizeof(val));

        sockaddr_in server;
        memset(&server,0,sizeof(server));
        server.sin_family = AF_INET;
        server.sin_port = htons(_port);
        server.sin_addr.s_addr = INADDR_ANY;
        int n = bind(_listenfd,(const sockaddr *)&server,sizeof(server));
        if(n == -1)
        {
           std::cout << "bind error!" << std::endl;
            exit(-1);
        }
        
        n = listen(_listenfd,5);
        if(n == -1)
        {
            std::cout << "listen error!" << std::endl;
            exit(-1);
        }
    }

    void start()
    {
        // 开始接受客户端请求
        struct sigaction act;
        act.sa_handler = wait_child_proc;
        sigemptyset(&act.sa_mask);
        act.sa_flags = 0;
        sigaction(SIGCHLD,&act,0);

        sockaddr_in client;
        socklen_t len = sizeof(client);
        while(true)
        {

            int sockfd = accept(_listenfd,(sockaddr *)&client,&len);
            if(sockfd == -1)
            {
                printf("accept error!\n");
                continue;
            }
            std::cout << "有新的客户端..." << std::endl;
            pid_t id = fork();
            if(id == 0)
            {
                close(_listenfd);//子进程不负责监听业务
                int recv_len = 0;
                char buffer[1024] = {0};
                while((recv_len = read(sockfd,buffer,sizeof(buffer))) != 0)
                {
                    std::cout << "recv_len = " << recv_len << std::endl;
                    write(sockfd,buffer,recv_len);
                }
                close(sockfd);
                std::cout << "子进程处理完业务!退出!" << getpid() << std::endl;
                exit(0);
            }
            else if(id > 0)
            {
                close(sockfd);// 父进程不处理业务
            }
            else 
            {
               std::cout << "fork error!" << std::endl;
                exit(-1);
            }
        }
    }

    static void wait_child_proc(int sig)
    {
        int status;
        pid_t pid = waitpid(-1,&status,WNOHANG);
        if(WIFEXITED(status))
        {
            printf("子进程已经退出!%d\n",pid);
            printf("子进程退出码: %d\n",WEXITSTATUS(status));
        }
    }
protected:
    int _listenfd;
    short _port;
};

int main(int argc,char *argv[])
{
    if(argc != 2)
    {
        printf("Usage: %s <port>\n",argv[0]);
        return -1;
    }
    short port = atoi(argv[1]);
    server s(port);
    s.start();
    return 0;
}