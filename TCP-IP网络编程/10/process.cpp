#include <unistd.h>
#include <iostream>
#include <sys/wait.h>
#include <signal.h>
#include <iostream>
#include <stdlib.h>
#include <sys/wait.h>

void wait_child_proc(int sig)
{
    if(sig == SIGCHLD) std::cout << "有子进程退出了!" << std::endl;
    int status;
    pid_t pid = waitpid(-1,&status,WNOHANG);
    if(WIFEXITED(status))
    {
        std::cout << "子进程: " << pid << " 被父进程: " << getpid() << "回收!" << std::endl;
        std::cout << "子进程退出码: " << WEXITSTATUS(status) << std::endl;
    }
}
int main()
{
    struct sigaction act;
    act.sa_handler = wait_child_proc;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

    sigaction(SIGCHLD,&act,0);// 注册信号

    pid_t id = fork();
    if(id == 0)
    {
        std::cout << "我是子进程: " << getpid() << std::endl;
        sleep(3);
        exit(88);
    }
    else 
    {
        std::cout << "我是父进程: " << getpid() << std::endl;
        sleep(88);
    }
    sleep(88);
    return 0;
}

// void timeout(int sig)
// {
//     if(sig == SIGALRM)
//         std::cout << "Time Out!" << std::endl;
//     alarm(2);
// }
// int main()
// {
//     signal(SIGALRM,timeout);
//     alarm(2);
//     for(int i=0;i<3;i++)
//     {
//         std::cout << "wait..." << i << std::endl;
//         sleep(100);
//     }
//     return 0;
// }

// int main()
// {
//     pid_t id = fork();
//     if(id == 0)
//     {
//         return 33;
//     }
//     else 
//     {
//         std::cout << "子进程pid: " << id << std::endl;
//         int status;
//         wait(&status);
//         if(WIFEXITED(status))
//         {
//             std::cout << "子进程正常退出，退出码: " << WEXITSTATUS(status) << std::endl;
//         }
//         sleep(20);
//     }
//     return 0;
// }