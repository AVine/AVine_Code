#include <sys/select.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
    fd_set reads;
    FD_ZERO(&reads);
    FD_SET(0,&reads);

    while(true)
    {
        struct timeval timeout;
        timeout.tv_sec = 5;
        timeout.tv_usec = 0;
        fd_set tmp = reads;

        int n = select(0+1,&tmp,nullptr,nullptr,&timeout);
        if(n == 0)
        {
            printf("TIme Out!\n");
        }
        else if(n == -1)
        {
            printf("select error!\n");
            break;
        }
        else 
        {
            if(FD_ISSET(0,&tmp))
            {
                char buffer[1024] = {0};
                int n = read(0,buffer,sizeof(buffer)-1);
                buffer[n] = 0;
                printf("接收到数据: %s\n",buffer);
            }
        }
    }
    return 0;
}