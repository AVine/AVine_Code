#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <iostream>

int main()
{
    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    int recv_buf = 0,send_buf = 0;
    socklen_t len = 4;
    int n = getsockopt(sockfd,SOL_SOCKET,SO_SNDBUF,(void *)&send_buf,&len);
    if(n == -1) 
    {
        std::cout << "错误1!" << std::endl;
        return -1;
    }
    n = getsockopt(sockfd,SOL_SOCKET,SO_RCVBUF,(void *)&recv_buf,&len);
    if(n == -1) 
    {
        std::cout << "错误2!" << std::endl;
        return -1;
    }
    std::cout << "默认接收缓冲区大小: " << recv_buf << std::endl;
    std::cout << "默认发送缓冲区大小: " << send_buf << std::endl;

    send_buf = 66;
    n = setsockopt(sockfd,SOL_SOCKET,SO_SNDBUF,(void *)&send_buf,sizeof(send_buf));
    if(n == -1)
    {
        std::cout << "错误3!" << std::endl;
        return -1;
    }
    recv_buf = 66;
    n = setsockopt(sockfd,SOL_SOCKET,SO_RCVBUF,(void *)&recv_buf,sizeof(recv_buf));
    if(n == -1)
    {
        std::cout << "错误4!" << std::endl;
        return -1;
    }

    std::cout << "修改后的接收缓冲区大小: " << recv_buf << std::endl;
    std::cout << "修改后的发送缓冲区大小: " << send_buf << std::endl;
    return 0;
}

// int main()
// {
//     sockaddr_in server;
//     memset(&server,0,sizeof(server));
//     server.sin_addr.s_addr = inet_addr("72.125.19.106");
//     hostent *host = gethostbyaddr((char *)&server.sin_addr,4,AF_INET);
//     if(host == NULL) 
//     {
//         std::cout << "错误！" << std::endl;
//         return -1;
//     }
//     std::cout << "域名: " << host->h_name << std::endl;
//     return 0;
// }

// int main()
// {
//     hostent *host = gethostbyname("www.tencent.com");
//     int index = 0;
//     while(host->h_addr_list[index] != NULL)
//     {
//         std::cout << "IP addr " << index+1 << " : " << inet_ntoa(*(in_addr *)host->h_addr_list[index]) << std::endl;
//         ++index;
//     }
//     return 0;
// }

// int main()
// {
//     int listenfd = socket(AF_INET,SOCK_STREAM,0);
    
//     sockaddr_in server;
//     memset(&server,0,sizeof(server));
//     server.sin_family = AF_INET;
//     server.sin_addr.s_addr = INADDR_ANY;
//     server.sin_port = htons(9090);
//     bind(listenfd,(const sockaddr *)&server,sizeof(server));

//     listen(listenfd,5);

//     for(int i=0;i<5;i++)// 可以服务5个客户端
//     {
//         sockaddr_in client;
//         socklen_t len = sizeof(client);
//         int sockfd = accept(listenfd,(sockaddr *)&client,&len);

//         FILE *fp = fopen("./test.txt","rb");
//         char buffer[1024];
//         int read_len = 0;
//         while(true)
//         {
//             read_len = fread((void *)buffer,1,sizeof(buffer),fp);
//             if(read_len < sizeof(buffer))
//             {
//                 write(sockfd,buffer,read_len);
//                 break;
//             }
//             write(sockfd,buffer,sizeof(buffer));
//         }
//         shutdown(sockfd,SHUT_WR);// 只关闭输出流
//         int n = read(sockfd,buffer,sizeof(buffer));
//         buffer[n] = 0;
//         printf("接收到消息: %s\n",buffer);
//         close(sockfd);
//         fclose(fp);
//     }
//     close(listenfd);
//     return 0;
// }