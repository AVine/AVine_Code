
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <iostream>
#include <fstream>
#include <sys/select.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

// int sockfd;

// void urg_handler(int sig)
// {
//     if(sig == SIGURG) printf("接收到紧急信号!\n");
//     printf("有紧急数据!%d\n",getpid());
// }

// int main()
// {
//     int listenfd = socket(AF_INET,SOCK_STREAM,0);
    
//     struct sockaddr_in server;
//     server.sin_family = AF_INET;
//     server.sin_port = htons(9090);
//     server.sin_addr.s_addr = INADDR_ANY;
//     int n = bind(listenfd,(const sockaddr *)&server,sizeof(server));

//     n = listen(listenfd,5);
    
//     struct sigaction act;
//     act.sa_handler = urg_handler;
//     sigemptyset(&act.sa_mask);
//     act.sa_flags = 0;
//     sigaction(SIGURG,&act,0);

//     // pid_t id = fork();
//     // if(id == 0)
//     // {
//     //     while(true)
//     //     {
//     //         printf("我是子进程!%d\n",getpid());
//     //         sleep(2);
//     //     }
//     // }
//     struct sockaddr_in client;
//     socklen_t len = sizeof(client);
//     sockfd = accept(listenfd,(sockaddr *)&client,&len);

//     fcntl(sockfd,F_SETOWN,getpid());

//     int recv_len = 0;
//     char buffer[1024] = {0};
//     while((recv_len = recv(sockfd,buffer,sizeof(buffer)-1,0)) != 0)
//     {
//         buffer[recv_len] = 0;
//         printf("接收到普通数据: %s\n",buffer);
//     }

//     close(listenfd);
//     return 0;
// }


// int main()
// {
//     int listenfd = socket(AF_INET,SOCK_STREAM,0);
    
//     struct sockaddr_in server;
//     server.sin_family = AF_INET;
//     server.sin_port = htons(9090);
//     server.sin_addr.s_addr = INADDR_ANY;
//     int n = bind(listenfd,(const sockaddr *)&server,sizeof(server));

//     n = listen(listenfd,5);

//     struct sockaddr_in client;
//     socklen_t len = sizeof(client);
//     int sockfd = accept(listenfd,(sockaddr *)&client,&len);

//     char buffer[1024] = {0};
//     int recv_len = 0;
//     while(true)
//     {
//         recv_len = recv(sockfd,buffer,sizeof(buffer)-1,MSG_PEEK | MSG_DONTWAIT);
//         if(recv_len > 0) break;
//     }
//     buffer[recv_len] = 0;
//     std::cout << "第一次接收到数据: " << buffer << std::endl;

//     recv_len = recv(sockfd,buffer,sizeof(buffer)-1,0);
//     buffer[recv_len] = 0;
//     std::cout << "第二次接收到的数据: " << buffer << std::endl;
//     close(listenfd);
//     return 0;
// }


#include <stdio.h>
#include <sys/uio.h>

// int main()
// {
//     iovec vec[2];
//     char buffer1[] = "1234";
//     char buffer2[] = "ABCDEF";
//     vec[0].iov_base = buffer1;
//     vec[0].iov_len = strlen(buffer1);
//     vec[1].iov_base = buffer2;
//     vec[1].iov_len = strlen(buffer2);

//     writev(1,vec,2);
//     return 0;
// }

int main()
{
    char buffer1[50] = {0,};
    char buffer2[100] = {0,};
    iovec vec[2];
    vec[0].iov_base = buffer1;
    vec[0].iov_len = 5;
    vec[1].iov_base = buffer2;
    vec[1].iov_len = 100;

    readv(0,vec,2);
    std::cout << "buffer1: " << buffer1 << std::endl;
    std::cout << "buffer2: " << buffer2 << std::endl;
    return 0;
}