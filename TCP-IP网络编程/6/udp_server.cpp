#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>

int main()
{
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);
    
    struct sockaddr_in server;
    memset(&server,0,sizeof(server));
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_family = AF_INET;
    server.sin_port = htons(9090);
    bind(sockfd,(const sockaddr*)&server,sizeof(server));

    while(true)
    {
        sockaddr_in client;
        socklen_t len = sizeof(client);
        char buffer[1024] = {0};
        recvfrom(sockfd,buffer,sizeof(buffer),0,(sockaddr *)&client,&len);
        std::cout << "接收到消息: " << buffer << std::endl;
        sendto(sockfd,buffer,sizeof(buffer),0,(sockaddr *)&client,len);
    }
    return 0;
}