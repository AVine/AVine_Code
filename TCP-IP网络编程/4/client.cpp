#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <iostream>

int main()
{
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	assert(sockfd != -1);

	struct sockaddr_in server;
	server.sin_family = AF_INET;
	server.sin_port = htons(9090);
	server.sin_addr.s_addr = inet_addr("120.27.236.117");
	connect(sockfd, (const sockaddr*)&server, sizeof(server));

	while (true)
	{
		char input[1024] = {0};
		fputs("input message(Q or q to quit): ",stdout);
		fgets(input, sizeof(input), stdin);x
        input[strlen(input)] = 0;
		if (!strcmp(input, "q\n") || !strcmp(input, "Q\n")) break;
		send(sockfd, input, strlen(input), 0);
		int send_len = strlen(input);

		char buffer[1024] = { 0 };
		int recv_len = 0;
		while (recv_len < send_len)
		{
			int n = recv(sockfd, &buffer[recv_len], 1, 0);
			if (n == -1)
			{
				std::cout << "读取数据出错!" << std::endl;
				break;
			}
			recv_len += n;
		}
		buffer[recv_len] = 0;
		std::cout << "接收到消息: " << buffer << std::endl;
	}
	return 0;
}