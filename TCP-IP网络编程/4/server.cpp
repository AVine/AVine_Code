#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <iostream>


int main()
{
    int listenfd = socket(AF_INET,SOCK_STREAM,0);
    
    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(9090);
    server.sin_addr.s_addr = INADDR_ANY;
    int n = bind(listenfd,(const sockaddr *)&server,sizeof(server));
    assert(n != -1);

    n = listen(listenfd,5);
    assert(n != -1);
    sleep(10);
    struct sockaddr_in client;
    socklen_t len = sizeof(client);

    for(int i=0;i<5;i++)
    {
        int sockfd = accept(listenfd,(sockaddr *)&client,&len);
        assert(sockfd != -1);

        char buffer[64] = {0};
        while(recv(sockfd,buffer,sizeof(buffer)-1,0))
            write(sockfd,buffer,strlen(buffer));
        close(sockfd);
    }

    close(listenfd);
    return 0;
}