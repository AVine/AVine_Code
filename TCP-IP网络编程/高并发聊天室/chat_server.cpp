#include "thread_poll.hpp"
#include "chat_server.hpp"
#include <functional>
#include <unistd.h>
#include <time.h>


int main(int argc,char *argv[])
{
    srand((unsigned int)time(nullptr));
    if(argc != 2)
    {
        printf("Usage: %s <port>\n",argv[0]);
        exit(-1);
    }
    int port = atoi(argv[1]);
    chat_server cs(port);
    cs.start();
    return 0;
}


// void test1()
// {
//     std::cout << "test1()" << std::endl;
// }

// void test2()
// {
//     std::cout << "test2()" << std::endl;
// }

// void test3()
// {
//     std::cout << "test3()" << std::endl;
// }

// int main()
// {
//     thread_poll<std::function<void()>> tp;
//     tp.start();
//     tp.push(test1);
//     tp.push(test2);
//     tp.push(test3);
//     tp.push(test2);
//     tp.push(test2);
//     tp.push(test1);
//     tp.push(test3);
//     while(true)
//     {
//         std::cout << "主线程在运行..." << std::endl;
//         sleep(1);
//     }
//     return 0;
// }