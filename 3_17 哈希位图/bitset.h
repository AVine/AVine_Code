#pragma once


template <size_t N>
class bitset
{
public:
	bitset()
	{
		//设置好正确的char
		_bits.resize(N / 8 + 1, 0);
		//_bits.resize((N >> 3) + 1, 0);
	}

	void set(size_t x)
	{
		int whichChar = x / 8;//确定在哪个char上
		int whichBit = x % 8;//确定在哪个比特位上

		_bits[whichChar] |= (1 << whichBit);//对应比特位由0置1
	}

	void reset(size_t x)
	{
		int whichChar = x / 8;//确定在哪个char上
		int whichBit = x % 8;//确定在哪个比特位上

		_bits[whichChar] &= ~(1 << whichBit);//对应比特位由1置0
	}

	bool test(size_t x)
	{
		int whichChar = x / 8;//确定在哪个char上
		int whichBit = x % 8;//确定在哪个比特位上

		//判断当前数字是否在存在
		//如果存在返回非0
		//不存在返回0
		return _bits[whichChar] & (1 << whichBit);
	}
private:
	vector<char> _bits;//一个char有8个比特位，对应8个数据
};


template <size_t N>
class towBitset
{
public:
	//默认构造调用自定义类型的构造
	void set(size_t x)
	{
		if (!_bit1.test(x) && !_bit2.test(x))//00
		{
			_bit2.set(x);//01 出现一次
		}
		else if (!_bit1.test(x) && _bit2.test(x))//01
		{
			_bit2.reset(x);
			_bit1.set(x);//10 出现两次
		}
		//出现了三次以后不管
	}

	void print()
	{
		for (int i = 0; i < N; i++)
		{
			if (!_bit1.test(i) && _bit2.test(i))//01
			{
				cout << i << " ";
			}
		}
		cout << endl;
	}
private:
	bitset<N> _bit1;
	bitset<N> _bit2;
};