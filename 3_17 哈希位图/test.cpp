#define _CRT_SECURE_NO_WARNINGS 1


#include <vector>
#include <iostream>
using namespace std;
#include "bitset.h"


void test1()//判断当前数是否存在
{
	bitset<100> b;
	for (int i = 0; i < 30; i++)
	{
		b.set(i);
	}
	cout << b.test(15) << endl;
	cout << b.test(13) << endl;
	cout << b.test(17) << endl;

	b.reset(17);

	cout << b.test(15) << endl;
	cout << b.test(13) << endl;
	cout << b.test(17) << endl;
}

void test2()//求只出现一次的数
{
	int arr[] = { 3,6,6,8,8,18,9,9,5,3,7,17,26,18,4,13 };
	towBitset<30> b;
	for (auto& e : arr)
	{
		b.set(e);
	}
	b.print();
}
int main()
{
	test2();
	return 0;
}