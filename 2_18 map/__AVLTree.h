#define _CRT_SECURE_NO_WARNINGS 1

template <class K>
struct AVLTreeNode
{
	AVLTreeNode<K>* _left;
	AVLTreeNode<K>* _right;
	AVLTreeNode<K>* _parent;	//指向父节点

	K _key;

	int _bf;	//平衡因子

	AVLTreeNode(const K& key)
		:_left(nullptr), _right(nullptr), _parent(nullptr),_key(key), _bf(0)
	{}
};

template<class K>
class AVLTree
{
	typedef AVLTreeNode<K> Node;
public:
	bool insert(const K& key)
	{  
		if (_root == nullptr)
		{
			_root = new Node(key);
			return true;
		}

		Node* parent = nullptr;	//记录cur节点的上一个节点
		Node* cur = _root;
		while (cur)
		{
			if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				// key和树中的某一键值相等，退出
				return false;
			}
		}

		// 找到了新节点的插入位置
		cur = new Node(key);

		if (key < parent->_key)
		{
			parent->_left = cur;
			cur->_parent = parent;	//注意维护指向父节点的指针
		}
		else if (key > parent->_key)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}

		// 现在需要维护平衡因子:右子树高度 - 左子树高度

	while (parent)	//parent节点不为空时，就需要对平衡因子维护
	{
		if (parent->_left == cur)	//当新增节点在parent的左边时
		{
			parent->_bf--;
		}
		else if (parent->_right == cur)	//新增节点在parent的右边时
		{
			parent->_bf++;
		}

		if (parent->_bf == 0)	//更新平衡因子后，parent的平衡因子为0，就说明左右子树高度相等
								//不需要进行处理
		{
			break;
		}
		else if (parent->_bf == 1 || parent->_bf == -1)	//parent的左子树或右子树的高度增加了
														//就需要往上更新平衡因子
		{
			cur = parent;
			parent = parent->_parent;
		}
		else if (parent->_bf == 2 || parent->_bf == -2)	//parent的左右子树高度差不符合要求了
														//需要根据平衡因子的关系来选用特定的算法调整
		{
			// ...调整算法
			if (parent->_bf == 2 && cur->_bf == 1)
			{
				RotateL(parent);
			}
			else if (parent->_bf == -2 && cur->_bf == -1)
			{
				RotateR(parent);
			}
			else if (parent->_bf == -2 && cur->_bf == 1)
			{
				RotateLR(parent);
			}
			else if (parent->_bf = 2 && cur->_bf == -1)
			{
				RotateRL(parent);
			}
			else
			{
				assert(false);
			}

			break;		//调整完即可视为插入完毕
		}
		else
		{
			assert(false);	//如果发生断言错误，则说明代码设计的有问题
		}
	}

		return true;
	}

	void RotateL(Node* parent)
	{
		Node* cur = parent->_right;
		Node* curL = cur->_left;	//cur的左树

		parent->_right = curL;	//cur的左树变成parent的右树
		if (curL)
		{
			curL->_parent = parent;
		}

		Node* oldParent = parent->_parent;	//记录parent的父节点
		parent->_parent = cur;	//cur作为parent的父节点
		cur->_left = parent;	//parent作为cur的左树

		if (oldParent == nullptr)	
		{
			_root = cur;	//直接让cur作为根节点(因为parent的旧父节点为空)
			cur->_parent = nullptr;
		}
		else
		{
			if (oldParent->_left == parent)
			{
				oldParent->_left = cur;
				cur->_parent = oldParent;
			}
			else if (oldParent->_right == parent)
			{
				oldParent->_right = cur;
				cur->_parent = oldParent;
			}
		}
		
		parent->_bf = cur->_bf = 0;	//平衡因子都置为0(推理得出结论)
	}

	void RotateR(Node* parent)
	{
		Node* cur = parent->_left;
		Node* curR = cur->_right;


		parent->_left = curR;	//cur的右树作为parent的左树
		if (curR)
		{
			curR->_parent = parent;
		}

		Node* oldParent = parent->_parent;
		parent->_parent = cur;	
		cur->_right = parent;	//parent作为cur的右树

		if (oldParent == nullptr)
		{
			_root = cur;
			cur->_parent = nullptr;
		}
		else
		{
			if (oldParent->_left == parent)
			{
				oldParent->_left = cur;
				cur->_parent = oldParent;
			}
			else if (oldParent->_right == parent)
			{
				oldParent->_right = cur;
				cur->_parent = oldParent;
			}
		}

		parent->_bf = cur->_bf = 0;
	}


	void RotateLR(Node* parent)
	{
		Node* cur = parent->_left;
		Node* curR = cur->_right;

		int bf = curR->_bf;	//旋转之前记录一下cur的孩子节点的平衡因子

		RotateL(cur);
		RotateR(parent);

		if (bf == 0)	//如图所示h==0时
		{
			parent->_bf = cur->_bf = curR->_bf = 0;
		}
		else if (bf == -1)	//如图所示h==1时第一种插入方式
		{
			parent->_bf = 1;
			cur->_bf = 0;
			curR->_bf = 0;
		}
		else if (bf == 1)	//如图所示h==1时第二种插入方式
		{
			parent->_bf = 0;
			cur->_bf = -1;
			curR->_bf = 0;
		}
	}


	void RotateRL(Node* parent)
	{
		Node* cur = parent->_right;
		Node* curL = cur->_left;

		int bf = curL->_bf;

		RotateR(cur);
		RotateL(parent);

		if (bf == 0)	//h==0时
		{
			parent->_bf = cur->_bf = curL->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 0;
			cur->_bf = 1;
			curL->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = -1;
			cur->_bf = 0;
			curL->_bf = 0;
		}
	}


	void level()
	{
		vector<vector<string>> vec;
		if (_root == nullptr)
		{
			return;
		}

		queue<Node*> q;
		q.push(_root);

		while (!q.empty())
		{
			int size = q.size();
			vector<string> tmp;
			while (size--)
			{
				Node* front = q.front();
				q.pop();

				if (front)
				{
					tmp.push_back(to_string(front->_key));
				}
				else
				{
					tmp.push_back("nullptr");
				}

				if (front)
				{
					if (front->_left)
					{
						q.push(front->_left);
					}
					else
					{
						q.push(nullptr);
					}

					if (front->_right)
					{
						q.push(front->_right);
					}
					else
					{
						q.push(nullptr);
					}
				}

			}
			vec.push_back(tmp);


		}

		for (int i = 0; i < vec.size(); i++)
		{
			for (int j = 0; j < vec[i].size(); j++)
			{
				cout << vec[i][j] << " ";
			}
			cout << endl;
		}
	}


	bool isAVLTree()
	{
		return isAVLTree(_root);
	}

	bool isAVLTree(Node* root)
	{
		if (root == nullptr)
		{
			return true;
		}

		int leftHeight = height(root->_left);
		int rightHeight = height(root->_right);

		if (root->_bf != (rightHeight - leftHeight))
		{
			cout << root->_key << "平衡因子出错:" << root->_bf << endl;
			return false;
		}

		return abs(rightHeight - leftHeight) < 2
			&& isAVLTree(root->_left)
			&& isAVLTree(root->_right);
	}


	int height(Node* root)
	{
		if (root == nullptr)
		{
			return 0;
		}

		int lh = height(root->_left);
		int rh = height(root->_right);

		return lh > rh ? lh + 1 : rh + 1;
	}
private:
	Node* _root = nullptr;
};