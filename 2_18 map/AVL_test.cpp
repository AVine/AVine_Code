#define _CRT_SECURE_NO_WARNINGS 1

#include <set>
#include <assert.h>
#include <string>
#include <iostream>
#include <queue>
using namespace std;

#include "AVLTree.h"

//void test1()
//{
//	AVLTree<int, int> a;
//	//a.insert(make_pair(5, 1));
//	//a.insert(make_pair(7, 1));
//	//a.insert(make_pair(3, 1));
//
//	//a.insert(make_pair(2, 1));
//	//a.insert(make_pair(6, 1));
//	//a.insert(make_pair(8, 1));
//
//	a.insert(make_pair(4, 1));
//	a.insert(make_pair(3, 1));
//	a.insert(make_pair(1, 1));
//
//	cout << a.isAVLTree() << endl;
//}

void test()
{
	int a[] = { 1,2,3,4,5,6,7,8,9};

	AVLTree<int, int> t;
	for (auto& e : a)
	{
		t.insert(make_pair(e, e));
	}

	t.level();
	cout << t.isAVLTree() << endl;
}
int main()
{
	test();
	return 0;
}