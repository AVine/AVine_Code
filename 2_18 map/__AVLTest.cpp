﻿#define _CRT_SECURE_NO_WARNINGS 1

#include <map>
#include <assert.h>
#include <queue>
#include <string>
#include <iostream>
using namespace std;

#include "__AVLTree.h"
//#include "AVLTree.h"




//void test1()
//{
//	AVLTree<int> t;
//	t.insert(3);
//	t.insert(2);
//	t.insert(1);
//
//}

//void test2()
//{
//	int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
//	AVLTree<int,int> t;
//	for (auto& e : a)
//	{
//		t.insert(make_pair(e,e));
//	}
//
//	t.level();
//	cout << t.isAVLTree() << endl;
//}

void test3()
{
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16,14 };
	AVLTree<int> t;
	for (auto& e : a)
	{
		t.insert(e);
	}

	t.level();
	cout << t.isAVLTree() << endl;
}
int main()
{
	test3();
	return 0;
}