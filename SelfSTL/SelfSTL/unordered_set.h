#pragma once


#include "hash_table.h"
#include "pair.h"

namespace self
{
	// 因为哈希函数的缘故。映射位置是用数字映射的
	// 所以当要把自定义类型放到哈希表里面时
	// 需要一种算法提取出数字(Func)
	template <class Key,class HashFunc = Func<Key>>
	class unordered_set
	{
	public:
		struct KeyOfSet
		{
			Key operator()(const Key& k)
			{
				return k;
			}
		};

		typedef typename HashTable<Key, Key, KeyOfSet, HashFunc>::const_iterator iterator;
		typedef typename HashTable<Key, Key, KeyOfSet, HashFunc>::const_iterator const_iterator;


		iterator begin() const
		{
			return _ht.begin();
		}
		iterator end() const
		{
			return _ht.end();
		}

		pair<iterator, bool> insert(const Key& k)
		{
			pair<typename HashTable<Key, Key, KeyOfSet, HashFunc>::iterator, bool> p(_ht.insert(k));
			return pair<iterator, bool>(p.first, p.second);
		}

		bool erase(const Key& k)
		{
			_ht.erase(k);
		}

		size_t size()
		{
			return _ht.size();
		}

		iterator find(const Key& k)
		{
			return _ht.find(k);
		}

		void clear()
		{
			_ht.clear();
		}
	private:
		HashTable<Key, Key, KeyOfSet, HashFunc> _ht;
	};
}