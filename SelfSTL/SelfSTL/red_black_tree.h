#pragma once

#include "pair.h"
#include "reverse_iterator.h"

namespace self
{
	enum color
	{
		red, black
	};

	template <class value>
	struct redBlackTreeNode
	{
		// 三叉链结构
		redBlackTreeNode<value>* _left;
		redBlackTreeNode<value>* _right;
		redBlackTreeNode<value>* _parent;
		color _cor = red;	// 每个节点颜色默认为红
		value _data;
		redBlackTreeNode(const value& data)
			:_left(nullptr), _right(nullptr), _parent(nullptr)
			,_data(data)
		{}
	};

	template <class value>
	redBlackTreeNode<value>* node;

	template <class value,class Ref,class Ptr>
	struct redBlackTreeIterator
	{
		typedef redBlackTreeNode<value> Node;
		typedef redBlackTreeIterator<value,Ref,Ptr> Self;
		typedef redBlackTreeIterator<value, value&, value*> iterator;
		

		redBlackTreeIterator(Node* node)
			:_node(node)
		{}

		redBlackTreeIterator(const iterator& it)
			:_node(it._node)
		{}

		Ref operator*()
		{
			return _node->_data;
		}

		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& it) const
		{
			return _node != it._node;
		}

		bool operator==(const Self& it) const
		{
			return _node == it._node;
		}
		
		Self& operator++()
		{
			if (_node->_right != nullptr)
			{
				Node* min = _node->_right;
				while (min && min->_left)
				{
					min = min->_left;
				}
				_node = min;
			}
			else
			{
				Node* cur = _node;
				Node* parent = cur->_parent;
				while (parent && parent->_right == cur)
				{
					cur = parent;
					parent = cur->_parent;
				}
				_node = parent;
			}
			return *this;
		}

		Self& operator++(int)
		{
			Self tmp(*this);
			operator++();
			return tmp;
		}

		Self& operator--()
		{
			if (_node == nullptr)
			{
				while (node<value> && node<value>->_right)
				{
					node<value> = node<value>->_right;
				}
				_node = node<value>;
				return *this;
			}

			if (_node->_left != nullptr)
			{
				Node* max = _node->_left;
				while (max->_right)
				{
					max = max->_right;
				}
				_node = max;
			}
			else
			{
				Node* cur = _node;
				Node* parent = cur->_parent;
				while (parent && parent->_left == cur)
				{
					cur = parent;
					parent = cur->_parent;
				}
				_node = parent;
			}
			return *this;
		}

		Self& operator--(int)
		{
			Self tmp(*this);
			operator--();
			return tmp;
		}

		Node* _node;
	};

	//         用作查找    用作数据类型
	template <class key, class keyValue,class KeyOfkeyvalue>
	class redBlackTree
	{
		typedef redBlackTreeNode<keyValue> Node;
	public:

		typedef redBlackTreeIterator<keyValue,keyValue&,keyValue*> iterator;
		typedef redBlackTreeIterator<keyValue, const keyValue&, const keyValue*> const_iterator;
		typedef reverseIterator<iterator, keyValue&, keyValue*> reverse_iterator;
		typedef reverseIterator<const_iterator, const keyValue&, const keyValue*> const_reverse_iterator;
		iterator begin()
		{
			Node* cur = _root;
			while (cur && cur->_left)
			{
				cur = cur->_left;
			}
			return iterator(cur);
		}

		iterator end()
		{
			return iterator(nullptr);
		}

		const_iterator begin() const
		{
			Node* cur = _root;
			while (cur && cur->_left)
			{
				cur = cur->_left;
			}
			return const_iterator(cur);
		}

		const_iterator end() const
		{
			return const_iterator(nullptr);
		}

		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		const_reverse_iterator rbegin() const
		{
			return const_reverse_iterator(end());
		}

		const_reverse_iterator rend() const
		{
			return const_reverse_iterator(begin());
		}


		~redBlackTree()
		{
			clear(_root);
			_root = nullptr;
			_size = 0;
		}

		void clear(Node* root)
		{
			if (root == nullptr)
			{
				return;
			}
			clear(root->_left);
			clear(root->_right);
			delete root;
			root = nullptr;
		}

		pair<iterator,bool> insert(const keyValue& data)
		{
			if (_root == nullptr)
			{
				_root = new Node(data);
				_root->_cor = black;
				node<keyValue> = _root;
				++_size;
				return make_pair(iterator(_root),true);
			}

			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (kof(data) < kof(cur->_data))
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (kof(data) > kof(cur->_data))
				{
					parent = cur;
					cur = cur->_right;
				}
				else
				{
					return make_pair(iterator(cur), false);
				}
			}

			// 每个新插入的节点都是红色!
			cur = new Node(data);
			Node* newnode = cur;
			if (kof(data) < kof(parent->_data))
			{
				parent->_left = cur;
				cur->_parent = parent;
			}
			else if (kof(data) > kof(parent->_data))
			{
				parent->_right = cur;
				cur->_parent = parent;
			}

			while (parent && parent->_cor == red)
			{
				Node* grand = parent->_parent;
				if (grand && grand->_left == parent)
				{
					Node* uncle = grand->_right;
					if (uncle && uncle->_cor == red)
					{
						parent->_cor = uncle->_cor = black;
						grand->_cor = red;

						cur = grand;
						parent = cur->_parent;
					}
					else
					{
						if (parent->_left == cur)
						{
							rotate_right(grand);
							parent->_cor = black;
							grand->_cor = red;
						}
						else if (parent->_right == cur)
						{
							rotate_left(parent);
							rotate_right(grand);
							cur->_cor = black;
							grand->_cor = red;
						}
					}
				}
				else if (grand && grand->_right == parent)
				{
					Node* uncle = grand->_left;
					if (uncle && uncle->_cor == red)
					{
						parent->_cor = uncle->_cor = black;
						grand->_cor = red;

						cur = grand;
						parent = cur->_parent;
					}
					else
					{
						if (parent->_right == cur)
						{
							rotate_left(grand);
							parent->_cor = black;
							grand->_cor = red;
						}
						else if (parent->_left == cur)
						{
							rotate_right(parent);
							rotate_left(grand);
							cur->_cor = black;
							grand->_cor = red;
						}
					}
				}
			}

			_root->_cor = black;
			node<keyValue> = _root;
			++_size;
			return make_pair(iterator(newnode), true);
		}

		pair<iterator, bool> multiinsert(const keyValue& data)
		{
			if (_root == nullptr)
			{
				_root = new Node(data);
				_root->_cor = black;
				node<keyValue> = _root;
				++_size;
				return make_pair(iterator(_root), true);
			}

			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (kof(data) < kof(cur->_data))
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (kof(data) >= kof(cur->_data))
				{
					parent = cur;
					cur = cur->_right;
				}
			}

			// 每个新插入的节点都是红色!
			cur = new Node(data);
			Node* newnode = cur;
			if (kof(data) < kof(parent->_data))
			{
				parent->_left = cur;
				cur->_parent = parent;
			}
			else if (kof(data) >= kof(parent->_data))
			{
				parent->_right = cur;
				cur->_parent = parent;
			}

			while (parent && parent->_cor == red)
			{
				Node* grand = parent->_parent;
				if (grand && grand->_left == parent)
				{
					Node* uncle = grand->_right;
					if (uncle && uncle->_cor == red)
					{
						parent->_cor = uncle->_cor = black;
						grand->_cor = red;

						cur = grand;
						parent = cur->_parent;
					}
					else
					{
						if (parent->_left == cur)
						{
							rotate_right(grand);
							parent->_cor = black;
							grand->_cor = red;
						}
						else if (parent->_right == cur)
						{
							rotate_left(parent);
							rotate_right(grand);
							cur->_cor = black;
							grand->_cor = red;
						}
					}
				}
				else if (grand && grand->_right == parent)
				{
					Node* uncle = grand->_left;
					if (uncle && uncle->_cor == red)
					{
						parent->_cor = uncle->_cor = black;
						grand->_cor = red;

						cur = grand;
						parent = cur->_parent;
					}
					else
					{
						if (parent->_right == cur)
						{
							rotate_left(grand);
							parent->_cor = black;
							grand->_cor = red;
						}
						else if (parent->_left == cur)
						{
							rotate_right(parent);
							rotate_left(grand);
							cur->_cor = black;
							grand->_cor = red;
						}
					}
				}
			}

			_root->_cor = black;
			node<keyValue> = _root;
			++_size;
			return make_pair(iterator(newnode), true);
		}

		Node*& get_root()
		{
			return _root;
		}

		size_t size()
		{
			return _size;
		}
	private:

		Node* _root;
		KeyOfkeyvalue kof;
		size_t _size = 0;

		void rotate_right(Node* node)
		{
			Node* cur = node->_left;
			Node* curRight = cur->_right;

			node->_left = curRight;
			if (curRight != nullptr)
			{
				curRight->_parent = node;
			}
			cur->_right = node;
			Node* pparent = node->_parent;
			node->_parent = cur;

			if (pparent == nullptr)
			{
				_root = cur;
				_root->_parent = nullptr;
			}
			else
			{
				if (pparent->_left == node)
				{
					pparent->_left = cur;
					cur->_parent = pparent;
				}
				else if (pparent->_right == node)
				{
					pparent->_right = cur;
					cur->_parent = pparent;
				}
			}
		}

		void rotate_left(Node* node)
		{
			Node* cur = node->_right;
			Node* curLeft = cur->_left;

			node->_right = curLeft;
			if (curLeft != nullptr)
			{
				curLeft->_parent = node;
			}
			cur->_left = node;
			Node* pparent = node->_parent;
			node->_parent = cur;

			if (pparent == nullptr)
			{
				_root = cur;
				_root->_parent = nullptr;
			}
			else
			{
				if (pparent->_left == node)
				{
					pparent->_left = cur;
					cur->_parent = pparent;
				}
				else if (pparent->_right == node)
				{
					pparent->_right = cur;
					cur->_parent = pparent;
				}
			}
		}

	};
}


