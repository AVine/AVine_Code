#pragma once

namespace self
{
	template <class key, class value>
	struct pair
	{
		key first;
		value second;

		pair()
		{}

		pair(const key& f, const value& v)
			:first(f), second(v)
		{}

		pair& operator=(const pair& other)
		{
			first = other.first;
			second = other.second;
		}

		pair(const pair& other)
			: first(other.first), second(other.second)
		{}

		bool operator==(const pair<key, value>& p)
		{
			if (first == p.first && second == p.second)
			{
				return true;
			}
			return false;
		}

	};

	template <class key, class value>
	pair<key, value> make_pair(const key& k, const value& v)
	{
		return pair<key, value>(k, v);
	}

}

