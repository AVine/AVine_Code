#pragma once

#include "hash_table.h"
#include "pair.h"

namespace self
{
	// 因为哈希函数的缘故。映射位置是用数字映射的
	// 所以当要把自定义类型放到哈希表里面时
	// 需要一种算法提取出数字(Func)
	template <class Key,class Value,class HashFunc = Func<Key>>
	class unordered_map
	{
	public:
		struct KeyOfMap
		{
			Key operator()(const pair<Key, Value>& kv)
			{
				return kv.first;
			}
		};

		template<class Iterator>
		unordered_map(Iterator left, Iterator right)
		{
			while (left != right)
			{
				insert(make_pair(left->first, left->second));
				++left;
			}
		}

		unordered_map(const unordered_map& um)
		{
			unordered_map tmp(um.begin(), um.end());
			swap(tmp);
		}

		unordered_map()
		{}

		void swap(unordered_map& um)
		{
			std::swap(_ht._tables, um._ht._tables);
			std::swap(_ht._n, um._ht._n);
		}

		typedef typename HashTable<Key, pair<Key, Value>, KeyOfMap, HashFunc>::iterator iterator;
		typedef typename HashTable<Key, pair<Key, Value>, KeyOfMap, HashFunc>::const_iterator const_iterator;

		iterator begin()
		{
			return _ht.begin();
		}
		iterator end()
		{
			return _ht.end();
		}

		const_iterator begin() const
		{
			return _ht.begin();
		}

		const_iterator end() const
		{
			return _ht.end();
		}

		pair<iterator,bool> insert(const pair<Key, Value>& kv)
		{
			return _ht.insert(kv);
		}

		bool erase(const Key& k)
		{
			_ht.erase(k);
		}

		Value& operator[](const Key& k)
		{
			pair<iterator, bool> ret = insert(make_pair(k, Value()));
			return ret.first->second;
		}

		size_t size()
		{
			return _ht.size();
		}

		iterator find(const Key& k)
		{
			return _ht.find(k);
		}

		void clear()
		{
			_ht.clear();
		}
	private:
		HashTable<Key, pair<Key, Value>, KeyOfMap, HashFunc> _ht;
	};
}

