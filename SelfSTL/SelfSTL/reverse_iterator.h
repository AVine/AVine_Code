#pragma once

namespace self
{
	template<class Iterator, class Ref, class Ptr>
	class reverseIterator
	{
		typedef reverseIterator<Iterator, Ref, Ptr> Self;
	public:
		reverseIterator(const Iterator& it)
			:_it(it)
		{}

		Self& operator++()
		{
			--_it;
			return *this;
		}

		Self& operator--()
		{
			++_it;
			return *this;
		}

		Self operator++(int)
		{
			Iterator tmp(*this);
			--_it;
			return tmp;
		}

		Self& operator--(int)
		{
			Iterator tmp(*this);
			++_it;
			return tmp;
		}

		Ref operator*()
		{
			Iterator tmp(_it);
			--tmp;
			return *tmp;
		}

		Ptr operator->()
		{
			return &(this->operator*());
		}

		bool operator!=(const Self& it) const
		{
			return _it != it._it;
		}

		bool operator==(const Self& it) const
		{
			return _it == it._it;
		}
	private:
		Iterator _it;
	};
}