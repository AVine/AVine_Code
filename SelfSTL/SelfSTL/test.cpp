#define _CRT_SECURE_NO_WARNINGS 1

#include "map.h"
#include "set.h"
#include "unordered_map.h"
#include "unordered_set.h"
#include <iostream>
#include "string.h"
using std::cout;
using std::endl;
using namespace self;

#include <vector>

#define MAKE_RAND() srand((unsigned int)time(nullptr) ^ 35346)



void test1()
{
	unordered_map<string, int> um;
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����",
"ƻ��", "�㽶", "ƻ��", "�㽶" };
	for (auto& e : arr)
	{
		um[e]++;
	}

	const unordered_map<string, int> um2(um);
	
	auto it1 = um.begin();
	while (it1 != um.end())
	{
		cout << it1->first << ":" << it1->second << endl;
		++it1;
	}
	cout << endl;

	auto it2 = um2.begin();
	while (it2 != um2.end())
	{
		cout << it2->first << ":" << it2->second << endl;
		++it2;
	}
	cout << endl;
}

int main()
{
	test1();
	return 0;
}
 