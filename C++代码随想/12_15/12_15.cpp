#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;




template <class T>
T Add(const T& x,const T& y)
{
	return x + y;
}

int Add(const int& x, const int& y)
{
	return x + y;
}


//template <class T1,class T2>
//T1 Add(const T1& x, const T2& y)
//{
//	return x + y;


int main()
{
	int i1 = 3, i2 = 4;
	double d1 = 3.14, d2 = 5.66;
	cout << Add(i1,i2) << endl;
	cout << Add(d1,d2) << endl;

	cout << Add(i1, d2) << endl;

	return 0;
}



//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		cout << "A(int a = 0)" << endl;
//	}
//	
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//};
//
//void test1()
//{
//	A* pa = new A[10];
//	delete pa;
//	//try
//	//{
//	//	while (1)
//	//	{
//	//		int* p = new int[1024 * 100];
//	//		cout << p << endl;
//	//	}
//	//}
//	//catch (exception& e)
//	//{
//	//	cout << e.what() << endl;
//	//}
//
//
//	//A* pa = new A;
//	//delete pa;
//}
//
//int main()
//{
//	//定位new
//	A* p = (A*)malloc(sizeof(A));
//	new(p)A();
//
//	p->~A();	//析构函数可以显式调用(构造不能)
//
//	free(p);
//
//	return 0;
//}




//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		cout << "A(int a = 0)" << endl;
//	}
//
//	//~A()
//	//{
//	//	cout << "~A()" << endl;
//	//}
//private:
//	int _a;
//};
//
//int main()
//{
//	//A* pa1 = new A;
//	////delete pa1;
//	//delete[] pa1;
//
//	A* pa2 = new A[10];
//	delete pa2;
//	return 0;
//}