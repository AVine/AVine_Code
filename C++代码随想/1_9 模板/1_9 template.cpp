#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
using namespace std;

template <class T>
void swap(const T& x, const T& y)
{
	T tmp = x;
	x = y;
	y = tmp;
}

template<class T1,typename T2>
void Print(const T1& x, const T2& y)
{
	//获取模板参数的类型
	cout << typeid(T1).name() << " " << typeid(T2).name() << endl;
}

template<>
void Print<int,int>(const int& x, const int& y)
{
	cout << "x = " << x << " y = " << y << endl;
}

//template<size_t N>
//void test()
//{
//	cout << N << endl;
//}



namespace ly
{
	template<class T>
	class stack
	{
	public:

	private:
		T* _a;		//我们可以自定义栈存储什么类型的数据
	};
}


template<class T,size_t N = 10>
class Arr
{
public:

private:
	T _a[N];
};


template <class T1,class T2>
class test
{
public:
	test()
	{
		//不管传入任何类型，最终都打印：
		cout << "test<T1,T2>" << endl;
	}
};

template <>
class test<int,int>
{
public:
	test()
	{
		//当传入的类型都为int时，单独打印：
		cout << "test<int,int>" << endl;
	}
};

template <class T1>
class test<T1, char>
{
public:
	test()
	{
		//模板参数的第二个类型为char时，打印：
		cout << "test<T1,char>" << endl;
	}
};


void test1() 
{
	int a = 1, b = 2;
	swap(a, b);
	cout << a << " " << b << endl;
}

void test2()
{
	Print(1, 2);
	Print(3.14, 2);
	Print("abc", 'a');

	Print<int, int>(3.14, 2);
	Print<string, char>("abc", 'b');
	//Print<string, int>(1.25, 2);	//无法类型转换，报错
}

void test3()
{
	test<double,double> t1;
	test<double, string> t2;
	test<int, int> t3;
	test<int, char> t4;


}

void test4()
{
	ly::stack<int> st1;
	ly::stack<char> st2;
	ly::stack<double> st3;
}

int main()
{
	test3();
	return 0;
}