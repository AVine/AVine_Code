#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

void f(int n ,int m = 3,int z = 6)		//语法错误
{
	cout << n << endl;
}
int main()
{
	f(1, , 8);	//不能跳跃传参
	f(1, 8);	//m被初始化为8，z才使用了缺省值
	return 0;
}


//namespace cpp
//{
//	int x = 0;
//	
//	void print()
//	{
//		cout << "hello world" << endl;
//	}
//}
//
//int x = 0;		//不同的作用域，使用相同的命名不会冲突
//int main()
//{
//	cpp::x += 3;
//	cpp::print();		//使用命名空间的内容必须指明作用域
//	return 0;
//}

//#include <stdio.h>
//
//int printf = 3;		//如果我们不知道printf是头文件里的一个函数
//int main()
//{
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//int main()
//{
//	cout << "hello world!" << endl;
//	return 0;
//}



//用自己写的栈解决括号匹配问题
//class Stack
//{
//public:
//	Stack(size_t capacity = 4)
//	{
//		_a = new char[capacity];
//		if (_a == nullptr)
//		{
//			perror("new:");
//			exit(-1);
//		}
//		_top = 0;
//		_capacity = capacity;
//
//	}
//
//	~Stack()
//	{
//		delete[] _a;
//		_a = nullptr;
//		_top = _capacity = 0;
//	}
//
//	void reserve(size_t n)
//	{
//		char* tmp = new char[n];
//		memcpy(tmp, _a, sizeof(char) * _top);
//		delete[] _a;
//		_a = nullptr;
//		_a = tmp;
//		_capacity = n;
//	}
//
//	void push(char x)
//	{
//		if (_top == _capacity)
//		{
//			reserve(2 * _capacity);
//		}
//		_a[_top++] = x;
//	}
//
//	char top()
//	{
//		return _a[_top-1];
//	}
//
//	void pop()
//	{
//		--_top;
//	}
//
//	bool empty()
//	{
//		return _top == 0;
//	}
//private:
//	char* _a;
//	size_t _top;
//	size_t _capacity;
//};
//
//class Solution {
//public:
//	bool isValid(string s) {
//		Stack st(100010);
//		auto it = s.begin();
//		while (it != s.end())
//		{
//			if (*it == '(' || *it == '[' || *it == '{') st.push(*it);
//			else
//			{
//				if (st.empty()) return false;
//
//				char ret = st.top();
//				st.pop();
//				if (ret == '{' && *it != '}' ||
//					ret == '[' && *it != ']' ||
//					ret == '(' && *it != ')')
//					return false;
//			}
//			++it;
//		}
//		return st.empty();
//	}
//};
//
//int main()
//{
//	Solution s;
//	bool ret = s.isValid("{}{}{{{");
//	
//	return 0;
//}





//验证this指针是调用函数对象的地址
//验证this指针是存在与栈帧中的(反汇编)
//class Test
//{
//public:
//	void print()
//	{
//		cout << this << endl;
//	}
//};
//
//void print()
//{
//	cout << "普通函数" << endl;
//}
//int main()
//{
//	Test* t1 = new Test;
//	delete t1;
//	t1 = nullptr;
//	t1->print();		//对象调用函数实际上没有发生寻址
//	cout << t1 << endl;
//	return 0;
//}

