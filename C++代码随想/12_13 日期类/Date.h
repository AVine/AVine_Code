#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
using namespace std;


class Date
{
public:

	//默认构造
	Date(int year = 1, int month = 1, int day = 1)
		:_year(year),_month(month),_day(day)
	{}

	int get_month_day(int year,int month) const
	{
		static int month_day[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
		{
			return 29;
		}
		return month_day[month];
	}

	void display() const
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}

	bool operator==(const Date& d) const
	{
		return _year == d._year && _month == d._month && _day == d._day;
	}

	bool operator!=(const Date& d) const
	{
		return !(*this == d);
	}

	bool operator>=(const Date& d) const;

	bool operator<=(const Date& d) const;

	bool operator<(const Date& d) const;

	bool operator>(const Date& d) const;

	

	Date& operator+=(int day);

	Date& operator-=(int day);

	Date operator+(int day) const;

	Date operator-(int day) const;

	Date& operator++();	//前置

	Date operator++(int);	//后置

	Date& operator--();

	Date operator--(int);

	int operator-(const Date& d) const;

	//使用编译器生成的足矣
	//Date(const Date& d)
	//{
	//	_year = d._year;
	//	_month = d._month;
	//	_day = d._day;
	//}

	/*Date& operator=(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
		return *this;
	}*/

	friend ostream& operator<<(ostream& out, const Date& d);

	friend istream& operator>>(istream& in, Date& d);

private:
	int _year;
	int _month;
	int _day;
};

static ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "/" << d._month << "/" << d._day;
	return out;
}

static istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}