#define _CRT_SECURE_NO_WARNINGS 1


#include "Date.h"

bool Date::operator>(const Date& d) const
{
	if (_year > d._year) return true;
	else if (_year == d._year && _month > d._month) return true;
	else if (_year == d._year && _month == d._month && _day > d._day) return true;
	return false;
}


bool Date::operator<=(const Date& d) const
{
	return !(*this > d);
}

bool Date::operator<(const Date& d) const
{
	return !(*this >= d);
}

bool Date::operator>=(const Date& d) const
{
	return *this > d || *this == d;
}

Date& Date::operator+=(int day)
{
	if (day < 0)
	{
		return *this -= abs(day);
	}
	_day += day;
	while (_day > get_month_day(_year, _month))
	{
		_day -= get_month_day(_year, _month);
		++_month;
		if (_month >= 13)
		{
			_month = 1;
			++_year;
		}
	}
	return *this;
}

Date& Date::operator-=(int day)
{
	if (day < 0)
	{
		return *this += abs(day);
	}
	_day -= day;
	while (_day <= 0)
	{
		_day += get_month_day(_year, _month);
		--_month;
		if (_month <=0)
		{
			_month = 12;
			--_year;
		}
	}
	return *this;
}

Date Date::operator-(int day) const
{
	Date tmp(*this);
	tmp -= day;
	return tmp;
}

Date Date::operator+(int day) const
{
	Date tmp(*this);
	tmp += day;
	return tmp;
}

Date& Date::operator++()
{
	*this += 1;
	return *this;
}

Date Date::operator++(int)
{
	Date ret(*this);
	*this += 1;
	return ret;
}


Date& Date::operator--()
{
	*this -= 1;
	return *this;
}

Date Date::operator--(int)
{
	Date ret(*this);
	*this -= 1;
	return ret;
}

int Date::operator-(const Date& d) const
{
	Date max = *this;
	Date min = d;
	int flag = 1;

	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}

	int cnt = 0;
	while (min != max)
	{
		++min;
		++cnt;
	}
	return cnt*flag;
}
