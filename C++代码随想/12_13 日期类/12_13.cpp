#define _CRT_SECURE_NO_WARNINGS 1



#include "Date.h"

void test1()
{
	Date d1;
	Date d2(2022, 12, 13);
	d1 = d2;

	//cout << (d1 == d2) << endl;
	
	(d1 + 5).display();
	d1 += 5;
	d1.display();

	(d2 - 10).display();
	d2 -= 10;
	d2.display();

	(++d1).display();
	d1.display();

	(d2++).display();
	d2.display();

}

void test2()
{
	Date d1(2022, 12, 13);
	d1 += -100;
	d1.display();

	Date d2(2022, 12, 13);
	d2 -= -100;
	d2.display();

	(d2 + (-100)).display();
}
int main()
{
	test2();



	return 0;
}




//int& f(int& x)
//{
//	static int ret = 3;
//	return ret;
//}
//int main()
//{
//	int a = 6;
//	int ret = f(a);
//	return 0;
//}


//int main()
//{
//	int x = 3;
//	int& rx = x;	//为 x 对象再取一个别名[rx]
//	int& rrx = x;
//	int& rrrx = rrx;	//对rrx操作就是对x操作
//
//	int& rrrrx;	//错误，引用必须被初始化
//	return 0;
//}


//void func(int x,double d)
//{
//	//……
//}
//int main()
//{
//	func(1, 2.2);
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//void f()
//{
//	cout << "f()" << endl;
//}
//
//// 两个函数构成重载，语法没有错误
//void f(int x = 3, double y = 4.1)
//{
//	cout << "f(int x,double y)" << endl;
//}
//
//int main()
//{
//	f();		//但是调用有二义性
//	return 0;
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//
//// 参数列表的形参个数、类型互不相同即可构成重载
//double Add(double x, double y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int int_sum = Add(1, 2);
//	double double_sum = Add(3.1, 2.6);
//	return 0;
//}