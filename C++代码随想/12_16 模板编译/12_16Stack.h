#define _CRT_SECURE_NO_WARNINGS 1

namespace ly
{
	template <class T>
	class stack
	{
	public:
		stack(int capacity = 4);
		void push(const T& x);
	private:
		T* _a;
		int _top = 0;
		int _capacity;
	};
}
