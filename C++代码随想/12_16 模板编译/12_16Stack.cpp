#define _CRT_SECURE_NO_WARNINGS 1


#include "12_16Stack.h"
using namespace ly;

//都是函数模板，没有真正的实例化成为函数

template<class T>
stack<T>::stack(int capacity)
{
	_a = new T[capacity];
	_capacity = capacity;
}


template<class T>
void stack<T>::push(const T& x)
{
	_a[_top++] = x;
}

template class stack<int>;
