#define _CRT_SECURE_NO_WARNINGS 1


#include <string>
#include <iostream>
using namespace std;

class Person
{
public:
	//Person()
	//{
	//	cout << "姓名：" << _name << endl;
	//	cout << "年龄：" << _age << endl;
	//}

	Person(const char* name = "ZhangSan", size_t age = 18)
		:_name(name),_age(age)
	{}

	Person(const Person& p)
	{
		_name = p._name;
		_age = p._age;
	}

	void func()
	{
		cout << "111" << endl;
	}

	void Print()
	{
		cout << "姓名：" << _name << endl;
		cout << "年龄：" << _age << endl;
	}
protected:
	string _name = "ZhangSan";
	size_t _age = 18;
};


class Student : public Person
{
public:

	Student(const char* name = "ZhangSan", size_t age = 18, size_t id = 2011)
		:Person(name,age),_id(id)
	{}

	Student(const Student& s)
		:Person(s)
	{
		_id = s._id;
	}

	void func(int x)
	{
		cout << "222" << endl;
	}

	void Print()
	{
		cout << "姓名：" << _name << endl;
		cout << "年龄：" << _age << endl;
		cout << "学号：" << _id << endl;

	}
private:
	size_t _id = 2011;
};


void test1()
{
	Person p1;
	Student s1;

	Person p2;
	p2 = s1;		//不存在强制类型转换

	Person& rp = s1;
	Person* pp = &s1;
}


void test2()
{
	Student s1("ly",21,2022);
	//s1.Print();
	//s1.Person::Print();

	Student s2(s1);
	s2.Print();
	//s2.Person::Print();

	//s1.func(10);
}


int main()
{
	test2();
	return 0;
}