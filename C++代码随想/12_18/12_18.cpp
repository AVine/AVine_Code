#define _CRT_SECURE_NO_WARNINGS 1

#include <vector>
#include <string>
#include <iostream>
using namespace std;

//将字符串中的空格替换成 "20%"
int main()
{
	string s("hello world hello linux hello visual studio!");

	size_t pos = s.find(' ');	//默认从第0个位置开始往后找
	while (pos != string::npos)
	{
		s.replace(pos, 1, "20%");
		pos = s.find(' ', pos + 3);		//插入了三个字符，跳过此三个字符
	}

	cout << s << endl;

	return 0;
}


//class Solution {
//public:
//    vector<int> searchRange(vector<int>& nums, int target) {
//        int right_border = get_rightr_border(nums, target);     //获取右边界
//        int left_border = get_left_border(nums, target);     //获取左边界
//
//        if (right_border == -2 || left_border == -2) return { -1,-1 };
//
//        if (right_border - left_border > 1) return { left_border + 1,right_border - 1 };
//
//        return { -1,-1 };
//
//    }
//
//private:
//    int get_rightr_border(vector<int>& nums, int target)   //获取右边界
//    {
//        int left = 0, right = nums.size() - 1;     //左闭右闭
//
//        int right_border = -2;
//        while (left <= right)
//        {
//            int mid = (left + right) / 2;
//            if (nums[mid] > target)
//            {
//                right = mid - 1;
//            }
//            else
//            {
//                left = mid + 1;
//                right_border = left;
//            }
//        }
//
//        return right_border;
//    }
//
//    int get_left_border(vector<int>& nums, int target)   //获取左边界
//    {
//        int left = 0, right = nums.size() - 1;     //左闭右闭
//
//        int left_border = -2;
//        while (left <= right)
//        {
//            int mid = (left + right) / 2;
//            if (nums[mid] >= target)
//            {
//                right = mid - 1;
//                left_border = right;
//            }
//            else
//            {
//                left = mid + 1;
//            }
//        }
//
//        return left_border;
//    }
//
//};
//
//
//
//int main()
//{
//    Solution s;
//    vector<int> vec{ 2,2 };
//    int target = 3;
//    s.searchRange(vec, target);
//    return 0;
//}