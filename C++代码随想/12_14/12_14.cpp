#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
using namespace std;

// 如果构造函数是一个私有成员，如果定义一个对象？
//class A
//{
//
//public:
//	static A get_A()
//	{
//		return A(5);
//	}
//private:
//	A(int x =3)
//		:_x(x)
//	{}
//	int _x;
//};
//
//
//int main()
//{
//	//A* pa = nullptr;
//	//A a1 =pa->get_A();
//
//	A a2 = A::get_A();
//	return 0;
//}




//内部类
//class A
//{
//public:
//	class B
//	{
//	public:
//		int get_x()
//		{
//			return _x;
//		}
//	};
//	
//private:
//	static int _x;
//};
//
//int A::_x = 3;
//
//int main()
//{
//	//B b;	//需要指明类域
//	A::B b;
//	cout << b.get_x() << endl;
//	return 0;
//}



//友元类
//class A
//{
//public:
//	friend class B;
//
//private:
//	int _x = 3;
//
//};
//
//class B
//{
//public:
//	int get_a()
//	{
//		return a._x;
//	}
//
//private:
//	A a;
//};
//
//int main()
//{
//	B b;
//	cout << b.get_a() << endl;
//	return 0;
//}




class Date
{
public:
	Date(int year = 1,int month = 1,int day = 1)
		:_year(year),_month(month),_day(day)
	{}

	friend void print(const Date& d);

	static int get_N()
	{
		return N;
	}
private:
	int _year;
	int _month;
	int _day;

public:
	static int N;
};

int Date::N = 0;

void print(const Date& d)
{
	cout << d._year << "/" << d._month << "/" << d._day << endl;
}

void test1()
{
	Date d1(2022);	//直接初始化
	Date d2 = 2022;	//隐式类型转换

	Date d3 = { 2022,12,14 };	//C++11支持多参数类型转换

	print(d3);
	print({ 2022,12,14 });

}

void test2()
{
	//Date d1;
	//cout << d1.get_N() << endl;	//通过对象去拿到静态成员，虽然可以但是不合理

	Date* pd = nullptr;
	cout << pd->get_N() << endl;	//虽然可以但是很怪

	cout << Date::get_N() << endl;	//使用静态成员函数
}
int main()
{
	test2();

	cout << Date::N << endl;
	return 0;
}