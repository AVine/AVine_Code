#include <iostream>
#include <string>
using namespace std;
#include <string.h>

//class String
//{
//public:
//	String(const char *str = "")
//	{
//		size_t len = strlen(str);
//		_size = len;
//		_capacity = _size + 1;// 还要容纳一个'\0'
//		_str = new char[_capacity];
//		std::copy(str, str + len, _str);
//		_str[_size] = 0;
//		cout << "String:构造函数" << endl;
//	}
//
//	~String()
//	{
//		delete[] _str;
//		_size = _capacity = 0;
//		cout << "String:析构函数" << endl;
//	}
//
//	String(const String &s)
//	{
//		_size = s._size;
//		_capacity = s._capacity;
//		_str = new char[_capacity];
//		std::copy(s._str, s._str + s._size, _str);
//		_str[_size] = 0;
//		cout << "String:拷贝构造" << endl;
//	}
//
	//String(String&& s)
	//{
	//	swap(s);
	//	s._str = nullptr;
	//	s._size = s._capacity = 0;
	//	cout << "String:移动构造" << endl;
	//}
//
//	String &operator=(String s)
//	{
//		swap(s);
//		cout << "String:移动赋值" << endl;
//		return *this;
//	}
//
//	void swap(String& s)
//	{
//		std::swap(_str, s._str);
//		std::swap(_size, s._size);
//		std::swap(_capacity, s._capacity);
//	}
//	size_t size() { return _size; }
//
//	size_t capacity() { return _capacity; }
//private:
//	char* _str;
//	size_t _size;
//	size_t _capacity;
//};
//
//class User
//{
//public:
	//template <class Tname,class Tage,class Tsex>
	//User(Tname &&name, Tage &&age, Tsex &&sex)
	//	:_name(std::forward<Tname>(name)),
	//	_age(std::forward<Tage>(age)),
	//	_sex(std::forward<Tsex>(sex))
	//{}
//
//	User() {}
//
//	User(const User &us)
//		:_name(us._name),_age(us._age),_sex(us._sex)
//	{}
//
//	User(User &&us)
//		:_name(std::forward<string>(us._name)),_age(us._age),_sex(std::forward<string>(us._sex))
//	{
//		cout << "User:移动构造" << endl;
//	}
//
//	User& operator=(User us)
//	{
//		swap(us);
//		return *this;
//	}
//	
//	void swap(User& us)
//	{
//		_name.swap(us._name);
//		_sex.swap(us._sex);
//		std::swap(_age, us._age);
//	}
//
//	User& GetFoo()// 返回左值
//	{
//		return *this;
//	}
//
//	User GetVal()// 返回右值
//	{
//		return *this;
//	}
//private:	
//	string _name;
//	int _age;
//	string _sex;
//};
//
//void test1()
//{
//	String s1("hello");
//	String s2(s1);
//	String s3(std::move(s1));
//	String s4("nice to meet you");
//	s4 = std::move(s3);
//}
//
//void test2()
//{
//	User u1("张三", 21, "男");
//	User u2("李四", 22, "女");
//	User u3(std::move(u1));
//}
//
//int main()
//{
//	test2();
//	return 0;
//}

//class MyClass {
//public:
//    MyClass(const std::string& str) : _name(str) {
//        std::cout << "Constructor with lvalue reference: " << _name << std::endl;
//    }
//
//    MyClass(std::string&& str) : _name(std::move(str)) {
//        std::cout << "Constructor with rvalue reference: " << _name << std::endl;
//    }
//
//private:
//    std::string _name;
//};
//
//class WrapperClass {
//public:
//    template <typename Args>
//    WrapperClass(Args&& args) : _myObj(std::forward<Args>(args)) {
//        std::cout << "WrapperClass Constructor" << std::endl;
//    }
//
//private:
//    MyClass _myObj;
//};
//
//int main() {
//    std::string str = "Hello, World!";
//    WrapperClass wrapper1(str); // 传递左值
//    std::cout << std::endl;
//
//    WrapperClass wrapper2("Hello, World!"); // 传递右值
//    std::cout << std::endl;
//
//    return 0;
//}


//int func() { return 100; }
//int main()
//{
//	int x = 3;// 可以取地址，左值
//	string s("hello");// 可以取地址、左值
//	string("world");// 不可取地址、右值
//	12;// 不可取地址、右值
//	func();// 返回值不可取地址，右值
//	return 0;
//}

//void Print(int&& t)
//{
//	cout << t << endl;
//}
//template <class T>
//void func(T &&t)// 即可以引用左值、也可引用右值
//{
//	Print(std::forward<T>(t));
//}
//
//int main()
//{
//	func(15);// 传递右值
//	return 0;
//}

//int main()
//{
//	int x = 3;
//	int& && rx = x;// 虽然这引用折叠最后会成为左值引用，但是不能在代码体现出来
//	return 0;
//}

template <class T>
void func(T&& t)
{
	cout << typeid(T).name() << endl;
}
int main()
{
	func(12);

	int x = 3;
	func(x);
	return 0;
}