#define _CRT_SECURE_NO_WARNINGS 1


#include <array>
#include <iostream>
#include "Date.h"
#include "priority_queue.h"
using std::cout;
using std::endl;
using namespace ly;


template<>
struct std::less <Date*>
{
	bool operator()(Date*& x, Date*& y)
	{
		return *x < *y;
	}
};
int main()
{
	priority_queue <Date> q1;
	q1.push(Date(2023, 1, 7));
	q1.push(Date(2023, 1, 8));
	q1.push(Date(2023, 1, 6));
	while (!q1.empty())
	{
		cout << q1.top() << " ";
		q1.pop();
	}
	cout << endl;

	priority_queue <Date,vector<Date>,ly::greater<Date>> q2;
	q2.push(Date(2023, 1, 7));
	q2.push(Date(2023, 1, 8));
	q2.push(Date(2023, 1, 6));
	while (!q2.empty())
	{
		cout << q2.top() << " ";
		q2.pop();
	}
	cout << endl;


	Date d1(2023, 1, 7);
	Date d2(2023, 1, 8);
	Date d3(2023, 1, 6);
	priority_queue <Date*,vector<Date*>,std::less<Date*>> q3;
	q3.push(&d1);
	q3.push(&d2);
	q3.push(&d3);
	while (!q3.empty())
	{
		cout << *q3.top() << " ";
		q3.pop();
	}
	cout << endl;
	
	return 0;
}

//template <class T1,class T2>
//class A
//{
//public:
//	A()
//	{
//		cout << "template<T1,T2>" << endl;
//	}
//};
//
////// 全特化
////template <>
////class A<double,double>
////{
////public:
////	A()
////	{
////		cout << "template<double,double>" << endl;
////	}
////};
////
////
////// 半特化/全特化
////template <class T1>
////class A<T1,char>
////{
////public:
////	A()
////	{
////		cout << "template<T1,char>" << endl;
////	}
////};
////
////template <class T2>
////class A<char, T2>
////{
////public:
////	A()
////	{
////		cout << "template<char,T2>" << endl;
////	}
////};
//
//template<>
//class A<char*,char*>
//{
//public:
//	A()
//	{
//		cout << "template<char*,char*>" << endl;
//	}
//};
//
//int main()
//{
//	//A<int,int> a1;
//	//A<double, double> a2;
//	//A<int, char> a3;
//	//A<char, int> a4;
//	//结论：模板匹配的时候，全特化>半特化>无特化
//
//	A<char*, char*> a5;
//	A<int, char*> a6;
//	//结论：想走全特化，实例化的类型必须符合全特化的模板参数
//
//
//	return 0;
//}


//template<class T>
//bool Less(const T& x, const T& y)
//{
//	return x < y;
//}
//
//// 模板特化：本来模板是生成统一操作的函数。但是我想针对某一类型进行特殊处理
////template<>
////bool Less<Date*>(Date* const & x, Date* const & y)
////{
////	return *x < *y;
////}
//
////对于函数模板我可以直接写函数重载啊！
//bool Less(Date* const& x, Date* const& y)
//{
//	return *x < *y;
//}
//
//int main()
//{
//	cout << Less(1, 2) << endl; // 可以比较，结果正确
//
//	Date d1(2022, 7, 9);
//	Date d2(2022, 7, 8);
//	cout << Less(d1, d2) << endl; // 可以比较，结果正确
//
//	Date* p1 = &d1;
//	Date* p2 = &d2;
//	cout << Less(p1, p2) << endl; // 可以比较，结果错误
//
//	return 0;
//}



//#define N 10		//为什么不写到一起呢？
//template<class T,size_t N = 10>		//模板的非类型参数
//class Arr
//{
//public:
//	
//private:
//	T _a[N];
//};
//
//int main()
//{
//	int arr[10];
//	array<int, 10> a;
//	//越界的读和写
//	cout << arr[10] << endl;
//	//arr[100] = 0;
//	cout << a[10] << endl;		//array 比 C语言的数组边界检查更加严格
//	
//	return 0;
//}