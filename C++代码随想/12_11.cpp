#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
using namespace std;




//探索一下引用作返回值返回局部变量的问题
//int& f()
//{
//	int n = 0;
//	++n;
//	return n;
//}
//
//void f2()
//{
//	int n = 100;
//}
//
//int main()
//{
//	int& ret = f();
//	cout << ret << endl;
//	cout << ret << endl;		//这些都是随机值(未定义的)
//
//	f2();
//	cout << ret << endl;		//f2 n定义的位置刚好与f 定义n的位置重叠(巧合)
//	
//	return 0;
//}