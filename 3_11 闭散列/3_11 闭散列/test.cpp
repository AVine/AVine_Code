#define _CRT_SECURE_NO_WARNINGS 1

#include <vector>
#include <map>
#include <string>
#include <iostream>
using namespace std;
#include "__HashTable.h"


void test1()
{
	HashTable<int, int> h;
	h.insert(make_pair(18,18));
	h.insert(make_pair(7, 7));
	h.insert(make_pair(17, 17));
	h.insert(make_pair(37, 37));
	h.insert(make_pair(28, 28));
	h.insert(make_pair(19, 19));
	h.insert(make_pair(14, 14));

	//cout << h.find(18) << endl;
	h.erase(18);
	//cout << h.find(18) << endl;
	//cout << h.find(19) << endl;

	h.insert(make_pair(13, 14));

}


void test2()
{
	HashTable<string, int> h;
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����",
"ƻ��", "�㽶", "ƻ��", "�㽶" };
	for (auto& e : arr)
	{
		auto ret = h.find(e);
		if (ret)
		{
			ret->_kv.second++;
		}
		else
		{
			h.insert(make_pair(e,1));
		}
	}

	
}

int main()
{
	test1();
	test2();
	return 0;
}