#pragma once


// 仿函数
template<class K>
struct HashFunc
{
	// 如果数据本身就是数字，那么直接可以类型转换
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};

// 使用模板特化来处理非数字类的数据，例如string类
template<>
struct HashFunc<string>
{
	// BKDR  -- 休息21：19
	size_t operator()(const string& key)
	{
		size_t hash = 0;//string类中每个字符的ASCII码值
		for (auto ch : key)
		{
			hash *= 131;//累加一次*=131
			//其目的在于：如果仅仅单纯的累加ASCII码值
			//就会造成 "abc" == "cba"，就会有更大几率造成哈希碰撞
			hash += ch;// 累加所有的ASCII码值
		}
		return hash;
	}
};

namespace closehash
{

	// 枚举出三种状态
	enum State
	{
		EMPTY,
		EXIST,
		DELETE,
	};


	// 假设使用KV模型来实现闭散列
	// 使用vector来存储数据，这个数据包括：
	// kv模型键值对以及这个位置的状态
	// 需要状态的原因在于：使用的是闭散列的开放地址法
	// 必须确保这个位置可以被写、可以被读、可以表示无效数据
	template<class K, class V>
	struct HashData
	{
		pair<K, V> _kv;
		State _state = EMPTY;
	};


	//因为哈希结构需要使用存储的数据来确定存储在哪个位置
	//所以需要通过数据%容器的大小
	//但因为数据不可能总为数字，所以需要一个仿函数
	//使用算法提取出不同类型对应的数字
	template<class K, class V, class Hash = HashFunc<K>>
	class HashTable
	{
		typedef HashData<K, V> Data;
	public:
		HashTable()
			:_n(0)
		{
			_tables.resize(10);
		}

		bool Insert(const pair<K, V>& kv)
		{
			//如果当前kv键值对已经在哈希表中保存了
			//那么就不需要进行插入
			if (Find(kv.first))
				return false;

			//负载因子 = 有效数据个数 / 表的大小
			//也就是说，有效数据个数最多只能占到表大小的百分之70
			//当新插入一个数据，其负载因子大于0.7
			//就需要对表扩容，以减少哈希碰撞
			//这是一种以空间换时间的做法
			if (_n * 10 / _tables.size() >= 7)
			{
				//我们可以直接开辟一个新的容器
				//将其容量设置为原来的2倍
				//然后重新使用算法映射
				//但是这是一种简单、粗暴、没有技巧的方式
				/*vector<Data> newTable;
				newTable.resize(_tables.size() * 2);
				for ()
				{
				}*/

				// 这是一种现代写法
				// 直接开辟一个新的对象,并把此对象的容器大小设置为原来的2倍
				// 此时其负载因子为0，新插入一个数据也不可能使得负载因子 >= 0.7
				// 也就说，新的对象再调用insert，是不满足if条件的，也就不会执行下面的代码
				HashTable<K, V, Hash> newHT;
				newHT._tables.resize(_tables.size() * 2);
				for (auto& e : _tables)
				{
					if (e._state == EXIST)//把旧表中存在的数据插入到新的表
					{
						newHT.Insert(e._kv);
					}
				}
				
				// 最后，交换一下当前对象的容器和新对象的容器
				// 新对象将在此作用域结束后，自动释放
				_tables.swap(newHT._tables);
			}

			Hash hf;//仿函数对象
			size_t hashi = hf(kv.first) % _tables.size();//确定映射位置
			while (_tables[hashi]._state == EXIST)//如果映射到的位置已经存在有效数据，就必须跳过它
			{
				++hashi;//跳过

				//因为使用vector作容器，难免会发生越界现象
				//所以必须保证映射位置在合法的范围之内
				hashi %= _tables.size();
			}

			_tables[hashi]._kv = kv;//找到了正确的映射位置，将数据放好
			_tables[hashi]._state = EXIST;//将状态设置好
			++_n;//不忘递增有效数据个数

			return true;
		}

		// 返回值的设计与迭代器非常类似
		Data* Find(const K& key)//通过key查找
		{
			Hash hf;//仿函数对象
			size_t hashi = hf(key) % _tables.size();//找到映射位置
			while (_tables[hashi]._state != EMPTY)//如果映射到的位置是空。
												//说明key在表中不存在
			{
				// 如果找到的数据是存在的，并且是符合条件的
				if (_tables[hashi]._state == EXIST
					&& _tables[hashi]._kv.first == key)
				{
					return &_tables[hashi];
				}

				//否则继续往下个位置寻找，同时注意不要越界
				++hashi;
				hashi %= _tables.size();
			}

			return nullptr;//返回空
		}

		bool Erase(const K& key)//删除也是通过key删除的
		{
			Data* ret = Find(key);
			if (ret)//如果返回值不为空，说明该key在表中确实存在
			{
				//不需要实质删除数据！仅仅设置状态和递减一下有效数据即可！
				ret->_state = DELETE;
				--_n;
				return true;
			}
			else
			{
				return false;
			}
		}
	private:
		vector<Data> _tables;// 用来保存数据的容器
		size_t _n = 0;	//有效数据个数
	};

	void TestHT1()
	{
		HashTable<int, int> ht;
		int a[] = { 18, 8, 7, 27, 57, 3, 38, 18 };
		for (auto e : a)
		{
			ht.Insert(make_pair(e, e));
		}

		ht.Insert(make_pair(17, 17));
		ht.Insert(make_pair(5, 5));

		cout << ht.Find(7) << endl;
		cout << ht.Find(8) << endl;

		ht.Erase(7);
		cout << ht.Find(7) << endl;
		cout << ht.Find(8) << endl;
	}

	void TestHT2()
	{
		string arr[] = { "苹果", "西瓜", "香蕉", "草莓", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };

		//HashTable<string, int, HashFuncString> countHT;
		HashTable<string, int> countHT;
		for (auto& e : arr)
		{
			HashData<string, int>* ret = countHT.Find(e);
			if (ret)
			{
				ret->_kv.second++;
			}
			else
			{
				countHT.Insert(make_pair(e, 1));
			}
		}

		HashFunc<string> hf;
		cout << hf("abc") << endl;
		cout << hf("bac") << endl;
		cout << hf("cba") << endl;
		cout << hf("aad") << endl;
	}
}

