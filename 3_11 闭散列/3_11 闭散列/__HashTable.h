#pragma once


enum Status
{
	//空   存在   删除
	EMPTY,EXIST,DELETE
};

template <class K,class V>
struct HashData
{
	pair<K,V> _kv;
	Status _st;

};

// 仿函数
template <class K>
struct HashFunc
{
	//如果是数字类型，直接转化为size_t类型即可
	size_t operator()(const K& k)
	{
		return (size_t)k;
	}
};

//如果不是数字类型，例如string，就需要特化单独处理
template<>
struct HashFunc<string>
{
	size_t operator()(const string& k)
	{
		size_t hash = 0;//返回值
		for (auto& e : k)
		{
			hash *= 131;//每次累加之前，先乘等131
			hash += e;
		}
		return hash;
	}
};

template <class K,class V>
class HashTable
{
	typedef HashData<K, V> Data;
	typedef HashFunc<K> Func;
public:
	HashTable()
		:_n(0)//一开始的有效数据个数为0
	{
		_tables.resize(10);//一开始确保表的大小为10
		//通过resize可以确保size == capacity
	}

	Data* find(const K& k)
	{
		Func f;
		size_t hashi = f(k) % _tables.size();
		while (_tables[hashi]._st == EXIST)//只有存在才说明可能有这个数据存在
		{
			//因为我们删除数据不是彻底删除数据
			//而是将其状态改变
			//所以比较的时候需要确保当前位置的数据不能是删除状态
			if (_tables[hashi]._st != DELETE && _tables[hashi]._kv.first == k)
			{
				return &_tables[hashi];
			}
			++hashi;
			hashi %= _tables.size();
		}
		return nullptr;
	}

	bool insert(const pair<K,V>& kv)
	{
		//如果新插入的数据已经存在，就不需要插入了
		if (find(kv.first))
		{
			return false;
		}

		//首先需要确保负载因子不大于0.7才进行插入
		// 否则就要对表进行扩容
		//double lf = _n / _tables.size();
		//if(lf >= 0.7)
		//{ }

		if (_n * 10 / _tables.size() >= 7)
		{
			HashTable<K, V> newHash;//创建一个新的对象
			//这个对象的表的容量是当前表容量的2倍
			newHash._tables.resize(_tables.size() * 2);

			//将旧表的内容拷贝至新表
			for (auto& e : _tables)
			{
				newHash.insert(kv);
			}

			//最后交换这两个表
			//新对象出了此作用域后自动销毁
			swap(_tables, newHash._tables);
		}

		//不管是否已经扩容,这里都进行新数据的插入操作
		//当然，key不可能永远为数字类型
		//size_t hashi = kv.first % _tables.size();

		Func f;
		size_t hashi = f(kv.first) % _tables.size();
		while (_tables[hashi]._st == EXIST)//如果映射位置已经存在数据
		{
			++hashi;//就到下一个位置去
			hashi %= _tables.size();//确保不越界
		}

		// 此时就找到了状态为EMPTY DELETE的位置
		_tables[hashi]._kv = kv;//插入
		_tables[hashi]._st = EXIST;//设置状态

		++_n;//递增有效数据个数
		return true;
	}

	bool erase(const K& k)//通过key值来删除
	{
		Data* del = find(k);
		if (del)
		{
			del->_st = DELETE;
			--_n;
			return true;
		}
		return false;
	}
private:
	vector<Data> _tables;//存储数据的表
	size_t _n;//有效数据个数
};