#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;


class A {
public:
	A(const char* s) { cout << s << endl; }
	~A() {}
};
class B :virtual public A
{
public:
	B(const char* s1, const char* s2) :A(s1) { cout << s2 << endl; }
};
class C :virtual public A
{
public:
	C(const char* s1, const char* s2) :A(s1) { cout << s2 << endl; }
};
class D :public B, public C
{
public:
	D(const char* s1, const char* s2, const char* s3, const char* s4) :B(s1, s2), C(s1, s3), A(s1)
	{
		cout << s4 << endl;
	}
};
int main() {
	D* p = new D("class A", "class B", "class C", "class D");
	delete p;
	return 0;
}



//class Base {
//public:
//	virtual void func1() { cout << "Base::func1" << endl; }
//	virtual void func2() { cout << "Base::func2" << endl; }
//private:
//	int a = 1;
//};
//
//class Derive :public Base {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//private:
//	int b = 2;
//};

//class Base1 {
//public:
//	static void func1() { cout << "Base1::func1" << endl; }
//	virtual void func2() { cout << "Base1::func2" << endl; }
//private:
//	int b1;
//};
//
//class Base2 {
//public:
//	virtual void func1() { cout << "Base2::func1" << endl; }
//	virtual void func2() { cout << "Base2::func2" << endl; }
//private:
//	int b2;
//};
//
//class Derive : public Base1, public Base2 {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//private:
//	int d1;
//};
//
//typedef void (*VFPTR)();
//
//void print_VFtable(VFPTR vf[])
//{
//	cout << "虚表地址:0x" << vf << endl;
//	for (int i = 0; vf[i] != nullptr; i++)
//	{
//		printf("[%d]:0x%p->", i, vf[i]);
//		vf[i]();
//	}
//	cout << endl;
//}
//int main()
//{
//	Base1 b1;
//	Base2 b2;
//	Derive d;
//	
//	// 对象的前4/8个字节存放的是虚表指针
//	VFPTR* vfb1 = (VFPTR*)(*(void**)&b1);
//	print_VFtable(vfb1);	//打印b1对象的虚表
//
//	VFPTR* vfb2 = (VFPTR*)(*(void**)&b2);
//	print_VFtable(vfb2);	//打印b2对象的虚表
//
//	VFPTR* vfd1 = (VFPTR*)(*(void**)&d);
//	print_VFtable(vfd1);	//打印d对象的第一张虚表
//
//	VFPTR* vfd2 = (VFPTR*)(*(void**)((char*)&d + sizeof(Base1)));
//	print_VFtable(vfd2);	//打印d对象的第二张虚表
//
//	return 0;
//}


//class Person
//{
//public:
//	virtual void print()
//	{
//		cout << "Person::全价" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual void print()
//	{
//		cout << "Student::半价" << endl;
//	}
//};
//
//void test(Person& ptr)	//传入的对象不同调用不同的虚函数
//{
//	ptr.print();	//必须是基类的指针或引用调用虚函数
//}
//int main()
//{
//	Student s;
//	Person p;
//
//	test(p);
//	test(s);
//
//	return 0;
//}



//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//private:
//	int _d = 2;
//};
//
//int main()
//{
//	Base b;
//	Derive d;
//	return 0;
//}




// sizeof(Base)是多少？
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//
//int main()
//{
//	cout << sizeof(Base) << endl;
//	
//	Base b;
//	return 0;
//}


// 下面程序输出结果是什么？
//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//class B : public A
//{
//public:
//	void func(int val = 0) { std::cout << "B->" << val << std::endl; }
//};
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();
//	return 0;
//}
// A: A->0 B : B->1 C : A->1 D : B->0 E : 编译出错 F : 以上都不正确



//class A final
//{};
//
//class B : public A	//错误，A类不能被继承
//{};

//class Car {
//public:
//	virtual void Drive () const
//	{}
//};
//
//class Benz :public Car {
//public:
//	// 报错，多余Drive函数，派生类没有正确重写
//	virtual void Drive() override
//	{}
//};


//class Car
//{
//public:
//	virtual void Drive() = 0;
//};
//
//class Benz :public Car
//{
//public:
//};
//
//int main()
//{
//	Benz b;	//错误，Benz类没有完成基类纯虚函数的重写
//	return 0;
//}

//class A
//{
//public:
//	virtual ~A()
//	{
//		cout << "A::~A()" << endl;
//	}
//};
//
//class B : public A
//{
//public:
//	~B()
//	{
//		cout << "B:~B()" << endl;
//	}
//};
//
//int main()
//{
//	// 如果基类和派生类的析构函数不构成多态系统
//	// 那么在清理资源时就会产生重复释放
//	A* pa = new A;
//	A* pb = new B;
//	
//	delete pa;
//	delete pb;
//	return 0;
//}

//class A
//{};
//class B : public A
//{};
//
//// 基类虚函数可以返回任意具有继承关系的基类的指针或引用
//class Person
//{
//public:
//	virtual A* print()
//	{
//		cout << "Person::全价" << endl;
//		return nullptr;
//	}
//};
//
//// 派生类虚函数可以返回任意具有继承关系的派生类的指针或引用
//class Student : public Person
//{
//public:
//	virtual B* print()
//	{
//		cout << "Student::半价" << endl;
//		return nullptr;
//	}
//};
//
//void test(Person* ptr)	//传入的对象不同调用不同的虚函数
//{
//	ptr->print();	//必须是基类的指针或引用调用虚函数
//}
//int main()
//{
//	Student s;
//	Person p;
//
//	test(&p);
//	test(&s);
//	
//	return 0;
//}