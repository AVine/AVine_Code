#pragma once 

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <error.h>
#include <stdlib.h>
#include <assert.h>
/*模拟实现C语言缓冲区超级迷你版 
 * 仅学习使用，无实质意义
 * 进行IO的缓冲区在FILE结构体里*/

#define SIZE 1024

#define SYNC_LINE 1      //行刷新
typedef struct _FILE
{
    int flags;      //缓冲区刷新策略
    int fileno;     //对应底层的文件描述符
    char buffer[SIZE];       //缓冲区
    int cap;
    int size;
}_FILE;

_FILE* _fopen(const char* filename,const char* mode);        //模拟C语言fopen接口 
void _fclose(_FILE* fp);
void _fflush(_FILE* fp);
void _fwrite(const char* ptr,int len,_FILE* fp);









