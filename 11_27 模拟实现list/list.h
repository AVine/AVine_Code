#pragma once


#include <iostream>
#include <algorithm>

#include "reverse_iterator.h"

using std::cout;
using std::endl;
namespace ly
{
	//需要一个节点类型。内置类型没有，自定义类型

	template<class T>
	struct list_node
	{
		list_node* next;
		list_node* prev;
		T data;

		list_node(const T& val = T())
			:data(val)
		{}
	};



	//const 迭代器 : const * || * const   前者


	template<class T,class Ref,class Ptr>	//通过多一个参数来达到构成const迭代器的目的
	struct list_iterator
	{
		typedef list_node<T> node;
		typedef list_iterator<T, Ref,Ptr> Self;

		list_iterator(node* p)
			:pnode(p)
		{}

		Ref& operator*()
		{
			return pnode->data;	//const 迭代器保证返回的元素内容不能修改
		}

		Ptr operator->()
		{
			return &pnode->data;
		}


		bool operator!=(const Self& lt) const
		{
			return pnode != lt.pnode;
		}

		bool operator==(const Self& lt)
		{
			return pnode == lt.pnode;
		}


		Self operator++()
		{
			pnode = pnode->next;
			return pnode;
		}


		Self operator--()
		{
			pnode = pnode->prev;
			return pnode;
		}



		node* pnode;
	};

	template<class T>
	class list
	{
	public:
		typedef list_node<T> node;
		typedef list_iterator<T,T&,T*> iterator;
		typedef list_iterator<T, const T&,const T*> const_iterator;

		typedef ReverseIterator<iterator, T&, T*> reverse_iterator;
		typedef ReverseIterator<const_iterator, const T&, const T*> const_reverse_iterator;


		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		const_reverse_iterator rbegin() const
		{
			return const_reverse_iterator(end());
		}

		const_reverse_iterator rend() const
		{
			return const_reverse_iterator(begin());
		}

		iterator begin()
		{
			return iterator(_head->next);
		}

		iterator end()
		{
			return iterator(_head);
		}
		
		const_iterator begin() const
		{
			return list_iterator<T, const T&,const T*>(_head->next);
		}

		const_iterator end() const
		{
			return const_iterator(_head);
		}



		void empty_init()
		{
			_head = new node;
			_head->next = _head;
			_head->prev = _head;
			_size = 0;
		}

		list()
		{
			empty_init();
		}



		/*传统写法*/
		//list(const list<T>& lt)	
		//{
		//	empty_init();

		//	for (const auto& e : lt)
		//	{
		//		push_back(e);
		//	}
		//}


		void swap(list<T>& lt)
		{
			std::swap(_head, lt._head);
			std::swap(_size, lt._size);
		}

		//list(const list<T>& lt)
		list(const list& lt)	//在<类模板>里面，类名可以代表类型
		{
			empty_init();

			list<T> tmp(lt.begin(), lt.end());
			swap(tmp);
		}


		~list()
		{

			//cout << "析构函数" << endl;
			clear();

			//cout << "clear" << endl;

			delete _head;
			_head = nullptr;
		}
		

		//范围拷贝

		 //lt2(lt1.begin(),lt1.end())
		template <class InputIterator>
		list(InputIterator first, InputIterator last)
		{
			empty_init();
			
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}


		// lt(lt) —— 可以避免这种行为
		//list<T>& operator=(const list<T>& lt)
		//{
		//	if (this != &lt)
		//	{

		//		clear();


		//		for (const auto& e : lt)
		//		{
		//			push_back(e);
		//		}
		//	}

		//	return *this;
 	//	}


		list<T>& operator=(list<T> lt)
		{
			swap(lt);
			return *this;
		}

		void push_back(const T& val)
		{
			//node* newnode = new node(val);
			//node* tail = _head->prev;

			//tail->next = newnode;
			//newnode->prev = tail;
			//_head->prev = newnode;
			//newnode->next = _head;

			insert(end(),val);

		}

		void push_front(const T& val)
		{
			insert(begin(), val);
		}

		void pop_front()
		{
			erase(begin());
		}


		void pop_back()
		{
			erase(--end());
		}


		void clear()
		{
			auto it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}


		iterator insert(iterator pos, const T& val)
		{
			node* newnode = new node(val);	//新节点
			
			node* cur = pos.pnode;
			node* prev = cur->prev;

			prev->next = newnode;
			newnode->prev = prev;
			newnode->next = cur;
			cur->prev = newnode;

			++_size;
			return iterator(newnode);
		}



		iterator erase(iterator pos)
		{
			node* cur = pos.pnode;
			node* prev = cur->prev;
			node* next = cur->next;

			prev->next = next;
			next->prev = prev;

			cur->next = nullptr;
			cur->prev = nullptr;
			delete cur;
			cur = nullptr;

			--_size;
			return iterator(next);
		}


		iterator find(iterator begin, iterator end, const T& val)
		{
			while (begin != end)
			{
				if (*begin == val) return begin;
				++begin;
			}
			return end;
		}


		size_t size()
		{
			
		}

		bool empty()
		{

		}

	private:
		node* _head;

		size_t _size;
	};

}