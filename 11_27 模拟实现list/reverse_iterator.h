#define _CRT_SECURE_NO_WARNINGS 1


/*反向迭代器(适配器)
利用原有的迭代器实现出反向迭代器*/
template <class Iterator,class Ref,class Ptr>
class ReverseIterator
{
public:

	typedef ReverseIterator<Iterator,Ref,Ptr> Self;

	ReverseIterator(const Iterator& it)
		:_it(it)
	{}

	Self& operator++()
	{
		--_it;
		return *this;
	}

	Self operator++(int)
	{
		Iterator tmp(_it);
		--_it;
		return tmp;
	}

	Self& operator--()
	{
		++_it;
		return *this;
	}

	Self operator--(int)
	{
		Iterator tmp(_it);
		++_it;
		return tmp;
	}

	Ref operator*() 
	{
		Iterator tmp(_it);
		return *(--tmp);
	}

	Ptr operator->()
	{
		return &(operator*());
	}


	bool operator!=(const Self& it) const
	{
		return _it != it._it;
	}
private:
	Iterator _it;
};