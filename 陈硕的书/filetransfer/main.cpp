#include "version1.hpp"
#include <iostream>

int main(int argc,char *argv[])
{
    if(argc != 2)
    {
        printf("Usage: %d <file_name>\n",argv[0]);
        return -1;
    } 

    file_name = argv[1];

    muduo::net::EventLoop loop;
    FileServer fs(&loop,muduo::net::InetAddress(9090));
    fs.start();
    loop.loop();
    return 0;
}