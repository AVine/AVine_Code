#include "discard.hpp"
#include "daytime.hpp"
#include "time.hpp"

int main()
{
    muduo::net::EventLoop loop;
    DiscardServer ds(&loop,muduo::net::InetAddress(9090));
    ds.start();

    DaytimeServer dts(&loop,muduo::net::InetAddress(9091));
    dts.start();

    TimeServer ts(&loop,muduo::net::InetAddress(9092));
    ts.start();
    
    loop.loop();
    return 0;
}