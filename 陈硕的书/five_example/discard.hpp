#pragma once

#include <muduo/net/TcpServer.h>
#include <muduo/net/EventLoop.h>
#include <muduo/base/Logging.h>

class DiscardServer
{
public:
    DiscardServer(muduo::net::EventLoop *loop,const muduo::net::InetAddress &listenAddr)
        :_server(loop,listenAddr,"DiscardServer")
    {
        _server.setConnectionCallback(std::bind(&DiscardServer::connection_handler,this,std::placeholders::_1));
        _server.setMessageCallback(std::bind(&DiscardServer::message_handler,this,
        std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
    }

    void start()
    {
        _server.start();
    }
    
    void connection_handler(const muduo::net::TcpConnectionPtr &conn)
    {
        LOG_INFO << "DiscardServer - " << conn->peerAddress().toIpPort() << " -> "
        << conn->localAddress().toIpPort() << " is "
        << (conn->connected() ? "UP" : "DOWN");
    }

    void message_handler(const muduo::net::TcpConnectionPtr &conn,muduo::net::Buffer *buf,muduo::Timestamp time)
    {
        std::string msg(buf->retrieveAllAsString());
        LOG_INFO << conn->name() << " discards " << msg.size()
            << " bytes received at " << time.toString();
    }
protected:
    muduo::net::TcpServer _server;
};