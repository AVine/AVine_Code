#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <windows.h>
#include <memory>


class Test
{
public:
	~Test()
	{
		std::cout << "~Test()" << std::endl;
	}

	void print()
	{
		std::cout << "x = " << 2 << std::endl;
	}
protected:
	int _x = 2;
};

int main()
{
	std::shared_ptr<Test> sp(new Test);
	sp.reset();
	std::shared_ptr<Test> sp2(sp);
	sp->print();
	return 0;
}

//class TestMutable
//{
//public:
//	TestMutable()
//		:_x(100)
//	{}
//
//	void Mutable() const
//	{
//		--_x;
//	}
//protected:
//	mutable int _x;
//};
//
//int main()
//{
//	return 0;
//}

//class Test
//{
//public:
//	Test()
//		:_ptr(new int[100])
//	{
//		std::cout << "Test()" << std::endl;
//	}
//
//	void push()
//	{
//		_mutex.lock();
//		std::cout << "push()" << std::endl;
//		_ptr[22] = 3;
//		_mutex.unlock();
//	}
//
//	~Test()
//	{
//		_mutex.lock();
//		std::cout << "~Test()" << std::endl;
//		delete[] _ptr;
//		_ptr = nullptr;
//		_mutex.unlock();
//	}
//
//protected:
//	std::mutex _mutex;
//	int* _ptr;
//};
//
//int main()
//{
//	Test t;
//	std::thread thread1([&]()
//		{
//			while (true)
//			{
//				std::cout << "thread1: ";
//				t.push();
//				Sleep(1000);
//			}
//		});
//
//	std::thread thread2([&]()
//		{
//			int cnt = 1;
//			while (true)
//			{
//				if (cnt % 6 == 0)
//				{
//					t.~Test();
//					break;
//				}
//				std::cout << "thread2: ";
//				t.push();
//				Sleep(1000);
//				++cnt;
//			}
//		});
//
//	thread1.join();
//	thread2.join();
//	return 0;
//}

//// 观察者接口
//class Observer {
//public:
//    virtual void update() = 0;
//};
//
//// 具体观察者A
//class ConcreteObserverA : public Observer {
//public:
//    void update() {
//        std::cout << "ConcreteObserverA received the update.\n";
//    }
//};
//
//// 具体观察者B
//class ConcreteObserverB : public Observer {
//public:
//    void update() {
//        std::cout << "ConcreteObserverB received the update.\n";
//    }
//};
//
//// 被观察者
//class Subject {
//private:
//    std::vector<Observer*> observers;// 一堆父类指针
//
//public:
//    void attach(Observer* observer) {// 注册一个新的观察者
//        observers.push_back(observer);
//    }
//
//    void detach(Observer* observer) {// 删除一个观察者
//        auto it = std::find(observers.begin(), observers.end(), observer);
//        if (it != observers.end()) {
//            observers.erase(it);
//        }
//    }
//
//    void notify() {// 通知每一位观察者
//        for (Observer* observer : observers) {
//            observer->update();
//        }
//    }
//};
//
//int main() {
//    // 创建具体观察者对象
//    ConcreteObserverA observerA;
//    ConcreteObserverB observerB;
//
//    // 创建被观察者对象
//    Subject subject;
//
//    // 注册观察者
//    subject.attach(&observerA);
//    subject.attach(&observerB);
//
//    // 发送通知
//    subject.notify();
//
//    // 移除观察者A
//    subject.detach(&observerA);
//
//    // 再次发送通知
//    subject.notify();
//
//    return 0;
//}