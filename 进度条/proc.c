#include "proc.h"

void Proc()
{
    int cnt=0;
    char proc[101]={0};
    char rotate[4]={'|','\\','-','/'};
    while(cnt<=100)
    {
        printf("[%-100s][%d%%][%c]\r",proc,cnt,rotate[cnt%4]);
        fflush(stdout);
        usleep(80000);
        proc[cnt++]='-';
    }
    printf("\n");
}
