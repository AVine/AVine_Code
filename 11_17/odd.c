#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int num = 100;
int main()
{
    pid_t id = fork();
    if(id < 0)
    {
        printf("创建进程失败!\n");
        return 1;
    }
    else if(id == 0)
    {
        int cnt=0;
        while(1)
        {
            printf("我是子进程，num=%d,&num=%p\n",num,&num);
            sleep(1);
            cnt++;
            if(cnt == 5)
            {
                num = 500;
                printf("子进程把num修改为%d啦!\n",num);
                sleep(1);
            }
        }
    }
    else 
    {
        while(1)
        {
            printf("我是父进程,num=%d,&num=%p\n",num,&num);
            sleep(2);
        }
    }
    return 0;
}
