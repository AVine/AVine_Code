#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main()
{
    const char* msg_write="write\n";
    const char* msg_fprintf="fprintf\n";
    const char* msg_fwrite="fwrite\n";
    printf("printf\n");
    fwrite(msg_fwrite,sizeof(char),strlen(msg_fwrite),stdout);
    fprintf(stdout,"%s",msg_fprintf);
    fputs("fputs\n",stdout);
    write(1,msg_write,strlen(msg_write));       //这五个函数都是向标准输出输出
    
    fork();     //创建一个子进程
    return 0;
}

//#define ONE (1<<0)
//#define TOW (1<<1)
//#define THREE (1<<2)
//#define FOUR (1<<3)
//
//void select(int flags)
//{
//    if(flags & ONE) printf("one\n");
//    if(flags & TOW) printf("tow\n");
//    if(flags & THREE) printf("three\n");
//    if(flags & FOUR) printf("four\n");
//}
//
//int main()
//{
//
//    select(ONE);
//    select(ONE | TOW);
//    select(ONE | TOW | THREE);
//    select(ONE | TOW | THREE | FOUR);
//  //  int fd = open("log.txt",O_WRONLY | O_CREAT | O_TRUNC,0666);
//  //  dup2(fd,1);
//  //  printf("hello world");
//  //  fflush(stdout);
//
//
//
//    //close(1);
//    //int fd = open("log.txt",O_WRONLY | O_CREAT | O_TRUNC,0666);
//    //printf("hello world\n");
//    //fflush(stdout);
//
//
//    //close(0);
//    //close(2);       //关闭掉标准输入和标准错误文件，即清空数组的占用
//    //int fd = open("log.txt",O_RDONLY);
//    //printf("%d\n",fd);
//
//    //int fd = open("log.txt",O_RDONLY);
//    //char buffer[64];
//    //read(fd,buffer,sizeof(buffer)-1);
//    //buffer[strlen(buffer)]=0;
//    //printf("%s",buffer);
//    //printf("%d\n",fd);
//    //umask(0);       //将umask置0
//    //int fd = open("log.txt",O_WRONLY | O_CREAT | O_TRUNC,0666);     //相当于C语言的fopen的"w"，只写、自动创建、自动覆盖
//    //if(fd < 0) return 1;
//    //    
//    //char* msg = "hello Linux\n";
//    //write(fd,msg,strlen(msg));      //像fd描述的文件写入msg指向的字符串
//    //close(fd);
//    return 0;
//}

