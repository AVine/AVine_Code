

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <error.h>
#include <errno.h>
#include <stdlib.h>



#define SIZE 1024

#define ALL 0       //全缓冲策略
#define LINE 1      //行刷新策略(暂时只实现一个吧)
typedef struct _FILE
{
    int flags;      //缓冲区刷新策略
    int fileno;     //文件描述符
    char cush[SIZE];        //缓冲区
    int size;       //有效数据
    int cap;        //最大容量
}_FILE;

_FILE* _fopen(const char* filename,const char* mode);       //模拟fopen
void _fclose(_FILE* fp);     //模拟fclose
void _fflush(_FILE* fp);        //模拟fflush
void _fwrite(const char* msg,int size,int len,_FILE* fp);     //模拟fwrite
