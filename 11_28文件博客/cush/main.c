#include "_stdio.h"

#include <stdio.h>
int main()
{
    _FILE* fp = _fopen("log.txt","w");

    const char* msg="hello Linux\n";

    int cnt = 10;
    while(cnt)
    {
        _fwrite(msg,sizeof(char),strlen(msg),fp);
        printf("count:%d\n",cnt--);
        sleep(1);
    }
    _fclose(fp);
    return 0;
}
